clear all
close all

%% Read the filename (point cloud data are obtained from ModelNet)

% guitar airplane human
filename = fullfile(pwd, 'data', 'human.txt');
pc = csvread(filename); 

%% Rotate the point cloud along x and y direction

direction = 'x';
pcx = rotatePointCloudAlongZ(pc, direction);

direction = 'y';
pcy = rotatePointCloudAlongZ(pc, direction);

%% 

fig1 = figure(1);
set(fig1, 'unit', 'normalized', 'outerposition', [0 0 1 1])
clf 
subplot(1,3,1)
hold on
plot3(pc(:, 1), pc(:, 2), pc(:, 3), 'b.')
V = pca(pc);
quiver3(0,0,0,V(1, 1),V(2, 1),V(3, 1), 'g', 'LineWidth', 4);
quiver3(0,0,0,V(1, 2),V(2, 2),V(3, 2), 'k', 'LineWidth', 4);
quiver3(0,0,0,V(1, 3),V(2, 3),V(3, 3), 'r', 'LineWidth', 4);
axis equal
grid on; grid minor; box on;
xlabel('x'); ylabel('y'); zlabel('z')
view(3)
title('Origin (Arrows are PCA eigenvectors)')

subplot(1,3,2)
plot3(pcx(:, 1), pcx(:, 2), pcx(:, 3), 'r*')
axis equal
grid on; grid minor; box on;
xlabel('x'); ylabel('y'); zlabel('z')
view(3)
title({'PCA highest eigenvalue along z-axis','2nd highest eigenvalue along x-axis'})

subplot(1,3,3)
plot3(pcy(:, 1), pcy(:, 2), pcy(:, 3), 'r*')
axis equal
grid on; grid minor; box on;
xlabel('x'); ylabel('y'); zlabel('z')
view(3)
title({'PCA highest eigenvalue along z-axis','2nd highest eigenvalue along y-axis'})
% saveas(fig1, [filename(1:end-4) '.jpg'])