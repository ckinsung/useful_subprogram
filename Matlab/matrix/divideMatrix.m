function matrix = divideMatrix(map, region)

    rows = size(map, 1)/region(1);
    cols = size(map, 2)/region(2);
    matrix = zeros(region(1), region(2), rows, cols);

    for i = 1:region(1)
        
        for j = 1:region(2)
            nthRow = (i - 1)*rows + 1: i*rows;
            mthCol = (j - 1)*cols + 1: j*cols;
            matrix(i, j, :, :) = map(nthRow, mthCol);
        end
        
    end
end