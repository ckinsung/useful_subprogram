import cv2
import os
import matplotlib.pyplot as plt
import numpy as np
import glob
import copy
from facenet_pytorch import MTCNN
mtcnn = MTCNN(keep_all=True, device='cpu')

path = 'C:/Users/PC\Documents/Git Temporary/face_detect_train_dataset_temp/data_20220725 qvga/ir'

for i in range(1, len(glob.glob(path + '/*csv'))+1):

    filename = os.path.join(path, f'ir_{i:07d}.csv')
    print(filename)
    img_origin = np.genfromtxt(filename, delimiter=',').astype(np.float32)

    max_num = np.sort(img_origin.flatten())[-100]
    min_num = 0
    img = copy.copy(img_origin)
    img = ((img_origin - max_num)/(max_num - min_num)*255).astype('uint8')

    # img[img >= np.sort(img.flatten())[-200]] = 0

    # Stack the image to have the matrix elements along the channel dimension
    img = np.dstack([img] * 3)

    boxes, probs = mtcnn.detect(img, landmarks=False)

    try:
        if boxes is not None:
            x1, y1, x2, y2 = int(boxes[0][0]), int(boxes[0][1]), int(boxes[0][2]), int(boxes[0][3])
            img = cv2.rectangle(img, pt1=(x1, y1), pt2=(x2, y2), color=(255, 255, 255), thickness=1)

            xc, yc = int((x1 + x2) / 2), int((y1 + y2) / 2)
            resize_ratio = 1.2
            width, height = int(x2 - x1), int(y2 - y1)
            width, height = width * resize_ratio, height * resize_ratio
            x1_new, y1_new = int(xc - width / 2), int(yc - height / 2)
            x2_new, y2_new = int(xc + width / 2), int(yc + height / 2)

            f1 = lambda x, a: x == a if x > a else x
            x2_new = f1(x2_new, img.shape[1])
            y2_new = f1(y2_new, img.shape[0])
            f2 = lambda x: x == 0 if x < 0 else x
            x1_new = f2(x1_new)
            y1_new = f2(y1_new)

            img_face = img_origin[y1_new:y2_new, x1_new:x2_new]
            # img_face = cv2.medianBlur(img_face, 5)
            # max_num = np.sort(img_face.flatten())[-3]
            max_num = img_face.max()
            min_num = img_face.min()
            img_face = ((img_face - min_num) / (max_num - min_num) * 255).astype('uint8')
            img_face = np.dstack([img_face] * 3)

        # cv2.imwrite(filename=os.path.basename(filename)[0:-4] + '.jpg', img=img)
        cv2.imwrite(filename=os.path.basename(filename)[0:-4] + '_face.jpg', img=img_face)

    except:
        pass

