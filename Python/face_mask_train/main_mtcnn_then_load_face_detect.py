import os
import sys
import cv2
sys.path.insert(0, os.path.join(os.path.dirname(os.getcwd()), 'AI'))
from AI_cks import object_detection as od
model = od.face_detection.deep_learning.facenet_pytorch_mtcnn()
# model = od.face_detection.deep_learning.mtcnn()

model.image(img_path='./data/mask_public/102.jpeg', selection='rgb')  # 84
# model.image(img_path='./data/public/02.jpg', selection='rgb')