import os
import numpy as np
import matplotlib.pyplot as plt

folder = os.path.join('results', 'model_48_48_grayscale_train_both_rgb_data_tof_data')
face1_test = np.genfromtxt(fname=os.path.join(folder, 'face1', 'test_result.txt'))
face1_tof = np.genfromtxt(fname=os.path.join(folder, 'face1', 'tof_result.txt'))
face1_train = np.genfromtxt(fname=os.path.join(folder, 'face1', 'train_result.txt'))

face3_test = np.genfromtxt(fname=os.path.join(folder, 'face3', 'test_result.txt'))
face3_tof = np.genfromtxt(fname=os.path.join(folder, 'face3', 'tof_result.txt'))

face4_test = np.genfromtxt(fname=os.path.join(folder, 'face4', 'test_result.txt'))
face4_tof = np.genfromtxt(fname=os.path.join(folder, 'face4', 'tof_result.txt'))

face5_test = np.genfromtxt(fname=os.path.join(folder, 'face5', 'test_result.txt'))
face5_tof = np.genfromtxt(fname=os.path.join(folder, 'face5', 'tof_result.txt'))

plt.figure(1)
plt.clf()
plt.plot(np.linspace(0,  50, 401), face1_train, 'r-.')
plt.plot(np.linspace(0,  50, 401), face1_test, 'k-.')
# plt.plot(np.linspace(0,  50, 401), face1_tof, 'b-.')
plt.grid()
# plt.legend(['train validation (rgb + tof)', 'test validation (rgb)', 'test validation (tof)'])
plt.legend(['train validation', 'test validation'])
plt.xlabel('Epoch')
plt.ylabel('Accuracy (%)')
plt.axis([0, 2, 0, 1])
plt.savefig('fig1.jpg')
plt.show(False)

result = np.concatenate((face1_train[0:11].reshape(-1, 1), face1_test[0:11].reshape(-1, 1), face1_tof[0:11].reshape(-1, 1)), axis=1)
np.savetxt(fname='result.txt', X=result, fmt='%0.2f')

plt.figure(2)
plt.clf()
plt.plot(np.linspace(0,  50, 401), face1_test, 'r-.')
# plt.plot(np.linspace(0,  50, 401), face1_tof, 'r.')
plt.plot(np.linspace(0,  50, 401), face3_test, 'b-.')
# plt.plot(np.linspace(0,  50, 401), face3_tof, 'b.')
plt.plot(np.linspace(0,  50, 401), face4_test, 'g-.')
# plt.plot(np.linspace(0,  50, 401), face4_tof, 'g.')
plt.plot(np.linspace(0,  50, 401), face5_test, 'c-.')
# plt.plot(np.linspace(0,  50, 401), face5_tof, 'c.')
plt.grid()
# plt.legend(['face1 test validation (rgb)', 'face1 tof validation (tof)',
#             'face2 test validation (rgb)', 'face2 tof validation (tof)',
#             'face3 test validation (rgb)', 'face3 tof validation (tof)',
#             'face4 test validation (rgb)', 'face4 tof validation (tof)'])
plt.legend(['face1 test validation',
            'face2 test validation',
            'face3 test validation',
            'face4 test validation'])
plt.xlabel('Epoch')
plt.ylabel('Accuracy (%)')
plt.axis([40, 50, 0.9, 1])
plt.savefig('fig2b.jpg')
plt.show(False)

####################################################################################################

folder = os.path.join('results', 'model_48_48_grayscale_train_rgb_data_only')
face1_train = np.genfromtxt(fname=os.path.join(folder, 'face1', 'train_result.txt'))
face1_test = np.genfromtxt(fname=os.path.join(folder, 'face1', 'test_result.txt'))
face1_tof = np.genfromtxt(fname=os.path.join(folder, 'face1', 'tof_result.txt'))


plt.figure(2)
plt.clf()
plt.plot(np.linspace(0,  50, 401), face1_train, 'r-.')
plt.plot(np.linspace(0,  50, 401), face1_test, 'r-.')
plt.plot(np.linspace(0,  50, 401), face1_tof, 'r.')
plt.grid()
plt.legend(['face1 test validation (rgb)', 'face1 tof validation (tof)'])
plt.xlabel('Epoch')
plt.ylabel('Accuracy (%)')
plt.savefig('fig1.jpg')
plt.show(False)