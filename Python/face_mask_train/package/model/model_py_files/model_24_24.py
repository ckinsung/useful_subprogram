import torch
from torch import nn

class face1(nn.Module):
    """
        从 model_48_48.py 里的 face5 拿过来相关模型代码
        total parameters: 1900
        训练得比较久 (18 epochs)，但是依然能达到 99%
        Accuracy = 0.5030242204666138
        Epoch = 1; Batch = 100; Loss = 0.37724441289901733
        Accuracy = 0.8921371102333069
        Epoch = 1; Batch = 200; Loss = 0.31950390338897705
        Accuracy = 0.9052419066429138
        Epoch = 1; Batch = 300; Loss = 0.13808836042881012
        Accuracy = 0.9122983813285828
        Epoch = 2; Batch = 100; Loss = 0.2425120323896408
        Accuracy = 0.9213709831237793
        Epoch = 2; Batch = 200; Loss = 0.26931801438331604
        Accuracy = 0.9314516186714172
        Epoch = 2; Batch = 300; Loss = 0.30382782220840454
        Accuracy = 0.9375
        Epoch = 3; Batch = 100; Loss = 0.10432105511426926
        Accuracy = 0.9546371102333069
        Epoch = 3; Batch = 200; Loss = 0.06948636472225189
        Accuracy = 0.9506048560142517
        Epoch = 3; Batch = 300; Loss = 0.1431846022605896
        Accuracy = 0.9606854915618896
        Epoch = 4; Batch = 100; Loss = 0.30226796865463257
        Accuracy = 0.9606854915618896
        Epoch = 4; Batch = 200; Loss = 0.17421624064445496
        Accuracy = 0.9586693644523621
        Epoch = 4; Batch = 300; Loss = 0.3141551613807678
        Accuracy = 0.9657257795333862
        Epoch = 5; Batch = 100; Loss = 0.1536829024553299
        Accuracy = 0.9707661271095276
        Epoch = 5; Batch = 200; Loss = 0.12801411747932434
        Accuracy = 0.9727822542190552
        Epoch = 5; Batch = 300; Loss = 0.009251907467842102
        Accuracy = 0.9737903475761414
        Epoch = 6; Batch = 100; Loss = 0.03454343602061272
        Accuracy = 0.9697580933570862
        Epoch = 6; Batch = 200; Loss = 0.032335538417100906
        Accuracy = 0.9737903475761414
        Epoch = 6; Batch = 300; Loss = 0.08871390670537949
        Accuracy = 0.9677419066429138
        Epoch = 7; Batch = 100; Loss = 0.01371249370276928
        Accuracy = 0.9697580933570862
        Epoch = 7; Batch = 200; Loss = 0.017881004139780998
        Accuracy = 0.9788306355476379
        Epoch = 7; Batch = 300; Loss = 0.021072160452604294
        Accuracy = 0.975806474685669
        Epoch = 8; Batch = 100; Loss = 0.0037040289025753736
        Accuracy = 0.9808467626571655
        Epoch = 8; Batch = 200; Loss = 0.03912736847996712
        Accuracy = 0.9768145084381104
        Epoch = 8; Batch = 300; Loss = 0.02626277506351471
        Accuracy = 0.9798387289047241
        Epoch = 9; Batch = 100; Loss = 0.008205425925552845
        Accuracy = 0.9838709831237793
        Epoch = 9; Batch = 200; Loss = 0.025811530649662018
        Accuracy = 0.9788306355476379
        Epoch = 9; Batch = 300; Loss = 0.13056406378746033
        Accuracy = 0.9768145084381104
        Epoch = 10; Batch = 100; Loss = 0.03660449758172035
        Accuracy = 0.9788306355476379
        Epoch = 10; Batch = 200; Loss = 0.04031965509057045
        Accuracy = 0.9798387289047241
        Epoch = 10; Batch = 300; Loss = 0.0038493885658681393
        Accuracy = 0.9828628897666931
        Epoch = 11; Batch = 100; Loss = 0.0749669149518013
        Accuracy = 0.9778226017951965
        Epoch = 11; Batch = 200; Loss = 0.016145028173923492
        Accuracy = 0.9788306355476379
        Epoch = 11; Batch = 300; Loss = 0.04790623486042023
        Accuracy = 0.9798387289047241
        Epoch = 12; Batch = 100; Loss = 0.04653184860944748
        Accuracy = 0.9818548560142517
        Epoch = 12; Batch = 200; Loss = 0.0013316851109266281
        Accuracy = 0.9858871102333069
        Epoch = 12; Batch = 300; Loss = 0.07960112392902374
        Accuracy = 0.9848790168762207
        Epoch = 13; Batch = 100; Loss = 0.006437363103032112
        Accuracy = 0.9858871102333069
        Epoch = 13; Batch = 200; Loss = 0.024884453043341637
        Accuracy = 0.9798387289047241
        Epoch = 13; Batch = 300; Loss = 0.013689769431948662
        Accuracy = 0.9879032373428345
        Epoch = 14; Batch = 100; Loss = 0.07832261919975281
        Accuracy = 0.975806474685669
        Epoch = 14; Batch = 200; Loss = 0.036016445606946945
        Accuracy = 0.9858871102333069
        Epoch = 14; Batch = 300; Loss = 0.002145008184015751
        Accuracy = 0.9889112710952759
        Epoch = 15; Batch = 100; Loss = 0.020511038601398468
        Accuracy = 0.9868951439857483
        Epoch = 15; Batch = 200; Loss = 0.01583460345864296
        Accuracy = 0.9899193644523621
        Epoch = 15; Batch = 300; Loss = 0.01851649396121502
        Accuracy = 0.9879032373428345
        Epoch = 16; Batch = 100; Loss = 0.008941450156271458
        Accuracy = 0.9858871102333069
        Epoch = 16; Batch = 200; Loss = 0.011675423011183739
        Accuracy = 0.9868951439857483
        Epoch = 16; Batch = 300; Loss = 0.04260188341140747
        Accuracy = 0.9788306355476379
        Epoch = 17; Batch = 100; Loss = 0.020800864323973656
        Accuracy = 0.9889112710952759
        Epoch = 17; Batch = 200; Loss = 0.008993666619062424
        Accuracy = 0.9899193644523621
        Epoch = 17; Batch = 300; Loss = 0.004228772595524788
        Accuracy = 0.9919354915618896
        Epoch = 18; Batch = 100; Loss = 0.017789851874113083
        Accuracy = 0.9899193644523621
        Epoch = 18; Batch = 200; Loss = 0.0018222462385892868
        Accuracy = 0.992943525314331
    """

    def __init__(self):

        super(face1, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=8)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=8)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=8, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 3 * 3, out_features=5 * 3 * 1)
        self.linear2 = nn.Linear(in_features=5 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x


class face2(nn.Module):
    """
        从 model_48_48.py 里的 face5 拿过来相关模型代码
        total parameters: 1321
        训练得比较久 (20 epochs)，但是依然能达到 98%
        Accuracy = 0.5131048560142517
        Epoch = 1; Batch = 100; Loss = 0.12530842423439026
        Accuracy = 0.8850806355476379
        Epoch = 1; Batch = 200; Loss = 0.1753651201725006
        Accuracy = 0.8961693644523621
        Epoch = 1; Batch = 300; Loss = 0.20002305507659912
        Accuracy = 0.899193525314331
        Epoch = 2; Batch = 100; Loss = 0.21149349212646484
        Accuracy = 0.9042338728904724
        Epoch = 2; Batch = 200; Loss = 0.1375768929719925
        Accuracy = 0.9173387289047241
        Epoch = 2; Batch = 300; Loss = 0.10479901731014252
        Accuracy = 0.9213709831237793
        Epoch = 3; Batch = 100; Loss = 0.04303116723895073
        Accuracy = 0.9284273982048035
        Epoch = 3; Batch = 200; Loss = 0.19208110868930817
        Accuracy = 0.9375
        Epoch = 3; Batch = 300; Loss = 0.10772916674613953
        Accuracy = 0.9455645084381104
        Epoch = 4; Batch = 100; Loss = 0.05319514870643616
        Accuracy = 0.9546371102333069
        Epoch = 4; Batch = 200; Loss = 0.08398851752281189
        Accuracy = 0.9596773982048035
        Epoch = 4; Batch = 300; Loss = 0.1401723325252533
        Accuracy = 0.9586693644523621
        Epoch = 5; Batch = 100; Loss = 0.013004494830965996
        Accuracy = 0.9677419066429138
        Epoch = 5; Batch = 200; Loss = 0.13374775648117065
        Accuracy = 0.9576612710952759
        Epoch = 5; Batch = 300; Loss = 0.022760355845093727
        Accuracy = 0.9627016186714172
        Epoch = 6; Batch = 100; Loss = 0.04398946836590767
        Accuracy = 0.9707661271095276
        Epoch = 6; Batch = 200; Loss = 0.059732794761657715
        Accuracy = 0.9667338728904724
        Epoch = 6; Batch = 300; Loss = 0.01882414147257805
        Accuracy = 0.9667338728904724
        Epoch = 7; Batch = 100; Loss = 0.057826098054647446
        Accuracy = 0.96875
        Epoch = 7; Batch = 200; Loss = 0.07328670471906662
        Accuracy = 0.9697580933570862
        Epoch = 7; Batch = 300; Loss = 0.008352180942893028
        Accuracy = 0.9697580933570862
        Epoch = 8; Batch = 100; Loss = 0.0417218878865242
        Accuracy = 0.9697580933570862
        Epoch = 8; Batch = 200; Loss = 0.09399110078811646
        Accuracy = 0.9727822542190552
        Epoch = 8; Batch = 300; Loss = 0.0666482076048851
        Accuracy = 0.9707661271095276
        Epoch = 9; Batch = 100; Loss = 0.04094938933849335
        Accuracy = 0.9737903475761414
        Epoch = 9; Batch = 200; Loss = 0.023497669026255608
        Accuracy = 0.9737903475761414
        Epoch = 9; Batch = 300; Loss = 0.011730192229151726
        Accuracy = 0.9707661271095276
        Epoch = 10; Batch = 100; Loss = 0.08005531877279282
        Accuracy = 0.9747983813285828
        Epoch = 10; Batch = 200; Loss = 0.08769802749156952
        Accuracy = 0.9788306355476379
        Epoch = 10; Batch = 300; Loss = 0.02846348285675049
        Accuracy = 0.9747983813285828
        Epoch = 11; Batch = 100; Loss = 0.07799088954925537
        Accuracy = 0.9788306355476379
        Epoch = 11; Batch = 200; Loss = 0.00824727863073349
        Accuracy = 0.975806474685669
        Out[3]: 1321
        Epoch = 11; Batch = 300; Loss = 0.006673918105661869
        Accuracy = 0.9737903475761414
        Epoch = 12; Batch = 100; Loss = 0.044909048825502396
        Accuracy = 0.9818548560142517
        Epoch = 12; Batch = 200; Loss = 0.02123323269188404
        Accuracy = 0.9798387289047241
        Epoch = 12; Batch = 300; Loss = 0.009174777194857597
        Accuracy = 0.9798387289047241
        Epoch = 13; Batch = 100; Loss = 0.004449035506695509
        Accuracy = 0.9768145084381104
        Epoch = 13; Batch = 200; Loss = 0.05925485119223595
        Accuracy = 0.9788306355476379
        Epoch = 13; Batch = 300; Loss = 0.024766722694039345
        Accuracy = 0.975806474685669
        Epoch = 14; Batch = 100; Loss = 0.02544177696108818
        Accuracy = 0.9798387289047241
        Epoch = 14; Batch = 200; Loss = 0.06432810425758362
        Accuracy = 0.975806474685669
        Epoch = 14; Batch = 300; Loss = 0.0553366094827652
        Accuracy = 0.9778226017951965
        Epoch = 15; Batch = 100; Loss = 0.06305833160877228
        Accuracy = 0.975806474685669
        Epoch = 15; Batch = 200; Loss = 0.009936298243701458
        Accuracy = 0.9798387289047241
        Epoch = 15; Batch = 300; Loss = 0.019399156793951988
        Accuracy = 0.9788306355476379
        Epoch = 16; Batch = 100; Loss = 0.06975899636745453
        Accuracy = 0.9707661271095276
        Epoch = 16; Batch = 200; Loss = 0.04078266769647598
        Accuracy = 0.9808467626571655
        Epoch = 16; Batch = 300; Loss = 0.13207776844501495
        Accuracy = 0.9788306355476379
        Epoch = 17; Batch = 100; Loss = 0.06141747534275055
        Accuracy = 0.9808467626571655
        Epoch = 17; Batch = 200; Loss = 0.008853238075971603
        Accuracy = 0.9798387289047241
        Epoch = 17; Batch = 300; Loss = 0.1499772071838379
        Accuracy = 0.9798387289047241
        Epoch = 18; Batch = 100; Loss = 0.05069355666637421
        Accuracy = 0.9858871102333069
        Epoch = 18; Batch = 200; Loss = 0.002881245454773307
        Accuracy = 0.9828628897666931
        Epoch = 18; Batch = 300; Loss = 0.010960441082715988
        Accuracy = 0.9808467626571655
        Epoch = 19; Batch = 100; Loss = 0.02881675399839878
        Accuracy = 0.9838709831237793
        Epoch = 19; Batch = 200; Loss = 0.04455310106277466
        Accuracy = 0.9778226017951965
        Epoch = 19; Batch = 300; Loss = 0.015683501958847046
        Accuracy = 0.9828628897666931
        Epoch = 20; Batch = 100; Loss = 0.011129770427942276
        Accuracy = 0.9818548560142517
        Epoch = 20; Batch = 200; Loss = 0.00014649057993665338
        Accuracy = 0.9818548560142517
        Epoch = 20; Batch = 300; Loss = 0.005878562573343515
        Accuracy = 0.9848790168762207
        Epoch = 21; Batch = 100; Loss = 0.0026900507509708405
        Accuracy = 0.9818548560142517
        Epoch = 21; Batch = 200; Loss = 0.003800366073846817
        Accuracy = 0.9808467626571655
        Epoch = 21; Batch = 300; Loss = 0.01567978411912918
        Accuracy = 0.9848790168762207
        Epoch = 22; Batch = 100; Loss = 0.0013576468918472528
        Accuracy = 0.9848790168762207
        Epoch = 22; Batch = 200; Loss = 0.003224289510399103
        Accuracy = 0.9858871102333069
        Epoch = 22; Batch = 300; Loss = 0.0015481372829526663
        Accuracy = 0.9818548560142517
        Epoch = 23; Batch = 100; Loss = 0.026710642501711845
        Accuracy = 0.9868951439857483
        Epoch = 23; Batch = 200; Loss = 0.002208802616223693
        Accuracy = 0.9808467626571655
        Epoch = 23; Batch = 300; Loss = 0.018307890743017197
        Accuracy = 0.9828628897666931
        Epoch = 24; Batch = 100; Loss = 0.08291947096586227
        Accuracy = 0.9818548560142517
        Epoch = 24; Batch = 200; Loss = 0.011806401424109936
        Accuracy = 0.9868951439857483
        Epoch = 24; Batch = 300; Loss = 0.004612339194864035
        Accuracy = 0.9858871102333069
        Epoch = 25; Batch = 100; Loss = 0.00016331026563420892
        Accuracy = 0.9858871102333069
        Epoch = 25; Batch = 200; Loss = 0.0036391555331647396
        Accuracy = 0.9858871102333069
    """

    def __init__(self):

        super(face2, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=5)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=5)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 3 * 3, out_features=5 * 3 * 1)
        self.linear2 = nn.Linear(in_features=5 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x


class face3(nn.Module):
    """
        从 model_48_48.py 里的 face5 拿过来相关模型代码
        total parameters: 523
        训练得比较久 (20 epochs)，但是依然能达到 98%
        Accuracy = 0.4868951737880707
        Epoch = 1; Batch = 100; Loss = 0.3454478979110718
        Accuracy = 0.8760080933570862
        Epoch = 1; Batch = 200; Loss = 0.23624448478221893
        Accuracy = 0.8981854915618896
        Epoch = 1; Batch = 300; Loss = 0.2403896152973175
        Accuracy = 0.9052419066429138
        Epoch = 2; Batch = 100; Loss = 0.18164055049419403
        Accuracy = 0.9143145084381104
        Epoch = 2; Batch = 200; Loss = 0.03479281812906265
        Accuracy = 0.9193548560142517
        Epoch = 2; Batch = 300; Loss = 0.19452956318855286
        Accuracy = 0.9243951439857483
        Epoch = 3; Batch = 100; Loss = 0.07634894549846649
        Accuracy = 0.9314516186714172
        Epoch = 3; Batch = 200; Loss = 0.12596501410007477
        Accuracy = 0.9354838728904724
        Epoch = 3; Batch = 300; Loss = 0.14037425816059113
        Accuracy = 0.9385080933570862
        Epoch = 4; Batch = 100; Loss = 0.12108723819255829
        Accuracy = 0.944556474685669
        Epoch = 4; Batch = 200; Loss = 0.07523853331804276
        Accuracy = 0.9425403475761414
        Epoch = 4; Batch = 300; Loss = 0.024052945896983147
        Accuracy = 0.9506048560142517
        Epoch = 5; Batch = 100; Loss = 0.2798024117946625
        Accuracy = 0.9596773982048035
        Epoch = 5; Batch = 200; Loss = 0.01479676179587841
        Accuracy = 0.9606854915618896
        Epoch = 5; Batch = 300; Loss = 0.09098589420318604
        Accuracy = 0.9516128897666931
        Epoch = 6; Batch = 100; Loss = 0.14364489912986755
        Accuracy = 0.9707661271095276
        Epoch = 6; Batch = 200; Loss = 0.03397713601589203
        Accuracy = 0.9475806355476379
        Epoch = 6; Batch = 300; Loss = 0.07572606205940247
        Accuracy = 0.9667338728904724
        Epoch = 7; Batch = 100; Loss = 0.09191369265317917
        Accuracy = 0.9717742204666138
        Epoch = 7; Batch = 200; Loss = 0.085675448179245
        Accuracy = 0.9627016186714172
        Epoch = 7; Batch = 300; Loss = 0.04345086216926575
        Accuracy = 0.9677419066429138
        Epoch = 8; Batch = 100; Loss = 0.03268326818943024
        Accuracy = 0.9707661271095276
        Epoch = 8; Batch = 200; Loss = 0.015243234112858772
        Accuracy = 0.9657257795333862
        Epoch = 8; Batch = 300; Loss = 0.05235153064131737
        Accuracy = 0.9727822542190552
        Epoch = 9; Batch = 100; Loss = 0.010855427011847496
        Accuracy = 0.9727822542190552
        Epoch = 9; Batch = 200; Loss = 0.07339508086442947
        Accuracy = 0.9737903475761414
        Epoch = 9; Batch = 300; Loss = 0.06863008439540863
        Accuracy = 0.9697580933570862
        Epoch = 10; Batch = 100; Loss = 0.1251816600561142
        Accuracy = 0.9637096524238586
        Epoch = 10; Batch = 200; Loss = 0.042278025299310684
        Accuracy = 0.9707661271095276
        Epoch = 10; Batch = 300; Loss = 0.009097756817936897
        Accuracy = 0.9717742204666138
        Epoch = 11; Batch = 100; Loss = 0.012671534903347492
        Accuracy = 0.9707661271095276
        Epoch = 11; Batch = 200; Loss = 0.045620642602443695
        Accuracy = 0.975806474685669
        Epoch = 11; Batch = 300; Loss = 0.013535183854401112
        Accuracy = 0.9747983813285828
        Epoch = 12; Batch = 100; Loss = 0.02671990543603897
        Accuracy = 0.9747983813285828
        Epoch = 12; Batch = 200; Loss = 0.0650729387998581
        Accuracy = 0.9717742204666138
        Epoch = 12; Batch = 300; Loss = 0.09830775856971741
        Accuracy = 0.9727822542190552
        Epoch = 13; Batch = 100; Loss = 0.005188629496842623
        Accuracy = 0.9737903475761414
        Epoch = 13; Batch = 200; Loss = 0.01683330163359642
        Accuracy = 0.9747983813285828
        Epoch = 13; Batch = 300; Loss = 0.011767223477363586
        Accuracy = 0.9737903475761414
        Epoch = 14; Batch = 100; Loss = 0.13607212901115417
        Accuracy = 0.9727822542190552
        Epoch = 14; Batch = 200; Loss = 0.015380934812128544
        Accuracy = 0.9727822542190552
        Epoch = 14; Batch = 300; Loss = 0.045461736619472504
        Accuracy = 0.9697580933570862
        Epoch = 15; Batch = 100; Loss = 0.09526511281728745
        Accuracy = 0.9717742204666138
        Epoch = 15; Batch = 200; Loss = 0.0861172080039978
        Accuracy = 0.96875
        Epoch = 15; Batch = 300; Loss = 0.04578740894794464
        Accuracy = 0.9747983813285828
        Epoch = 16; Batch = 100; Loss = 0.002093648537993431
        Accuracy = 0.96875
        Epoch = 16; Batch = 200; Loss = 0.017699964344501495
        Accuracy = 0.9747983813285828
        Epoch = 16; Batch = 300; Loss = 0.026024650782346725
        Accuracy = 0.975806474685669
        Epoch = 17; Batch = 100; Loss = 0.13679495453834534
        Accuracy = 0.9717742204666138
        Epoch = 17; Batch = 200; Loss = 0.020131349563598633
        Accuracy = 0.9768145084381104
        Epoch = 17; Batch = 300; Loss = 0.2641359567642212
        Accuracy = 0.9667338728904724
        Epoch = 18; Batch = 100; Loss = 0.05349238961935043
        Accuracy = 0.9737903475761414
        Epoch = 18; Batch = 200; Loss = 0.013557331636548042
        Accuracy = 0.9677419066429138
        Epoch = 18; Batch = 300; Loss = 0.012126392684876919
        Accuracy = 0.9737903475761414
        Epoch = 19; Batch = 100; Loss = 0.009544995613396168
        Accuracy = 0.9788306355476379
        Epoch = 19; Batch = 200; Loss = 0.014239879325032234
        Accuracy = 0.9778226017951965
        Epoch = 19; Batch = 300; Loss = 0.005884518381208181
        Accuracy = 0.9768145084381104
        Epoch = 20; Batch = 100; Loss = 0.005305471830070019
        Accuracy = 0.9798387289047241
        Epoch = 20; Batch = 200; Loss = 0.011415170505642891
        Accuracy = 0.9808467626571655
        Epoch = 20; Batch = 300; Loss = 0.05935301631689072
        Accuracy = 0.9707661271095276
        Epoch = 21; Batch = 100; Loss = 0.057724207639694214
        Accuracy = 0.975806474685669
        Epoch = 21; Batch = 200; Loss = 0.3496829569339752
        Accuracy = 0.9808467626571655
        Epoch = 21; Batch = 300; Loss = 0.004625203553587198
        Accuracy = 0.9536290168762207
        Epoch = 22; Batch = 100; Loss = 0.133264422416687
        Accuracy = 0.9798387289047241
        Epoch = 22; Batch = 200; Loss = 0.021241137757897377
        Accuracy = 0.9768145084381104
        Epoch = 22; Batch = 300; Loss = 0.023871172219514847
        Accuracy = 0.9778226017951965
        Epoch = 23; Batch = 100; Loss = 0.07612285017967224
        Accuracy = 0.9747983813285828
        Epoch = 23; Batch = 200; Loss = 0.053427234292030334
        Accuracy = 0.9768145084381104
        Epoch = 23; Batch = 300; Loss = 0.08561861515045166
        Accuracy = 0.9798387289047241
        Epoch = 24; Batch = 100; Loss = 0.01881220005452633
        Accuracy = 0.9707661271095276
        Epoch = 24; Batch = 200; Loss = 0.007030903361737728
        Accuracy = 0.9778226017951965
        Epoch = 24; Batch = 300; Loss = 0.004531639162451029
        Accuracy = 0.975806474685669
        Epoch = 25; Batch = 100; Loss = 0.07521729171276093
        Accuracy = 0.9788306355476379
        Epoch = 25; Batch = 200; Loss = 0.019704006612300873
        Accuracy = 0.9818548560142517
        Epoch = 25; Batch = 300; Loss = 0.08005556464195251
        Accuracy = 0.9788306355476379
        Epoch = 26; Batch = 100; Loss = 0.016196614131331444
        Accuracy = 0.9747983813285828
        Epoch = 26; Batch = 200; Loss = 0.016730137169361115
        Accuracy = 0.9747983813285828
        Epoch = 26; Batch = 300; Loss = 0.1927773803472519
        Accuracy = 0.9788306355476379
        Epoch = 27; Batch = 100; Loss = 0.027455395087599754
        Accuracy = 0.9838709831237793
        Epoch = 27; Batch = 200; Loss = 0.009831951931118965
        Accuracy = 0.9868951439857483
        Epoch = 27; Batch = 300; Loss = 0.057908572256565094
        Accuracy = 0.9808467626571655
        Epoch = 28; Batch = 100; Loss = 0.013798605650663376
        Accuracy = 0.975806474685669
        Epoch = 28; Batch = 200; Loss = 0.026006899774074554
        Accuracy = 0.9737903475761414
        Epoch = 28; Batch = 300; Loss = 0.007364661432802677
        Accuracy = 0.9798387289047241
        Epoch = 29; Batch = 100; Loss = 0.014833795838057995
        Accuracy = 0.9717742204666138
        Epoch = 29; Batch = 200; Loss = 0.042434267699718475
        Accuracy = 0.9778226017951965
        Epoch = 29; Batch = 300; Loss = 0.008531879633665085
        Accuracy = 0.9808467626571655
        Epoch = 30; Batch = 100; Loss = 0.08862137794494629
        Accuracy = 0.9788306355476379
        Epoch = 30; Batch = 200; Loss = 0.04612044617533684
        Accuracy = 0.9778226017951965
        Epoch = 30; Batch = 300; Loss = 0.1236547976732254
        Accuracy = 0.9818548560142517
    """

    def __init__(self):

        super(face3, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=3, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=3)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=3, out_channels=3, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=3)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=3, out_channels=3, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=3)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=3 * 3 * 3, out_features=3 * 3 * 1)
        self.linear2 = nn.Linear(in_features=3 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x

