import torch
from torch import nn

class face1(nn.Module):
    """
    """

    def __init__(self):

        super(face1, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=1, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=8)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=8)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=8, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 3 * 3, out_features=5 * 3 * 1)
        self.linear2 = nn.Linear(in_features=5 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x


class face2(nn.Module):
    """
    """

    def __init__(self):

        super(face2, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=1, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=5)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=5)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=5, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 3 * 3, out_features=5 * 3 * 1)
        self.linear2 = nn.Linear(in_features=5 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x


class face3(nn.Module):
    """
        Accuracy = 0.4868951737880707
        Epoch = 1; Batch = 100; Loss = 0.6888558864593506
        Accuracy = 0.4969758093357086
        Epoch = 1; Batch = 200; Loss = 0.47365331649780273
        Accuracy = 0.8014112710952759
        Epoch = 1; Batch = 300; Loss = 0.37908196449279785
        Accuracy = 0.8578628897666931
        Epoch = 2; Batch = 100; Loss = 0.2785431146621704
        Accuracy = 0.8760080933570862
        Epoch = 2; Batch = 200; Loss = 0.275993287563324
        Accuracy = 0.8891128897666931
        Epoch = 2; Batch = 300; Loss = 0.2996975779533386
        Accuracy = 0.8911290168762207
        Epoch = 3; Batch = 100; Loss = 0.35852840542793274
        Accuracy = 0.9022177457809448
        Epoch = 3; Batch = 200; Loss = 0.2767619490623474
        Accuracy = 0.9042338728904724
        Epoch = 3; Batch = 300; Loss = 0.450740784406662
        Accuracy = 0.9163306355476379
        Epoch = 4; Batch = 100; Loss = 0.07735887914896011
        Accuracy = 0.9203628897666931
        Epoch = 4; Batch = 200; Loss = 0.15712803602218628
        Accuracy = 0.9143145084381104
        Epoch = 4; Batch = 300; Loss = 0.2973056435585022
        Accuracy = 0.9284273982048035
        Epoch = 5; Batch = 100; Loss = 0.2201523780822754
        Accuracy = 0.9284273982048035
        Epoch = 5; Batch = 200; Loss = 0.22881942987442017
        Accuracy = 0.9264112710952759
        Epoch = 5; Batch = 300; Loss = 0.19575797021389008
        Accuracy = 0.9233871102333069
        Epoch = 6; Batch = 100; Loss = 0.22073490917682648
        Accuracy = 0.9254032373428345
        Epoch = 6; Batch = 200; Loss = 0.10222962498664856
        Accuracy = 0.9344757795333862
        Epoch = 6; Batch = 300; Loss = 0.13914556801319122
        Accuracy = 0.9324596524238586
        Epoch = 7; Batch = 100; Loss = 0.10437144339084625
        Accuracy = 0.9395161271095276
        Epoch = 7; Batch = 200; Loss = 0.2860701084136963
        Accuracy = 0.9435483813285828
        Epoch = 7; Batch = 300; Loss = 0.3128426671028137
        Accuracy = 0.9395161271095276
        Epoch = 8; Batch = 100; Loss = 0.1542285978794098
        Accuracy = 0.9364919066429138
        Epoch = 8; Batch = 200; Loss = 0.11948879808187485
        Accuracy = 0.9375
        Epoch = 8; Batch = 300; Loss = 0.1462707817554474
        Accuracy = 0.9385080933570862
        Epoch = 9; Batch = 100; Loss = 0.08977307379245758
        Accuracy = 0.9354838728904724
        Epoch = 9; Batch = 200; Loss = 0.16336877644062042
        Accuracy = 0.9455645084381104
        Epoch = 9; Batch = 300; Loss = 0.22487613558769226
        Accuracy = 0.944556474685669
        Epoch = 10; Batch = 100; Loss = 0.1859056055545807
        Accuracy = 0.9395161271095276
        Epoch = 10; Batch = 200; Loss = 0.14567610621452332
        Accuracy = 0.9465726017951965
        Epoch = 10; Batch = 300; Loss = 0.0921676754951477
        Accuracy = 0.9475806355476379
        Epoch = 11; Batch = 100; Loss = 0.28131479024887085
        Accuracy = 0.9495967626571655
        Epoch = 11; Batch = 200; Loss = 0.05582795664668083
        Accuracy = 0.9455645084381104
        Epoch = 11; Batch = 300; Loss = 0.11789383739233017
        Accuracy = 0.9455645084381104
        Epoch = 12; Batch = 100; Loss = 0.13838444650173187
        Accuracy = 0.9395161271095276
        Epoch = 12; Batch = 200; Loss = 0.14129604399204254
        Accuracy = 0.9485887289047241
        Epoch = 12; Batch = 300; Loss = 0.03091234713792801
        Accuracy = 0.9455645084381104
        Epoch = 13; Batch = 100; Loss = 0.06941238045692444
        Accuracy = 0.9506048560142517
        Epoch = 13; Batch = 200; Loss = 0.32386451959609985
        Accuracy = 0.9475806355476379
        Epoch = 13; Batch = 300; Loss = 0.27604228258132935
        Accuracy = 0.9526209831237793
        Epoch = 14; Batch = 100; Loss = 0.10202450305223465
        Accuracy = 0.9465726017951965
        Epoch = 14; Batch = 200; Loss = 0.034344229847192764
        Accuracy = 0.944556474685669
        Epoch = 14; Batch = 300; Loss = 0.250019907951355
        Accuracy = 0.9465726017951965
        Epoch = 15; Batch = 100; Loss = 0.0327252596616745
        Accuracy = 0.9485887289047241
        Epoch = 15; Batch = 200; Loss = 0.09943129867315292
        Accuracy = 0.9475806355476379
        Epoch = 15; Batch = 300; Loss = 0.03549768030643463
        Accuracy = 0.9506048560142517
        Epoch = 16; Batch = 100; Loss = 0.18382519483566284
        Accuracy = 0.9495967626571655
        Epoch = 16; Batch = 200; Loss = 0.17488741874694824
        Accuracy = 0.9506048560142517
        Epoch = 16; Batch = 300; Loss = 0.1297263503074646
        Accuracy = 0.9506048560142517
        Epoch = 17; Batch = 100; Loss = 0.03828181326389313
        Accuracy = 0.9516128897666931
        Epoch = 17; Batch = 200; Loss = 0.19272705912590027
        Accuracy = 0.9465726017951965
        Epoch = 17; Batch = 300; Loss = 0.1932130604982376
        Accuracy = 0.9516128897666931
        Epoch = 18; Batch = 100; Loss = 0.06695301085710526
        Accuracy = 0.9495967626571655
        Epoch = 18; Batch = 200; Loss = 0.18193084001541138
        Accuracy = 0.9485887289047241
        Epoch = 18; Batch = 300; Loss = 0.06740208715200424
        Accuracy = 0.9506048560142517
        Epoch = 19; Batch = 100; Loss = 0.1718621850013733
        Accuracy = 0.9475806355476379
        Epoch = 19; Batch = 200; Loss = 0.15845628082752228
        Accuracy = 0.9536290168762207
        Epoch = 19; Batch = 300; Loss = 0.22800302505493164
        Accuracy = 0.9455645084381104
        Epoch = 20; Batch = 100; Loss = 0.09841141849756241
        Accuracy = 0.9405242204666138
        Epoch = 20; Batch = 200; Loss = 0.2850935459136963
        Accuracy = 0.9526209831237793
        Epoch = 20; Batch = 300; Loss = 0.21575072407722473
        Accuracy = 0.9485887289047241
        Epoch = 21; Batch = 100; Loss = 0.21139079332351685
        Accuracy = 0.9526209831237793
        Epoch = 21; Batch = 200; Loss = 0.044871602207422256
        Accuracy = 0.9395161271095276
        Epoch = 21; Batch = 300; Loss = 0.15007270872592926
        Accuracy = 0.9506048560142517
        Epoch = 22; Batch = 100; Loss = 0.171762615442276
        Accuracy = 0.9485887289047241
        Epoch = 22; Batch = 200; Loss = 0.10324481129646301
        Accuracy = 0.9485887289047241
        Epoch = 22; Batch = 300; Loss = 0.08558844029903412
        Accuracy = 0.9526209831237793
        Epoch = 23; Batch = 100; Loss = 0.2527625858783722
        Accuracy = 0.9536290168762207
        Epoch = 23; Batch = 200; Loss = 0.03496171906590462
        Accuracy = 0.9485887289047241
        Epoch = 23; Batch = 300; Loss = 0.1359509527683258
        Accuracy = 0.944556474685669
        Epoch = 24; Batch = 100; Loss = 0.18179723620414734
        Accuracy = 0.9526209831237793
        Epoch = 24; Batch = 200; Loss = 0.08746307343244553
        Accuracy = 0.9485887289047241
        Epoch = 24; Batch = 300; Loss = 0.11484666168689728
        Accuracy = 0.9526209831237793
        Epoch = 25; Batch = 100; Loss = 0.09237997233867645
        Accuracy = 0.9465726017951965
        Epoch = 25; Batch = 200; Loss = 0.08161652088165283
        Accuracy = 0.9465726017951965
        Epoch = 25; Batch = 300; Loss = 0.15206843614578247
        Accuracy = 0.9516128897666931
        Epoch = 26; Batch = 100; Loss = 0.17603754997253418
        Accuracy = 0.9455645084381104
        Epoch = 26; Batch = 200; Loss = 0.10191582143306732
        Accuracy = 0.9556451439857483
        Epoch = 26; Batch = 300; Loss = 0.20989990234375
        Accuracy = 0.9495967626571655
        Epoch = 27; Batch = 100; Loss = 0.021457666531205177
        Accuracy = 0.9495967626571655
        Epoch = 27; Batch = 200; Loss = 0.09071991592645645
        Accuracy = 0.9516128897666931
        Epoch = 27; Batch = 300; Loss = 0.29529979825019836
        Accuracy = 0.944556474685669
        Epoch = 28; Batch = 100; Loss = 0.06170367822051048
        Accuracy = 0.9526209831237793
        Epoch = 28; Batch = 200; Loss = 0.12006564438343048
        Accuracy = 0.9566532373428345
        Epoch = 28; Batch = 300; Loss = 0.14892147481441498
        Accuracy = 0.9556451439857483
        Epoch = 29; Batch = 100; Loss = 0.11857838928699493
        Accuracy = 0.9506048560142517
        Epoch = 29; Batch = 200; Loss = 0.07838818430900574
        Accuracy = 0.9516128897666931
        Epoch = 29; Batch = 300; Loss = 0.19496512413024902
        Accuracy = 0.9596773982048035
        Epoch = 30; Batch = 100; Loss = 0.07341261208057404
        Accuracy = 0.9526209831237793
        Epoch = 30; Batch = 200; Loss = 0.18418090045452118
        Accuracy = 0.9536290168762207
        Epoch = 30; Batch = 300; Loss = 0.07164895534515381
        Accuracy = 0.9586693644523621
    """

    def __init__(self):

        super(face3, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=1, out_channels=3, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=3)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=3, out_channels=3, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=3)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=3, out_channels=3, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=3)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=3 * 3 * 3, out_features=3 * 3 * 1)
        self.linear2 = nn.Linear(in_features=3 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x

