import torch
from torch import nn

class face1(nn.Module):

    """
        achieve 99% accuracy at about 5 epochs
    """

    def __init__(self):

        super(face1, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=10, out_channels=20, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=20)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=20, out_channels=20, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=20)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv4 = nn.Conv2d(in_channels=20, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu4 = nn.PReLU(num_parameters=10)
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=10*12*12, out_features=10*12*6)
        self.linear2 = nn.Linear(in_features=10*12*6, out_features=10*12*2)
        self.linear3 = nn.Linear(in_features=10*12*2, out_features=48)
        self.linear4 = nn.Linear(in_features=48, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        # x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        # x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = self.conv4(x)
        x = self.prelu4(x)
        x = self.pool4(x)

        x = x.view(x.shape[0], -1)  # [batch_size, 1080]
        x = self.linear1(x)  # [batch_size, 720]
        x = self.linear2(x)  # [batch_size, 180]
        x = self.linear3(x)  # [batch_size, 1]
        x = self.linear4(x)  # [batch_size, 1]

        x = torch.sigmoid(x)

        return x


class face2(nn.Module):
    """

    """

    def __init__(self):

        super(face2, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=10, out_channels=20, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=20)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=20, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=10)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=10 * 24 * 24, out_features=10 * 24 * 6)
        self.linear2 = nn.Linear(in_features=10 * 24 * 6, out_features=10 * 24 * 1)
        self.linear3 = nn.Linear(in_features=240, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)

        x = self.conv2(x)
        x = self.prelu2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)
        x = self.linear3(x)

        x = torch.sigmoid(x)

        return x


class face3(nn.Module):
    """

    """

    def __init__(self):

        super(face3, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=10, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=10)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=10, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 12 * 12, out_features=5 * 12 * 6)
        self.linear2 = nn.Linear(in_features=5 * 12 * 6, out_features=5 * 12 * 1)
        self.linear3 = nn.Linear(in_features=60, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)
        x = self.linear3(x)

        x = torch.sigmoid(x)

        return x


class face4(nn.Module):
    """
        训练得比较久，但是依然能达到 99%
        Accuracy = 0.5131048560142517
        Epoch = 1; Batch = 100; Loss = 0.38907432556152344
        Accuracy = 0.9092742204666138
        Epoch = 1; Batch = 200; Loss = 0.057506389915943146
        Accuracy = 0.9385080933570862
        Epoch = 1; Batch = 300; Loss = 0.05102555826306343
        Accuracy = 0.9465726017951965
        Epoch = 2; Batch = 100; Loss = 0.08846748620271683
        Accuracy = 0.9667338728904724
        Epoch = 2; Batch = 200; Loss = 0.0053645167499780655
        Accuracy = 0.9737903475761414
        Epoch = 2; Batch = 300; Loss = 0.186961367726326
        Accuracy = 0.9697580933570862
        Epoch = 3; Batch = 100; Loss = 0.01981036365032196
        Accuracy = 0.9808467626571655
        Epoch = 3; Batch = 200; Loss = 0.21187320351600647
        Accuracy = 0.9768145084381104
        Epoch = 3; Batch = 300; Loss = 0.035282522439956665
        Accuracy = 0.9828628897666931
        Epoch = 4; Batch = 100; Loss = 0.03491542860865593
        Accuracy = 0.9768145084381104
        Epoch = 4; Batch = 200; Loss = 0.06457220762968063
        Accuracy = 0.9848790168762207
        Epoch = 4; Batch = 300; Loss = 0.003939385060220957
        Accuracy = 0.9868951439857483
        Epoch = 5; Batch = 100; Loss = 0.016029560938477516
        Accuracy = 0.9747983813285828
        Epoch = 5; Batch = 200; Loss = 0.002026407979428768
        Accuracy = 0.9889112710952759
        Epoch = 5; Batch = 300; Loss = 0.0257649514824152
        Accuracy = 0.9858871102333069
        Epoch = 6; Batch = 100; Loss = 0.0010996127966791391
        Accuracy = 0.9909273982048035
        Epoch = 6; Batch = 200; Loss = 0.011984930373728275
        Accuracy = 0.9889112710952759
        Epoch = 6; Batch = 300; Loss = 0.00835876353085041
        Accuracy = 0.9879032373428345
        Epoch = 7; Batch = 100; Loss = 0.022833358496427536
        Accuracy = 0.9919354915618896
        Epoch = 7; Batch = 200; Loss = 0.016082895919680595
        Accuracy = 0.9919354915618896
        Epoch = 7; Batch = 300; Loss = 0.022083211690187454
        Accuracy = 0.9868951439857483
        Epoch = 8; Batch = 100; Loss = 0.011268865317106247
        Accuracy = 0.9899193644523621
        Epoch = 8; Batch = 200; Loss = 0.043379515409469604
        Accuracy = 0.9919354915618896
        Epoch = 8; Batch = 300; Loss = 0.03491778299212456
        Accuracy = 0.9889112710952759
        Epoch = 9; Batch = 100; Loss = 0.0011392391752451658
        Accuracy = 0.9899193644523621
        Epoch = 9; Batch = 200; Loss = 0.007856963202357292
        Accuracy = 0.9919354915618896
        Epoch = 9; Batch = 300; Loss = 0.004224217962473631
        Accuracy = 0.9939516186714172
        Epoch = 10; Batch = 100; Loss = 0.0034734075888991356
        Accuracy = 0.9919354915618896
        Epoch = 10; Batch = 200; Loss = 0.003059208393096924
        Accuracy = 0.992943525314331
        Epoch = 10; Batch = 300; Loss = 0.051734283566474915
        Accuracy = 0.9889112710952759
        Epoch = 11; Batch = 100; Loss = 0.000936422380618751
        Accuracy = 0.992943525314331
    """

    def __init__(self):

        super(face4, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=10, out_channels=10, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=10)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=10, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 6 * 6, out_features=5 * 6 * 3)
        self.linear2 = nn.Linear(in_features=5 * 6 * 3, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x


class face5(nn.Module):
    """
        total parameters: 3925
        训练得比较久，但是依然能达到 99%
        Accuracy = 0.5131048560142517
        Epoch = 1; Batch = 100; Loss = 0.2972929775714874
        Accuracy = 0.9052419066429138
        Epoch = 1; Batch = 200; Loss = 0.1957811564207077
        Accuracy = 0.9233871102333069
        Epoch = 1; Batch = 300; Loss = 0.4280094504356384
        Accuracy = 0.9415322542190552
        Epoch = 2; Batch = 100; Loss = 0.15349292755126953
        Accuracy = 0.9495967626571655
        Epoch = 2; Batch = 200; Loss = 0.07191399484872818
        Accuracy = 0.9536290168762207
        Epoch = 2; Batch = 300; Loss = 0.0744529590010643
        Accuracy = 0.9546371102333069
        Epoch = 3; Batch = 100; Loss = 0.2160627692937851
        Accuracy = 0.9637096524238586
        Epoch = 3; Batch = 200; Loss = 0.05229559913277626
        Accuracy = 0.96875
        Epoch = 3; Batch = 300; Loss = 0.013039147481322289
        Accuracy = 0.9707661271095276
        Epoch = 4; Batch = 100; Loss = 0.034112073481082916
        Accuracy = 0.9717742204666138
        Epoch = 4; Batch = 200; Loss = 0.0757002979516983
        Accuracy = 0.9768145084381104
        Epoch = 4; Batch = 300; Loss = 0.03396043926477432
        Accuracy = 0.9788306355476379
        Epoch = 5; Batch = 100; Loss = 0.026645319536328316
        Accuracy = 0.9808467626571655
        Epoch = 5; Batch = 200; Loss = 0.02745429426431656
        Accuracy = 0.9798387289047241
        Epoch = 5; Batch = 300; Loss = 0.004044320434331894
        Accuracy = 0.9808467626571655
        Epoch = 6; Batch = 100; Loss = 0.1071123406291008
        Accuracy = 0.9848790168762207
        Epoch = 6; Batch = 200; Loss = 0.004252870101481676
        Accuracy = 0.9808467626571655
        Epoch = 6; Batch = 300; Loss = 0.010620281100273132
        Accuracy = 0.9848790168762207
        Epoch = 7; Batch = 100; Loss = 0.0012543669436126947
        Accuracy = 0.9848790168762207
        Epoch = 7; Batch = 200; Loss = 0.003098652232438326
        Accuracy = 0.9899193644523621
        Epoch = 7; Batch = 300; Loss = 0.007558962795883417
        Accuracy = 0.9838709831237793
        Epoch = 8; Batch = 100; Loss = 0.07301675528287888
        Accuracy = 0.9828628897666931
        Epoch = 8; Batch = 200; Loss = 0.008543593809008598
        Accuracy = 0.9879032373428345
        Epoch = 8; Batch = 300; Loss = 0.011097528971731663
        Accuracy = 0.9838709831237793
        Epoch = 9; Batch = 100; Loss = 0.00907999835908413
        Accuracy = 0.9868951439857483
        Epoch = 9; Batch = 200; Loss = 0.004028887487947941
        Accuracy = 0.9657257795333862
        Epoch = 9; Batch = 300; Loss = 0.001754207187332213
        Accuracy = 0.9868951439857483
        Epoch = 10; Batch = 100; Loss = 0.04058900848031044
        Accuracy = 0.9858871102333069
        Epoch = 10; Batch = 200; Loss = 0.2101980447769165
        Accuracy = 0.9939516186714172
        Epoch = 10; Batch = 300; Loss = 0.011960667558014393
        Accuracy = 0.9848790168762207
        Epoch = 11; Batch = 100; Loss = 0.0005980021087452769
        Accuracy = 0.9879032373428345
        Epoch = 11; Batch = 200; Loss = 0.05508396402001381
        Accuracy = 0.9899193644523621
        Epoch = 11; Batch = 300; Loss = 0.06057411804795265
        Accuracy = 0.9868951439857483
        Epoch = 12; Batch = 100; Loss = 0.10531638562679291
        Accuracy = 0.9899193644523621
        Epoch = 12; Batch = 200; Loss = 0.015717025846242905
        Accuracy = 0.9879032373428345
        Epoch = 12; Batch = 300; Loss = 0.04248358681797981
        Accuracy = 0.9889112710952759
        Epoch = 13; Batch = 100; Loss = 0.01341581903398037
        Accuracy = 0.9788306355476379
        Epoch = 13; Batch = 200; Loss = 0.012523193843662739
        Accuracy = 0.9909273982048035
        Epoch = 13; Batch = 300; Loss = 0.14247633516788483
        Accuracy = 0.9818548560142517
        Epoch = 14; Batch = 100; Loss = 0.011899841949343681
        Accuracy = 0.9868951439857483
        Epoch = 14; Batch = 200; Loss = 0.15312623977661133
        Accuracy = 0.9909273982048035
        Epoch = 14; Batch = 300; Loss = 0.00357297295704484
        Accuracy = 0.9868951439857483
        Epoch = 15; Batch = 100; Loss = 0.05105600506067276
        Accuracy = 0.9868951439857483
        Epoch = 15; Batch = 200; Loss = 0.01160518079996109
        Accuracy = 0.9889112710952759
        Epoch = 15; Batch = 300; Loss = 0.0009287964785471559
        Accuracy = 0.9868951439857483
        Epoch = 16; Batch = 100; Loss = 0.06888186186552048
        Accuracy = 0.9889112710952759
        Epoch = 16; Batch = 200; Loss = 0.004592934623360634
        Accuracy = 0.9879032373428345
        Epoch = 16; Batch = 300; Loss = 0.0001608298916835338
        Accuracy = 0.9909273982048035
        Epoch = 17; Batch = 100; Loss = 0.007353140041232109
        Accuracy = 0.9858871102333069
        Epoch = 17; Batch = 200; Loss = 0.18559354543685913
        Accuracy = 0.9919354915618896
        Epoch = 17; Batch = 300; Loss = 0.002112538553774357
        Accuracy = 0.9909273982048035
        Epoch = 18; Batch = 100; Loss = 0.003797262441366911
        Accuracy = 0.9879032373428345
        Epoch = 18; Batch = 200; Loss = 0.010992435738444328
        Accuracy = 0.9879032373428345
        Epoch = 18; Batch = 300; Loss = 0.0671544224023819
        Accuracy = 0.9879032373428345
        Epoch = 19; Batch = 100; Loss = 0.0026806157547980547
        Accuracy = 0.9909273982048035
        Epoch = 19; Batch = 200; Loss = 0.00036960895522497594
        Accuracy = 0.9889112710952759
        Epoch = 19; Batch = 300; Loss = 0.07443995773792267
        Accuracy = 0.9808467626571655
        Epoch = 20; Batch = 100; Loss = 0.002560537541285157
        Accuracy = 0.9879032373428345
        Epoch = 20; Batch = 200; Loss = 0.06483473628759384
        Accuracy = 0.9879032373428345
        Epoch = 20; Batch = 300; Loss = 0.1596500724554062
        Accuracy = 0.9909273982048035
        Epoch = 21; Batch = 100; Loss = 0.0004978609504178166
        Accuracy = 0.9868951439857483
        Epoch = 21; Batch = 200; Loss = 0.004094662144780159
        Accuracy = 0.9899193644523621
        Epoch = 21; Batch = 300; Loss = 0.03935201093554497
        Accuracy = 0.9889112710952759
        Epoch = 22; Batch = 100; Loss = 0.005296493414789438
        Accuracy = 0.9899193644523621
        Epoch = 22; Batch = 200; Loss = 0.011371949687600136
        Accuracy = 0.9889112710952759
        Epoch = 22; Batch = 300; Loss = 0.003352281404659152
        Accuracy = 0.9899193644523621
        Epoch = 23; Batch = 100; Loss = 0.00046913852565921843
        Accuracy = 0.9919354915618896
        Epoch = 23; Batch = 200; Loss = 0.10472748428583145
        Accuracy = 0.9949596524238586
        Epoch = 23; Batch = 300; Loss = 0.02959388867020607
        Accuracy = 0.9899193644523621
    """

    def __init__(self):

        super(face5, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.prelu1 = nn.PReLU(num_parameters=8)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(in_channels=8, out_channels=8, kernel_size=3, stride=1, padding=1)
        self.prelu2 = nn.PReLU(num_parameters=8)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(in_channels=8, out_channels=5, kernel_size=3, stride=1, padding=1)
        self.prelu3 = nn.PReLU(num_parameters=5)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.linear1 = nn.Linear(in_features=5 * 6 * 6, out_features=5 * 3 * 1)
        self.linear2 = nn.Linear(in_features=5 * 3 * 1, out_features=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        x = x.view(x.shape[0], -1)
        x = self.linear1(x)
        x = self.linear2(x)

        x = torch.sigmoid(x)

        return x

