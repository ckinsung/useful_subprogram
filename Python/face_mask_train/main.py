import numpy as np
import os
import torch
from torch import nn
from package.model.model_py_files.model_48_48 import face2 as face
# from package.model.model_py_files.model_48_48_grayscale import face1 as face
# from package.model.model_24_24 import face1 as face
# from package.model.model_py_files.model_24_24_grayscale import face3 as face
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision import transforms

torch.manual_seed(0)

device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

resize_size = (48, 48)
train_transform = transforms.Compose(transforms=[transforms.ToTensor(),  # 归一化并定义为 torch.float32
                                                 transforms.Grayscale(num_output_channels=3),  # 可以选择转灰度图
                                                 transforms.RandomRotation(degrees=10),  # 随机旋转正负10度
                                                 transforms.RandomHorizontalFlip(p=0.5),  # 随机左右反转
                                                 transforms.Resize(size=resize_size),  # 降采样
                                                 transforms.Normalize(mean=[0.5], std=[0.5])])  # 归一化

test_transform = transforms.Compose(transforms=[transforms.ToTensor(),  # 归一化并定义为 torch.float32
                                                transforms.Grayscale(num_output_channels=3),  # 可以选择转灰度图
                                                transforms.Resize(size=resize_size),  # 降采样
                                                transforms.Normalize(mean=[0.5], std=[0.5])])  # 归一化

# 加载数据集
# train_dataset.class_to_idx -> {'WithMask': 0, 'WithoutMask': 1}
batch_size = 128
train_dataset = datasets.ImageFolder(root=os.path.join('./data/Train/'), transform=train_transform)
train_dataloader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
test_dataset = datasets.ImageFolder(root=os.path.join('./data/Test/'), transform=test_transform)
test_dataloader = DataLoader(dataset=test_dataset, batch_size=batch_size, shuffle=True)
tof_dataset = datasets.ImageFolder(root=os.path.join('./data/TOF_data/resize_ratio_1.2'), transform=test_transform)
tof_dataloader = DataLoader(dataset=tof_dataset, batch_size=batch_size, shuffle=True)

# 加载模型
model = face().to(device=device)

# 定义损失函数
loss_fn = nn.BCELoss()

# 定义优化器
optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-3)
# optimizer = torch.optim.SGD(params=model.parameters(), lr=1e-3)

# 打印参数数量
num_of_params = sum([p.numel() for p in model.parameters()])
print(f'num of parameters = {num_of_params}')


# 验证测试集
def validation(dataloader=test_dataloader, model=model):
    accs = []
    model.eval()

    count = 0
    with torch.no_grad():
        for img, label in dataloader:
            img = img.to(device=device)
            label = label.to(device=device)

            y_pred = model(img)

            acc = torch.mean((y_pred > 0.5).flatten() == (label == 1).flatten(), dtype=torch.float32)

            accs.append(acc)
            count += 1

            if count >= 4:
                break

        accs = np.array(accs, dtype=np.float32).mean()
        print(f'Accuracy = {np.mean(accs)}')

        return accs


# Before training accuracy (Expect accuracy to be 50%, as random guess of 2 classification problem)
epochs = 50
accs_train_all = []
accs_tof_all = []
accs_test_all = []
print('train dataloader')
# accs_train = validation(dataloader=train_dataloader)
print('test dataloader')
accs_test = validation(dataloader=test_dataloader)
# print('tof dataloader')
# accs_tof = validation(dataloader=tof_dataloader)
# accs_train_all.append(accs_train)
# accs_tof_all.append(accs_tof)
accs_test_all.append(accs_test)

for epoch in range(0, epochs):

    count = 1

    # 训练模型
    for img, label in train_dataloader:

        model.train()

        img = img.to(device=device)
        label = label.to(device=device)

        # 模型计算
        y_pred = model(img)

        # 计算损失
        loss = loss_fn(y_pred.flatten(), label.to(torch.float32))

        # 梯度清空
        optimizer.zero_grad()

        # 反向传播
        loss.backward()

        # 更新参数
        optimizer.step()

        count += 1

        if count % 10 == 0:
            print(f'Epoch = {epoch + 1}; Batch = {count}; Loss = {loss}')
            print('*' * 100)
            # print('train dataloader')
            # accs_train = validation(dataloader=train_dataloader)
            print('test dataloader')
            accs_test = validation(dataloader=test_dataloader)
            # print('tof dataloader')
            # accs_tof = validation(dataloader=tof_dataloader)

            # accs_train_all.append(accs_train)
            # accs_tof_all.append(accs_tof)
            accs_test_all.append(accs_test)
            print('*' * 100)
            torch.save(model.state_dict(), "state_dict_model_face.pt")

# np.savetxt(fname='train_result.txt', X=accs_train_all)
np.savetxt(fname='test_result.txt', X=accs_test_all)
# np.savetxt(fname='tof_result.txt', X=accs_tof_all)