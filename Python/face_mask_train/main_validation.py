import numpy as np
import os
import torch
# from package.model.model_48_48 import face5 as face
# from package.model.model_24_24 import face1 as face
# from package.model.model_py_files.model_24_24_grayscale import face3 as face
from package.model.model_py_files.model_48_48_grayscale import face1 as face
from torchvision import datasets
from torch.utils.data import DataLoader
from torchvision import transforms
torch.manual_seed(0)

device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

# 加载 transforms 数据变换
resize_size = (48, 48)
validation_transform = transforms.Compose(transforms=[transforms.ToTensor(),  # 归一化并定义为 torch.float32
                                           transforms.Grayscale(num_output_channels=1),
                                           transforms.Resize(size=resize_size),  # 降采样
                                           transforms.Normalize(mean=[0.5], std=[0.5])])  # 归一化
                                           # transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5])])  # 归一化

# 加载数据集
# path = os.path.join('data', 'Validation')
path = os.path.join('data', 'TOF_data', 'resize_ratio_1.2')
validation_dataset = datasets.ImageFolder(root=os.path.join(path), transform=validation_transform)
validation_dataloader = DataLoader(dataset=validation_dataset, batch_size=45, shuffle=False)

# 加载训练模型
# train_path = "package/model/pt_files/pt_24_24_grayscale/state_dict_model_face3.pt"
train_path = "package/model/pt_files/pt_48_48_grayscale_tof/state_dict_model_face1a.pt"
# train_path = "package/model/pt_files/pt_24_24_grayscale_tof/state_dict_model_face3.pt"
model = face().to(device=device)
model.load_state_dict(torch.load(train_path))
model.eval()

print(validation_dataset.class_to_idx)

# 验证测试集
def validation(dataloader=validation_dataloader, model=model):

    accs = []
    model.eval()

    with torch.no_grad():

        for img, label in dataloader:

            img = img.to(device=device)
            label = label.to(device=device)

            y_pred = model(img)

            acc = torch.mean((y_pred > 0.5).flatten() == (label == 1).flatten(), dtype=torch.float32)

            accs.append(acc)

        accs = np.array(accs, dtype=np.float32).mean()
        print(f'Accuracy = {np.mean(accs)}')

# Before training accuracy (Expect accuracy to be 50%, as random guess of 2 classification problem)
validation()