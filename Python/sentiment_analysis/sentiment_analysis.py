import numpy as np
import jieba
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch
from torch import nn
from torch.nn import functional as F
import matplotlib.pyplot as plt

def load_data(folder, train_file, test_file):

    """
        读取原始数据
    """

    # 训练集文本和标签
    X_train = []
    y_train = []

    # 构建词典
    # UNK：未知词
    # PAD：填充词
    words_set = {"<UNK>", "<PAD>"}  # 添加 padding 补丁, unknown 不知道的数

    # 训练集读取
    with open(file=train_file, mode="r", encoding="utf8") as f:
        for line in f.readlines():
            file_name, label = line.strip().split(",")
            y_train.append(int(label))
            with open(file=folder + file_name, mode="r", encoding="gbk", errors="ignore") as f1:
                txt = f1.read().replace(" ", "").replace("\n", "").replace("\t", "")
                txt_cut = jieba.lcut(txt)
                words_set = words_set.union(set(txt_cut))
                X_train.append(txt_cut)

    # 测试集文本和标签
    X_test = []
    y_test = []

    # 测试集读取
    with open(file=test_file, mode="r", encoding="utf8") as f:
        for line in f.readlines():
            file_name, label = line.strip().split(",")
            y_test.append(int(label))
            with open(file=folder + file_name, mode="r", encoding="gbk", errors="ignore") as f1:
                txt = f1.read().replace(" ", "").replace("\n", "").replace("\t", "")
                txt_cut = jieba.lcut(txt)
                X_test.append(txt_cut)

    return X_train, y_train, X_test, y_test, words_set

def fix_sequence_length(X_train, X_test, seq_len):

    """
        截取固定长度 86个词
    """
    # 训练集
    X_train1 = []
    for x in X_train:
        temp = x + ["<PAD>"] * seq_len
        X_train1.append(temp[:seq_len])

    # 测试集
    X_test1 = []
    for x in X_test:
        temp = x + ["<PAD>"] * seq_len
        X_test1.append(temp[:seq_len])

    return X_train1, X_test1

def transform_word_to_idx(X_train1, X_test1):

    """
        向量化
    """
    # 训练集向量化

    X_train2 = []
    for x in X_train1:
        temp = []
        for word in x:
            idx = word2idx[word] if word in word2idx else word2idx["<UNK>"]
            temp.append(idx)
        X_train2.append(temp)

    # 测试集向量化

    X_test2 = []
    for x in X_test1:
        temp = []
        for word in x:
            idx = word2idx[word] if word in word2idx else word2idx["<UNK>"]
            temp.append(idx)
        X_test2.append(temp)

    return X_train2, X_test2

class MyDataSet(Dataset):

    """
        定义数据加载器
    """

    def __init__(self, X, y):
        self.X = X
        self.y = y

    def __len__(self):
        return len(self.X)

    def __getitem__(self, idx):
        x = self.X[idx]
        y = self.y[idx]

        return torch.tensor(data=x).long(), torch.tensor(data=y).long()

device = "cuda:0" if torch.cuda.is_available() else "cpu"

folder = './data/hotel data/'
train_file = folder + "./train.csv"
test_file = folder + "./test.csv"

# 读取数据集
X_train, y_train, X_test, y_test, words_set = load_data(folder, train_file, test_file)

word2idx = {word: idx for idx, word in enumerate(words_set)}
idx2word = {idx: word for word, idx in word2idx.items()}

# 字典长度
dict_len = len(words_set)

# 序列长度（平均句子长度）(要取多少，自己定) (如果句子里的词取太多，则计算量太大)
# 为什么要截取？不提取也可以。没有可以不可以。
# 就选择所有句子的平均长度
seq_len = 86

# 把一个句子的词汇数量固定成一定的长度 (少于 seq_len, 填补 "<PAD>")
X_train1, X_test1 = fix_sequence_length(X_train, X_test, seq_len)

# 向量化 (把词汇转换成 idx)
X_train2, X_test2 = transform_word_to_idx(X_train1, X_test1)

# 定义数据加载器
train_dataset = MyDataSet(X=X_train2, y=y_train)
train_dataloader = DataLoader(dataset=train_dataset, shuffle=True, batch_size=128)

test_dataset = MyDataSet(X=X_test2, y=y_test)
test_dataloader = DataLoader(dataset=test_dataset, shuffle=False, batch_size=256)

# 定义模型
# from model import TextCNN2D as model
# from model import TextCNN1D as model
# from model import TextRNN1 as model
# from model import TextRNN2 as model
# from model import TextRNN3 as model
from model import TextGRU as model
model = model(num_embeddings=dict_len, embedding_dim=256, padding_idx=word2idx["<PAD>"], device=device, num_layers=1)

# from model import TextCNN1D_YeZhang_myself as model
# model = model(num_embeddings=dict_len, embedding_dim=256, padding_idx=word2idx["<PAD>"], num_of_filters=10)

model.to(device=device)

# 定义优化器
optimizer = torch.optim.SGD(params=model.parameters(), lr=1e-2)

# 定义损失函数
loss_fn = nn.CrossEntropyLoss()

# 定义训练轮次
epochs = 20

# 定义过程监控函数

def get_acc(dataloader=train_dataloader, model=model):
    accs = []
    model.eval()
    with torch.no_grad():
        for X, y in dataloader:
            X = X.to(device=device)
            y = y.to(device=device)
            y_pred = model(X)
            y_pred = y_pred.argmax(dim=1)
            acc = (y_pred == y).float().mean().item()
            accs.append(acc)
    return np.array(accs).mean()


# 定义训练过程
def train(model=model,
          optimizer=optimizer,
          loss_fn=loss_fn,
          epochs=epochs,
          train_dataloader=train_dataloader,
          test_dataloader=test_dataloader):

    for epoch in range(1, epochs + 1):

        print(f"正在进行第 {epoch} 轮训练:")

        model.train()

        for X, y in train_dataloader:

            X = X.to(device=device)
            y = y.to(device=device)

            # 正向传播
            y_pred = model(X)

            # 清空梯度
            optimizer.zero_grad()

            # 计算损失
            loss = loss_fn(y_pred, y)

            # 梯度下降
            loss.backward()

            # 优化一步
            optimizer.step()

        print(f"train_acc: {get_acc(dataloader=train_dataloader)}, test_acc: {get_acc(dataloader=test_dataloader)}")

train(epochs=50)

plt.pause(0.1)
