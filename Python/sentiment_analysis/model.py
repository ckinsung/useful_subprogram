import torch
from torch import nn
from torch.nn import functional as F

class TextCNN2D(nn.Module):
    """
        使用二维卷积来处理文本分类
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, device="cuda:0"):

        super(TextCNN2D, self).__init__()

        # 嵌入层
        self.embed = nn.Embedding(num_embeddings=num_embeddings,
                                  embedding_dim=embedding_dim,
                                  padding_idx=padding_idx)

        # 第一组
        self.conv2d1 = nn.Conv2d(in_channels=256,
                                 out_channels=512,
                                 kernel_size=(3, 1),  # [B, 256, 86, 1], 需要 [3, 1] 的卷积核卷积
                                 stride=(1, 1),
                                 padding=(1, 0)
                                 )
        self.bn1 = nn.BatchNorm2d(num_features=512)
        self.maxpool1 = nn.MaxPool2d(kernel_size=(2, 1), stride=(2, 1), padding=(0, 0))

        # 第二组
        self.conv2d2 = nn.Conv2d(in_channels=512,
                                 out_channels=512,
                                 kernel_size=(3, 1),
                                 stride=(1, 1),
                                 padding=(1, 0)
                                 )
        self.bn2 = nn.BatchNorm2d(num_features=512)

        self.maxpool2 = nn.MaxPool2d(kernel_size=(2, 1), stride=(2, 1), padding=(0, 0))

        # 第三组
        self.conv2d3 = nn.Conv2d(in_channels=512,
                                 out_channels=1024,
                                 kernel_size=(3, 1),
                                 stride=(1, 1),
                                 padding=(1, 0)
                                 )
        self.bn3 = nn.BatchNorm2d(num_features=1024)
        self.maxpool3 = nn.MaxPool2d(kernel_size=(2, 1), stride=(2, 1), padding=(0, 0))

        # 全连接层
        self.fc1 = nn.Linear(in_features=10240, out_features=512)
        self.dropout = nn.Dropout(p=0.2)
        self.fc2 = nn.Linear(in_features=512, out_features=2)

    def forward(self, x):

        # B个句子/样本, 86个词汇转换成 B个句子 86个词汇 256个维度
        x = self.embed(x)  # [B, 86] --> [B, 86, 256]

        # 我们是 [N, C, H, W] 256 是 C 应该在中间
        x = x.permute(0, 2, 1)  # [B, 86, 256] --> [B, 256, 86]

        # [B, 256, 86] --> [B, 256, 86, 1]  # 转换成卷积的概念 [N, C, H, W]
        x = torch.unsqueeze(input=x, dim=-1)

        # 第一组卷积
        x = self.conv2d1(x)
        x = self.bn1(x)
        x = F.relu(x)

        # 第一个池化
        x = self.maxpool1(x)

        # 第二组卷积
        x = self.conv2d2(x)
        x = self.bn2(x)
        x = F.relu(x)

        # 第二个池化
        x = self.maxpool2(x)

        # 第三组卷积
        x = self.conv2d3(x)
        x = self.bn3(x)
        x = F.relu(x)

        # 第二个池化
        x = self.maxpool3(x)

        # 展平层
        x = x.view(x.size(0), -1)

        x = self.fc1(x)
        x = self.dropout(x)
        x = self.fc2(x)

        return x

    """
    train_acc: 0.6558388157894737, test_acc: 0.6232954502105713
    正在进行第 2 轮训练:
    train_acc: 0.5600328947368421, test_acc: 0.5438920497894287
    正在进行第 3 轮训练:
    train_acc: 0.6217105263157895, test_acc: 0.5909090876579285
    正在进行第 4 轮训练:
    train_acc: 0.7286184210526315, test_acc: 0.6617897748947144
    正在进行第 5 轮训练:
    train_acc: 0.8601973684210527, test_acc: 0.7544034123420715
    正在进行第 6 轮训练:
    train_acc: 0.9444901315789473, test_acc: 0.8391335248947144
    正在进行第 7 轮训练:
    train_acc: 0.8865131578947368, test_acc: 0.7660511374473572
    正在进行第 8 轮训练:
    train_acc: 0.5256990131578947, test_acc: 0.5156960248947143
    正在进行第 9 轮训练:
    train_acc: 0.9944490131578947, test_acc: 0.86875
    正在进行第 10 轮训练:
    train_acc: 0.997327302631579, test_acc: 0.87109375
    正在进行第 11 轮训练:
    train_acc: 0.998766447368421, test_acc: 0.8433948874473571
    正在进行第 12 轮训练:
    train_acc: 0.9991776315789473, test_acc: 0.8734375
    正在进行第 13 轮训练:
    train_acc: 0.9993832236842105, test_acc: 0.8702414751052856
    正在进行第 14 轮训练:
    train_acc: 0.9995888157894737, test_acc: 0.8678977251052856
    正在进行第 15 轮训练:
    import sys; print('Python %s on %s' % (sys.version, sys.platform))
    train_acc: 0.9995888157894737, test_acc: 0.8551136374473571
    正在进行第 16 轮训练:
    """

class TextCNN1D_YeZhang_myself(nn.Module):

    """
        20220718 day23 上课后老师叫我们自己写代码完成
        https://arxiv.org/pdf/1510.03820.pdf 文献里的模型

        使用一维卷积来实现 TextCNN 模型
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, num_of_filters=2):

        super(TextCNN1D_YeZhang_myself, self).__init__()

        # 嵌入层
        self.embed = nn.Embedding(num_embeddings=num_embeddings,
                                  embedding_dim=embedding_dim,
                                  padding_idx=padding_idx)

        # Region Size 4 (2 filters) (kernel size = 4*embedding_dim) (文献 kernel size = 4*5) (文献: out_channels=2)
        self.conv1d_size4 = nn.Conv1d(in_channels=embedding_dim, out_channels=num_of_filters, kernel_size=4, stride=1, padding=0)
        self.bn1_size4 = nn.BatchNorm1d(num_features=num_of_filters)

        # Region Size 3 (2 filters) (kernel size = 3*embedding_dim) (文献 kernel size = 3*5) (文献: out_channels=2)
        self.conv1d_size3 = nn.Conv1d(in_channels=embedding_dim, out_channels=num_of_filters, kernel_size=3, stride=1, padding=0)
        self.bn1_size3 = nn.BatchNorm1d(num_features=num_of_filters)

        # Region Size 2 (2 filters) (kernel size = 2*embedding_dim) (文献 kernel size = 2*5) (文献: out_channels=2)
        self.conv1d_size2 = nn.Conv1d(in_channels=embedding_dim, out_channels=num_of_filters, kernel_size=2, stride=1, padding=0)
        self.bn1_size2 = nn.BatchNorm1d(num_features=num_of_filters)

        # 全连接层 (in_features = conv1d_size4 的 out_channels + conv1d_size3 的 out_channels + conv1d_size2 的 out_channels
        # 文献的 in_features = 6 (6个 filters)
        in_features = self.conv1d_size4.out_channels + self.conv1d_size3.out_channels + self.conv1d_size2.out_channels
        self.linear = nn.Linear(in_features=in_features, out_features=2)

    def forward(self, x):

        # 86个词汇转换成 86个词汇 embedding_dim 个维度 (embedding_dim 默认 256)
        x = self.embed(x)  # [B, 86] --> [B, 86, embedding_dim]

        # 我们是 [N, C, H, W] 256 是 C 应该在中间
        x = x.permute(0, 2, 1)  # [B, embedding_dim, 5] --> [B, 5, embedding_dim]

        # Region Size 4 组
        x4 = self.conv1d_size4(x)
        x4 = self.bn1_size4(x4)
        x4 = F.relu(x4)
        x4, _ = torch.max(input=x4, dim=2)

        # Region Size 3 组
        x3 = self.conv1d_size3(x)
        x3 = self.bn1_size3(x3)
        x3 = F.relu(x3, inplace=True)
        x3, _ = torch.max(input=x3, dim=2)

        # Region Size 2 组
        x2 = self.conv1d_size2(x)
        x2 = self.bn1_size3(x2)
        x2 = F.relu(x2, inplace=True)
        x2, _ = torch.max(input=x2, dim=2)

        # Concatenate
        x_vec = torch.cat((x4, x3, x2), dim=1)

        # 全连接
        x = self.linear(x_vec)

        # 其实不需要做 softmax
        # x = torch.softmax(input=x, dim=1)

        return x

    # 直接使用文献的模型框架 (sentence matrix: 7*5; 2 filters for each region size)
    """
    正在进行第 1 轮训练:
    train_acc: 0.5532483552631579, test_acc: 0.5453835248947143
    正在进行第 2 轮训练:
    train_acc: 0.7232730263157895, test_acc: 0.6934659123420716
    正在进行第 3 轮训练:
    train_acc: 0.7559621710526315, test_acc: 0.73671875
    正在进行第 4 轮训练:
    train_acc: 0.7660361842105263, test_acc: 0.7447443127632141
    正在进行第 5 轮训练:
    train_acc: 0.770764802631579, test_acc: 0.7466619372367859
    正在进行第 6 轮训练:
    train_acc: 0.7802220394736842, test_acc: 0.7497869372367859
    正在进行第 7 轮训练:
    train_acc: 0.7888569078947368, test_acc: 0.7539772748947143
    正在进行第 8 轮训练:
    train_acc: 0.7944078947368421, test_acc: 0.7629261374473572
    正在进行第 9 轮训练:
    train_acc: 0.8036595394736842, test_acc: 0.7730113625526428
    正在进行第 10 轮训练:
    train_acc: 0.8104440789473685, test_acc: 0.77109375
    正在进行第 11 轮训练:
    train_acc: 0.815172697368421, test_acc: 0.77109375
    正在进行第 12 轮训练:
    train_acc: 0.8199013157894737, test_acc: 0.7757102251052856
    正在进行第 13 轮训练:
    train_acc: 0.8238075657894737, test_acc: 0.7776278376579284
    正在进行第 14 轮训练:
    train_acc: 0.8223684210526315, test_acc: 0.7800426125526428
    正在进行第 15 轮训练:
    train_acc: 0.8299753289473685, test_acc: 0.7784090876579285
    正在进行第 16 轮训练:
    train_acc: 0.8328536184210527, test_acc: 0.7830965876579284
    正在进行第 17 轮训练:
    train_acc: 0.8361430921052632, test_acc: 0.7796164751052856
    正在进行第 18 轮训练:
    train_acc: 0.8365542763157895, test_acc: 0.7808238625526428
    正在进行第 19 轮训练:
    train_acc: 0.8404605263157895, test_acc: 0.7835227251052856
    正在进行第 20 轮训练:
    train_acc: 0.8433388157894737, test_acc: 0.7882102251052856
    正在进行第 21 轮训练:
    train_acc: 0.8443667763157895, test_acc: 0.7855113625526429
    正在进行第 22 轮训练:
    train_acc: 0.8513569078947368, test_acc: 0.7886363625526428
    正在进行第 23 轮训练:
    train_acc: 0.8536184210526315, test_acc: 0.7901988625526428
    正在进行第 24 轮训练:
    train_acc: 0.8591694078947368, test_acc: 0.7909801125526428
    正在进行第 25 轮训练:
    train_acc: 0.8608141447368421, test_acc: 0.7890625
    正在进行第 26 轮训练:
    train_acc: 0.8597861842105263, test_acc: 0.79375
    正在进行第 27 轮训练:
    train_acc: 0.864514802631579, test_acc: 0.79140625
    正在进行第 28 轮训练:
    train_acc: 0.8655427631578947, test_acc: 0.790625
    正在进行第 29 轮训练:
    train_acc: 0.8682154605263158, test_acc: 0.7909801125526428
    正在进行第 30 轮训练:
    train_acc: 0.8696546052631579, test_acc: 0.7933238625526429
    正在进行第 31 轮训练:
    train_acc: 0.8719161184210527, test_acc: 0.7933238625526429
    正在进行第 32 轮训练:
    train_acc: 0.8745888157894737, test_acc: 0.7964488625526428
    正在进行第 33 轮训练:
    train_acc: 0.8770559210526315, test_acc: 0.7949573874473572
    正在进行第 34 轮训练:
    train_acc: 0.8787006578947368, test_acc: 0.79765625
    正在进行第 35 轮训练:
    train_acc: 0.8828125, test_acc: 0.8019176125526428
    正在进行第 36 轮训练:
    train_acc: 0.8828125, test_acc: 0.8011363625526429
    正在进行第 37 轮训练:
    train_acc: 0.8856907894736842, test_acc: 0.8007102251052857
    正在进行第 38 轮训练:
    train_acc: 0.885485197368421, test_acc: 0.8026988625526428
    正在进行第 39 轮训练:
    train_acc: 0.8875411184210527, test_acc: 0.80390625
    正在进行第 40 轮训练:
    train_acc: 0.890625, test_acc: 0.8030539751052856
    正在进行第 41 轮训练:
    train_acc: 0.8924753289473685, test_acc: 0.8026278376579284
    正在进行第 42 轮训练:
    train_acc: 0.895764802631579, test_acc: 0.8053977251052856
    正在进行第 43 轮训练:
    train_acc: 0.8943256578947368, test_acc: 0.8073153376579285
    正在进行第 44 轮训练:
    train_acc: 0.8992598684210527, test_acc: 0.8120028376579285
    正在进行第 45 轮训练:
    train_acc: 0.9000822368421053, test_acc: 0.8123579502105713
    正在进行第 46 轮训练:
    train_acc: 0.9000822368421053, test_acc: 0.8115767002105713
    正在进行第 47 轮训练:
    train_acc: 0.9031661184210527, test_acc: 0.8139204502105712
    正在进行第 48 轮训练:
    train_acc: 0.9043996710526315, test_acc: 0.8142755627632141
    正在进行第 49 轮训练:
    train_acc: 0.9027549342105263, test_acc: 0.8107954502105713
    正在进行第 50 轮训练:
    train_acc: 0.9056332236842105, test_acc: 0.8170454502105713
    Backend TkAgg is interactive backend. Turning interactive mode on.
    """

    # 不使用文献的模型框架, 修改成 (sentence matrix: 7*5; 2 filters for each region size)
    """
    正在进行第 1 轮训练:
    train_acc: 0.674547697368421, test_acc: 0.6382102251052857
    正在进行第 2 轮训练:
    train_acc: 0.7516447368421053, test_acc: 0.7214488625526428
    正在进行第 3 轮训练:
    train_acc: 0.7818667763157895, test_acc: 0.7463778376579284
    正在进行第 4 轮训练:
    train_acc: 0.7826891447368421, test_acc: 0.7520596623420716
    正在进行第 5 轮训练:
    train_acc: 0.8013980263157895, test_acc: 0.7629261374473572
    正在进行第 6 轮训练:
    train_acc: 0.8170230263157895, test_acc: 0.7840198874473572
    正在进行第 7 轮训练:
    train_acc: 0.8250411184210527, test_acc: 0.7800426125526428
    正在进行第 8 轮训练:
    train_acc: 0.8326480263157895, test_acc: 0.7864346623420715
    正在进行第 9 轮训练:
    train_acc: 0.848889802631579, test_acc: 0.7911931872367859
    正在进行第 10 轮训练:
    train_acc: 0.8544407894736842, test_acc: 0.7950284123420716
    正在进行第 11 轮训练:
    train_acc: 0.862047697368421, test_acc: 0.7996448874473572
    正在进行第 12 轮训练:
    train_acc: 0.876233552631579, test_acc: 0.8081676125526428
    正在进行第 13 轮训练:
    train_acc: 0.8844572368421053, test_acc: 0.8093039751052856
    正在进行第 14 轮训练:
    train_acc: 0.8858963815789473, test_acc: 0.8085227251052857
    正在进行第 15 轮训练:
    train_acc: 0.8978207236842105, test_acc: 0.8096590876579285
    正在进行第 16 轮训练:
    train_acc: 0.9056332236842105, test_acc: 0.8178267002105712
    正在进行第 17 轮训练:
    train_acc: 0.9122121710526315, test_acc: 0.8232244372367858
    正在进行第 18 轮训练:
    train_acc: 0.9138569078947368, test_acc: 0.8124289751052857
    正在进行第 19 轮训练:
    train_acc: 0.9229029605263158, test_acc: 0.8220880627632141
    正在进行第 20 轮训练:
    train_acc: 0.9194078947368421, test_acc: 0.8198863625526428
    正在进行第 21 轮训练:
    train_acc: 0.9325657894736842, test_acc: 0.8272017002105713
    正在进行第 22 轮训练:
    train_acc: 0.9381167763157895, test_acc: 0.8295454502105712
    正在进行第 23 轮训练:
    train_acc: 0.9449013157894737, test_acc: 0.8361505627632141
    正在进行第 24 轮训练:
    train_acc: 0.947985197368421, test_acc: 0.8392755627632141
    正在进行第 25 轮训练:
    train_acc: 0.953125, test_acc: 0.8397017002105713
    正在进行第 26 轮训练:
    train_acc: 0.9555921052631579, test_acc: 0.8415482997894287
    正在进行第 27 轮训练:
    train_acc: 0.9617598684210527, test_acc: 0.8424005627632141
    正在进行第 28 轮训练:
    train_acc: 0.9617598684210527, test_acc: 0.8389204502105713
    正在进行第 29 轮训练:
    train_acc: 0.9662828947368421, test_acc: 0.8408380627632142
    正在进行第 30 轮训练:
    train_acc: 0.96875, test_acc: 0.8451704502105712
    正在进行第 31 轮训练:
    train_acc: 0.9720394736842105, test_acc: 0.8450994372367859
    正在进行第 32 轮训练:
    train_acc: 0.9732730263157895, test_acc: 0.8428267002105713
    正在进行第 33 轮训练:
    train_acc: 0.9763569078947368, test_acc: 0.8466619372367858
    正在进行第 34 轮训练:
    train_acc: 0.9798519736842105, test_acc: 0.8459517002105713
    正在进行第 35 轮训练:
    train_acc: 0.983141447368421, test_acc: 0.8475142002105713
    正在进行第 36 轮训练:
    train_acc: 0.983141447368421, test_acc: 0.8525568127632142
    正在进行第 37 轮训练:
    train_acc: 0.985608552631579, test_acc: 0.8514204502105713
    正在进行第 38 轮训练:
    train_acc: 0.9874588815789473, test_acc: 0.8522017002105713
    正在进行第 39 轮训练:
    train_acc: 0.98828125, test_acc: 0.8564630627632142
    正在进行第 40 轮训练:
    train_acc: 0.9899259868421053, test_acc: 0.8556818127632141
    正在进行第 41 轮训练:
    train_acc: 0.9915707236842105, test_acc: 0.8533380627632141
    正在进行第 42 轮训练:
    train_acc: 0.993421052631579, test_acc: 0.8549005627632141
    正在进行第 43 轮训练:
    train_acc: 0.9942434210526315, test_acc: 0.8541193127632141
    正在进行第 44 轮训练:
    train_acc: 0.9942434210526315, test_acc: 0.8561079502105713
    正在进行第 45 轮训练:
    train_acc: 0.9954769736842105, test_acc: 0.8583806872367858
    正在进行第 46 轮训练:
    train_acc: 0.9958881578947368, test_acc: 0.8607244372367859
    正在进行第 47 轮训练:
    train_acc: 0.9958881578947368, test_acc: 0.8576704502105713
    正在进行第 48 轮训练:
    train_acc: 0.9967105263157895, test_acc: 0.8595880627632141
    正在进行第 49 轮训练:
    train_acc: 0.9967105263157895, test_acc: 0.8603693127632142
    正在进行第 50 轮训练:
    train_acc: 0.9969161184210527, test_acc: 0.8600142002105713
    Backend TkAgg is interactive backend. Turning interactive mode on.
    """

class TextCNN1D(nn.Module):

    """
        采用一维卷积来实现TextCNN模型
        https://arxiv.org/pdf/1510.03820.pdf 文献里的模型

        这个函数和我自己写的是类似的 TextCNN1D_YeZhang_myself
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, device="cuda:0"):

        super(TextCNN1D, self).__init__()

        # [b, seq_len] --> [b, seq_len, embedding_dim]
        self.embed = nn.Embedding(num_embeddings=num_embeddings,
                                  embedding_dim=embedding_dim,
                                  padding_idx=padding_idx)

        # 2-gram kernel_size=2
        self.conv1d_ks2 = nn.Conv1d(in_channels=embedding_dim, out_channels=256, kernel_size=2)
        self.bn2 = nn.BatchNorm1d(num_features=256)
        self.maxpool2 = nn.MaxPool1d(kernel_size=85)

        # 3-gram kernel_size=3
        self.conv1d_ks3 = nn.Conv1d(in_channels=embedding_dim, out_channels=256, kernel_size=3)
        self.bn3 = nn.BatchNorm1d(num_features=256)
        self.maxpool3 = nn.MaxPool1d(kernel_size=84)
        # 4-gram kernel_size=4
        self.conv1d_ks4 = nn.Conv1d(in_channels=embedding_dim, out_channels=256, kernel_size=4)
        self.bn4 = nn.BatchNorm1d(num_features=256)
        self.maxpool4 = nn.MaxPool1d(kernel_size=83)

        # dropout 层
        self.dropout = nn.Dropout(p=0.2)

        # 分类输出
        self.fc = nn.Linear(in_features=768, out_features=2)

    def forward(self, x):

        x = self.embed(x)
        x = torch.permute(input=x, dims=(0, 2, 1))

        x1 = self.conv1d_ks2(x)
        #         x1 = self.bn2(x1)
        x1 = F.relu(x1)
        x1 = self.maxpool2(x1)

        x2 = self.conv1d_ks3(x)
        #         x2 = self.bn3(x2)
        x2 = F.relu(x2)
        x2 = self.maxpool3(x2)

        x3 = self.conv1d_ks4(x)
        #         x3 = self.bn4(x3)
        x3 = F.relu(x3)
        x3 = self.maxpool4(x3)

        # 拼接起来
        x = torch.cat(tensors=(x1, x2, x3), dim=1)

        x = x.view(x.size(0), -1)

        self.dropout(x)

        x = self.fc(x)

        return x

class TextRNN1(nn.Module):

    """
        基于基础 RNN 结构来进行情感分析 (sentiment analysis)
        only use the hn output of the rnn
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, device="cuda:0", num_layers=1):

        super(TextRNN1, self).__init__()

        self.device = device

        self.num_layers = num_layers

        self.embed = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim, padding_idx=padding_idx)

        self.rnn = nn.RNN(input_size=embedding_dim, hidden_size=512, num_layers=num_layers)

        self.fc = nn.Linear(in_features=512, out_features=2)

    def forward(self, x):

        # [b, 86] -> [b, 86, 256]
        x = self.embed(x)

        # [b, 86, 256] -> [86, b, 256] ([L, B, C])
        x = torch.permute(input=x, dims=(1, 0, 2))

        # 第一个词是没有隐藏层的, 定义为 0 ([1, B, C])
        h0 = torch.zeros(size=(self.num_layers, x.shape[1], 512)).to(device=self.device)

        # input: x = [L, B, 256] or (L, N, H_{in})
        # h0 = [1, B, 512] or (D * \text{num\_layers}, N, H_{out})
        _, hn = self.rnn(x, h0)

        # [1, B, 512] -> [B, 512]
        hn = torch.squeeze(input=hn, dim=0)

        # [B, 512] -> [B, 2]
        out = self.fc(hn)

        return out

class TextRNN2(nn.Module):

    """
        基于基础 RNN 结构来进行情感分析 (sentiment analysis)
        sum over the output of RNN
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, device="cuda:0", num_layers=1):

        super(TextRNN2, self).__init__()

        self.device = device

        self.num_layers = num_layers

        self.embed = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim, padding_idx=padding_idx)

        self.rnn = nn.RNN(input_size=embedding_dim, hidden_size=512, num_layers=num_layers)

        self.fc = nn.Linear(in_features=512, out_features=2)

    def forward(self, x):

        # [b, 86] -> [b, 86, 256]
        x = self.embed(x)

        # [b, 86, 256] -> [86, b, 256] ([L, B, C])
        x = torch.permute(input=x, dims=(1, 0, 2))

        # 第一个词是没有隐藏层的, 定义为 0 ([1, B, C])
        h0 = torch.zeros(size=(self.num_layers, x.shape[1], 512)).to(device=self.device)

        # input: x = [L, B, 256] or (L, N, H_{in})
        # h0 = [1, B, 512] or (D * \text{num\_layers}, N, H_{out})
        out, hn = self.rnn(x, h0)
        # out: [86, b, 512]

        # Choosing to use hn or out from the RNN model
        # Choose out allows one to make use of all the previous output
        # Choose hn only use the last output
        choice = 2

        if choice == 1:

            # 把所有维度的信息加起来，不要像 RNN1 丢弃所有前过程的结果
            out = torch.sum(input=out, dim=0)

        elif choice == 2:

            # 所有 layers 的结果的叠加
            out = torch.sum(input=hn, dim=0)

        # [1, B, 512] -> [B, 512]
        out = torch.squeeze(input=out, dim=0)

        # [B, 512] -> [B, 2]
        out = self.fc(out)

        return out

class TextRNN3(nn.Module):

    """
        基于基础 LSTM 结构来进行情感分析 (sentiment analysis)
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, device="cuda:0", num_layers=1):

        super(TextRNN3, self).__init__()

        self.device = device

        self.num_layers = num_layers

        self.embed = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim, padding_idx=padding_idx)

        self.lstm = nn.LSTM(input_size=embedding_dim, hidden_size=512)

        self.fc = nn.Linear(in_features=512, out_features=2)

    def forward(self, x):

        # [b, 86] -> [b, 86, 256]
        x = self.embed(x)

        # [b, 86, 256] -> [86, b, 256] ([L, B, C])
        x = torch.permute(input=x, dims=(1, 0, 2))

        # 第一个词是没有隐藏层的, 定义为 0 ([1, B, C])
        # model.to(device) 并没有把 forward 里的定义导入 device 里, 所以需要手动导入 device
        h0 = torch.zeros(size=(self.num_layers, x.shape[1], 512)).to(device=self.device)
        c0 = torch.zeros(size=(self.num_layers, x.shape[1], 512)).to(device=self.device)

        # input: x = [L, B, 256] or (L, N, H_{in})
        # h0 = [1, B, 512] or (D * \text{num\_layers}, N, H_{out})
        out, (hn, cn) = self.lstm(x, (h0, c0))
        # out: [86, b, 512]

        # Choosing to use hn or out from the LSTM model
        # Choose out allows one to make use of all the previous output
        # Choose hn only use the last output
        choice = 2

        if choice == 1:

            # 把所有维度的信息加起来，不要像 RNN1 丢弃所有前过程的结果
            out = torch.sum(input=out, dim=0)

        elif choice == 2:

            # 所有 layers 的结果的叠加
            out = torch.sum(input=hn, dim=0)

        elif choice == 3:

            # 所有 layers 的结果的叠加
            out = torch.sum(input=cn, dim=0)

        # 把所有维度的信息加起来，不要像 RNN1 丢弃所有前过程的结果
        out = torch.sum(input=out, dim=0)

        # [1, B, 512] -> [B, 512]
        out = torch.squeeze(input=out, dim=0)

        # [B, 512] -> [B, 2]
        out = self.fc(out)

        return out

class TextGRU(nn.Module):

    """
        基于基础 GRU 结构来进行情感分析 (sentiment analysis)
        only use the hn output of the rnn
    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx, device="cuda:0", num_layers=1):

        super(TextGRU, self).__init__()

        self.device = device

        self.num_layers = num_layers

        self.embed = nn.Embedding(num_embeddings=num_embeddings, embedding_dim=embedding_dim, padding_idx=padding_idx)

        self.gru = nn.GRU(input_size=embedding_dim, hidden_size=512, num_layers=num_layers, bidirectional=True)

        self.fc1 = nn.Linear(in_features=512, out_features=128)

        self.fc2 = nn.Linear(in_features=128, out_features=2)

    def forward(self, x):

        # [b, 86] -> [b, 86, 256]
        x = self.embed(x)

        # [b, 86, 256] -> [86, b, 256] ([L, B, C])
        x = torch.permute(input=x, dims=(1, 0, 2))

        # 第一个词是没有隐藏层的, 定义为 0 ([1, B, C])
        h0 = torch.zeros(size=(2, x.shape[1], 512)).to(device=self.device)

        # input: x = [L, B, 256] or (L, N, H_{in})
        # h0 = [1, B, 512] or (D * \text{num\_layers}, N, H_{out})
        _, hn = self.gru(x, h0)

        hn = torch.mean(input=hn, dim=0)

        # [1, B, 512] -> [B, 512]
        hn = torch.squeeze(input=hn, dim=0)

        # [B, 512] -> [B, 2]
        out = self.fc1(hn)
        out = self.fc2(out)

        return out
