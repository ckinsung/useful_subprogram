import numpy as np
import random

import torch
from torchvision import transforms as T
from torchvision.transforms import functional as F


def pad_if_smaller(img, size, fill=0):

    # 如果图像最小边长小于给定size，则用数值fill进行padding
    min_size = min(img.size)

    if min_size < size:  # 如果最小的尺寸小于指定的尺寸

        # 原图的宽和高
        ow, oh = img.size

        # 计算需要补的
        padh = size - oh if oh < size else 0  # 添加行数
        padw = size - ow if ow < size else 0  # 添加列数

        # 图像补 fill 值，补 padw (最后几列) 和 padh (最后几行) 的数量
        img = F.pad(img, (0, 0, padw, padh), fill=fill)

    return img


class Compose(object):

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, target):

        for t in self.transforms:
            image, target = t(image, target)

        return image, target


class RandomResize(object):

    def __init__(self, min_size, max_size=None):

        self.min_size = min_size

        if max_size is None:
            max_size = min_size

        self.max_size = max_size

    def __call__(self, image, target):

        size = random.randint(self.min_size, self.max_size)

        # 这里size传入的是int类型，所以是将图像的最小边长缩放到size大小, 另一个尺寸则缩放到同样的比例 (aspect ratio)
        # If size is an int, the smaller edge of the image will be matched to this number maintaining the aspect ratio.
        image = F.resize(image, size)

        # 这里的interpolation注意下，在torchvision(0.9.0)以后才有InterpolationMode.NEAREST
        # 如果是之前的版本需要使用PIL.Image.NEAREST
        target = F.resize(target, size, interpolation=T.InterpolationMode.NEAREST)

        return image, target


class RandomHorizontalFlip(object):
    def __init__(self, flip_prob):
        self.flip_prob = flip_prob

    def __call__(self, image, target):
        if random.random() < self.flip_prob:
            image = F.hflip(image)
            target = F.hflip(target)
        return image, target


class RandomCrop(object):

    def __init__(self, size):
        self.size = size

    def __call__(self, image, target):
        image = pad_if_smaller(image, self.size)
        target = pad_if_smaller(target, self.size, fill=255)
        crop_params = T.RandomCrop.get_params(image, (self.size, self.size))
        image = F.crop(image, *crop_params)
        target = F.crop(target, *crop_params)
        return image, target


class CenterCrop(object):

    def __init__(self, size):
        self.size = size

    def __call__(self, image, target):
        image = F.center_crop(image, self.size)
        target = F.center_crop(target, self.size)
        return image, target


class ToTensor(object):

    def __call__(self, image, target):
        image = F.to_tensor(image)
        target = torch.as_tensor(np.array(target), dtype=torch.int64)
        return image, target


class Normalize(object):

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, image, target):
        image = F.normalize(image, mean=self.mean, std=self.std)
        return image, target
