import cv2
import torch
import numpy as np
import os

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"


def get_anchors(feature_size, small_anchor_size, big_anchor_size):
    """
        锚框生成？？？
            - 大小
            - 位置

        - 每个特征点都生成一套框
        - 一套框：大，小，高，宽
        - 以特征点为中心来生成四个框
    """
    # 取均分分布的相对坐标
    steps = (np.arange(feature_size) + 0.5) / feature_size

    # 生成中心点
    points = []
    for i in steps:
        for j in steps:
            points.append((i, j))

    # 生成锚框
    anchors = []
    for point in points:
        # 大框
        x0 = point[0] - big_anchor_size / 2
        y0 = point[1] - big_anchor_size / 2
        x1 = point[0] + big_anchor_size / 2
        y1 = point[1] + big_anchor_size / 2
        # 追加
        anchors.append((x0, y0, x1, y1))

        # 小框
        x0 = point[0] - small_anchor_size / 2
        y0 = point[1] - small_anchor_size / 2
        x1 = point[0] + small_anchor_size / 2
        y1 = point[1] + small_anchor_size / 2
        # 追加
        anchors.append((x0, y0, x1, y1))

        # 宽框
        x0 = point[0] - small_anchor_size * (2 ** 0.5) / 2
        y0 = point[1] - small_anchor_size / (2 ** 0.5) / 2
        x1 = point[0] + small_anchor_size * (2 ** 0.5) / 2
        y1 = point[1] + small_anchor_size / (2 ** 0.5) / 2
        # 追加
        anchors.append((x0, y0, x1, y1))

        # 高框
        x0 = point[0] - small_anchor_size / (2 ** 0.5) / 2
        y0 = point[1] - small_anchor_size * (2 ** 0.5) / 2
        x1 = point[0] + small_anchor_size / (2 ** 0.5) / 2
        y1 = point[1] + small_anchor_size * (2 ** 0.5) / 2
        # 追加
        anchors.append((x0, y0, x1, y1))

    return torch.tensor(data=anchors).float()


def show(x, target, anchors):
    x = x.detach().permute(1, 2, 0)
    print(x.shape)
    x = x.numpy().astype(np.uint8)
    target = (target.detach().numpy() * 256).astype(int)
    anchors = (anchors.detach().numpy() * 256).astype(int)
    cv2.rectangle(img=x, pt1=(target[0], target[1]),
                  pt2=(target[2], target[3]), color=(200, 200, 200))
    for anchor in anchors:
        cv2.rectangle(img=x, pt1=(anchor[0], anchor[1]),
                      pt2=(anchor[2], anchor[3]), color=(10, 10, 200))

    cv2.imshow(winname="anchor", mat=x)
    cv2.waitKey(delay=0)




if __name__ == '__main__':
    from data import train_dataloader
    for x, anchor, label in train_dataloader:
        img = x[0]
        target = anchor[0]
        anchors = anchor[1:]
        show(x=img, target=target, anchors=anchors)
        break

