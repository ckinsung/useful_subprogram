import cv2
import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset
from torch.utils.data import DataLoader


class MyDataset(Dataset):
    def __init__(self, file="train.csv"):
        self.file = file
        self.images = []
        self.anchors = []
        self.labels = []
        self._read()

    def _read(self):
        with open(file=self.file, mode="r", encoding="utf8") as f:
            line = f.readline()
            while True:
                line = f.readline().strip()
                if line:
                    line = line.split(",")
                    self.images.append(line[0])
                    self.anchors.append(line[1:-1])
                    self.labels.append(line[-1])
                else:
                    break

    def __getitem__(self, index):
        image = self.images[index]
        image = cv2.imread(filename=image)
        image = torch.tensor(data=image).float()
        image = image.permute(2, 0, 1)
        anchor = torch.tensor(data=[float(ele) for ele in self.anchors[index]]).float()
        label = torch.tensor(data=int(self.labels[index])).long()
        return image, anchor, label

    def __len__(self):
        return len(self.images)

# 训练集数据加载器
train_dataset = MyDataset(file="train.csv")
train_dataloader = DataLoader(dataset=train_dataset, shuffle=True, batch_size=32)

test_dataset = MyDataset(file="test.csv")
test_dataloader = DataLoader(dataset=test_dataset, shuffle=False, batch_size=32)

if __name__ == '__main__':
    for ele in train_dataloader:
        print(ele)
        break
