import numpy as np
import torch
import cv2
import matplotlib.pyplot as plt

def get_anchor(image_size, anchor_size_small, anchor_size_big):

    """
        锚框生成策略

        image_size: 特征图的尺寸问题
            38， 19， 10， 5， 3， 1
            每个特征点对应一个感受野
            每个特征点/每个感受野 套4个框

        anchor_size_small:
            小锚框的尺寸大小 (以归一化后的坐标 [0, 1] 为单位, 所以不能超出 1 范围) (例子: [0.37])

        anchor_size_big:
            大锚框的尺寸大小 (以归一化后的坐标 [0, 1] 为单位, 所以不能超出 1 范围) (例子: [0.447])
    """
    # 计算每个像素中心点的相对坐标
    step = (np.arange(image_size) + 0.5) / image_size  # 行列坐标归一化为 [0, 1]后, 像素中心点在新坐标系下的位置
    point = []
    for i in range(image_size):
        for j in range(image_size):
            # 类似 meshgrid 操作，差别在于 meshgrid 的 i, j 是真实 i, j, 这里是归一化的 i, j
            # 归一化后的像素中心位置，[x, y] -> [step[j], step[i]]
            point.append([step[j], step[i]])

    anchors = []

    # 遍历每个特征点的坐标
    # 每个特征点生成4个锚框
    for i in range(len(point)):
        # 取出一个特征点

        # [中心点横坐标，中心点纵坐标] ---> [cx, cy] -- > [0.25, 0.013157894736842105]

        # 生成大框
        x0 = point[i][0] - anchor_size_big / 2  # 大框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_big / 2  # 大框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_big / 2  # 大框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_big / 2  # 大框, 右下角的 y 值

        # 追加大框
        anchors.append([x0, y0, x1, y1])

        # 生成小框
        x0 = point[i][0] - anchor_size_small / 2  # 小框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_small / 2  # 小框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_small / 2  # 小框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_small / 2  # 小框, 右下角的 y 值

        # 追加小框
        anchors.append([x0, y0, x1, y1])

        # 生成宽框
        x0 = point[i][0] - anchor_size_small * (2.0 ** 0.5) / 2   # 宽框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_small / (2.0 ** 0.5) / 2   # 宽框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_small * (2.0 ** 0.5) / 2   # 宽框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_small / (2.0 ** 0.5) / 2   # 宽框, 右下角的 y 值

        # 追加宽框
        anchors.append([x0, y0, x1, y1])

        # 生成一个高框
        x0 = point[i][0] - anchor_size_small * (0.5 ** 0.5) / 2  # 高框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_small / (0.5 ** 0.5) / 2  # 高框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_small * (0.5 ** 0.5) / 2  # 高框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_small / (0.5 ** 0.5) / 2  # 高框, 右下角的 y 值

        # 追加一个高框
        anchors.append([x0, y0, x1, y1])

    # 数据转张量
    anchors = torch.FloatTensor(anchors)

    return anchors

if __name__ == '__main__':

    def plot_anchors_box(image_size):

        '''
        Plot all the anchor boxes
        '''

        for i in range(int(image_size ** 2)):

            img = np.zeros(shape=(image_size, image_size, 3))
            # img = cv2.imread('./train/0.jpg')

            # Selective save of all the anchor boxes
            if i % np.floor(image_size/4) != 0:
                continue

            for j in range(i * 4, (i + 1) * 4):

                # Obtain the 4 anchor boxes for each coordinate and plot them
                x0, y0, x1, y1 = anchors[j] * image_size
                cv2.rectangle(img=img, pt1=(x0, y0), pt2=(x1, y1), color=(0, 255, 0), thickness=1)

            cv2.imwrite(f'img{i}.jpg', img)

        return None

    # Generate the anchor boxes
    image_size = 100
    anchors = get_anchor(image_size=image_size, anchor_size_small=0.37, anchor_size_big=0.447)

    # Plot the anchor boxes and save the image
    plot_anchors_box(image_size=image_size)

    plt.pause(0.1)
