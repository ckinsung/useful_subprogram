import torch
import numpy as np
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"

import cv2
import numpy as np
import torch
from torch import nn
from PIL import Image
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from PIL import Image
import matplotlib.pyplot as plt
import cv2

torch.manual_seed(0)


def get_anchor(image_size, anchor_size_small, anchor_size_big):

    """
        锚框生成策略

        image_size: 特征图的尺寸问题
            38， 19， 10， 5， 3， 1
            每个特征点对应一个感受野
            每个特征点/每个感受野 套4个框

        anchor_size_small:
            小锚框的尺寸大小 (以归一化后的坐标 [0, 1] 为单位, 所以不能超出 1 范围) (例子: [0.37])

        anchor_size_big:
            大锚框的尺寸大小 (以归一化后的坐标 [0, 1] 为单位, 所以不能超出 1 范围) (例子: [0.447])
    """
    # 计算每个像素中心点的相对坐标
    step = (np.arange(image_size) + 0.5) / image_size  # 行列坐标归一化为 [0, 1]后, 像素中心点在新坐标系下的位置
    point = []
    for i in range(image_size):
        for j in range(image_size):
            # 类似 meshgrid 操作，差别在于 meshgrid 的 i, j 是真实 i, j, 这里是归一化的 i, j
            # 归一化后的像素中心位置，[x, y] -> [step[j], step[i]]
            point.append([step[j], step[i]])

    anchors = []

    # 遍历每个特征点的坐标
    # 每个特征点生成4个锚框
    for i in range(len(point)):
        # 取出一个特征点

        # [中心点横坐标，中心点纵坐标] ---> [cx, cy] -- > [0.25, 0.013157894736842105]

        # 生成大框
        x0 = point[i][0] - anchor_size_big / 2  # 大框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_big / 2  # 大框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_big / 2  # 大框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_big / 2  # 大框, 右下角的 y 值

        # 追加大框
        anchors.append([x0, y0, x1, y1])

        # 生成小框
        x0 = point[i][0] - anchor_size_small / 2  # 小框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_small / 2  # 小框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_small / 2  # 小框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_small / 2  # 小框, 右下角的 y 值

        # 追加小框
        anchors.append([x0, y0, x1, y1])

        # 生成宽框
        x0 = point[i][0] - anchor_size_small * (2.0 ** 0.5) / 2   # 宽框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_small / (2.0 ** 0.5) / 2   # 宽框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_small * (2.0 ** 0.5) / 2   # 宽框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_small / (2.0 ** 0.5) / 2   # 宽框, 右下角的 y 值

        # 追加宽框
        anchors.append([x0, y0, x1, y1])

        # 生成一个高框
        x0 = point[i][0] - anchor_size_small * (0.5 ** 0.5) / 2  # 高框, 左上角的 x 值
        y0 = point[i][1] - anchor_size_small / (0.5 ** 0.5) / 2  # 高框, 左上角的 y 值
        x1 = point[i][0] + anchor_size_small * (0.5 ** 0.5) / 2  # 高框, 右下角的 x 值
        y1 = point[i][1] + anchor_size_small / (0.5 ** 0.5) / 2  # 高框, 右下角的 y 值

        # 追加一个高框
        anchors.append([x0, y0, x1, y1])

    # 数据转张量
    anchors = torch.FloatTensor(anchors)

    return anchors


class MyDataset(Dataset):

    def __init__(self, file="train.csv"):

        self.file = file
        self.images = []
        self.anchors = []
        self.labels = []
        self._read()

    def _read(self):

        """
        读取数据集，然后导出
        self.images: 图像路径
        self.anchors: 图像里的目标
        self.labels: 图像里目标的标签
        """

        with open(file=self.file, mode="r", encoding="utf8") as f:
            line = f.readline()
            while True:
                line = f.readline().strip()
                if line:
                    line = line.split(",")
                    self.images.append(line[0])
                    self.anchors.append(line[1:-1])
                    self.labels.append(line[-1])
                else:
                    break

    def __getitem__(self, index):

        """
        返回
        image: [N, C, H, W]; N个 H*W*C 的图像
        anchor: [N, 4]; N个样本, 每个样本目标在图像的锚框位置 [x0, y0, x1, y1]
        label: [N]; N个样本的标签
        """

        # 图像数据
        image = self.images[index]
        image = np.array(Image.open(fp=image))
        image = torch.tensor(data=image).float()
        image = image.permute(2, 0, 1)

        # 真实框（标注结果）
        anchor = torch.tensor(data=[float(ele) for ele in self.anchors[index]]).float()

        # 标签是long类型
        label = torch.tensor(data=int(self.labels[index])).long()

        # 返回结果
        return image, anchor, label

    def __len__(self):
        return len(self.images)


def plot_anchor_on_image(train_dataloader):

    """
    把 anchor 绘在图像上
    """

    for image, anchor, label in train_dataloader:

        # PyTorch 里的 tensor 是 [N, C, H, W] 格式，转换成 OpenCV 的 [H, W, C] 格式
        img = image[0].permute(1, 2, 0).numpy().astype('uint8')
        img = cv2.cvtColor(src=img, code=cv2.COLOR_RGB2BGR)
        x0, y0, x1, y1 = np.array(anchor[0]*256).astype('uint8')

        cv2.rectangle(img=img, pt1=(x0, y0), pt2=(x1, y1), color=[0, 255, 0], thickness=1)
        cv2.imwrite('img0.jpg', img)

        break

    return None


def get_iou(anchor, target):

    """
    求 n个 anchor 和 target 的交并比
    anchor 的格式 [b, 4]
    target  的格式 [4]

    example of usage:
    anchor = torch.FloatTensor([[0, 0, 10, 10], [20, 20, 30, 30], [10, 10, 20, 20], [5, 5, 15, 15]])
    target = torch.FloatTensor([10, 10, 20, 20])
    iou = get_iou(anchor, target)
    """


    # 计算所有 anchors 的长度与宽度
    anchor_w = anchor[:, 2] - anchor[:, 0]
    anchor_h = anchor[:, 3] - anchor[:, 1]

    # 计算所有 anchors 自身的面积
    anchor_s = anchor_w * anchor_h

    # y部分的计算同理,形状都是[1],也就是标量
    target_w = target[2] - target[0]
    target_h = target[3] - target[1]
    target_s = target_w * target_h

    # 求重叠部分坐标
    cross = torch.empty(anchor.shape)

    # anchor 与 target 左上角的 xy 值比较，取最大值
    cross[:, 0] = torch.max(anchor[:, 0], target[0])  # 两个框左上角的 x 值
    cross[:, 1] = torch.max(anchor[:, 1], target[1])  # 两个框左上角的 y 值

    # anchor 与 target 右下角的 xy 值比较，取最小值
    cross[:, 2] = torch.min(anchor[:, 2], target[2])
    cross[:, 3] = torch.min(anchor[:, 3], target[3])

    # 右下坐标-左上坐标=重叠部分的宽度,高度
    # 如果两个矩形完全没有重叠,这里会出现负数
    # 这里不允许出现负数,所以最小值是0
    # 如果是我可能就会 if else 的方式 return iou = 0; 老师使用 clamp 的方法也很新异
    cross_w = (cross[:, 2] - cross[:, 0]).clamp(min=0)
    cross_h = (cross[:, 3] - cross[:, 1]).clamp(min=0)

    # 宽和高相乘,等于重叠部分面积
    # 如果宽和高中任意一个为0, 则面积为0
    cross_s = cross_w * cross_h

    # 求并集面积
    union_s = anchor_s + target_s - cross_s

    # 交并比,等于交集面积/并集面积
    iou = cross_s / union_s

    return iou


def get_offset(anchor, target):

    """

    计算 anchor 和 target 框的偏移量
    anchor: 格式是 [x0, y0, x1, y1]
    target: 格式是 [x0, y0, x1, y1]
    return:
    offset: 偏移量

    usage example:
    anchor = torch.FloatTensor([0, 0, 10, 10])
    target = torch.FloatTensor([10, 10, 20, 20])
    offset = get_offset(anchor, target)
    """

    #anchor -> [4]
    #target -> [4]

    #求出每个框的宽高
    anchor_w = anchor[2] - anchor[0]
    anchor_h = anchor[3] - anchor[1]

    #求出每个框的中心坐标
    anchor_cx = anchor[0] + anchor_w / 2
    anchor_cy = anchor[1] + anchor_h / 2

    #target的操作同理
    target_w = target[2] - target[0]
    target_h = target[3] - target[1]

    target_cx = target[0] + target_w / 2
    target_cy = target[1] + target_h / 2

    #计算中心点的误差
    offset_cx = (target_cx - anchor_cx) / anchor_w * 10
    offset_cy = (target_cy - anchor_cy) / anchor_h * 10

    #计算宽高的误差
    offset_w = torch.log(1e-6 + target_w / anchor_w) * 5
    offset_h = torch.log(1e-6 + target_h / anchor_h) * 5

    # 把计算 anchor 与 target 的 offset 的结果堆叠在一起
    offset = torch.tensor([offset_cx, offset_cy, offset_w, offset_h])

    return offset


def get_active(anchor, target):

    """
    求每个 anchor 是激活还是非激活
    如果 anchor 与 target 的 IoU 重合度大于阈值，激活
    如果 anchor 与 target 的 IoU 重合度小于阈值，不激活

    anchor -> [16, 4]  (anchor 都是批量的）
    target -> [4] (多个 anchor 对比一个 target)

    return:
    active
    """

    #不是0就是1,激活的是1,非激活的是0
    active = torch.zeros(len(anchor), dtype=torch.long)

    #求每个anchor和target的交并比
    iou = get_iou(anchor, target)

    # 两个激活策略
    active[iou >= 0.5] = 1  # 大于阈值 active
    active[torch.argmax(iou)] = 1  # 最大值 active  (避免所有都没有激活)
    # print('这里 torch.argmax(iou) 可能出现问题，以后要修改')
    # 其他都是非active

    return active == 1


def get_mask(active):

    """
    其实就是把 active 向量转换成矩阵 (估计是作为截取 anchors 用途)
    根据 active, 转换成 0 和 1, 显然 active的是 1, 非 active 的是 0
    我觉得一行代码可以搞定 torch.transpose(active.repeat(len(active), 4), dim0=0, dim1=1)
    """

    # [16, 4]
    mask = torch.zeros(len(active), 4)

    # 激活的行设置为 1
    mask[active, :] = 1

    return mask


def get_label(active):

    """
        计算每个激活的 anchor 的类别
        没有被激活的 anchor，则输出背景值 0
    """

    # [16]
    label = torch.zeros(len(active), dtype=torch.long)

    # 因为在我这份数据集中对象只有一个类别, 所以不需要从target中取类别
    label[active] = 1

    return label


def get_active_offset(active, anchor, target):

    """
    计算激活的 anchor 和 target 的 offset
    不激活的没必要计算

    active -> [16] (16个锚框里，哪个是激活的 true false vector)
    anchor -> [16, 4]  (假设 16 个锚框)
    target -> [4]  (目标 1个框)

    return:
    offset: 激活的 anchor 的 offset 值
    """

    # 初始化 (例子: [16, 4])
    offset = torch.zeros(len(active), 4)

    # 遍历所有的 anchor
    for i in range(len(active)):
        if active[i]:  # 如果相关 anchor 被激活了
            offset[i, :] = get_offset(anchor[i], target)  # 计算相关 anchor 的偏移

    return offset


def get_truth(anchor, target):

    """
    计算每一个检测目标 target 与 所有锚框 anchors 的
        1) offset 值
        2) label 标签值
        3) mask True False 激活值

    anchor -> [16, 4] (anchor 可以是多个)
    target -> [2, 4] (target 可以是多个)

    usage example:
    anchor = torch.FloatTensor([[0.0500, 0.0500, 0.4500, 0.4500],
                                [0.2000, 0.2000, 0.3000, 0.3000],
                                [0.1793, 0.2146, 0.3207, 0.2854],
                                [0.2146, 0.1793, 0.2854, 0.3207],
                                [0.5500, 0.0500, 0.9500, 0.4500],
                                [0.7000, 0.2000, 0.8000, 0.3000],
                                [0.6793, 0.2146, 0.8207, 0.2854],
                                [0.7146, 0.1793, 0.7854, 0.3207],
                                [0.0500, 0.5500, 0.4500, 0.9500],
                                [0.2000, 0.7000, 0.3000, 0.8000],
                                [0.1793, 0.7146, 0.3207, 0.7854],
                                [0.2146, 0.6793, 0.2854, 0.8207],
                                [0.5500, 0.5500, 0.9500, 0.9500],
                                [0.7000, 0.7000, 0.8000, 0.8000],
                                [0.6793, 0.7146, 0.8207, 0.7854],
                                [0.7146, 0.6793, 0.7854, 0.8207]])

    target = torch.FloatTensor([[0.0500, 0.0500, 0.4500, 0.4500],
                                [0.7000, 0.2000, 0.8000, 0.3000]])

    labels, offsets, masks = get_truth(anchor, target)

    return:
        labels: 每一个 target 与所有 anchors 的标签结果 (例子: 有 16 个 anchors)
            例子: labels.shape -> [2, 16]
                第一个 target 与 所有 anchors 的标签 labels[0, :]
                第二个 target 与 所有 anchors 的标签 labels[1, :]
                labels = tensor([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]])

        offsets: 每一个 target 与所有激活 anchors 的偏移值 (没激活的 anchor 都是 0)
            例子: offsets.shape -> [2, 64]
                第一个 target 与所有激活 anchors 的偏移值  offsets[0, :]
                第二个 target 与所有激活 anchors 的偏移值  offsets[1, :]

        masks: 每一个 target 与所有 anchors 的位置的 True False 值
            例子: offsets.shape -> [2, 64]
                第一个 target 与所有 anchors 的位置的 True False 值  masks[0, :]
                    masks[0, 0:4] 是第一个 target 与 第一个 anchor 的 True False 值
                    masks[4, 4:8] 是第一个 target 与 第二个 anchor 的 True False 值
                第二个 target 与所有 anchors 的位置的 True False 值  masks[1, :]

    """

    labels = []
    offsets = []
    masks = []

    for i in range(len(target)):

        # 求每个anchor是激活 (iou > 0.5) 还是非激活 (举例: active 大小 [16]）
        active = get_active(anchor, target[i])

        # 根据 active, 转换成0和1, 显然 active 的是1, 非 active 的是 0 (举例: [16, 4])
        mask = get_mask(active)
        masks.append(mask.reshape(-1))  # 展平成 1 维度向量

        # 根据 active, 计算每个激活的 anchor 的类别 (举例: [16])
        label = get_label(active)
        labels.append(label)

        # 计算激活的 anchor 和 target 的 offset (举例: [16, 4])
        offset = get_active_offset(active, anchor, target[i])
        offsets.append(offset.reshape(-1))

    # 从 list, stacking 的方式转换成 tensor
    labels = torch.stack(labels)  # [2, 64]  # 第 0 个维度（默认）去堆的
    offsets = torch.stack(offsets)  # [2, 64]  # 第 0 个维度（默认）去堆的
    masks = torch.stack(masks)  # [2, 64]  # 第 0 个维度（默认）去堆的

    return labels, offsets, masks


class FirstModel(torch.nn.Module):

    """

    把图像导入，计算处 x, anchor, label 和 offset

    return:
        x: 特征图 [N, C, H, W] (256*256 的输入，H, W = 32, 32]
        anchor: 特征图的所有锚框 [H*W*k] = [32*32*4=4096]
        label: 特征图所有锚框的标签 [B, (32*32*4*2)=8192]
        offset: 特征图所有锚框的偏移值 [B, (32*32*4*4)=16384]

    usage example:
        x = torch.zeros((2, 3, 256, 256))
        x, anchor, label, offset = FirstModel()(x)
        x.shape, anchor.shape, label.shape, offset.shape
        (torch.Size([2, 64, 32, 32]),
         torch.Size([4096, 4]),
         torch.Size([2, 8192]),
         torch.Size([2, 16384]))

    """

    def __init__(self):
        super().__init__()

        # 特征提取
        self.cnn = torch.nn.Sequential(

            # 1
            torch.nn.Conv2d(in_channels=3,
                            out_channels=16,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.ReLU(),

            # 2
            torch.nn.Conv2d(in_channels=16,
                            out_channels=16,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=16),
            torch.nn.ReLU(),

            # 池化 (特征图变一半)
            torch.nn.MaxPool2d(kernel_size=2),

            # 3
            torch.nn.Conv2d(in_channels=16,
                            out_channels=32,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ReLU(),

            # 4
            torch.nn.Conv2d(in_channels=32,
                            out_channels=32,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=32),
            torch.nn.ReLU(),

            # 池化 (特征图变一半)
            torch.nn.MaxPool2d(kernel_size=2),

            # 5
            torch.nn.Conv2d(in_channels=32,
                            out_channels=64,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=64),
            torch.nn.ReLU(),

            # 6
            torch.nn.Conv2d(in_channels=64,
                            out_channels=64,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=64),
            torch.nn.ReLU(),

            # 池化 (特征图变一半)
            torch.nn.MaxPool2d(kernel_size=2)
        )

        # 标签输出 (每个框需要两个 class 的输出, 背景和皮卡丘，out_channels=4个框*2=8)
        # 构建输出，跟原来的全连接工作一样的
        self.label = torch.nn.Conv2d(in_channels=64,
                                     out_channels=8,
                                     kernel_size=3,
                                     padding=1)

        # 偏移输出 (每个框需要4个偏移量的输出, out_channels=4*4=16)
        self.offset = torch.nn.Conv2d(in_channels=64,
                                      out_channels=16,
                                      kernel_size=3,
                                      padding=1)

    def forward(self, x):

        # 提取特征
        x = self.cnn(x)  # [b, 3, 256, 256] -> [2, 64, 32, 32]

        # 生成所有锚框的位置 [0, 1] 归一化 (anchor.shape = [4096, 4])
        # [2, 64, 32, 32] -> [4096, 4] (32*32 个像素 * 每个像素4个框 -> 32*32*4 = 1张特征图有 4096 个锚框)
        anchor = get_anchor(image_size=x.size(-1),  # 输入尺寸大小 32*32
                            anchor_size_small=0.2,  # 这个超参需要尝试调整
                            anchor_size_big=0.272)  # 这个超参需要尝试调整

        # 所有锚框的标签的置信度 (label.shape = [B, 8192])
        label = self.label(x)  # [2, 64, 32, 32] -> [2, 8, 32, 32]
        label = label.permute(0, 2, 3, 1)  # [2, 8, 32, 32] -> [2, 32, 32, 8]
        label = label.flatten(start_dim=1)  # [2, 32, 32, 8] -> [2, 8192] (4096 个框 * 2个置信度 = 8192 个置信度神经元)

        # 当前层所有锚框的偏移 (offset.shape = [B, 16384])
        offset = self.offset(x)  # [2, 64, 32, 32] -> [2, 16, 32, 32]
        offset = offset.permute(0, 2, 3, 1)  # [2, 16, 32, 32] -> [2, 32, 32, 16]
        offset = offset.flatten(start_dim=1)  # [2, 32, 32, 16] -> [2, 16384] (4096 个框 * 每个框4个值 = 16396 个置信度神经元)

        # 当前的特征层，当前层的所有锚框的坐标，当前层所有锚框的标签的置信度，当前层所有锚框的偏移、
        return x, anchor, label, offset


class MiddleModel(torch.nn.Module):

    """

    usage example:
        x = torch.zeros((2, 64, 32, 32))
        x, anchor, label, offset = MiddleModel(c_in=64,
                                               anchor_size_small=0.37,
                                               anchor_size_big=0.447)(x)
        x.shape, anchor.shape, label.shape, offset.shape
        (torch.Size([2, 128, 16, 16]),
         torch.Size([1024, 4]),
         torch.Size([2, 2048]),
         torch.Size([2, 4096]))

    """

    def __init__(self, c_in, anchor_size_small, anchor_size_big):
        super().__init__()

        self.anchor_size_small = anchor_size_small
        self.anchor_size_big = anchor_size_big

        self.cnn = torch.nn.Sequential(
            # 1
            torch.nn.Conv2d(in_channels=c_in,
                            out_channels=128,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=128),
            torch.nn.ReLU(),

            # 2
            torch.nn.Conv2d(in_channels=128,
                            out_channels=128,
                            kernel_size=3,
                            padding=1),
            torch.nn.BatchNorm2d(num_features=128),
            torch.nn.ReLU(),

            # 池化
            torch.nn.MaxPool2d(kernel_size=2),
        )

        self.label = torch.nn.Conv2d(in_channels=128,
                                     out_channels=8,
                                     kernel_size=3,
                                     padding=1)

        self.offset = torch.nn.Conv2d(in_channels=128,
                                      out_channels=16,
                                      kernel_size=3,
                                      padding=1)

    def forward(self, x):

        # 提取特征
        x = self.cnn(x)  # [2, 64, 32, 32] -> [2, 128, 16, 16]

        # 生成所有锚框的位置 [0, 1] 归一化 (anchor.shape = [1024, 4])
        # [2, 128, 16, 16] -> [1024, 4] (16*16 个像素 * 每个像素4个框 -> 16*16*4 = 1张特征图有 1024 个锚框)
        anchor = get_anchor(image_size=x.shape[-1],  # [2, 128, 16, 16] -> [1024, 4]
                            anchor_size_small=self.anchor_size_small,
                            anchor_size_big=self.anchor_size_small)

        # 所有锚框的标签的置信度 (label.shape = [B, 2048])
        label = self.label(x)  # [2, 128, 16, 16] -> [2, 8, 16, 16]
        label = label.permute(0, 2, 3, 1)  # [2, 8, 16, 16] -> [2, 16, 16, 8]
        label = label.flatten(start_dim=1)  # [2, 16, 16, 8] -> [2, 2048]

        # 当前层所有锚框的偏移 (offset.shape = [B, 4096])
        offset = self.offset(x)  # [2, 128, 16, 16] -> [2, 16, 16, 16]
        offset = offset.permute(0, 2, 3, 1)  # [2, 16, 16, 16] -> [2, 16, 16, 16]
        offset = offset.flatten(start_dim=1)  # [2, 16, 16, 16] -> [2, 4096]

        return x, anchor, label, offset


class LastModel(torch.nn.Module):

    """
        usage example:
            x = torch.zeros((2, 128, 4, 4))
            x, anchor, label, offset = LastModel()(x)
            x.shape, anchor.shape, label.shape, offset.shape
    """

    def __init__(self):

        super().__init__()

        # 将特征图强行改变为 1*1
        self.cnn = torch.nn.AdaptiveMaxPool2d(output_size=(1, 1))

        self.label = torch.nn.Conv2d(in_channels=128,
                                     out_channels=8,  # 毕竟是最后一层 1*1 个像素, 所以永远只有 8 个神经元
                                     kernel_size=3,
                                     padding=1)

        self.offset = torch.nn.Conv2d(in_channels=128,
                                      out_channels=16,  # 毕竟是最后一层 1*1 个像素, 所以永远只有 16 个神经元
                                      kernel_size=3,
                                      padding=1)

    def forward(self, x):

        x = self.cnn(x)  # [2, 128, 4, 4] -> [2, 128, 1, 1]

        # 生成所有锚框的位置 [0, 1] 归一化 (anchor.shape = [1024, 4])
        # [2, 128, 1, 1] -> [4, 4] (1*1 个像素 * 每个像素4个框 -> 1*1*4 = 1张特征图有 4 个锚框)
        anchor = get_anchor(image_size=1,  # [2, 128, 1, 1] -> [4, 4]
                            anchor_size_small=0.88,  # 最后一层像素了, 所以生成的 anchor 比较大 (小框占有 88% 原图)
                            anchor_size_big=0.961)  # 最后一层像素了, 所以生成的 anchor 比较大 (大框占有 96% 原图)

        # 所有锚框的标签的置信度 (label.shape = [B, 8])
        label = self.label(x)  # [2, 128, 1, 1] -> [2, 8, 1, 1]
        label = label.permute(0, 2, 3, 1)  # [2, 8, 1, 1] -> [2, 1, 1, 8]
        label = label.flatten(start_dim=1)  # [2, 1, 1, 8] -> [2, 8]

        # 当前层所有锚框的偏移 (offset.shape = [B, 16])
        offset = self.offset(x)  # [2, 128, 1, 1] -> [2, 16, 1, 1]
        offset = offset.permute(0, 2, 3, 1)  # [2, 16, 1, 1] -> [2, 1, 1, 16]
        offset = offset.flatten(start_dim=1)  # [2, 1, 1, 16] -> [2, 16]

        return x, anchor, label, offset


class Model(torch.nn.Module):

    """
    结合所有模块生成最终的 SSD 模型

    usage example:
        x = torch.zeros((2, 3, 256, 256))
        anchor, label, offset = Model()(x)
        x.shape, anchor.shape, label.shape, offset.shape
        Out:
        (torch.Size([2, 3, 256, 256]), # 输入 2 张 256*256 的 RGB 图像
         torch.Size([5444, 4]), # 生成 5444 个锚框
         torch.Size([2, 5444, 2]),  # 每张图像里，有 5444 个锚框，每个锚框有 2 个置信度
         torch.Size([2, 21776]))  # 每张图像里，有 5444 个锚框，每个锚框有 4 个框的位置
    """

    def __init__(self):

        super().__init__()

        # 一个头部模块
        self.first = FirstModel()

        # 论文引用了 5 次，自己可以修改引用次数 (3 个中间模块)
        self.middle_1 = MiddleModel(c_in=64, anchor_size_small=0.37, anchor_size_big=0.447)
        self.middle_2 = MiddleModel(c_in=128, anchor_size_small=0.54, anchor_size_big=0.619)
        self.middle_3 = MiddleModel(c_in=128, anchor_size_small=0.71, anchor_size_big=0.79)

        # 1 个尾部模块
        self.last = LastModel()

    def forward(self, x):

        # 定义容器: 定位框, 类别, 偏移量; 储存每个模块的输出结果
        anchor, label, offset = [None] * 5, [None] * 5, [None] * 5

        # 第 1 个模块放入 anchor, label, offset 列表里的第 1 个元素
        # [2, 3, 256, 256] -> [2, 64, 32, 32],[4096, 4],[2, 8192],[2, 16384]
        x, anchor[0], label[0], offset[0] = self.first(x)

        # 第 2, 3, 4 个模块放入 anchor, label, offset 列表里的第 2, 3, 4 个元素
        x, anchor[1], label[1], offset[1] = self.middle_1(x)  #[2,64,32,32] -> [2,128,16,16],[1024,4],[2,2048],[2,4096]
        x, anchor[2], label[2], offset[2] = self.middle_2(x)  #[2,128,16,16] -> [2,128,8,8],[256,4],[2,512],[2,1024]
        x, anchor[3], label[3], offset[3] = self.middle_3(x)  #[2,128,8,8] -> [2,128,4,4],[64,4],[2,128],[2,256]

        # 最后个模块放入 anchor, label, offset 列表里的最后个元素
        #[2, 128, 4, 4] -> [2, 128, 1, 1],[4, 4],[2, 8],[2, 16]
        x, anchor[4], label[4], offset[4] = self.last(x)

        # 结果整合在一起 (cat 不会产生新的维度)
        anchor = torch.cat(anchor, dim=0)  # [4096+1024+256+64+4, 4] -> [5444, 4]
        label = torch.cat(label, dim=1)  # [2, 8192+2048+512+128+8] -> [2, 10888]
        label = label.reshape(label.shape[0], -1, 2)  #[2, 10888] -> [2, 5444, 2]
        offset = torch.cat(offset, dim=1)  #[2, 16384+4096+1024+256+16] -> [2, 21776]

        return anchor, label, offset


# 设置批量样本的交叉熵损失单独输出, 不做均值聚合
get_loss_cls = nn.CrossEntropyLoss(reduction='none')  # 分类损失
get_loss_box = nn.L1Loss(reduction='none')  # 一般都是上 MSE loss, 这里用的是绝对差 (MAE)


def get_loss(label_pred, offset_pred, label, offset, masks):

    """
        求 loss
        label_pred: 预测出来的标签
        offset_pred: 预测出来的标签的偏移值

        这个损失函数的计算方式很绕, 不应该把 loss 的 reduction 定义为 None
    """

    # label_pred -> [32, 5444, 2]  # 所有的概率值都在这里
    # offset_pred -> [32, 21776]

    # label -> [32, 5444]  # 所有锚框的标签
    # offset -> [32, 21776]  # 所有锚框的偏移
    # masks -> [32, 21776]  # 如果有标签，框的位置都是 True

    # 模型的原始输出
    label_pred = label_pred.reshape(-1, 2)  #[32, 5444, 2] -> [174208, 2]  # 二分类问题所有有两个输出

    # 标签索引
    label = label.reshape(-1)  # [32, 5444] -> [174208]

    # 上面把 label 与 label_pred 对口型之后就可以计算 loss 了
    loss_cls = get_loss_cls(label_pred, label)  # 因为 loss 的定义 reduction='None' 所以没有做聚合的数是这么多 [174208]
    loss_cls = loss_cls.reshape(32, -1)  # [174208] -> [32, 5444]  (得到了每个样本的损失)
    loss_cls = loss_cls.mean(dim=1)  # [32, 5444] -> [32]  # 这里又把 loss 计算 mean 多此一举

    # 确保把没有激活的 offset 变成 0 (之前 torch.zeros 已经定义过了，但是作者为了再次确保)
    offset_pred *= masks  # [32, 21776] * [32, 21776] -> [32, 21776]
    offset *= masks  # [32, 21776] * [32, 21776] -> [32, 21776]

    # 偏移损失
    loss_box = get_loss_box(offset_pred, offset)  # [32, 21776]
    loss_box = loss_box.mean(dim=1)  # [32, 21776] -> [32]  # 32 个样本的偏移损失

    # 损失相加
    loss = loss_cls + loss_box  # [32] + [32] = [32]

    return loss


def inverse_offset(anchor, offset):
    # anchor -> [4]
    # offset -> [4]

    # x0,y0,x1,y0转换为cent_x,cent_y,w,h
    anchor_center = torch.empty(4)

    # cx
    anchor_center[0] = (anchor[0] + anchor[2]) / 2
    # cy
    anchor_center[1] = (anchor[1] + anchor[3]) / 2
    # w
    anchor_center[2] = anchor[2] - anchor[0]
    # h
    anchor_center[3] = anchor[3] - anchor[1]

    # 真正的预测框
    pred = torch.empty(4)

    # target 是手动标注的框，预测出来的框靠近 target
    # anchor 是套住了皮卡丘的人为设置的死框，是需要矫正的
    # 把anchor 根据 offset 矫正完了，就是 pred，理想情况下：也就是target

    # offset.x = (target.x - anchor.x) / anchor.w * 10
    # pred.x = offset.x * anchor.w * 0.1 + anchor.x
    pred[0] = (offset[0] * anchor_center[2] * 0.1) + anchor_center[0]

    # offset.y = (target.y - anchor.y) / anchor.h * 10
    # pred.y = offset.y * anchor.h * 0.1 + anchor.y
    pred[1] = (offset[1] * anchor_center[3] * 0.1) + anchor_center[1]

    # offset.w = log(tagret.w / anchor.w) * 5
    # pred.w  = exp(offset.w / 5) * anchor.w
    pred[2] = torch.exp(offset[2] / 5) * anchor_center[2]

    # offset.h = log(tagret.h / anchor.h) * 5
    # pred.h  = exp(offset.h / 5) * anchor.h
    pred[3] = torch.exp(offset[3] / 5) * anchor_center[3]

    # cent_x,cent_y,w,h转换为x0,y0,x1,y1
    pred_corner = torch.empty(4)
    pred_corner[0] = pred[0] - 0.5 * pred[2]
    pred_corner[1] = pred[1] - 0.5 * pred[3]
    pred_corner[2] = pred[0] + 0.5 * pred[2]
    pred_corner[3] = pred[1] + 0.5 * pred[3]

    # 返回标准坐标 x0,y0,x1,y1
    return pred_corner


def predict(x, net):

    net.eval()

    # [3, 256, 256] -> [1, 3, 256, 256]
    x = x.unsqueeze(dim=0)

    # [5444, 4],[1, 5444, 2],[1, 21776]
    anchor, label_pred, offset_pred = net(x)

    # [1, 21776] -> [5444, 4]
    offset_pred = offset_pred.reshape(-1, 4)

    # 偏移量变换公式的逆运算
    # [5444, 4] -> [5444, 4]
    anchor_pred = torch.empty(5444, 4)
    for i in range(5444):
        anchor_pred[i] = inverse_offset(anchor[i], offset_pred[i])

    # softmax,让背景的概率和皮卡丘的概率相加为1
    # [1, 5444, 2] -> [1, 5444, 2]
    label_pred = torch.nn.functional.softmax(label_pred, dim=2)

    # 取anchor中物体是皮卡丘的概率
    # [1, 5444, 2] -> [5444]
    label_pred = label_pred[0, :, 1]

    # 只保留皮卡丘的概率高于阈值的结果???
    anchor_pred = anchor_pred[label_pred > 0.1]
    label_pred = label_pred[label_pred > 0.1]

    return anchor_pred, label_pred


if __name__ == '__main__':

    # 训练集数据加载器
    train_dataset = MyDataset(file="train.csv")
    train_dataloader = DataLoader(dataset=train_dataset, shuffle=True, batch_size=32)

    # 测试集数据加载器
    test_dataset = MyDataset(file="test.csv")
    test_dataloader = DataLoader(dataset=test_dataset, shuffle=False, batch_size=32)

    # 把 anchor 绘在图像上
    # plot_anchor_on_image(train_dataloader)

    # 构建模型
    net = Model()

    # 优化器
    optimizer = torch.optim.SGD(net.parameters(), lr=0.1)

    #训练
    for epoch in range(20):

        net.train()

        for i, (imgs, target_gt, target_label) in enumerate(train_dataloader):

            # 这里有个问题！！！因为数据只有 0, 1 所以没有用到  target_label 这个变量
            # 如果考虑到多物体检测的问题，肯定需要导入 target_label 这个变量。需要修改。

            optimizer.zero_grad()

            # 预测模型
            anchor, label_pred, offset_pred = net(imgs)  # [5444, 4],[32, 5444, 2],[32, 21776]

            # 获取每个 anchor 是否激活, 偏移量, 标签
            label, offset, masks = get_truth(anchor, target_gt)  # [32, 5444],[32, 21776],[32, 21776]

            #计算loss
            loss = get_loss(label_pred, offset_pred, label, offset, masks)
            loss.mean().backward()
            optimizer.step()

            if i % 10 == 0:
                print(epoch, i, loss.mean().item())

    # 储存模型
    torch.save(net, './ssd.model')

    # 加载模型
    net = torch.load('./ssd.model')

    # 调用测试集预测结果
    for i, (x, y, z) in enumerate(test_dataloader):

        anchor_pred, label_pred = predict(x[i], net)
        if len(anchor_pred) == 0:
            print('没有发现目标')
            continue

        img = x[i].permute(1, 2, 0).numpy()
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

        for k in range(len(anchor_pred)):
            x0, y0, x1, y1 = int(anchor_pred[k][0].item()*256), int(anchor_pred[k][1].item()*256), int(anchor_pred[k][2].item()*256), int(anchor_pred[k][3].item()*256)
            cv2.rectangle(img=img, pt1=(x0, y0), pt2=(x1, y1), color=[0, 0, 255], thickness=1)

        cv2.imwrite(f'imp_test_{i}.jpg', img)