import os
import os.path
import sys
import torch
import torch.utils.data as data
import cv2
import numpy as np


class WiderFaceDetection(data.Dataset):

    def __init__(self, txt_path, preproc=None):

        # 数据增强或者前期处理
        self.preproc = preproc

        # list格式保存所有的图像路径
        self.imgs_path = []

        # list 格式保存所有的人脸标签
        self.words = []

        # 读取标注文档
        f = open(txt_path, 'r')

        # 读取文档里的所有数据
        lines = f.readlines()

        # 如果是第1行
        isFirst = True

        labels = []

        # 循环标注数据txt里的每一行
        for line in lines:
            line = line.rstrip()
            if line.startswith('#'):
                if isFirst is True:
                    isFirst = False
                else:
                    labels_copy = labels.copy()
                    self.words.append(labels_copy)
                    labels.clear()
                path = line[2:]
                path = txt_path.replace('label.txt', 'images/') + path  # 生成图像路径
                self.imgs_path.append(path)  # Append 图像路径
            else:
                # 读标签
                line = line.split(' ')
                label = [float(x) for x in line]
                labels.append(label)

        self.words.append(labels)  # 12880张图

    def __len__(self):
        return len(self.imgs_path)

    def __getitem__(self, index):

        # 读取第 index 的图像
        img = cv2.imread(self.imgs_path[index])
        height, width, _ = img.shape
        self.width_store = []
        self.height_store = []

        # 读取第 index 的标注数据
        labels = self.words[index]

        # 保存标注数据 (1个框 (4个数) + 5个关键点 (10个数) + 1个分类目标(1个数))
        annotations = np.zeros((0, 15))

        if len(labels) == 0:
            return annotations

        # 循环此图的所有人脸标签
        for idx, label in enumerate(labels):
            annotation = np.zeros((1, 15))

            # bbox location
            annotation[0, 0] = label[0]  # x1
            annotation[0, 1] = label[1]  # y1
            annotation[0, 2] = label[0] + label[2]  # x2 = x1 + width
            annotation[0, 3] = label[1] + label[3]  # y2 = y1 + height

            # 5 landmarks
            annotation[0, 4] = label[4]    # l0_x
            annotation[0, 5] = label[5]    # l0_y
            annotation[0, 6] = label[7]    # l1_x
            annotation[0, 7] = label[8]    # l1_y
            annotation[0, 8] = label[10]   # l2_x
            annotation[0, 9] = label[11]   # l2_y
            annotation[0, 10] = label[13]  # l3_x
            annotation[0, 11] = label[14]  # l3_y
            annotation[0, 12] = label[16]  # l4_x
            annotation[0, 13] = label[17]  # l4_y

            cv_img = cv2.imread(self.imgs_path[index])
            cv2.rectangle(cv_img, (0, 50), (700, 10), (0, 0, 0), -1)

            # 因为人脸检测是2分类, 只需要两个分类结果就可以
            if (annotation[0, 4]<0):  # 标注值是-1
                annotation[0, 14] = -1  # 不是人脸的标签 / 背景 (不是很明白既然是二分类，为什么要标注是非人脸数据。细看都是异常的人脸, 自己排除掉)
            else:
                annotation[0, 14] = 1  # 是人脸的标签

            annotations = np.append(annotations, annotation, axis=0)

        # target -> (n, 15) n 个人脸
        target = np.array(annotations)

        # 数据增强或者前期处理
        if self.preproc is not None:
            img, target = self.preproc(img, target)

        return torch.from_numpy(img), target

def detection_collate(batch):

    """
    Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
                imgs[0], imgs[1] 都是 [3, 640, 640] 转换成 [batch, 3, 640, 640]
            2) (list of tensors) annotations for a given image are stacked on 0 dim
                targets[0], targets[1] are just list of torch tensor [n, 15]
    """

    targets = []
    imgs = []
    for _, sample in enumerate(batch):
        for _, tup in enumerate(sample):
            if torch.is_tensor(tup):
                imgs.append(tup)
            elif isinstance(tup, type(np.empty(0))):
                annos = torch.from_numpy(tup).float()
                targets.append(annos)

    return (torch.stack(imgs, 0), targets)
