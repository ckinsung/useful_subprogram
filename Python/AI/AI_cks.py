import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import pandas as pd
import seaborn as sn  # for heatmaps
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from torch.utils.data import DataLoader
import cv2
import re
import os

class fixed_point_arithmetic():

    def __init__(self, sign=1, integer=3, frac=12):
        pass

        self.max = 2**(integer + sign - 1) - 2**(-frac)  # Calculate the maximum number
        self.min = -2**(integer + sign - 1)  # Calculate the minimum number
        self.len_fxp = sign + integer + frac

    def double_to_fixed(self, x, scale):
        return int(round(x * (1 << scale)))

    def fixed_to_double(self, x, scale):
        return float(x / (1 << scale))

    def generate_table(self, total=8):

        sign = 1

        results = []

        for integer in range(total):

            # 计算小数位
            frac = total - integer - sign

            max = 2**(integer + sign - 1) - 2**(-frac)
            min = -2**(integer + sign - 1)
            precision = 2**(-frac)

            results.append([f'Q{sign + integer}.{frac}', integer, frac, min, max, precision])

        return results

    def fxp_to_float(self, x, sign=1, integer=3, len_fxp=16):

        '''
        定点转换成浮点的函数

        :param x: 二进制值
        :param sign: 符号位 1 或者 0
        :param integer: 整数位
        :param frac: 小数位
        :return:

        x = Fxp(1.2512340897110348, signed=1, n_int=3, n_frac=12)
        x.bin()
        Out: '0001010000000101'
        x.__float__()
        Out: 1.2512207
        float_x = sum(list_num_float)
        # Out:
        # -1.251220703125
        '''

        if isinstance(x, str) is False:
            raise('x is not string')

        order = [-i + integer for i in range(len_fxp)]

        list_num_float = [int(num)*2**order[i] for i, num in enumerate(x)]

        if sign == 1:
            list_num_float[0] = -list_num_float[0]

        float_x = sum(list_num_float)

        return float_x

class useful_func():

    def __init__(self):

        return None

    def find_all_loc_substring_within_string(self, vars, substring):

        '''
        Find the location for all substrings within a string variable
        :param substring: substring
        :return:
        pos: location for all the substrings

        vars = 'http://localhost:8888/notebooks/Untitled.ipynb'
        substring = '/'
        pos = find_all_loc_substring_within_string(vars, key)
        print(pos)
        [5, 6, 21, 31]
        '''

        # Define the initial condition
        pos, start, end = [], 0, len(substring)

        while True:

            # Find the first location
            loc = vars.find(substring, start, end)

            # If no returned location, break the loop
            if loc is -1:
                break
            else:
                # Put the location in the list
                pos.append(loc)
                start = loc + len(substring)

        return pos

    def count_parameters(self, model):

        '''
        This function calculates the total parameters involved in the model
        :return:
        total number of parameters
        '''

        # Loop through all the parameters in each pair of layers
        params = [p.numel() for p in model.parameters() if p.requires_grad]

        for item in params:
            print(f'{item:>6}')

        total_params = sum(params)

        print(f'______\n{total_params:>6}')

        return total_params

    def count_output_features_size(self, n_in, p, k, s):

        # n_out = [(n_in + 2p - k)/s] + 1
        # n_in: number of input features
        # n_out: number of output features
        # k: convolution kernel size
        # p: convolution padding size
        # s: convolution stride size

        n_out = ((n_in + 2*p - k)/s) + 1

        return n_out

    def check_cuda(self):

        '''
        This subfunction check the
        availability,
        device's id,
        device's name,
        memory allocation,
        cache,
        of gpu usage
        :return:
        '''

        # Check if cuda is available for usage (True or False)
        print('torch.cuda.is_available(): ', torch.cuda.is_available())

        # Check id for the device that is using cuda
        # Possibly using more than 1 GPU and can have multiple IDs
        device = torch.cuda.current_device()
        print('torch.cuda.current_device: ', device)

        # Check the name of the device for the id (for example: 'GeForce GTX 1660 Ti with Max-Q Design')
        print('torch.cuda.get_device_name(): ', torch.cuda.get_device_name(device=device))

        # Check the memory allocation (unit in bytes)
        print('torch.cuda.memory_allocated(): ', torch.cuda.memory_allocated(device=device))

        # Check the memory cache
        print('torch.cuda.memory_cached(): ', torch.cuda.memory_cached(device=device))

        return None

class tabular_data():

    class kaggle_NYC_taxi_fare(nn.Module):

        # 'regression' or 'classification' problem
        # Predicts the price of the taxi fare (regression)
        # Predicts if the price is more or less than $10 (classification)
        # Table input features: categorical + continuous data

        def __init__(self, problem, emb_szs, n_cont, out_sz, hidden_layers, p=0.5):

            '''
            problem: type of problem to solve ('regression' or 'classification')
            emb_szs: list of tuples: each categorical variable size is paired with an embedding size
            n_cont: int: number of continuous variables
            out_sz: int: output size
            hidden_layers: list of ints: layer sizes (exclude input layer and output layer size)
            p: float: dropout probability for each layer (for simplicity we'll use the same value throughout)
            '''

            # Call the superclass (nn.Module) model from this subclass (kaggle_NYC_taxi_fare)
            super().__init__()

            # Set up the embedded layers with torch.nn.ModuleList() and torch.nn.Embedding()
            # Categorical data will be filtered through these Embeddings in the forward section.
            self.embeds = nn.ModuleList([nn.Embedding(ni, nf) for ni, nf in emb_szs])

            # Set up a dropout function for the embeddings with torch.nn.Dropout() The default p-value=0.5
            self.emb_drop = nn.Dropout(p)

            # Set up a normalization function for the continuous variables with torch.nn.BatchNorm1d()
            self.bn_cont = nn.BatchNorm1d(n_cont)

            # Calculate the size for the input features
            n_emb = sum((nf for ni, nf in emb_szs))
            n_in = n_emb + n_cont  # Total number of input features

            # Establish a model with lots of layers, create a list first
            layerlist = []
            for i in hidden_layers:
                layerlist.append(nn.Linear(n_in, i))  # n_in input neurons connected to i number of output neurons
                layerlist.append(nn.ReLU(inplace=True))  # Apply activation function - ReLU
                layerlist.append(nn.BatchNorm1d(i))  # Apply batch normalization
                layerlist.append(nn.Dropout(p))  # Apply dropout to prevent overfitting
                n_in = i  # Reassign number of input neurons as the number of neurons from previous last layer

            # Establish the FCC between the last hidden layer and output layer
            layerlist.append(nn.Linear(hidden_layers[-1], out_sz))

            # Use the * operator to expand the list into positional arguments when calling nn.Sequential()
            self.layers = nn.Sequential(*layerlist)

            # Define the loss function depending on the problem type
            if problem == 'regression':
                self.loss_function = nn.MSELoss()
            elif problem == 'classification':
                self.loss_function = nn.CrossEntropyLoss()

            # Define the optimization method
            self.optimizer = torch.optim.Adam(self.parameters(), lr=0.001)

            # Define the variable losses to store the calculated loss from training
            self.losses = []

        def forward(self, x_cat, x_cont):

            '''
            :param x_cat: categorical data (torch.tensor format)
            :param x_cont: continuous data (torch.tensor format)
            :return:
            '''

            embeddings = []

            # Convert categorical data to one code embeddings
            for i, e in enumerate(self.embeds):
                embeddings.append(e(x_cat[:, i]))

            # Linearize into n*m matrix type
            x = torch.cat(embeddings, 1)

            # Dropout
            x = self.emb_drop(x)

            # Normalization for the continuous data
            x_cont = self.bn_cont(x_cont)

            # Combine the embeddings and the continuous data
            x = torch.cat([x, x_cont], 1)

            # Insert both embeddings and continuous data to the forward function
            x = self.layers(x)

            return x

        def train_model(self, cat_train, con_train, y_train, problem, epochs=300):

            '''
            :param cat_train: categorical data (torch.tensor format)
            :param con_train: continuous data (torch.tensor format)
            :param y_train: training y data (torch.tensor format)
            :return:
            '''

            import time
            start_time = time.time()

            for i in range(epochs):

                i += 1

                # Predict the y value
                y_pred = self.forward(cat_train, con_train)

                if problem == 'regression':
                    # Calculate the loss value (RMSE)
                    loss = torch.sqrt(self.loss_function(y_pred, y_train))
                elif problem == 'classification':
                    # Calculate the cross entropy loss
                    loss = self.loss_function(y_pred, y_train)

                # Append the loss value
                self.losses.append(loss)

                if i % 25 == 1:
                    print(f'epoch: {i:3}  loss: {loss.item():10.8f}')

                # Gradients accumulate with every backpropagation
                # To prevent compounding we need to reset the stored gradient for each new epoch
                self.optimizer.zero_grad()

                # Now we can backpropagate
                loss.backward()

                # Update the hyperparameters of our model
                self.optimizer.step()

            print(f'epoch: {i:3}  loss: {loss.item():10.8f}')  # print the last line
            print(f'\nDuration: {time.time() - start_time:.0f} seconds')  # print the time elapsed

            return None

class regression():

    class linear_model_in_1_out_1():

        # Input feature: 1
        # Output: 1
        # Regression problem. Finding a perceptron model that fits a regression problem.

        def __init__(self, in_features=1, out_features=1):

            # Build the model
            self.model = nn.Linear(in_features, out_features)

            # Set the Mean Square Error Loss Function
            self.loss_function = nn.MSELoss()

            # Define the optimization algorithm (stochastic gradient descent)
            self.optimizer = torch.optim.SGD(self.model.parameters(), lr=0.001)

            # Variable to store the losses vs iterations
            self.losses = []

        def train_model(self, x, y, epochs):

            for i in range(epochs):

                i += 1

                # Calculate the predicted values
                y_pred = self.model(x)

                # Based on the predicted values, calculate the value of the loss function
                loss = self.loss_function(y_pred, y)

                # Store the calculated loss value
                self.losses.append(loss)

                # Print out the weight and bias found
                print(f'epoch: {i:2}  loss: {loss.item():10.8f}  weight: {self.model.weight.item():10.8f}  bias: {self.model.bias.item():10.8f}')

                # Gradients accumulate with every backpropagation
                # To prevent compounding we need to reset the stored gradient for each new epoch
                self.optimizer.zero_grad()

                # Now we can backpropagate
                loss.backward()

                # Update the hyperparameters of our model
                self.optimizer.step()

            return None

class classification():

    class iris(nn.Module):

        # Iris classification
        # Input features: 4 continuous data
        # Output: 3 categorical data

        def __init__(self, in_features=4, h1=8, h2=9, out_features=3):

            # Call the superclass (nn.Module) model from this subclass (iris)
            super().__init__()

            # Build up the layers
            self.fc1 = nn.Linear(in_features, h1)
            self.fc2 = nn.Linear(h1, h2)
            self.out = nn.Linear(h2, out_features)

            # Set the loss function
            self.loss_function = nn.CrossEntropyLoss()

            # Define the optimization algorithm
            self.optimizer = torch.optim.Adam(self.parameters(), lr=0.01)

            # Variable to store the losses vs iterations
            self.losses = []

        # Define function for forward propagation
        def forward(self, a0):

            # apply activation function
            a1 = F.relu(self.fc1(a0))  # 0th and 1st layer
            a2 = F.relu(self.fc2(a1))   # 1st and 2nd layer
            a3 = F.softmax(self.out(a2))  # 2nd and 3rd layer

            return a3

        def train_model(self, x, y, epochs=100):

            for i in range(epochs):

                # Forward propagation using the inputs
                # Calculated y_pred is N*3 type while y is categorical.
                # Cross entropy doesn't require both to be the same type
                # A bit confusing. Come back to this point next time.
                y_pred = self.forward(x)

                # Calculate the loss value
                loss = self.loss_function(y_pred, y)

                # Append the loss value
                self.losses.append(loss)

                # Print out the loss value
                print(f'epoch: {i:2}  loss: {loss.item():10.8f}')

                # Gradients accumulate with every backpropagation
                # To prevent compounding we need to reset the stored gradient for each new epoch
                self.optimizer.zero_grad()

                # Now we can backpropagate
                loss.backward()

                # Update the hyperparameters of our model
                self.optimizer.step()

            return None

    class MultilayerPerceptron(nn.Module):

        '''
        Artificial Neural Network (ANN) or multilayer perceptron (MLP) used for MNIST dataset
        '''

        def __init__(self, in_sz=784, out_sz=10, hidden_layers=[120, 84], dropout_p=0.5):

            super().__init__()

            # Establish a model with lots of layers, create a list first
            layerlist, n_in = [], in_sz
            for i in hidden_layers:
                layerlist.append(nn.Linear(n_in, i))  # n_in input neurons connected to i number of output neurons
                layerlist.append(nn.ReLU(inplace=True))  # Apply activation function - ReLU
                layerlist.append(nn.Dropout(dropout_p))  # Apply dropout to prevent overfitting
                n_in = i  # Reassign number of input neurons as the number of neurons from previous last layer

            # Establish the FCC between the last hidden layer and output layer
            layerlist.append(nn.Linear(hidden_layers[-1], out_sz))

            # Use the * operator to expand the list into positional arguments when calling nn.Sequential()
            self.layers = nn.Sequential(*layerlist)

            # Define dropout
            self.dropout = nn.Dropout(p=dropout_p)

            # Set the loss function
            self.loss_function = nn.CrossEntropyLoss()

            # Define the optimization algorithm
            self.optimizer = torch.optim.Adam(self.parameters(), lr=0.001)

            # Variable to store the losses vs iterations
            self.losses = []

        def forward(self, X):

            '''
            Define the forward function
            :param X: input parameter
            :return:
            Y_prob: probability of each class
            '''

            # Insert the input to the layers
            X = self.layers(X)

            # Calculate the probability for each class using softmax function
            # Y_prob = F.log_softmax(X, dim=1)

            # https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html#torch.nn.CrossEntropyLoss
            # CrossEntropyLoss: This criterion combines LogSoftmax and NLLLoss in one single class.
            # You either define nn.NLLLoss + F.log_softmax(X, dim=1) or just define nn.CrossEntropyLoss will do

            return X

        def train_model(self, train_loader, batch_size_train, epochs=10):

            import time
            start_time = time.time()

            train_losses = []
            train_correct = []

            for i in range(epochs):

                # Number of correct training data
                trn_corr = 0

                # Run the training batches
                # MNIST training data has 60000 samples (with batch_size of 100), there are total of 600 runs in each epoch
                for b, (X_train, y_train) in enumerate(train_loader):

                    b = b + 1

                    # Apply the model
                    # y_pred consists of 100*10, where 100 refers to the batch size of 100 samples,
                    # while 10 refers to the probabilities for each class
                    y_pred = self.forward(X_train.view(batch_size_train, -1))  # Here we flatten X_train

                    # Calculate the loss value based on the predicted data and the actual data
                    loss = self.loss_function(y_pred, y_train)

                    # Obtain the predicted class by choosing the class with the highest probability
                    predicted = torch.max(y_pred.data, 1)[1]

                    # Tally the number of correct predictions
                    batch_corr = (predicted == y_train).sum()

                    # Keep track of the total number of correctly predicted samples
                    trn_corr = trn_corr + batch_corr

                    # Update parameters
                    self.optimizer.zero_grad()
                    loss.backward()
                    self.optimizer.step()

                    # Print interim results
                    if b % 200 == 0:
                        print(f'epoch: {i:2}  batch: {b:4} [{100 * b:6}/60000]  loss: {loss.item():10.8f}  '
                              f'accuracy: {trn_corr.item() * 100 / (100 * b):7.3f}%')

                    # Update train loss & accuracy for the epoch
                    train_losses.append(loss.detach().numpy())
                    train_correct.append(trn_corr.detach().numpy())

            # print the time elapsed
            print(f'\nDuration: {time.time() - start_time:.0f} seconds')

            return train_losses, train_correct

        def validate_data(self, test_loader, batch_size_test):

            # Run the testing batches
            test_correct = []
            test_losses = []
            tst_corr = 0

            with torch.no_grad():

                for b, (X_test, y_test) in enumerate(test_loader):

                    # Apply the model
                    y_val = self.forward(X_test.view(batch_size_test, -1))  # Here we flatten X_test

                    # Tally the number of correct predictions
                    predicted = torch.max(y_val.data, 1)[1]
                    tst_corr += (predicted == y_test).sum()

            # Update test loss & accuracy for the epoch
            loss = self.loss_function(y_val, y_test)
            test_losses.append(loss)
            test_correct.append(tst_corr)

            return test_losses, test_correct

    class CNN_MNIST(nn.Module):

        '''
        Convolutional Neural Network (CNN) used for MNIST dataset
        '''

        def __init__(self, dropout_p=0.5):

            super().__init__()

            # in_channels (int) – Number of channels in the input image
            # out_channels (int) – Number of channels produced by the convolution (also number of filters/kernels used)
            # kernel_size (int or tuple) – Size of the convolving kernel (3 means 3*3 kernel size)
            # (can be 3*5 also if kernel_size = (3, 5))
            # stride (int or tuple, optional) – Stride of the convolution. Default: 1
            # padding (int, tuple or str, optional) – Padding added to all four sides of the input. Default: 0

            # 2D convolution: 1 color channel, 6 filters (output channels), 3*3 kernel, stride=1
            self.conv1 = nn.Conv2d(in_channels=1, out_channels=6, kernel_size=3, stride=1)

            # 2D convolution: 6 input filters from conv1, 16 filters as output, 3*3 kernel, stride=1
            self.conv2 = nn.Conv2d(in_channels=6, out_channels
            =16, kernel_size = 3, stride = 1)

            self.fc1 = nn.Linear(in_features=5 * 5 * 16, out_features=120)

            self.fc2 = nn.Linear(in_features=120, out_features=84)

            self.fc3 = nn.Linear(in_features=84, out_features=10)

            # Define dropout (dropout may not be useful for CNN. do more studies first before use it)
            self.dropout = nn.Dropout(p=dropout_p)

            # Define the loss function
            self.criterion = nn.CrossEntropyLoss()
            # Define the optimization algorithm
            self.optimizer = torch.optim.Adam(self.parameters(), lr=0.001)

        def forward(self, X):

            # X is of the shape (N,C,H,W)
            # N: number of samples
            # C: channels of the matrix
            # H: height of the matrix
            # W: width of the matrix

            # First CNN layer
            # Input X is of shape [batch_size, 1, 28, 28]
            X = F.relu(self.conv1(X))
            # Output X is of shape [batch_size, 6, 26, 26] (n_out = ((28 + 0 - 3)/1) + 1 = 26)

            # Max Pooling of 2*2
            # Input X is of shape [batch_size, 6, 26, 26]
            X = F.max_pool2d(X, 2, 2)
            # Output X is of shape [batch_size, 6, 13, 13]

            # ReLU activation function
            # Input X is of shape [batch_size, 6, 13, 13]
            X = F.relu(self.conv2(X))
            # Output X is of shape [batch_size, 16, 11, 11] (n_out = ((13 + 0 - 3)/1) + 1 = 11)

            # Max Pooling of 2*2
            # Input X is of shape [batch_size, 16, 11, 11]
            X = F.max_pool2d(X, 2, 2)
            # Output X is of shape [batch_size, 16, 5, 5]

            # Linearize the activations
            # Input X is of shape [batch_size, 16, 5, 5]
            X = X.view(-1, 5 * 5 * 16)
            # Output X is of shape [batch_size, 400]

            # Input X is of shape [batch_size, 400]
            X = F.relu(self.fc1(X))
            # Output X is of shape [batch_size, 120]

            # Input X is of shape [batch_size, 120]
            X = F.relu(self.fc2(X))
            # Output X is of shape [batch_size, 84]

            # Input X is of shape [batch_size, 84]
            X = self.fc3(X)
            # Output X is of shape [batch_size, 10]

            # Calculate the probability for each class using softmax function
            # Y_prob = F.log_softmax(X, dim=1)

            # https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html#torch.nn.CrossEntropyLoss
            # CrossEntropyLoss: This criterion combines LogSoftmax and NLLLoss in one single class.
            # You either define nn.NLLLoss + F.log_softmax(X, dim=1) or just define nn.CrossEntropyLoss will do

            return X

        def train(self, train_loader, epochs=5):

            import time
            start_time = time.time()

            train_losses = []
            train_correct = []

            for i in range(epochs):

                trn_corr = 0

                # Run the training batches
                for b, (X_train, y_train) in enumerate(train_loader):

                    # X_train is of the shape (batch_size,C,H,W)

                    b += 1

                    # Apply the model
                    y_pred = self.forward(X_train)  # we don't flatten X-train here

                    # Calculate the loss value
                    loss = self.criterion(input=y_pred, target=y_train)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_pred.data, dim=1)[1]

                    # Calculate the correct predictions
                    batch_corr = (predicted == y_train).sum()

                    # Calculate the total correct predictions
                    trn_corr += batch_corr

                    # Update the parameters
                    self.optimizer.zero_grad()
                    loss.backward()
                    self.optimizer.step()

                    # Print interim results
                    if b % 600 == 0:
                        print(f'epoch: {i:2}  batch: {b:4} [{10 * b:6}/60000]  loss: {loss.item():10.8f}  \
            accuracy: {trn_corr.item() * 100 / (10 * b):7.3f}%')

                train_losses.append(loss)
                train_correct.append(trn_corr)

            print(f'\nDuration: {time.time() - start_time:.0f} seconds')  # print the time elapsed

            return train_losses, train_correct

        def validate(self, test_loader):

            # Define variables for storing loss values vs epoch
            # Suggest to define the variables as global variables (self) in the future
            test_losses = []
            test_correct = []
            tst_corr = 0

            # Run the testing batches
            with torch.no_grad():

                for b, (X_test, y_test) in enumerate(test_loader):

                    # Apply the model
                    y_val = self.forward(X_test)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_val.data, 1)[1]
                    tst_corr += (predicted == y_test).sum()

            loss = self.criterion(y_val, y_test)
            test_losses.append(loss)
            test_correct.append(tst_corr)

            return test_losses, test_correct

    class CNN_MNIST_cuda(nn.Module):

        '''
        Convolutional Neural Network (CNN) used for MNIST dataset
        '''

        def __init__(self, dropout_p=0.5):

            super().__init__()

            # in_channels (int) – Number of channels in the input image
            # out_channels (int) – Number of channels produced by the convolution (also number of filters/kernels used)
            # kernel_size (int or tuple) – Size of the convolving kernel (3 means 3*3 kernel size)
            # (can be 3*5 also if kernel_size = (3, 5))
            # stride (int or tuple, optional) – Stride of the convolution. Default: 1
            # padding (int, tuple or str, optional) – Padding added to all four sides of the input. Default: 0

            # 2D convolution: 1 color channel, 6 filters (output channels), 3*3 kernel, stride=1
            self.conv1 = nn.Conv2d(in_channels=1, out_channels=6, kernel_size=3, stride=1)

            # 2D convolution: 6 input filters from conv1, 16 filters as output, 3*3 kernel, stride=1
            self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=3, stride=1)

            self.fc1 = nn.Linear(in_features=5 * 5 * 16, out_features=120)

            self.fc2 = nn.Linear(in_features=120, out_features=84)

            self.fc3 = nn.Linear(in_features=84, out_features=10)

            # Define dropout (dropout may not be useful for CNN. do more studies first before use it)
            self.dropout = nn.Dropout(p=dropout_p)

        def forward(self, X):

            # X is of the shape (N,C,H,W)
            # N: number of samples
            # C: channels of the matrix
            # H: height of the matrix
            # W: width of the matrix

            # First CNN layer
            # Input X is of shape [batch_size, 1, 28, 28]
            X = F.relu(self.conv1(X))
            # Output X is of shape [batch_size, 6, 26, 26] (n_out = ((28 + 0 - 3)/1) + 1 = 26)

            # Max Pooling of 2*2
            # Input X is of shape [batch_size, 6, 26, 26]
            X = F.max_pool2d(X, 2, 2)
            # Output X is of shape [batch_size, 6, 13, 13]

            # ReLU activation function
            # Input X is of shape [batch_size, 6, 13, 13]
            X = F.relu(self.conv2(X))
            # Output X is of shape [batch_size, 16, 11, 11] (n_out = ((13 + 0 - 3)/1) + 1 = 11)

            # Max Pooling of 2*2
            # Input X is of shape [batch_size, 16, 11, 11]
            X = F.max_pool2d(X, 2, 2)
            # Output X is of shape [batch_size, 16, 5, 5]

            # Linearize the activations
            # Input X is of shape [batch_size, 16, 5, 5]
            X = X.view(-1, 5 * 5 * 16)
            # Output X is of shape [batch_size, 400]

            # Input X is of shape [batch_size, 400]
            X = F.relu(self.fc1(X))
            # Output X is of shape [batch_size, 120]

            # Input X is of shape [batch_size, 120]
            X = F.relu(self.fc2(X))
            # Output X is of shape [batch_size, 84]

            # Input X is of shape [batch_size, 84]
            X = self.fc3(X)
            # Output X is of shape [batch_size, 10]

            # Calculate the probability for each class using softmax function
            # Y_prob = F.log_softmax(X, dim=1)

            # https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html#torch.nn.CrossEntropyLoss
            # CrossEntropyLoss: This criterion combines LogSoftmax and NLLLoss in one single class.
            # You either define nn.NLLLoss + F.log_softmax(X, dim=1) or just define nn.CrossEntropyLoss will do

            return X

    class CNN_CIFAR10(nn.Module):

        def __init__(self):

            super().__init__()

            # in_channels (int) – Number of channels in the input image
            # out_channels (int) – Number of channels produced by the convolution (also number of filters/kernels used)
            # kernel_size (int or tuple) – Size of the convolving kernel (3 means 3*3 kernel size)
            # (can be 3*5 also if kernel_size = (3, 5))
            # stride (int or tuple, optional) – Stride of the convolution. Default: 1
            # padding (int, tuple or str, optional) – Padding added to all four sides of the input. Default: 0

            '''
            Thought processes of calculating the size for each layer
            For CIFAR 10 dataset, image size is 3*32*32
            3*32*32 (input layer) -> 6*30*30 (CNN) -> 
            6*15*15 (max pooling) -> 6*15*15 (relu) -> 
            16*13*13 (CNN) -> 16*6*6 (max pooling) -> 
            16*6*6 (relu) -> 16*6*6 = 216 (FC) -> 
            84 (FC) -> 10 (FC)
            '''

            # 2D convolution: 3 color channel, 6 filters (output channels), 3*3 kernel, stride=1
            self.conv1 = nn.Conv2d(in_channels=3, out_channels=6, kernel_size=3, stride=1)

            # 2D convolution: 6 input filters from conv1, 16 filters as output, 3*3 kernel, stride=1
            self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=3, stride=1)

            self.fc1 = nn.Linear(in_features=16*6*6, out_features=120)

            self.fc2 = nn.Linear(in_features=120, out_features=84)

            self.fc3 = nn.Linear(in_features=84, out_features=10)

            # Define the loss function
            self.criterion = nn.CrossEntropyLoss()

            # Define the optimization algorithm
            self.optimizer = torch.optim.Adam(self.parameters(), lr=0.001)

        def forward(self, X):

            X = F.relu(self.conv1(X))
            X = F.max_pool2d(X, 2, 2)
            X = F.relu(self.conv2(X))
            X = F.max_pool2d(X, 2, 2)
            X = X.view(-1, 16*6*6)
            X = F.relu(self.fc1(X))
            X = F.relu(self.fc2(X))
            X = self.fc3(X)

            # return F.log_softmax(X, dim=1)

            return X

        def train(self, train_loader, epochs=10):

            import time
            start_time = time.time()

            train_losses = []
            train_correct = []

            for i in range(epochs):

                trn_corr = 0

                # Run the training batches
                for b, (X_train, y_train) in enumerate(train_loader):

                    # X_train is of the shape (batch_size,C,H,W)

                    b += 1

                    # Apply the model
                    y_pred = self.forward(X_train)  # we don't flatten X-train here

                    # Calculate the loss value
                    loss = self.criterion(y_pred, y_train)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_pred.data, dim=1)[1]

                    # Calculate the correct predictions
                    batch_corr = (predicted == y_train).sum()

                    # Calculate the total correct predictions
                    trn_corr += batch_corr

                    # Update the parameters
                    self.optimizer.zero_grad()
                    loss.backward()
                    self.optimizer.step()

                    # Print interim results
                    if b % 1000 == 0:
                        print(f'epoch: {i:2}  batch: {b:4} [{10 * b:6}/50000]  loss: {loss.item():10.8f}  \
                    accuracy: {trn_corr.item() * 100 / (10 * b):7.3f}%')

                train_losses.append(loss.detach().numpy())
                train_correct.append(trn_corr.detach().numpy())

            print(f'\nDuration: {time.time() - start_time:.0f} seconds')  # print the time elapsed

            return train_losses, train_correct

        def validate(self, data, class_names):

            '''
            Perform validation after the model has been trained
            :param test_loader: test dataset
            :return:
            '''

            tst_corr = 0

            # Run the testing batches
            with torch.no_grad():

                for b, (X_test, y_test) in enumerate(data):

                    # Apply the model
                    y_val = self.forward(X_test)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_val.data, 1)[1]
                    tst_corr += (predicted == y_test).sum()

            # Calculate the loss value using the loss function
            loss = self.criterion(y_val, y_test)
            test_loss = loss.detach().numpy()
            test_correct = tst_corr.detach().numpy()

            # Calculate the confusion matrix and convert it to panda format
            cm = confusion_matrix(y_test, predicted)
            df_cm = pd.DataFrame(cm, class_names, class_names)

            # Plot the graph
            plt.figure(2)
            sn.heatmap(df_cm, annot=True, fmt="d", cmap='BuGn')
            plt.xlabel("prediction")
            plt.ylabel("label (ground truth)")
            plt.show(block=False)

            return test_loss, test_correct

    class CNN_custom_dogs_cats(nn.Module):

        def __init__(self):

            super().__init__()

            # 2D convolution: 3 color channel, 6 filters (output channels), 3*3 kernel, stride=1
            self.conv1 = nn.Conv2d(in_channels=3, out_channels=6, kernel_size=3, stride=1)

            # 2D convolution: 6 input filters from conv1, 16 filters as output, 3*3 kernel, stride=1
            self.conv2 = nn.Conv2d(in_channels=6, out_channels=16, kernel_size=3, stride=1)

            self.fc1 = nn.Linear(in_features=16*54*54, out_features=120)

            self.fc2 = nn.Linear(in_features=120, out_features=84)

            self.fc3 = nn.Linear(in_features=84, out_features=2)

            self.criterion = nn.CrossEntropyLoss()

            self.optimizer = torch.optim.Adam(self.parameters(), lr=0.001)

        def forward(self, X):

            # X is of the shape (N,C,H,W)
            # N: number of samples
            # C: channels of the matrix
            # H: height of the matrix
            # W: width of the matrix

            # First CNN layer
            # ReLU activation function applied to CNN
            # Input X is of shape [batch_size, 3, 224, 224]
            X = F.relu(self.conv1(X))
            # Output X is of shape [batch_size, 6, 222, 222] (n_out = ((224 + 0 - 3)/1) + 1 = 222)

            # Max Pooling of 2*2
            # Input X is of shape [batch_size, 6, 222, 222]
            X = F.max_pool2d(X, 2, 2)
            # Output X is of shape [batch_size, 6, 111, 111]

            # Second CNN layer
            # ReLU activation function applied to CNN
            # Input X is of shape [batch_size, 6, 111, 111]
            X = F.relu(self.conv2(X))
            # Output X is of shape [batch_size, 16, 109, 109]

            # Max Pooling of 2*2
            # Input X is of shape [batch_size, 16, 109, 109]
            X = F.max_pool2d(X, 2, 2)
            # Output X is of shape [batch_size, 16, 54, 54]

            # Linearize the activations
            # Input X is of shape [batch_size, 16, 54, 54]
            X = X.view(-1, 16 * 54 * 54)
            # Output X is of shape [batch_size, 16*54*54]

            # Input X is of shape [batch_size, 16*54*54]
            X = F.relu(self.fc1(X))
            # Output X is of shape [batch_size, 120]

            # Input X is of shape [batch_size, 120]
            X = F.relu(self.fc2(X))
            # Output X is of shape [batch_size, 84]

            # Input X is of shape [batch_size, 84]
            X = self.fc3(X)
            # Output X is of shape [batch_size, 2]

            # cross entropy has already include the softmax function
            # F.log_softmax(X, dim=1)

            return X

        def train(self, train_loader):

            import time
            start_time = time.time()

            epochs = 3

            max_trn_batch = 800

            train_losses = []
            train_correct = []

            for i in range(epochs):
                trn_corr = 0

                # Run the training batches
                for b, (X_train, y_train) in enumerate(train_loader):

                    # Limit the number of batches
                    if b == max_trn_batch:
                        break
                    b += 1

                    # Apply the model
                    y_pred = self.forward(X_train)
                    loss = self.criterion(y_pred, y_train)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_pred.data, 1)[1]
                    batch_corr = (predicted == y_train).sum()
                    trn_corr += batch_corr

                    # Update parameters
                    self.optimizer.zero_grad()
                    loss.backward()
                    self.optimizer.step()

                    # Print interim results
                    if b % 200 == 0:
                        print(f'epoch: {i:2}  batch: {b:4} [{10 * b:6}/8000]  loss: {loss.item():10.8f}  \
            accuracy: {trn_corr.item() * 100 / (10 * b):7.3f}%')

                train_losses.append(loss.detach().numpy())
                train_correct.append(trn_corr.detach().numpy())

            print(f'\nDuration: {time.time() - start_time:.0f} seconds')  # print the time elapsed

            return train_losses, train_correct

        def validate(self, test_loader):

            test_losses = []
            test_correct = []
            tst_corr = 0
            max_tst_batch = 300

            # Run the testing batches
            with torch.no_grad():
                for b, (X_test, y_test) in enumerate(test_loader):

                    # Limit the number of batches
                    if b == max_tst_batch:
                        break

                    # Apply the model
                    y_val = self.forward(X_test)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_val.data, 1)[1]
                    tst_corr += (predicted == y_test).sum()

            loss = self.criterion(y_val, y_test)
            test_losses.append(loss.numpy())
            test_correct.append(tst_corr.numpy())

            return test_losses, test_correct

class object_detection():

    class face_detection():

        class traditional_method():

            class haar_cascade_method():

                def __init__(self):
                    pass

                def webcam(self, selection='rgb'):

                    '''
                    Use Haar Cascade model for face detection
                    :param selection: select to operate the webcam in rgb or grayscale mode
                    :return:
                    bounding box of the face if it is detected
                    '''

                    # Import the Haar Cascade face detection model
                    face_cascade = cv2.CascadeClassifier('../haar cascades/haarcascade_frontalface_alt2.xml')

                    # video capture
                    cap = cv2.VideoCapture(0)

                    while True:

                        # Read image from webcam
                        success, img = cap.read()

                        # Use the model
                        faces = face_cascade.detectMultiScale(img, 1.3, 5)

                        # Convert to grayscale image if selection is grayscale
                        if selection == 'grayscale':

                            # Convert image to grayscale
                            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                            # Stack the image to have the matrix elements along the channel dimension
                            img = np.dstack([gray]*3)

                        # Draw the bounding box of the face if found
                        for (x, y, w, h) in faces:
                            img = cv2.rectangle(img, (x, y), (x + w, y + h), (255, 255, 255), 2)

                        # Show the image
                        cv2.imshow('Image', img)

                        if cv2.waitKey(1) & 0xFF == ord('q'):
                            break

                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    return None

            class pico_method():

                def __init__(self):
                    pass

                def webcam(self):

                    '''

                    This program successfully run in Ubuntu 18.04 but not in Windows.

                    Codes below are basically completely copied from demo.py
                    ./rnt/samples/python/demo.py

                    which requires the following files for running
                    ./rnt/picornt.c
                    ./rnt/picornt.h
                    ./rnt/cascades/facefinder

                    :return:
                    '''

                    import numpy
                    import os
                    import ctypes
                    import cv2
                    import time
                    import numpy as np
                    import platform

                    if platform.system() == 'Windows':
                        print(f'This program can only run on LINUX, not {platform.system()} !!!')
                        return

                    # os.system('cc ./model/pico/picornt.c -O3 -fPIC -shared -o ./model/pico/picornt.lib.so') # Run the picornt.c file and generate picornt.lib.so file
                    pico = ctypes.cdll.LoadLibrary('./model/pico/picornt.lib.so') # Load the picornt.lib.so library file
                    # os.system('rm ./model/pico/picornt.lib.so')  # Remove the picornt.lib.so library file

                    # Import the trained model
                    bytes = open('./model/pico/cascades/facefinder', 'rb').read()
                    cascade = numpy.frombuffer(bytes, dtype=numpy.uint8)

                    slot = numpy.zeros(1, dtype=numpy.int32)
                    nmemslots = 5
                    maxslotsize = 1024
                    memory = numpy.zeros(4 * nmemslots * maxslotsize, dtype=numpy.float32)
                    counts = numpy.zeros(nmemslots, dtype=numpy.int32)

                    def process_frame(gray):
                        #
                        maxface = min(gray.shape[0], gray.shape[1])
                        minface = maxface // 5
                        #
                        maxndets = 2048
                        dets = numpy.zeros(4 * maxndets, dtype=numpy.float32)

                        ndets = pico.find_objects(
                            ctypes.c_void_p(dets.ctypes.data), ctypes.c_int(maxndets),
                            ctypes.c_void_p(cascade.ctypes.data), ctypes.c_float(0.0),
                            ctypes.c_void_p(gray.ctypes.data), ctypes.c_int(gray.shape[0]), ctypes.c_int(gray.shape[1]),
                            ctypes.c_int(gray.shape[1]),
                            ctypes.c_float(1.1), ctypes.c_float(0.1), ctypes.c_float(minface), ctypes.c_float(maxface)
                        )

                        ndets = pico.update_memory(
                            ctypes.c_void_p(slot.ctypes.data),
                            ctypes.c_void_p(memory.ctypes.data), ctypes.c_void_p(counts.ctypes.data),
                            ctypes.c_int(nmemslots), ctypes.c_int(maxslotsize),
                            ctypes.c_void_p(dets.ctypes.data), ctypes.c_int(ndets), ctypes.c_int(maxndets)
                        )

                        ndets = pico.cluster_detections(
                            ctypes.c_void_p(dets.ctypes.data), ctypes.c_int(ndets)
                        )

                        return list(dets.reshape(-1, 4))[0:ndets]

                    cap = cv2.VideoCapture(0)

                    while (True):

                        # Read the frame from the camera
                        ret, frm = cap.read()
                        if max(frm.shape[0], frm.shape[1]) > 640:
                            frm = cv2.resize(frm, (0, 0), fx=0.5, fy=0.5)

                        # Convert from RGB to grayscale image
                        # print('frame size = ' + str(np.shape(frm)))
                        gray = numpy.ascontiguousarray(frm[:, :, 1].reshape((frm.shape[0], frm.shape[1])))
                        # gray = cv2.resize(src=gray, dsize=(100, 100))
                        # gray = cv2.resize(gray, dsize=(160, 120))
                        # print('gray size = ' + str(np.shape(gray)))
                        #

                        t = time.time()
                        dets = process_frame(gray)  # gray needs to be numpy.uint8 array
                        print("* frame processed in %d [ms]" % int(1000.0 * (time.time() - t)))
                        for det in dets:
                            if det[3] >= 50.0:
                                cv2.circle(gray, (int(det[1]), int(det[0])), int(det[2] / 2.0), (0, 0, 255), 4)
                        #
                        cv2.imshow('...', cv2.resize(gray, (640, 480)))
                        #
                        if cv2.waitKey(5) & 0xFF == ord('q'):
                            break

                    cap.release()
                    cv2.destroyAllWindows()

                    return None

        class deep_learning():

            class mtcnn():

                def __init__(self):
                    pass

                def webcam(self, selection='rgb', resize=(640, 480)):

                    # Import the deep learning model
                    from mtcnn import MTCNN
                    detector = MTCNN()

                    # video capture
                    cap = cv2.VideoCapture(0)

                    while True:

                        # Read image from webcam
                        success, img = cap.read()

                        # Resize
                        img = cv2.resize(img, resize)

                        if selection == 'grayscale':

                            # Convert image to grayscale
                            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                            # Stack the image to have the matrix elements along the channel dimension
                            img = np.dstack([img]*3)

                        # Extract all the faces detected by the face detection algorithm
                        results = detector.detect_faces(img)

                        # If results are not empty
                        if results != list():

                            # Loop through all the faces detected
                            for i in range(len(results)):

                                # if face is detected and not empty
                                result = results[i]

                                # Extract and plot the bounding box for the face
                                [x, y, w, h] = result['box']
                                img = cv2.rectangle(img, pt1=(x, y), pt2=(x + w, y + h), color=(255, 255, 255), thickness=1)

                                # Extract and plot the face features (eyes, nose and tongue)
                                features = result['keypoints']

                                for key in features.keys():

                                    # Location for each feature
                                    [row, col] = features[key]

                                    # Plot the location of the features
                                    img = cv2.circle(img, (row, col), radius=1, color=(0, 0, 255), thickness=1)

                        # Show the image
                        cv2.imshow('Image', img)

                        if cv2.waitKey(1) & 0xFF == ord('q'):
                            break

                    cv2.waitKey(0)
                    cv2.destroyAllWindows()

                    return None

                def image(self, img_path, selection='grayscale', resize=(320, 240)):

                    # Import the deep learning model
                    from mtcnn import MTCNN
                    detector = MTCNN()

                    # Read image
                    img = cv2.imread(img_path)

                    # Resize
                    img = cv2.resize(img, resize)

                    if selection == 'grayscale':

                        # Convert image to grayscale
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                        # Stack the image to have the matrix elements along the channel dimension
                        img = np.dstack([img]*3)

                    # Extract all the faces detected by the face detection algorithm
                    results = detector.detect_faces(img)

                    # If results are not empty
                    if results != list():

                        # Loop through all the faces detected
                        for i in range(len(results)):

                            # if face is detected and not empty
                            result = results[i]

                            # Extract and plot the bounding box for the face
                            [x, y, w, h] = result['box']
                            img = cv2.rectangle(img, pt1=(x, y), pt2=(x + w, y + h), color=(255, 255, 255), thickness=1)

                            # Extract and plot the face features (eyes, nose and tongue)
                            features = result['keypoints']

                            for key in features.keys():

                                # Location for each feature
                                [row, col] = features[key]

                                # Plot the location of the features
                                img = cv2.circle(img, (row, col), radius=1, color=(0, 0, 255), thickness=1)

                    # Show the image
                    cv2.imshow('Image', img)

                    return None

            class facenet_pytorch_mtcnn():

                '''
                Face Recognition using Pytorch
                https://github.com/timesler/facenet-pytorch
                '''

                def __init__(self):
                    pass

                def webcam(self, selection='grayscale', resize=(320, 240), saved_video='no'):

                    from facenet_pytorch import MTCNN
                    import torch
                    import numpy as np
                    import cv2

                    # device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
                    # print('Running on device: {}'.format(device))

                    mtcnn = MTCNN(margin=20, thresholds=[0.5, 0.5, 0.7], min_face_size=10, post_process=False, keep_all=True, device='cpu')
                    # mtcnn = MTCNN(keep_all=True, device='cpu')

                    # video capture
                    cap = cv2.VideoCapture(0)

                    # Get the fps
                    fps = cap.get(cv2.CAP_PROP_FPS)
                    print("Frame rate: ", int(fps), "FPS")

                    # Save video
                    if saved_video == 'yes':
                        # Define the codec and create VideoWriter object.
                        fourcc = cv2.VideoWriter_fourcc(*'XVID')
                        output = cv2.VideoWriter('output.mp4', fourcc, fps, (resize[0], resize[1]))

                    while True:

                        # Read image from webcam
                        success, img = cap.read()

                        # Resize
                        img = cv2.resize(img, resize)

                        if selection == 'grayscale':

                            # Convert image to grayscale
                            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                            # Stack the image to have the matrix elements along the channel dimension
                            img = np.dstack([img]*3)

                        # Extract all the faces detected by the face detection algorithm
                        # Detect faces
                        boxes, prob, points = mtcnn.detect(img, landmarks=True)

                        # If at least one face is detected
                        if boxes is not None:

                            # Loop through all the faces
                            for i in range(np.shape(boxes)[0]):

                                # Draw face
                                x1, y1, x2, y2 = boxes[i][0], boxes[i][1], boxes[i][2], boxes[i][3]
                                img = cv2.rectangle(img, pt1=(x1, y1), pt2=(x2, y2), color=(255, 255, 255), thickness=1)

                                # Plot the location of the features
                                for x, y in points[i]:
                                    img = cv2.circle(img, (x, y), radius=1, color=(0, 0, 255), thickness=1)

                        # Show the image
                        cv2.imshow('Image', img)

                        if saved_video == 'yes':
                            # Write the frame into the file 'output.avi'
                            output.write(img)

                        if cv2.waitKey(1) & 0xFF == ord('q'):
                            break

                    cap.release()

                    if saved_video == 'yes':
                        output.release()

                    cv2.destroyAllWindows()

                def image(self, img_path, selection='grayscale', resize=(320, 240), landmarks=True):

                    from facenet_pytorch import MTCNN
                    import torch
                    import numpy as np
                    import cv2

                    # device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
                    # print('Running on device: {}'.format(device))

                    # mtcnn = MTCNN(margin=20, thresholds=[0.1, 0.1, 0.1], min_face_size=5, keep_all=False, device='cpu')
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    # read image
                    img = cv2.imread(img_path)

                    # Resize
                    img = cv2.resize(img, resize)

                    if selection == 'grayscale':

                        # Convert image to grayscale
                        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                        # Stack the image to have the matrix elements along the channel dimension
                        img = np.dstack([img]*3)

                    # Extract all the faces detected by the face detection algorithm
                    # Detect faces

                    if landmarks is True:
                        boxes, probs, points = mtcnn.detect(img, landmarks=True)
                    elif landmarks is False:
                        boxes, probs = mtcnn.detect(img, landmarks=False)
                    else:
                        pass

                    if boxes is not None:

                        # Draw faces
                        x1, y1, x2, y2 = boxes[0][0], boxes[0][1], boxes[0][2], boxes[0][3]
                        img = cv2.rectangle(img, pt1=(x1, y1), pt2=(x2, y2), color=(255, 255, 255), thickness=1)

                        if landmarks is True:

                            # Plot the location of the features
                            for x, y in points[0]:
                                img = cv2.circle(img, (x, y), radius=1, color=(0, 0, 255), thickness=1)

                    # Show the image
                    cv2.imshow('Image', img)

                    return None

                def TOF_opnous(self, ir):

                    '''
                    TOF IR 图像人脸检测
                    :param ir: TOF 的 IR 原始图
                    :return:
                    '''

                    from facenet_pytorch import MTCNN
                    import numpy as np
                    import cv2

                    # Standard linearization method
                    def interp(ir, ir_min, ir_max, interp_min=0, interp_max=255):

                        # IR 值如果太小，进行上下限处理
                        ir_max = np.min([ir.max(), ir_max])
                        ir_min = np.max([ir.min(), ir_min])
                        ir[ir < ir_min] = ir_min
                        ir[ir > ir_max] = ir_max

                        # 计算斜率
                        m = (interp_max - interp_min)/(ir_max - ir_min)

                        # 计算线性换算值
                        ir2 = m*(ir - ir_min)

                        # 换算成 uint8
                        ir2 = ir2.astype('uint8')

                        # 把原始 IR 图线性化，介于 (0, 255) 之间
                        # 以下代码出现问题，不然就直接调用
                        # ir2 = np.interp(ir, ir_min, ir_max, (0, 255)).astype('uint8')

                        return ir2

                    # wanghao 8132 data
                    def interp2(ir, ir_min, ir_max, interp_min=0, interp_max=255):

                        # IR 值如果太小，进行上下限处理
                        ir_max = np.max([ir.max(), ir_max])
                        ir_min = np.max([ir.min(), ir_min])
                        ir[ir < ir_min] = ir_min
                        ir[ir > ir_max] = ir_max

                        # 计算斜率
                        m = (interp_max - interp_min)/(ir_max - ir_min)

                        # 计算线性换算值
                        ir2 = m*(ir - ir_min)

                        # 换算成 uint8
                        ir2 = ir2.astype('uint8')

                        # 把原始 IR 图线性化，介于 (0, 255) 之间
                        # 以下代码出现问题，不然就直接调用
                        # ir2 = np.interp(ir, ir_min, ir_max, (0, 255)).astype('uint8')

                        return ir2

                    # 把原始 IR 图线性化，介于 (0, 255) 之间
                    # sort_ir = np.sort(ir.flatten())
                    # ir_max = sort_ir[param.ir_sort_max_count]
                    # ir = interp(ir, ir_min=0, ir_max=ir_max, interp_min=0, interp_max=255)

                    # Function below is only used for '8132', 'face_001.csv' data
                    ir = interp2(ir, ir_min=100, ir_max=200, interp_min=0, interp_max=255)

                    # 生成 MTCNN 模型
                    # device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
                    # print('Running on device: {}'.format(device))
                    # mtcnn = MTCNN(margin=20, thresholds=[0.1, 0.1, 0.1], min_face_size=5, keep_all=False, device='cpu')
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    # 把图转换成三维
                    img = np.dstack([ir]*3)

                    # 检测人脸，提取人脸窗口
                    landmarks = False
                    if landmarks is True:
                        boxes, probs, points = mtcnn.detect(img, landmarks=True)
                    else:
                        boxes, probs = mtcnn.detect(img, landmarks=False)

                    # 如果检测到人脸
                    if boxes is not None:

                        # 在人脸画一个窗口
                        x1, y1, x2, y2 = boxes[0][0], boxes[0][1], boxes[0][2], boxes[0][3]
                        img = cv2.rectangle(img, pt1=(x1, y1), pt2=(x2, y2), color=(255, 255, 255), thickness=1)

                        # Plot the location of the features
                        if landmarks is True:
                            for x, y in points[0]:
                                img = cv2.circle(img, (x, y), radius=1, color=(0, 0, 255), thickness=1)

                    # 显示图
                    # cv2.imshow('Image', img)
                    # cv2.imwrite(f'img_{i:04d}.jpg', img)

                    return img

                def calculate_total_parameters_bad_method(self):

                    '''
                    This subfunction analyze the total number of prameters in the neural network
                    :return:
                    total_parameters: total number of parameters for mtcnn network
                    count_pnet: total parameters for pnet
                    count_rnet: total parameters for rnet
                    count_onet: total parameters for onet
                    output_pnet: [name, multiplication, size] for each layer of pnet
                    output_rnet: [name, multiplication, size] for each layer of net
                    output_onet: [name, multiplication, size] for each layer of onet
                    '''

                    # Import the MTCNN network
                    from facenet_pytorch import MTCNN
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    # Define function that list all the parameters for the corresponding network
                    def mtcnn_count_params(model):

                        # obtain the name and parameters pair
                        nn = [p for p in model.named_parameters()]

                        # Initialize count variable to store the total parameters
                        count = 0

                        # Output store the [name, multiplication factors, total parameters]
                        output = list()

                        # Loop through all the layers
                        for i in range(np.shape(nn)[0]):

                            # Obtain the size of the corresponding layer
                            size = np.shape(nn[i][1])

                            # Initialize the count variable for the corresponding tensor for each layer
                            sum_param = 1

                            # Initialize the string variable to store the tensor's size
                            string_multiply = ''

                            # Loop through the size of the tensor
                            for j, k in enumerate(size):

                                # Put the * sign for non last term
                                if 0 <= j < len(size) - 1:

                                    string_multiply = string_multiply + str(k) + '*'

                                elif j == len(size) - 1:

                                    string_multiply = string_multiply + str(k)

                                else:

                                    pass

                                # Calculate the total parameters for this layer
                                sum_param = sum_param * k

                            # Sum to the total parameters
                            count = count + sum_param

                            # Append the multiplication result for each layer
                            output.append([nn[i][0], string_multiply, sum_param])

                        return count, output

                    # Calculate the total parameters for each neural network
                    count_pnet, output_pnet = mtcnn_count_params(mtcnn.pnet)
                    count_rnet, output_rnet = mtcnn_count_params(mtcnn.rnet)
                    count_onet, output_onet = mtcnn_count_params(mtcnn.onet)

                    # Print total parameters for each neural network
                    print(f'Pnet:{count_pnet}, Rnet:{count_rnet}, Onet:{count_onet}')

                    # Print total number of parameters
                    total_parameters = count_pnet + count_rnet + count_onet

                    return total_parameters, count_pnet, count_rnet, count_onet, \
                            output_pnet, output_rnet, output_onet

                def calculate_total_parameters(self):

                    '''
                    This subfunction analyze the total number of prameters in the neural network
                    :return:
                    total_parameters: total number of parameters for mtcnn network
                    count_pnet: total parameters for pnet
                    count_rnet: total parameters for rnet
                    count_onet: total parameters for onet
                    output_pnet: [name, multiplication, size] for each layer of pnet
                    output_rnet: [name, multiplication, size] for each layer of net
                    output_onet: [name, multiplication, size] for each layer of onet
                    '''

                    # Import the MTCNN network
                    from facenet_pytorch import MTCNN
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    # Define function that list all the parameters for the corresponding network
                    def mtcnn_count_params(model):

                        # obtain the name and parameters pair
                        nn = [p for p in model.named_parameters()]

                        # Names of the network's layer
                        names = [nn[i][0] for i in range(np.shape(nn)[0])]

                        # Total elements for each layer
                        shape_nn = [nn[i][1].numel() for i in range(np.shape(nn)[0])]

                        # Shape for each layer
                        shape_nn_star = [list(np.shape(nn[i][1])).__str__().replace(', ', '*')[1:-1] for i in range(np.shape(nn)[0])]

                        # Output all the relevant results
                        output = [i for i in zip(names, shape_nn_star, shape_nn)]

                        # Total parameters for the model
                        count = np.sum(shape_nn)

                        return count, output

                    # Calculate the total parameters for each neural network
                    count_pnet, output_pnet = mtcnn_count_params(mtcnn.pnet)
                    count_rnet, output_rnet = mtcnn_count_params(mtcnn.rnet)
                    count_onet, output_onet = mtcnn_count_params(mtcnn.onet)

                    # Print total parameters for each neural network
                    print(f'Pnet:{count_pnet}, Rnet:{count_rnet}, Onet:{count_onet}')

                    # Print total number of parameters
                    total_parameters = count_pnet + count_rnet + count_onet

                    return total_parameters, count_pnet, count_rnet, count_onet, \
                            output_pnet, output_rnet, output_onet

                def calculate_ops(self):

                    '''
                    This function calculates the total ops for each cascaded network (Pnet, Rnet and Onet)
                    Return 4 different types of counts (addition and subtraction, multiplication, if-else, exp)
                    :return:
                    '''

                    # Import the MTCNN network
                    from facenet_pytorch import MTCNN
                    mtcnn = MTCNN(keep_all=True, device='cpu')
                    import re

                    def obtain(model):

                        # Convert model to string
                        string_model = str(next(model.modules()))

                        # Split the string with '\n'
                        string_split = string_model.split('\n')

                        # Remove spacing
                        output = [i.replace(' ', '') for i in string_split]

                        # Remove the first and last row
                        output = output[1:-1]

                        return output

                    def generate_input_output_size(model):

                        '''
                        Generate the input shape and output shape for each layer
                        :return:
                        '''

                        if model == 'pnet':

                            input_shape = ['(conv1):12*12*3',
                                           '(prelu1):10*10*10',
                                           '(pool1):10*10*10',
                                           '(conv2):5*5*10',
                                           '(prelu2):3*3*16',
                                           '(conv3):3*3*16',
                                           '(prelu3):1*1*32',
                                           '(conv4_1):1*1*32',
                                           '(softmax4_1):1*1*2',
                                           '(conv4_2):1*1*32']

                            output_shape = ['(conv1):10*10*10',
                                            '(prelu1):10*10*10',
                                            '(pool1):5*5*10',
                                            '(conv2):3*3*16',
                                            '(prelu2):3*3*16',
                                            '(conv3):1*1*32',
                                            '(prelu3):1*1*32',
                                            '(conv4_1):1*1*2',
                                            '(softmax4_1):1*1*2',
                                            '(conv4_2):1*1*4']

                        elif model == 'rnet':

                            input_shape = ['(conv1):24*24*3',
                                           '(prelu1):22*22*28',
                                           '(pool1):22*22*28',
                                           '(conv2):11*11*28',
                                           '(prelu2):9*9*48',
                                           '(pool2):9*9*48',
                                           '(conv3):4*4*48',
                                           '(prelu3):3*3*64',
                                           '(conv4):4*4*64',
                                           '(prelu4):3*3*128',
                                           '(dense5):576',
                                           '(prelu5):128',
                                           '(dense6_1):128',
                                           '(softmax6_1):2',
                                           '(dense6_2):4',
                                           '(dense6_3):10']

                            output_shape = ['(conv1):22*22*28',
                                           '(prelu1):22*22*28',
                                           '(pool1):11*11*28',
                                           '(conv2):9*9*48',
                                           '(prelu2):9*9*48',
                                           '(pool2):4*4*48',
                                           '(conv3):3*3*64',
                                           '(prelu3):3*3*64',
                                           '(dense4):128',
                                           '(prelu4):128',
                                           '(dense5_1):2',
                                           '(softmax5_1):2',
                                           '(dense5_2):4']

                        elif model == 'onet':

                            input_shape = ['(conv1):48*48*3',
                                           '(prelu1):46*46*32',
                                           '(pool1):46*46*32',
                                           '(conv2):23*23*32',
                                           '(prelu2):21*21*64',
                                           '(pool2):21*21*64',
                                           '(conv3):10*10*64',
                                           '(prelu3):8*8*64',
                                           '(pool3):4*4*64',
                                           '(conv4):4*4*64',
                                           '(prelu4):3*3*128',
                                           '(dense5):3*3*128',
                                           '(prelu5):256',
                                           '(dense6_1):256',
                                           '(softmax6_1):2',
                                           '(dense6_2):256',
                                           '(dense6_3):256']

                            output_shape = ['(conv1):46*46*32',
                                           '(prelu1):46*46*32',
                                           '(pool1):23*23*32',
                                           '(conv2):21*21*64',
                                           '(prelu2):21*21*64',
                                           '(pool2):10*10*64',
                                           '(conv3):8*8*64',
                                           '(prelu3):8*8*64',
                                           '(pool3):4*4*64',
                                           '(conv4):3*3*128',
                                           '(prelu4):3*3*128',
                                           '(dense5):256',
                                           '(prelu5):256',
                                           '(dense6_1):2',
                                           '(softmax6_1):2',
                                           '(dense6_2):4',
                                           '(dense6_3):10']

                        return input_shape, output_shape

                    def count_conv(text, layer_structure, input_shape_n, output_shape_n):

                        # (number of kernels, kernel height, kernel width, kernel depth)
                        n_kernels, ker_h, ker_w, ker_d = (int(layer_structure[3]), int(layer_structure[5]),
                                                          int(layer_structure[6]), int(layer_structure[2]))

                        # width, height, depth of input matrix
                        h1, w1, d1 = int(input_shape_n[1]), int(input_shape_n[2]), int(input_shape_n[3])

                        # width, height, depth of output matrix
                        h2, w2, d2 = int(output_shape_n[1]), int(output_shape_n[2]), int(output_shape_n[3])

                        # Write the padding and stride
                        padding, stride = 0, (int(layer_structure[8]), int(layer_structure[9]))

                        # Multiplication count
                        count_multiplication = (ker_h * ker_w * ker_d) * (h2 * w2) * n_kernels

                        # Addition subtraction count
                        count_add_subtract = (ker_h * ker_w * ker_d - 1) * (h2 * w2) * n_kernels

                        # Addition count contributed from the bias
                        count_add_subtract += n_kernels

                        string_line = '*' * 50
                        print(
                            f'{text}\nmultiplication count:{count_multiplication}\naddition subtraction count:{count_add_subtract}\n{string_line}')

                        return count_add_subtract, count_multiplication

                    def count_prelu(text, layer_structure, input_shape_n, output_shape_n):

                        # Prelu formula is y = alpha*x if x < 0 else x
                        # Assume maximum multiplication
                        count_multiplication = int(layer_structure[-1])

                        string_line = '*' * 50
                        print(f'{text}\nmultiplication count:{count_multiplication} \n {string_line}')

                        return count_multiplication

                    def count_pool(text, layer_structure, input_shape_n, output_shape_n):

                        kernel_size = (int(layer_structure[3]))

                        # width, height, depth of input matrix
                        h1, w1, d1 = int(input_shape_n[1]), int(input_shape_n[2]), int(input_shape_n[3])

                        # width, height, depth of output matrix
                        h2, w2, d2 = int(output_shape_n[1]), int(output_shape_n[2]), int(output_shape_n[3])

                        count = d2 * (kernel_size * kernel_size * h2) * (kernel_size * kernel_size * w2)

                        string_line = '*' * 50
                        print(f'{text}\nif-else count:{count} \n {string_line}')

                        return count

                    def count_dense(text, layer_structure, input_shape_n, output_shape_n):

                        # Input size for the dense layer
                        n_i = int(layer_structure[3])

                        # Output size for the dense layer
                        n_o = int(layer_structure[5])

                        # Multiplication contributed from the weights (w1x1, w2x2, ..., w_n*x_n)
                        count_multiplication = n_i * n_o

                        # Addition or subtraction contributed from the weights (w1x1 + w2x2 + ... + w_n*x_n)
                        count_add_subtract = (n_i - 1) * n_o

                        # Addition or subtraction contributed from the bias term  (b1, b2, ..., b_n_o)
                        count_add_subtract += n_o

                        string_line = '*' * 50
                        print(f'{text}\nmultiplication count:{count_multiplication}\naddition subtraction count:{count_add_subtract}\n{string_line}')

                        return count_add_subtract, count_multiplication

                    def count_parameters_model(model, model_name):

                        '''
                        This function count the total parameters for the neural network
                        :param model: model's name (mtcnn.pnet, mtcnn.rnet, or mtcnn.onet)
                        :param model_name: string type ('pnet', 'rnet', or 'onet')
                        :return:
                        '''

                        # Convert model to string
                        str_model = obtain(model)

                        # Initialize the counts
                        count_add_subtract, count_multiplication, count_if_else, count_exp = 0, 0, 0, 0

                        # Input shape of the model
                        input_shape, output_shape = generate_input_output_size(model_name)

                        for n, text in enumerate(str_model):

                            # Get the name of the network's layer
                            pattern = ':|\,|\=|\(|\)|\*'
                            name_list = re.split(pattern, text)
                            input_shape_n = re.split(pattern, input_shape[n])
                            output_shape_n = re.split(pattern, output_shape[n])

                            # Rermove ''
                            layer_structure = [i for i in name_list if i is not '']
                            input_shape_n = [i for i in input_shape_n if i is not '']
                            output_shape_n = [i for i in output_shape_n if i is not '']

                            # Obtain the name of the current layer and its neural name (Pytorch's definition)
                            layer_name, nn_layer_name = layer_structure[0], layer_structure[1]

                            if nn_layer_name == 'Conv2d':  # Convolution

                                count1, count2 = count_conv(text, layer_structure, input_shape_n, output_shape_n)

                                count_add_subtract += count1
                                count_multiplication += count2

                            elif nn_layer_name == 'PReLU':  # Activation - Prelu

                                count_multiplication += count_prelu(text, layer_structure, input_shape_n, output_shape_n)

                            elif nn_layer_name == 'MaxPool2d':  # Pooling - Max Pooling

                                count_if_else += count_pool(text, layer_structure, input_shape_n, output_shape_n)

                            elif nn_layer_name == 'Linear':  # Dense layer

                                count_linear_add_subtract, count_linear_multiplication = count_dense\
                                    (text, layer_structure, input_shape_n, output_shape_n)

                                count_add_subtract += count_linear_add_subtract
                                count_multiplication += count_linear_multiplication

                            elif nn_layer_name == 'Softmax':  # Softmax layer

                                count_exp += 3

                                string_line = '*' * 50
                                print(f'{text}\nexp count:{count_exp} \n {string_line}')

                            else:

                                print(f'neural network layer not found!!!')
                                print(f'{text}')
                                pass

                        return count_add_subtract, count_multiplication, count_if_else, count_exp

                    print('Pnet')
                    count_pnet_add_subtract, count_pnet_multiplication, count_pnet_if_else, count_pnet_exp = \
                        count_parameters_model(mtcnn.pnet, 'pnet')

                    print('Rnet')
                    count_rnet_add_subtract, count_rnet_multiplication, count_rnet_if_else, count_rnet_exp = \
                        count_parameters_model(mtcnn.rnet, 'rnet')

                    print('Onet')
                    count_onet_add_subtract, count_onet_multiplication, count_onet_if_else, count_onet_exp = \
                        count_parameters_model(mtcnn.onet, 'onet')

                    print(f'Pnet \n {count_pnet_add_subtract, count_pnet_multiplication, count_pnet_if_else, count_pnet_exp} \n'
                          f'Rnet \n {count_rnet_add_subtract, count_rnet_multiplication, count_rnet_if_else, count_rnet_exp} \n'
                          f'Onet \n {count_onet_add_subtract, count_onet_multiplication, count_onet_if_else, count_onet_exp} \n')

                    return None

                def forward_test(self):

                    # Calculated output size may not tally exactly using formula (for pooling, not for convolution)
                    # So I calculate the forward output values for each layer
                    # Check the size of the output allows me to know the exact output size
                    # Import the MTCNN network

                    from facenet_pytorch import MTCNN
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    torch.manual_seed(0)

                    # The following are from Onet with random number as input

                    y_conv1 = mtcnn.onet.conv1.forward(torch.randint(0, 255, (1, 3, 48, 48)).float())
                    y_prelu1 = mtcnn.onet.prelu1.forward(y_conv1)
                    y_pool1 = mtcnn.onet.pool1.forward(y_prelu1)

                    y_conv2 = mtcnn.onet.conv2.forward(y_pool1)
                    y_prelu2 = mtcnn.onet.prelu2.forward(y_conv2)
                    y_pool2 = mtcnn.onet.pool2.forward(y_prelu2)

                    y_conv3 = mtcnn.onet.conv3.forward(y_pool2)
                    y_prelu3 = mtcnn.onet.prelu3.forward(y_conv3)
                    y_pool3 = mtcnn.onet.pool3.forward(y_prelu3)

                    y_conv4 = mtcnn.onet.conv4.forward(y_pool3)
                    y_prelu4 = mtcnn.onet.prelu4.forward(y_conv4)

                    y_dense5 = mtcnn.onet.dense5.forward(y_prelu4.view(1, -1))  # Or can replace view(1, -1) with flatten(1, 3)
                    y_prelu5 = mtcnn.onet.prelu5.forward(y_dense5)

                    y_dense6_1 = mtcnn.onet.dense6_1.forward(y_prelu5)

                    # Output the probability that it is a face (2)
                    y_softmax6_1 = mtcnn.onet.softmax6_1.forward(y_dense6_1)

                    # Output the bounding box (4)
                    y_dense6_2 = mtcnn.onet.dense6_2.forward(y_prelu5)

                    # Output the face landmarks (10)
                    y_dense6_3 = mtcnn.onet.dense6_3.forward(y_prelu5)

                    return None

                def save_parameters_to_text(self):

                    '''
                    这个函数是输出神经网络的权重与系数
                    :return:
                    '''

                    from facenet_pytorch import MTCNN
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    #############################################################################################

                    conv1_w = mtcnn.pnet.conv1.weight.detach().numpy()
                    conv1_b = mtcnn.pnet.conv1.bias.detach().numpy()

                    # filename
                    conv1_w_filename = str(np.shape(conv1_w))[1:-1].replace(', ', '_')
                    conv1_b_filename = str(len(conv1_b))

                    # .reshape(-1, 3) 是一个很好的点子，决定了以后所有权重输出的格式
                    # 张量格式转换乘2维矩阵格式
                    np.savetxt(f'conv1_weight_{conv1_w_filename}.txt', conv1_w.reshape(-1, 3), fmt='%.32f')  # convolutional kernel with widht 3
                    np.savetxt(f'conv1_bias_{conv1_b_filename}.txt', conv1_b.reshape(-1, 1), fmt='%.32f')

                    #############################################################################################

                    # pnet.conv2

                    conv2_w = mtcnn.pnet.conv2.weight.detach().numpy()
                    conv2_b = mtcnn.pnet.conv2.bias.detach().numpy()

                    # filename
                    conv2_w_filename = str(np.shape(conv2_w))[1:-1].replace(', ', '_')
                    conv2_b_filename = str(len(conv2_b))

                    # .reshape(-1, 3) 是一个很好的点子，决定了以后所有权重输出的格式
                    # 张量格式转换乘2维矩阵格式
                    np.savetxt(f'conv2_weight_{conv2_w_filename}.txt', conv2_w.reshape(-1, 3),
                               fmt='%.32f')  # convolutional kernel with widht 3
                    np.savetxt(f'conv2_bias_{conv2_b_filename}.txt', conv2_b.reshape(-1, 1), fmt='%.32f')

                    #############################################################################################

                    # 从 pnet class 里的 forward 的 x = self.conv1(x) 储存为 input 和 output
                    np.savetxt(f'input_conv1.txt', x.reshape(-1, 22), fmt='%.32f')
                    np.savetxt(f'output_conv1.txt', x.reshape(-1, 20), fmt='%.32f')

                    # Prelu1
                    np.savetxt(f'input_prelu1.txt', x.reshape(-1, 18), fmt='%.32f')
                    np.savetxt(f'output_prelu1.txt', x.reshape(-1, 18), fmt='%.32f')
                    prelu1 = next(mtcnn.pnet.prelu1.parameters()).detach().numpy()
                    np.savetxt(f'prelu1_weight.txt', prelu1.reshape(-1, 1), fmt='%.32f')

                    # Pool1
                    np.savetxt(f'input_pool1.txt', x.reshape(-1, 18), fmt='%.32f')
                    np.savetxt(f'output_pool1.txt', x.reshape(-1, 9), fmt='%.32f')
                    pool1 = str(mtcnn.pnet.pool1)
                    with open('pool1_weight.txt', 'w') as fd:
                        fd.write(pool1)

                    return None

                def save_parameters_to_text_automatic(self):

                    '''
                    这个函数是输出神经网络的权重与系数
                    :return:
                    '''

                    from facenet_pytorch import MTCNN
                    mtcnn = MTCNN(keep_all=True, device='cpu')

                    def obtain(model):

                        # Convert model to string
                        string_model = str(next(model.modules()))

                        # Split the string with '\n'
                        string_split = string_model.split('\n')

                        # Remove spacing
                        output = [i.replace(' ', '') for i in string_split]

                        # Remove the first and last row
                        output = output[1:-1]

                        return output

                    def save_all_coefficients_to_txt(model, name_list, name_model):

                        # Put the coefficients inside a folder with model as its name
                        if os.path.isdir(name_model) is False:
                            os.mkdir(name_model)

                        for i, name in enumerate(model):

                            print(model[i])
                            saved_folder = os.path.join(name_model, name_list[i])
                            print(saved_folder)
                            if os.path.isdir(saved_folder) is False:
                                os.mkdir(saved_folder)

                            # Split the model's name and parameters for each line
                            param = [j for j in re.split(pattern='\:|\(|\,|\=|\)', string=model[i]) if j is not '']

                            if 'Conv2d' in name:

                                # Extract the kernel size
                                kernel_size = (int(param[5]), int(param[6]))

                                # Define filename to be saved
                                w_fn = f'ker{param[3]}_h{param[5]}_w{param[6]}_d{param[2]}_stride{param[8]}'
                                b_fn = f'ker{param[3]}'
                                w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')
                                b_fn2 = os.path.join(saved_folder, f'{name_list[i]}_bias_{b_fn}.txt')

                                # Extract the coefficients
                                w = eval(f'mtcnn.{name_model}.{name_list[i]}.weight.detach().numpy()')
                                b = eval(f'mtcnn.{name_model}.{name_list[i]}.bias.detach().numpy()')

                                # Save all the coefficients for both weight and bias
                                np.savetxt(w_fn2, w.reshape(-1, kernel_size[1]), fmt='%.32f')
                                np.savetxt(b_fn2, b.reshape(-1, 1), fmt='%.32f')

                            elif 'PReLU' in name:

                                # Define filename to be saved
                                w_fn = f'ker{param[3]}'
                                w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')

                                # Extract the coefficients
                                w = eval(f'mtcnn.{name_model}.{name_list[i]}.weight.detach().numpy()')

                                # Save all the coefficients for the weight
                                np.savetxt(w_fn2, w.reshape(-1, 1), fmt='%.32f')

                            elif 'Linear' in name:

                                size = (int(param[3]), int(param[5]))

                                # Define filename to be saved
                                w_fn = f'in{param[3]}_out{param[5]}'
                                b_fn = f'{param[5]}'
                                w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')
                                b_fn2 = os.path.join(saved_folder, f'{name_list[i]}_bias_{b_fn}.txt')

                                # Extract the coefficients
                                w = eval(f'mtcnn.{name_model}.{name_list[i]}.weight.detach().numpy()')
                                b = eval(f'mtcnn.{name_model}.{name_list[i]}.bias.detach().numpy()')

                                # Save all the coefficients for both weight and bias
                                np.savetxt(w_fn2, w.T, fmt='%.32f')
                                np.savetxt(b_fn2, b.reshape(-1, 1), fmt='%.32f')

                            else:
                                pass

                    # Details of layers
                    pnet_model = obtain(mtcnn.pnet)
                    rnet_model = obtain(mtcnn.rnet)
                    onet_model = obtain(mtcnn.onet)

                    # Name for each layer
                    pnet_name_list = [name.split(':')[0][1:-1] for name in pnet_model]
                    rnet_name_list = [name.split(':')[0][1:-1] for name in rnet_model]
                    onet_name_list = [name.split(':')[0][1:-1] for name in onet_model]

                    # Save all the coefficients of the corresponding model
                    save_all_coefficients_to_txt(pnet_model, pnet_name_list, 'pnet')
                    save_all_coefficients_to_txt(rnet_model, rnet_name_list, 'rnet')
                    save_all_coefficients_to_txt(onet_model, onet_name_list, 'onet')

            class mtcnn_cks():

                def __init__(
                        self, image_size=160, margin=0, min_face_size=20,
                        thresholds=[0.6, 0.7, 0.7], factor=0.709, post_process=True,
                        select_largest=True, selection_method=None, keep_all=False, device=None,
                        img_h=100, img_w=100,
                        pnet_Qnm_sig=None, pnet_Qnm_n=None, pnet_Qnm_m=None, pnet_Qnm_qformat=None,
                        rnet_Qnm_sig=None, rnet_Qnm_n=None, rnet_Qnm_m=None, rnet_Qnm_qformat=None,
                        mode=2
                ):

                    super().__init__()

                    # 工作模式
                    self.mode = mode  # 模式零 (rnet 0 次), 模式一 (rnet 1 次)，模式二 (rnet 2 次) (物理意义: Rnet 跑的次数)

                    self.image_size = image_size
                    self.margin = margin
                    self.min_face_size = min_face_size
                    self.thresholds = thresholds  # [Pnet, Rnet, Onet] 的人脸检测概率阈值
                    self.inv_thresholds = [np.log(1/i-1) for i in thresholds]  # 这么定义的原因是不调用 softmax 函数
                    self.factor = factor
                    self.post_process = post_process
                    self.select_largest = select_largest
                    self.keep_all = keep_all
                    self.selection_method = selection_method
                    self.h, self.w = img_h, img_w
                    self.scales, self.sizes = self.scale_pyramid()
                    self.pnet_iou_thresh = [0.5, 0.7]
                    self.rnet_iou_thresh = 0.7

                    # remove_boxes_through_distance 函数调用的参数
                    # IR 图与 depth 图的尺度可能不一样 (depth 40*32 图是 IR 图的一半 80*64, depth_ir_size_ratio=0.5)
                    # 如果 IR 图与 depth 图的尺度一样, 则 depth_ir_size_ratio = 1
                    self.depth_filter_on_off = True  # 深度图筛选窗口算法开关按键, 如果关掉, 只有 IR 做人脸检测。大量的窗口导致耗时比较长
                    self.depth_ir_size_ratio = 0.5
                    self.head_height = (13, 33)  # 人脸高度范围 (14, 33) 的意思: 14 - 33 厘米之间
                    self.head_width = (11, 29)  # 人脸宽度范围 (13, 29) 的意思: 13 - 29 厘米之间
                    self.TOF_FOV_height = 60 / 180*np.pi  # 模组高度方向的 FOV (单位: radian 所以除于180*pi)
                    self.TOF_FOV_width = 45 / 180*np.pi  # 模组宽度方向的 FOV (单位: radian 所以除于180*pi)
                    # arctan 表
                    self.x_arctan = np.array([0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.2, 2.4, 2.6, 2.8, 3, 3.2, 3.4, 3.6, 3.8, 4])
                    self.y_arctan = np.array([0, 0.1970, 0.3810, 0.5400, 0.6750, 0.7850, 0.8760, 0.9510, 1.0120, 1.0640, 1.1070, 1.1440, 1.1760, 1.2040, 1.2280, 1.2490, 1.2680, 1.2850, 1.3000, 1.3130, 1.3260])

                    self.head_distance_all = []  # 'distance', 'head_width', 'head_height'

                    # 如果 Pnet 窗口的最大置信度值大于阈值, 直接输出结果。跳过 Rnet (自适应算法)
                    # 可以定义为一个比较大的阈值 (等于关掉) (例子: 10)
                    self.prob_pnet_max_direct_output = np.log2(1/0.97 - 1)  # 0.97  # np.log2(1/0.97 - 1)

                    # 神经网络输出窗口数量控制
                    # Pnet 输出窗口数量 (keep_max_pnet_prob_boxes 函数调用)
                    self.pnet_boxes_num_max = 2 if self.mode == 2 else 1
                    # Rnet 输出窗口数量 (keep_max_rnet_prob_boxes 函数调用)
                    self.rnet_boxes_num_max = 1

                    # 定义 Pnet Rnet 系数的结构
                    coef_format = {'pnet':
                                   {'conv1': 'Q4.12',
                                    'prelu1': 'Q3.13',
                                    'pool1': None,
                                    'conv2': 'Q3.13',
                                    'prelu2': 'Q3.13',
                                    'conv3': 'Q3.13',
                                    'prelu3': 'Q3.13',
                                    'conv4_1': 'Q3.13',
                                    'softmax4_1': None,
                                    'conv4_2': 'Q3.13'},
                               'rnet':
                                   {'conv1': 'Q2.14',
                                    'prelu1': 'Q2.14',
                                    'pool1': None,
                                    'conv2': 'Q2.14',
                                    'prelu2': 'Q2.14',
                                    'pool2': None,
                                    'conv3': 'Q2.14',
                                    'prelu3': 'Q2.14',
                                    'dense4': 'Q2.14',
                                    'prelu4': 'Q2.14',
                                    'dense5_1': 'Q2.14',
                                    'softmax5_1': None,
                                    'dense5_2': 'Q2.14'}}

                    # 定义系数 w 和 b 的 Qn.m 值
                    self.coef_format = self.define_coef_format(coef_format)

                    # 从原图 pixel_num (长乘宽 h*w) 抽取 i 个值，作为降采样的用途
                    # stop = pixel_num-1, 因为从0开始计算，随意索引的最大值是大小-1
                    f = lambda pixel_num: {i: np.int16(np.linspace(start=0, stop=pixel_num-1, num=i)) for i in range(12, pixel_num)}
                    self.resize_arg_idx_h = f(pixel_num=self.h)
                    self.resize_arg_idx_w = f(pixel_num=self.w)

                    self.device = torch.device('cpu')
                    if device is not None:
                        self.device = device
                        self.to(device)

                    if not self.selection_method:
                        self.selection_method = 'largest' if self.select_largest else 'probability'

                def arc_tan_func_lut(self, x_vec, y_vec, xi):

                    """
                    通过查表的方式计算 arctan 函数
                    x1=x_vec[idx1] <= xi < x2=x_vec[idx2]
                    y1=y_vec[idx1] <= yi < y2=y_vec[idx2]
                    :param x_vec: arctan 函数的 x 值表
                    :param y_vec: arctan 函数的 y 值表
                    :param xi: 需要计算的 xi 值
                    :return:
                    """

                    idx1, idx2 = 0, 0
                    assigned1, assigned2 = False, False

                    # 计算 x1 <= xi < x2 里, x1 与 x2 在表里的相应值
                    for i in range(len(x_vec)):

                        # 从i=0循环，刚开始的时候 xi 都是大于 x_vec[i] 里的值的
                        if xi - x_vec[i] >= 0:
                            x1 = x_vec[i]
                            idx1 = i
                            assigned1 = True
                        else:
                            # 当 xi < x_vec[i] 的时候，意味着拐点出现, x1 < xi < x2, x2 出现了
                            x2 = x_vec[i]
                            idx2 = i
                            assigned2 = True
                            break  # 既然找到x2和idx2了, 直接跳出

                    # 如果 没有找到 idx1 或者 idx2, 直接输出 y_vec 里的最大值(最后一个值)
                    # 这个情况可能性比较低, 但是为了防止后续的计算错误，还是添加这一行代码
                    # 出现情况的原因是 xi 值非常大, 大于表里最后一个值 x_vec[-1] (意义: 出现的人脸角度非常大) (depth 值出现错误)
                    if (assigned1 is False) or (assigned2 is False):
                        yi = y_vec[-1]
                        return yi

                    # 从表里, 相应 idx 下的 y 值
                    y1 = y_vec[idx1]
                    y2 = y_vec[idx2]

                    # 线性插值计算
                    yi = y1 + (y2 - y1) / (x2 - x1) * (xi - x1)

                    return yi

                def define_coef_format(self, coef_format):

                        # Parse the format
                        def parse_format(fmt):

                            n_sign, n_int, n_frac = None, None, None

                            if fmt is not None:
                                fmt2 = fmt.split('.')

                                n_sign = 1
                                n_int = int(fmt2[0][1:]) - 1
                                n_frac = int(fmt2[1])

                            return n_sign, n_int, n_frac

                        # Create a variable to store the dictionary
                        coef_format_new = dict()

                        for key1, value1 in coef_format.items():

                            coef_format_new[key1] = {}

                            for key2, value2 in coef_format[key1].items():
                                # Parse the format
                                n_sign, n_int, n_frac = parse_format(coef_format[key1][key2])

                                # Extract the Qn.m type
                                type = coef_format[key1][key2]

                                # Assign the dictionary {type, n_sign, n_int, n_frac}
                                coef_format_new[key1][key2] = {'type': type, 'n_sign': n_sign, 'n_int': n_int,
                                                               'n_frac': n_frac}

                        return coef_format_new

                def load_image(self, selection='grayscale'):

                    import os
                    import numpy as np

                    def interp(ir, ir_min, ir_max, interp_min=0, interp_max=255):

                        # IR 值如果太小，进行上下限处理
                        ir_max = np.max([ir.max(), ir_max])
                        ir_min = np.max([ir.min(), ir_min])
                        ir[ir < ir_min] = ir_min
                        ir[ir > ir_max] = ir_max

                        # 计算斜率
                        m = (interp_max - interp_min)/(ir_max - ir_min)

                        # 计算线性换算值
                        ir2 = m*(ir - ir_min)

                        # 换算成 uint8
                        ir2 = ir2.astype('uint8')

                        # 把原始 IR 图线性化，介于 (0, 255) 之间
                        # 以下代码出现问题，不然就直接调用
                        # ir2 = np.interp(ir, ir_min, ir_max, (0, 255)).astype('uint8')

                        return ir2

                    csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                            'face_001.csv')

                    ir = np.genfromtxt(csv_path, delimiter=',').astype(np.float32)

                    # Function below is only used for '8132', 'face_001.csv' data
                    ir = interp(ir, ir_min=100, ir_max=200, interp_min=0, interp_max=255)

                    if selection is 'RGB':
                        multiplier = 3
                    elif selection is 'grayscale':
                        multiplier = 1
                    else:
                        pass

                    # 把图转换成三维或者一维
                    img = np.dstack([ir]*multiplier)

                    # numpy array 数据格式转换
                    img = torch.FloatTensor(img)   # 转换成张量
                    img = img.unsqueeze(0)         # 添加一个维度
                    img = img.permute(0, 3, 1, 2)  # permute 位置

                    return img

                def load_image2(self, selection='grayscale'):

                    '''
                    20220401 至  20220701
                    标准数据. 此数据作为金标准，用来完成组内（瞿洋，王昊）的代码生成工作（夫电话，定点化，校验）
                    :param selection:
                    :return:
                    '''

                    import os
                    import numpy as np

                    csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                            'face_001.csv')

                    ir = np.genfromtxt(csv_path, delimiter=',').astype(np.float32)

                    if selection is 'RGB':
                        multiplier = 3
                    elif selection is 'grayscale':
                        multiplier = 1
                    else:
                        pass

                    # 把图转换成三维或者一维
                    img = np.dstack([ir]*multiplier)

                    # numpy array 数据格式转换
                    img = torch.FloatTensor(img)   # 转换成张量
                    img = img.unsqueeze(0)         # 添加一个维度
                    img = img.permute(0, 3, 1, 2)  # permute 位置

                    return img

                def load_image3_8132(self, choice):

                    import numpy as np

                    def load_data_8132_40_32(choice):

                        import os

                        ###########################################################################################

                        data_ir, data_depth = [], []

                        if choice is 1:

                            # 20220707 算法移植到 8132 后（视角为
                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220707_module1_face_at_various_distances')

                            data_ir.append(os.path.join(csv_path, '60cm', '2022-07-07-14-26-40IR.csv'))
                            data_ir.append(os.path.join(csv_path, '100cm', '2022-07-07-14-27-18IR.csv'))
                            data_ir.append(os.path.join(csv_path, '120cm', '2022-07-07-14-27-42IR.csv'))
                            data_ir.append(os.path.join(csv_path, '140cm', '2022-07-07-14-28-00IR.csv'))
                            data_ir.append(os.path.join(csv_path, '160cm', '2022-07-07-14-28-53IR.csv'))
                            data_ir.append(os.path.join(csv_path, '180cm', '2022-07-07-14-29-36IR.csv'))
                            data_ir.append(os.path.join(csv_path, '200cm', '2022-07-07-14-30-01IR.csv'))

                            data_depth.append(os.path.join(csv_path, '60cm', '2022-07-07-14-26-40Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '100cm', '2022-07-07-14-27-18Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '120cm', '2022-07-07-14-27-42Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '140cm', '2022-07-07-14-28-00Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '160cm', '2022-07-07-14-28-53Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '180cm', '2022-07-07-14-29-36Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '200cm', '2022-07-07-14-30-01Depth.csv'))

                        ###########################################################################################

                        elif choice is 2:

                            # depth has to be divided by 4

                            # 20220707 算法移植到 8132 后（视角为
                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220708_module_bad_fov_at_various_distance')

                            data_ir.append(os.path.join(csv_path, '2022-07-08-17-02-52IR.csv'))

                            data_depth.append(os.path.join(csv_path, '2022-07-08-17-02-52Depth.csv'))

                        elif choice is 3:

                            # depth has to be divided by 4

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220727b_校准距离4m_depth_不正确')

                            data_ir.append(os.path.join(csv_path, 'dev_350', '500', '2022-07-27-14-59-56IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_350', '750', '2022-07-27-14-59-19IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_350', '1000', '2022-07-27-14-58-30IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_350', '1250', '2022-07-27-14-57-42IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_350', '1500', '2022-07-27-14-56-33IR.csv'))  # 200 frames
                            data_ir.append(os.path.join(csv_path, 'dev_350', 'general', '2022-07-27-15-01-40IR.csv'))  # 200 frames

                            data_depth.append(os.path.join(csv_path, 'dev_350', '500', '2022-07-27-14-59-56Depth.csv'))  # 50 frames
                            data_depth.append(os.path.join(csv_path, 'dev_350', '750', '2022-07-27-14-59-19Depth.csv'))  # 50 frames
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1000', '2022-07-27-14-58-30Depth.csv'))  # 50 frames
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1250', '2022-07-27-14-57-42Depth.csv'))  # 50 frames
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1500', '2022-07-27-14-56-33Depth.csv'))  # 200 frames
                            data_depth.append(os.path.join(csv_path, 'dev_350', 'general', '2022-07-27-15-01-40Depth.csv'))  # 200 frames

                            data_ir.append(os.path.join(csv_path, 'dev_357', '500', '2022-07-27-15-16-11IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_357', '750', '2022-07-27-15-15-46IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1000', '2022-07-27-15-15-18IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1250', '2022-07-27-15-14-52IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1500', '2022-07-27-15-14-22IR.csv'))  # 50 frames
                            data_ir.append(os.path.join(csv_path, 'dev_357', 'general', '2022-07-27-15-17-09IR.csv'))  # 200 frames

                            data_depth.append(os.path.join(csv_path, 'dev_357', '500', '2022-07-27-15-16-11Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '750', '2022-07-27-15-15-46Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1000', '2022-07-27-15-15-18Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1250', '2022-07-27-15-14-52Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1500', '2022-07-27-15-14-22Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', 'general', '2022-07-27-15-17-09Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_384', '500', '2022-07-27-15-26-50IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '750', '2022-07-27-15-26-26IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1000', '2022-07-27-15-26-03IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1250', '2022-07-27-15-25-38IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1500', '2022-07-27-15-25-08IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', 'general', '2022-07-27-15-27-50IR.csv'))

                            data_depth.append(os.path.join(csv_path, 'dev_384', '500', '2022-07-27-15-26-50Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '750', '2022-07-27-15-26-26Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1000', '2022-07-27-15-26-03Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1250', '2022-07-27-15-25-38Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1500', '2022-07-27-15-25-08Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', 'general', '2022-07-27-15-27-50Depth.csv'))

                        elif choice is 4:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220727a_校准距离2m_depth_不正确')

                            # data_ir.append(os.path.join(csv_path, 'dev_350', '500', '2022-07-27-16-41-19IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', '750', '2022-07-27-16-40-55IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', '1000', '2022-07-27-16-40-32IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', '1250', '2022-07-27-16-40-08IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_350', '1500', '2022-07-27-16-39-43IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', 'general', '2022-07-27-16-42-18IR.csv'))

                            # data_depth.append(os.path.join(csv_path, 'dev_350', '500', '2022-07-27-16-41-19Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '750', '2022-07-27-16-40-55Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '1000', '2022-07-27-16-40-32Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '1250', '2022-07-27-16-40-08Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1500', '2022-07-27-16-39-43Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', 'general', '2022-07-27-16-42-18Depth.csv'))

                            # data_ir.append(os.path.join(csv_path, 'dev_357', '500', '2022-07-27-16-27-03IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_357', '750', '2022-07-27-16-26-39IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_357', '1000', '2022-07-27-16-26-15IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_357', '1250', '2022-07-27-16-25-48IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1500', '2022-07-27-16-25-22IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_357', 'general', '2022-07-27-16-28-02IR.csv'))

                            # data_depth.append(os.path.join(csv_path, 'dev_357', '500', '2022-07-27-16-27-03Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_357', '750', '2022-07-27-16-26-39Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_357', '1000', '2022-07-27-16-26-15Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_357', '1250', '2022-07-27-16-25-48Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1500', '2022-07-27-16-25-22Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_357', 'general', '2022-07-27-16-28-02Depth.csv'))

                            # data_ir.append(os.path.join(csv_path, 'dev_384', '500', '2022-07-27-16-19-49IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_384', '750', '2022-07-27-16-19-28IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_384', '1000', '2022-07-27-16-19-03IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_384', '1250', '2022-07-27-16-18-36IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1500', '2022-07-27-16-18-10IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_384', 'general', '2022-07-27-16-20-54IR.csv'))

                            # data_depth.append(os.path.join(csv_path, 'dev_384', '500', '2022-07-27-16-19-49Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_384', '750', '2022-07-27-16-19-28Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_384', '1000', '2022-07-27-16-19-03Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_384', '1250', '2022-07-27-16-18-36Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1500', '2022-07-27-16-18-10Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_384', 'general', '2022-07-27-16-20-54Depth.csv'))

                        elif choice == 5:

                            # dev 350
                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220801_calib_4m_non_face_objects')

                            data_ir.append(os.path.join(csv_path, '2022-08-01-17-59-14IR.csv'))

                            data_depth.append(os.path.join(csv_path, '2022-08-01-17-59-14Depth.csv'))

                        ###########################################################################################

                        else:
                            pass

                        width, height = 32, 40

                        # depth = depth / 4

                        return data_ir, data_depth, width, height

                    def load_data_8132_80_64(choice):

                        import os

                        ###########################################################################################

                        data_ir, data_depth = [], []

                        if choice == 1:

                            # dev 350
                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220805_8132_80_64_res_80cm')

                            data_ir.append(os.path.join(csv_path, 'ir.csv'))
                            data_depth.append(os.path.join(csv_path, 'fake_depth.csv'))

                        elif choice is 2:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220808_8132_80_64_res_50_75_100_125_150cm_distance')

                            # data_ir.append(os.path.join(csv_path, 'dev_350', '500', '2022-08-08-13-30-37IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', '750', '2022-08-08-13-31-19IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_350', '1000', '2022-08-08-13-31-58IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', '1250', '2022-08-08-13-32-37IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', '1500', '2022-08-08-13-33-11IR.csv'))

                            # Below are fake depth data
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '500', '2022-08-08-13-30-37Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '750', '2022-08-08-13-31-19Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1000', '2022-08-08-13-31-58Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '1250', '2022-08-08-13-32-37Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', '1500', '2022-08-08-13-33-11Depth.csv'))

                        elif choice is 3:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220810_8132_80_64_res_50_75_100_125_150cm_distance_with_depth')

                            data_ir.append(os.path.join(csv_path, 'dev_350', '50', '2022-08-10-15-18-01IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_350', '75', '2022-08-10-15-18-39IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_350', '100', '2022-08-10-15-19-16IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_350', '125', '2022-08-10-15-19-58IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_350', '150', '2022-08-10-15-20-41IR.csv'))
                            # data_ir.append(os.path.join(csv_path, 'dev_350', 'non_human', '2022-08-10-15-22-31IR.csv'))

                            data_depth.append(os.path.join(csv_path, 'dev_350', '50', '2022-08-10-15-18-01Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '75', '2022-08-10-15-18-39Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '100', '2022-08-10-15-19-16Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '125', '2022-08-10-15-19-58Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '150', '2022-08-10-15-20-41Depth.csv'))
                            # data_depth.append(os.path.join(csv_path, 'dev_350', 'non_human', '2022-08-10-15-22-31Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_357', '50', '2022-08-11-15-54-45IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '75', '2022-08-11-15-55-24IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '100', '2022-08-11-15-56-03IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '125', '2022-08-11-15-56-42IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '150', '2022-08-11-15-57-22IR.csv'))
                            #
                            data_depth.append(os.path.join(csv_path, 'dev_357', '50', '2022-08-11-15-54-45Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '75', '2022-08-11-15-55-24Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '100', '2022-08-11-15-56-03Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '125', '2022-08-11-15-56-42Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '150', '2022-08-11-15-57-22Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_384', '50', '2022-08-11-15-46-59IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '75', '2022-08-11-15-47-42IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '100', '2022-08-11-15-48-25IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '125', '2022-08-11-15-49-00IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '150', '2022-08-11-15-49-41IR.csv'))

                            data_depth.append(os.path.join(csv_path, 'dev_384', '50', '2022-08-11-15-46-59Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '75', '2022-08-11-15-47-42Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '100', '2022-08-11-15-48-25Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '125', '2022-08-11-15-49-00Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '150', '2022-08-11-15-49-41Depth.csv'))

                        elif choice is 4:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220812_8132_80_64_res_50cm_testing')

                            # data_ir.append(os.path.join(csv_path, '50', '2022-08-12-18-44-57IR.csv'))
                            data_ir.append(os.path.join(csv_path, '50', '2022-08-12-18-56-48IR.csv'))

                            # data_depth.append(os.path.join(csv_path, '50', '2022-08-12-18-44-57Depth.csv'))
                            data_depth.append(os.path.join(csv_path, '50', '2022-08-12-18-56-48Depth.csv'))

                        elif choice is 5:

                            'Depth data are fake'

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220812b_8132_80_64_res_50_75_100_125_150cm_distance')

                            # 50cm distance
                            data_ir.append(os.path.join(csv_path, 'dev_350', '50', '2022-08-12-19-28-34IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '50', '2022-08-12-19-15-25IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '50', '2022-08-12-19-21-42IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '50', '2022-08-12-19-28-34Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '50', '2022-08-12-19-15-25Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '50', '2022-08-12-19-21-42Depth.csv'))
                            #
                            # # 75cm distance
                            data_ir.append(os.path.join(csv_path, 'dev_350', '75', '2022-08-12-19-29-52IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '75', '2022-08-12-19-15-58IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '75', '2022-08-12-19-22-08IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '75', '2022-08-12-19-29-52Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '75', '2022-08-12-19-15-58Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '75', '2022-08-12-19-22-08Depth.csv'))

                            # # 100cm distance
                            data_ir.append(os.path.join(csv_path, 'dev_350', '100', '2022-08-12-19-30-27IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '100', '2022-08-12-19-16-21IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '100', '2022-08-12-19-22-35IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '100', '2022-08-12-19-30-27Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '100', '2022-08-12-19-16-21Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '100', '2022-08-12-19-22-35Depth.csv'))

                            # 125cm distance
                            data_ir.append(os.path.join(csv_path, 'dev_350', '125', '2022-08-12-19-30-55IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '125', '2022-08-12-19-16-43IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '125', '2022-08-12-19-23-02IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '125', '2022-08-12-19-30-55Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '125', '2022-08-12-19-16-43Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '125', '2022-08-12-19-23-02Depth.csv'))
                            #
                            # # 150cm distance
                            data_ir.append(os.path.join(csv_path, 'dev_350', '150', '2022-08-12-19-31-19IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '150', '2022-08-12-19-17-05IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '150', '2022-08-12-19-23-33IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '150', '2022-08-12-19-31-19Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '150', '2022-08-12-19-17-05Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '150', '2022-08-12-19-23-33Depth.csv'))

                        elif choice is 6:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220822_8132_ir_80_64_depth_40_32_testing')

                            data_ir.append(os.path.join(csv_path, '2022-08-22-15-10-04IR.csv'))
                            data_depth.append(os.path.join(csv_path, '2022-08-22-15-10-04Depth.csv'))

                        elif choice is 7:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220823_8132_ir_80_64_depth_40_32_noise_reduction_20220822_results_bad')

                            data_ir.append(os.path.join(csv_path, '2022-08-23-16-34-29IR.csv'))
                            data_depth.append(os.path.join(csv_path, '2022-08-23-16-34-29Depth.csv'))

                        elif choice is 8:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220823_ir_80_64_depth_40_32_distance')

                            # dev_350
                            data_ir.append(os.path.join(csv_path, 'dev_350', '500', '2022-08-23-16-53-30IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '500', '2022-08-23-17-28-12IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '500', '2022-08-23-18-44-11IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '500', '2022-08-23-16-53-30Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '500', '2022-08-23-17-28-12Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '500', '2022-08-23-18-44-11Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_350', '750', '2022-08-23-16-54-45IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '750', '2022-08-23-17-28-55IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '750', '2022-08-23-18-45-20IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '750', '2022-08-23-16-54-45Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '750', '2022-08-23-17-28-55Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '750', '2022-08-23-18-45-20Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_350', '1000', '2022-08-23-16-55-24IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1000', '2022-08-23-17-29-35IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1000', '2022-08-23-18-46-04IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1000', '2022-08-23-16-55-24Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1000', '2022-08-23-17-29-35Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1000', '2022-08-23-18-46-04Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_350', '1250', '2022-08-23-16-56-00IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1250', '2022-08-23-17-30-06IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1250', '2022-08-23-18-46-44IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1250', '2022-08-23-16-56-00Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1250', '2022-08-23-17-30-06Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1250', '2022-08-23-18-46-44Depth.csv'))

                            data_ir.append(os.path.join(csv_path, 'dev_350', '1500', '2022-08-23-16-56-35IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_357', '1500', '2022-08-23-17-30-39IR.csv'))
                            data_ir.append(os.path.join(csv_path, 'dev_384', '1500', '2022-08-23-18-47-20IR.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_350', '1500', '2022-08-23-16-56-35Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_357', '1500', '2022-08-23-17-30-39Depth.csv'))
                            data_depth.append(os.path.join(csv_path, 'dev_384', '1500', '2022-08-23-18-47-20Depth.csv'))

                        elif choice is 9:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220830_ir_80_64_fake_depth_debug_wanghao')


                            # data_ir.append(os.path.join(csv_path, '01_empty_space', '2022-08-30-12-34-30IR.csv'))
                            # data_depth.append(os.path.join(csv_path, '01_empty_space', '2022-08-30-12-34-30Depth.csv'))

                            # data_ir.append(os.path.join(csv_path, '02_empty_space', '2022-08-30-15-28-49IR.csv'))
                            # data_depth.append(os.path.join(csv_path, '02_empty_space', '2022-08-30-15-28-49Depth.csv'))

                            data_ir.append(os.path.join(csv_path, '03_trial', 'ir.csv'))
                            data_depth.append(os.path.join(csv_path, '03_trial', 'depth.csv'))

                        elif choice is 10:

                            csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132',
                                                    '20220902_ir_80_64_光博会最终版本', 'AE')


                            data_ir.append(os.path.join(csv_path, '500', '2022-09-05-11-25-57IR.csv'))
                            data_depth.append(os.path.join(csv_path, '500', '2022-09-05-11-25-57Depth.csv'))

                            data_ir.append(os.path.join(csv_path, '750', '2022-09-05-11-27-53IR.csv'))
                            data_depth.append(os.path.join(csv_path, '750', '2022-09-05-11-27-53Depth.csv'))

                            data_ir.append(os.path.join(csv_path, '1000', '2022-09-05-11-29-41IR.csv'))
                            data_depth.append(os.path.join(csv_path, '1000', '2022-09-05-11-29-41Depth.csv'))

                        ###########################################################################################

                        else:
                            pass

                        width_ir, height_ir = 64, 80
                        width_depth, height_depth = 32, 40

                        return data_ir, data_depth, width_ir, height_ir, width_depth, height_depth

                    # 加载数据
                    # data_ir, data_depth, width, height = load_data_8132_40_32(choice)  # depth divided by 4
                    data_ir, data_depth, width_ir, height_ir, width_depth, height_depth = load_data_8132_80_64(choice)

                    # 通过路径，读取路径下的所有数据
                    ir, depth = [], []
                    for i in range(len(data_ir)):
                        ir.append(np.genfromtxt(data_ir[i], delimiter=',').astype(np.float32))
                        depth.append(np.genfromtxt(data_depth[i], delimiter=',').astype(np.float32))

                    # Convert to NumPy array
                    ir, depth = np.array(ir), np.array(depth)

                    # Concatenate
                    ir = np.concatenate(ir).reshape(-1, height_ir, width_ir)
                    depth = np.concatenate(depth).reshape(-1, height_depth, width_depth)

                    # numpy array 数据格式转换
                    ir, depth = torch.FloatTensor(ir), torch.FloatTensor(depth)
                    ir, depth = ir.unsqueeze(1), depth.unsqueeze(1)  # 添加一个维度

                    # 转换成相应的格式 (N, C, H, W)
                    ir = ir.view(-1, 1, height_ir, width_ir)
                    depth = depth.view(-1, 1, height_depth, width_depth)

                    # 8132 IR 值需要除于 16
                    ir[ir > 60000] = 0
                    # depth = depth/4

                    return ir, depth

                def resize_pnet(self, img, size):

                    '''
                    这个函数是 IR 图 (img) 的前期处理
                    前期处理按次序分成三个步骤：降采样 -> 归一化 -> 定点化
                    :param size: 从原图 img，降采样到尺寸 size
                    :return:
                    '''

                    # 第一步: 降采样
                    img2 = torch.zeros((img.shape[0], img.shape[1], size[0], size[1]))
                    for i in range(size[0]):
                        for j in range(size[1]):
                            # 降采样的方法，从原图里抽取一定数量的值
                            # 举例1：从原图 img 40*32 的图里，抽取 一部分的值，生成 25*20 大小的 img2
                            # 举例2：从原图 img 40*32 的图里，抽取 一部分的值，生成 18*14 大小的 img2
                            img2[:, :, i, j] = img[:, :, self.resize_arg_idx_h[size[0]][i], self.resize_arg_idx_w[size[1]][j]]

                    return img2

                def load_model(self, model_path, output_type='every', coef_Qnm_format='original',
                               inout_Qnm_format=None, save_in_out=False):

                    '''

                    :param model_path:
                    :param output_type: 'every'(每一层输入输出都显示）or 'single' (只输出最后结果）
                    :param coef_Qnm_format: 'original' (原 facenet pytorch 系数) or 'quantization' (定点化后的系数）
                    :return:
                    '''

                    import torch

                    # 神经网络数据结构选项
                    if output_type is 'single':

                        # facenet pytorch 原模型
                        from model.mtcnn.facenet_pytorch.package.model import PNet
                        from model.mtcnn.facenet_pytorch.package.model import RNet
                        from model.mtcnn.facenet_pytorch.package.model import ONet

                        # 构造模型
                        pnet = PNet()
                        rnet = RNet()
                        onet = ONet()

                        print(f'加载模型; 只输出最后结果\n')

                    elif output_type is 'every':

                        # 输出每层的结果
                        from model.mtcnn.facenet_pytorch.package.model import PNet2 as PNet
                        from model.mtcnn.facenet_pytorch.package.model import RNet2 as RNet
                        from model.mtcnn.facenet_pytorch.package.model import ONet

                        # 构造模型
                        pnet = PNet()
                        rnet = RNet()
                        onet = ONet()

                        print(f'加载模型; 输出每一层的输出结果\n')

                    elif output_type is 'every_quantization':

                        # 输出每层的结果
                        from model.mtcnn.facenet_pytorch.package.model import PNet3 as PNet
                        from model.mtcnn.facenet_pytorch.package.model import RNet3 as RNet
                        from model.mtcnn.facenet_pytorch.package.model import ONet

                        # 构造模型
                        pnet = PNet(inout_Qnm_format['pnet'], save_in_out['pnet'])
                        rnet = RNet(inout_Qnm_format['rnet'], save_in_out['rnet'])
                        onet = ONet()

                        print(f'加载模型; 输出每一层的输出结果；每一层的输出结果都做定点化\n')

                    else:

                        # 定点化模型
                        # from model.mtcnn.facenet_pytorch.package.model import PNet3 as PNet
                        pass


                    # 神经网络模型系数结构（原浮点数 / 定点化)
                    if 'original' in coef_Qnm_format.keys():

                        state_dict_pnet = torch.load(model_path + 'pnet.pt')
                        state_dict_rnet = torch.load(model_path + 'rnet.pt')
                        state_dict_onet = torch.load(model_path + 'onet.pt')

                    elif 'quantization' in coef_Qnm_format.keys():

                        # Load Pnet model
                        if coef_Qnm_format['quantization']['pnet'] == 'Q2.14':
                            state_dict_pnet = torch.load(model_path + 'pnet_Q2_14.pt')
                            print('Load ' + model_path + 'pnet_Q2_14.pt')

                        elif coef_Qnm_format['quantization']['pnet'] == 'Q3.13':
                            state_dict_pnet = torch.load(model_path + 'pnet_Q3_13.pt')
                            print('Load ' + model_path + 'pnet_Q3_13.pt')

                        # Load Rnet model
                        if coef_Qnm_format['quantization']['rnet'] == 'Q1.15':
                            state_dict_rnet = torch.load(model_path + 'rnet_Q1_15.pt')
                            print('Load ' + model_path + 'rnet_Q1_15.pt')

                        elif coef_Qnm_format['quantization']['rnet'] == 'Q2.14':
                            state_dict_rnet = torch.load(model_path + 'rnet_Q2_14.pt')
                            print('Load ' + model_path + 'rnet_Q2_14.pt')

                        # Load Onet model
                        state_dict_onet = torch.load(model_path + 'onet.pt')

                    pnet.load_state_dict(state_dict_pnet)
                    rnet.load_state_dict(state_dict_rnet)
                    onet.load_state_dict(state_dict_onet)

                    return pnet, rnet, onet

                def resize_rnet_to_24_24(self, mat_old):

                    def calculate_weighted_value(i_old, j_old, i_old0, j_old0, i_old1, j_old1, mat_old, method='Euclidean'):

                        '''
                        这个函数根据 i_old, j_old
                        计算出4个相应点 (i_old0, j_old0), (i_old0, j_old1), (i_old1, j_old0), (i_old1, j_old1)
                        的权重分别以 w00, w01, w10, w11 表示
                        最终，返回加权平均后的值
                        :param i_old0: 行坐标
                        :param j_old0: 列坐标
                        :param i_old1: 行坐标
                        :param j_old1: 列坐标
                        :return:
                        val 加权平均后的值
                        '''


                        # 根据选项，选择相应的公式计算距离
                        if method is 'Euclidean':

                            # 计算欧氏距离
                            dist00 = np.sqrt((i_old - i_old0) ** 2 + (j_old - j_old0) ** 2)
                            dist01 = np.sqrt((i_old - i_old0) ** 2 + (j_old - j_old1) ** 2)
                            dist10 = np.sqrt((i_old - i_old1) ** 2 + (j_old - j_old0) ** 2)
                            dist11 = np.sqrt((i_old - i_old1) ** 2 + (j_old - j_old1) ** 2)

                        elif method is 'Manhattan':

                            # 曼哈顿距离
                            dist00 = np.abs(i_old - i_old0) + np.abs(j_old - j_old0)
                            dist01 = np.abs(i_old - i_old0) + np.abs(j_old - j_old1)
                            dist10 = np.abs(i_old - i_old1) + np.abs(j_old - j_old0)
                            dist11 = np.abs(i_old - i_old1) + np.abs(j_old - j_old1)

                        else:
                            pass

                        # 计算的 dist 距离值，离 (i_old, j_old) 越近的点，其权重越大
                        # 因此，通过一个值相减，得到越近，权重越大的对应关系
                        # 欧氏距离最大距离是其对角线的距离 sqrt(1^2 + 1^2) = sqrt(2) = 1.414
                        # 曼哈顿距离最大距离是其对角线的距离 1 + 1 = 2
                        if method is 'Euclidean':
                            d = 1.414
                        elif method is 'Manhattan':
                            d = 2
                        else:
                            pass
                        w00 = d - dist00
                        w01 = d - dist01
                        w10 = d - dist10
                        w11 = d - dist11

                        # 计算权重的相加
                        w_sum = w00 + w01 + w10 + w11

                        # 嵌入式计算除法比较耗时, 选择做一次性除法，后续调用
                        w_invert_sum = 1 / w_sum

                        # 计算每一个权重，通过 乘 w_invert_sum 归一化
                        w00 = w00 * w_invert_sum
                        w01 = w01 * w_invert_sum
                        w10 = w10 * w_invert_sum
                        w11 = w11 * w_invert_sum

                        # 根据权重，计算相应的值
                        val = w00 * mat_old[0, 0, i_old0, j_old0] + w01 * mat_old[0, 0, i_old0, j_old1] + \
                              w10 * mat_old[0, 0, i_old1, j_old0] + w11 * mat_old[0, 0, i_old1, j_old1]

                        return val

                    def calculate_arg_neareast_point(i_old, j_old, i_old0, j_old0, i_old1, j_old1, mat_old):

                        '''
                        此函数找出以下 4 个点
                        (i_old0, j_old0), (i_old0, j_old1), (i_old1, j_old0), (i_old1, j_old1)
                        哪个离 (i_old, j_old) 最近
                        最近的值 从 mat_old 取出
                        :param i_old:
                        :param j_old:
                        :param i_old0:
                        :param j_old0:
                        :param i_old1:
                        :param j_old1:
                        :param mat_old:
                        :return:
                        '''

                        # 曼哈顿距离
                        dist00 = np.abs(i_old - i_old0) + np.abs(j_old - j_old0)
                        dist01 = np.abs(i_old - i_old0) + np.abs(j_old - j_old1)
                        dist10 = np.abs(i_old - i_old1) + np.abs(j_old - j_old0)
                        dist11 = np.abs(i_old - i_old1) + np.abs(j_old - j_old1)

                        # 通过比较的方式找出最近的点
                        i_opt, j_opt, dist_opt = i_old0, j_old0, dist00
                        if dist01 < dist_opt:
                            i_opt, j_opt, dist_opt = i_old0, j_old1, dist01

                        if dist10 < dist_opt:
                            i_opt, j_opt, dist_opt = i_old1, j_old0, dist10

                        if dist11 < dist_opt:
                            i_opt, j_opt, dist_opt = i_old1, j_old1, dist11

                        val = mat_old[0, 0, i_opt, j_opt]

                        return val

                    import numpy as np

                    h, w = 24, 24

                    # 初始化矩阵，储存降采样/升采样后的值
                    mat_new = torch.zeros((1, 1, h, w))

                    # 输入矩阵的大小
                    _, _, h_old, w_old = mat_old.shape

                    # 计算 mat_new (h, w) 大小和 mat_old (h_old, w_old) 大小的比例
                    scaling_row, scaling_col = int((h_old - 1) / (h - 1) * 100) / 100, int(
                        (w_old - 1) / (w - 1) * 100) / 100

                    # 遍历 mat_new 矩阵的每个位置
                    for i_new in range(h):
                        for j_new in range(w):

                            # 计算 mat_new 矩阵里的 (i_new, j_new) 位置与 mat_old 里的 (i_old, j_old) 的对应关系
                            i_old, j_old = i_new * scaling_row, j_new * scaling_col

                            # 计算 (i_new, j_new) 坐标在 (i_old, j_old) 的位置
                            # 注意 (i_old, j_old) 是浮点数，但是矩阵的坐标位置只能是整数
                            # 所以如何决定 mat_old[i_old, j_old] 的值呢？
                            # 答案：通过 (i_old, j_old) 的临近点计算
                            # 有4个临近点 (i_old0, j_old0), (i_old0, j_old1), (i_old1, j_old0), (i_old1, j_old1)
                            i_old0, j_old0 = int(i_old), int(j_old)  # 向下取整
                            i_old1, j_old1 = int(np.ceil(i_old)), int(np.ceil(j_old))  # 向上取整

                            # 方法一: 'Euclidean' 调用 calculate_weighted_value 函数
                            # 方法二: 'Manhattan' 调用 calculate_weighted_value 函数
                            # 方法三: 取最近值，调用 calculate_arg_neareast_point

                            # 根据欧氏距离/曼哈顿距离计算出 mat_new 的相应赋值
                            # method = 'Manhattan'
                            # val = calculate_weighted_value(i_old, j_old, i_old0, j_old0, i_old1, j_old1, mat_old, method)

                            # 找出最近点的值
                            val = calculate_arg_neareast_point(i_old, j_old, i_old0, j_old0, i_old1, j_old1, mat_old)

                            # 把计算的值赋予新矩阵里
                            mat_new[0, 0, i_new, j_new] = val

                            # print(f'i_new, j_new = {i_new, j_new}; i_old, j_old = {i_old, j_old}; i_old0, j_old0 = '
                            #       f'{i_old0, j_old0}; 'f'i_old1, j_old1 = {i_old1, j_old1}')

                    return mat_new

                def scale_pyramid(self):

                    # 计算缩放比例
                    # min_face_size 是最小可检测人脸的像素大小：默认：20
                    m = 12.0 / self.min_face_size  # 12.0 是可检测人脸的最小像素点
                    minl = min(self.h, self.w)  # 从长乘宽取这两个维度的最小值
                    minl = minl * m

                    # Create scale pyramid
                    scale_i = m
                    scales = []
                    while minl >= 12:
                        scales.append(scale_i)
                        scale_i = scale_i * self.factor
                        minl = minl * self.factor

                    # 缩放后的尺寸大小
                    sizes = []
                    for scale in scales:
                        sizes.append((int(self.h * scale + 1), int(self.w * scale + 1)))

                    return scales, sizes

                def generateBoundingBox(self, reg, probs, scale, thresh):

                    # probs example: shape [1, 4, 2]
                    # reg example: shape[4, 1, 4, 2] first element has 4 bounding boxes
                    # [1, 4, 2] refers to the pixel size
                    stride = 2
                    cellsize = 12

                    reg = reg.permute(1, 0, 2, 3)  # 变换位置

                    mask = probs >= thresh  # 人脸概率与阈值比较
                    mask_inds = mask.nonzero()  # 提取相关人脸的索引
                    image_inds = mask_inds[:, 0]  # 提取相关人脸的图像位置
                    score = probs[mask]  # 提取大于阈值的人脸概率值
                    reg = reg[:, mask].permute(1, 0)  # 生成人脸候选窗口
                    bb = mask_inds[:, 1:].type(reg.dtype).flip(1)
                    q1 = ((stride * bb + 1) / scale).floor()
                    q2 = ((stride * bb + cellsize - 1 + 1) / scale).floor()
                    boundingbox = torch.cat([q1, q2, score.unsqueeze(1), reg], dim=1)
                    return boundingbox, image_inds

                def generateBoundingBox_cks(self, reg, probs, scale, thresh):

                    '''
                    自己编写提取候选窗口的函数
                    :param self:
                    :param reg: Pnet 输出的窗口值
                    :param probs: Pnet 输出的概率值
                    :param scale: Pnet 导入图像的缩放比例
                    :param inv_thresh: 人脸概率阈值反比 (inv_thresh = log(1/th - 1))
                    :return:
                    boundingbox: 候选窗口
                    '''

                    # probs example: shape [1, 4, 2]
                    # reg example: shape[1, 4, 8, 5] first element has 4 bounding boxes
                    # reg example: shape[1, 4, 4, 2] first element has 4 bounding boxes
                    # [1, 4, 2] refers to the pixel size
                    stride = 2
                    cellsize = 12
                    scale_inv = 1 / scale  # 嵌入式乘法比较快，所以先计算好 1/scale 后续乘法使用

                    import copy
                    probs1 = copy.copy(probs)
                    reg1 = copy.copy(reg)

                    probs1 = probs1[0].data.numpy()
                    reg1 = reg1[0].data.numpy()

                    # 因为没有选择调用 softmax 算子, 需要构建一个 probs 相减的变量 (不是人脸的概率 - 人脸的概率)
                    # 注意 probs 并没有通过 softmax 归一化，所以严格而言不算是概率值
                    probs_diff = probs1[0, :, :] - probs1[1, :, :]

                    boundingbox = []

                    for i in range(probs_diff.shape[0]):  # 遍历长度
                        for j in range(probs_diff.shape[1]):  # 遍历宽度

                            # 如果概率大于阈值，提取相关候选窗口的信息
                            if probs_diff[i, j] < thresh:  # 因为概率值相减，所以使用 < 而不是 >

                                # 计算 Pnet 候选窗口在原有尺寸 (40*32) 下的行列索引值
                                q1 = np.array([int((stride * j + 1) * scale_inv), int((stride * i + 1) * scale_inv)])
                                q2 = np.array([int((stride * j + cellsize) * scale_inv),
                                               int((stride * i + cellsize) * scale_inv)])

                                # 把相关信息导出 (原图坐标索引 + 概率)
                                # data = np.concatenate((q1, q2, [probs1[i, j]], reg1[:, i, j]))
                                # prob_base_2 = 2 ** probs1[1, i, j] / (2**probs1[0, i, j] + 2**probs1[1, i, j])
                                # prob_base_e = 1 / (np.exp(probs_diff[i, j]) + 1)
                                # prob_base_2 = 1 / (2**(probs_diff[i, j]) + 1)
                                # data = np.concatenate((q1, q2, [prob_base_2], reg1[:, i, j]))
                                prob_base_none = probs_diff[i, j]
                                data = np.concatenate((q1, q2, [prob_base_none], reg1[:, i, j]))
                                boundingbox.append(data)

                    boundingbox = torch.from_numpy(np.array(boundingbox)).float()

                    if boundingbox.ndim == 1:
                        boundingbox = torch.empty(size=(0, 9))

                    return boundingbox

                def statistical_analysis_and_plot_histogram(self, *args, string, saved_jpg_name, saved_filename):

                    """
                    对 args 参数做统计分析并返回统计分析结果
                    :param args: 待统计分析的变量
                    :return:
                    output: 统计分析结果
                    """

                    import matplotlib.pyplot as plt
                    plt.rcParams['font.sans-serif'] = ['SimHei']
                    plt.rcParams['axes.unicode_minus'] = False

                    # 最后第二个字符串变量是 args[:-2] 变量的名字（绘图用途）
                    # string = args[-2]

                    # 把分析结果是否绘成图
                    # save_jpg = args[-1]

                    output = dict()

                    for i, arg in enumerate(args):

                        print(i, string[i])
                        arr = arg.detach().numpy()
                        min_n, max_n, mean_n, median_n, sigma_n = arr.min(), arr.max(), arr.mean(), np.median(arr), np.std(arr)
                        y, x = np.histogram(arr, range=(min_n, max_n), bins=100)
                        width = (max_n - min_n)/100

                        if saved_jpg_name:

                            plt.figure(1)
                            x = (x[0:100] + x[1:101])/2
                            plt.bar(x, y, width)
                            plt.xlabel('x')
                            plt.ylabel('count')
                            plt.grid()
                            plt.title(f'{string[i]}; min: {min_n:0.3f}; max: {max_n:0.3f}; \n mean: {mean_n:0.3f}; median: {median_n:0.3f}; sigma: {sigma_n:0.3f}')
                            plt.savefig(f'{saved_jpg_name}_{string[i]}.jpg')
                            plt.clf()

                        # 把统计分析结果导入字典里
                        output[string[i]] = [min_n, mean_n - 3*sigma_n, mean_n - 2*sigma_n, mean_n - sigma_n, mean_n,
                                             median_n, mean_n + sigma_n, mean_n + 2*sigma_n, mean_n + 3*sigma_n, max_n]

                    # 把统计分析结果写在 txt 文件里
                    self.save_statistical_analysis_results(output, saved_filename)

                    return output

                def save_statistical_analysis_results(self, results, saved_filename):

                        '''
                        # 把输入输出的统计分析结果写在 txt 文件里
                        :param results:
                        :param saved_filename:
                        :return:
                        '''

                        # results 是字典

                        output = list()
                        for key, value_list in results.items():
                            out = []
                            out.append(key)
                            for value in value_list:
                                out.append(f'{value:0.9f}')
                            print(out)
                            output.append(out)

                        # If saved_filename is not None object, save it as txt file
                        if saved_filename:
                            with open(f'{saved_filename}.txt', 'w+', encoding='utf8') as fd:
                                fd.write(' min mean-3*sigma mean-2*sigma mean-sigma mean median mean+sigma mean+2*sigma mean+3*sigma max\n')
                                for i in output:
                                    fd.write(' '.join(i) + '\n')

                def obtain_model_namelist(self, model):

                    # Convert model to string
                    string_model = str(next(model.modules()))

                    # Split the string with '\n'
                    string_split = string_model.split('\n')

                    # Remove spacing
                    output = [i.replace(' ', '') for i in string_split]

                    # Remove the first and last row
                    output = output[1:-1]

                    # Extract the name for each layer
                    name_list = [name.split(':')[0][1:-1] for name in output]

                    return output, name_list

                def analyze_parameters(self, model, save_hist_jpg=None, save_results_txt=None):

                    '''

                    统计分析神经网络系数并输出结果

                    inputs:
                    model: 神经网络模型如 mtcnn.pnet 或者 mtcnn.rnet 作为输入

                    Exmple of usage:

                    model_path = './model/mtcnn/facenet_pytorch/pt_files/'
                    pnet, rnet, onet = self.load_model(model_path)
                    self.analyze_parameters(pnet, save_hist_jpg='coef', save_results_txt=True)
                    self.analyze_parameters(rnet, save_hist_jpg='coef', save_results_txt=True)
                    self.analyze_parameters(onet, save_hist_jpg='coef', save_results_txt=True)

                    '''

                    def analyze_all_coefficients(self, model_string, name_list, model, save_jpg=None):

                        '''
                        此函数分析神经网络的所有系数(w,b)
                        :param self:
                        :param model:
                        :param name_list:
                        :param name_model:
                        :param mtcnn:
                        :param save_jpg:
                        :return:
                        '''


                        # Store all the results of analysis (mean, median, sigma, etc) in dictionary format
                        results = dict()

                        for i, name in enumerate(model_string):

                            print(model_string[i])

                            # Split the model's name and parameters for each line
                            param = [j for j in re.split(pattern='\:|\(|\,|\=|\)', string=model_string[i]) if j is not '']

                            if 'Conv2d' in name:

                                # Extract the coefficients
                                w_var = f'model.{name_list[i]}.weight'
                                b_var = f'model.{name_list[i]}.bias'
                                w = eval(w_var)
                                b = eval(b_var)

                                # Plot histogram
                                results.update(self.statistical_analysis_and_plot_histogram(
                                    w, b, string=[w_var, b_var], saved_jpg_name=save_jpg, saved_filename=None))

                            elif 'PReLU' in name:

                                # Define filename to be saved
                                w_fn = f'ker{param[3]}'

                                # Extract the coefficients
                                w_var = f'model.{name_list[i]}.weight'
                                w = eval(w_var)

                                # Plot histogram
                                results.update(self.statistical_analysis_and_plot_histogram(
                                    w, string=[w_var], saved_jpg_name=save_jpg, saved_filename=None))

                            elif 'Linear' in name:

                                # Extract the coefficients
                                w_var = f'model.{name_list[i]}.weight'
                                b_var = f'model.{name_list[i]}.bias'
                                w = eval(w_var)
                                b = eval(b_var)

                                # Plot histogram
                                results.update(self.statistical_analysis_and_plot_histogram(
                                    w, b, string=[w_var, b_var], saved_jpg_name=save_jpg, saved_filename=None))

                            else:
                                pass

                        return results

                    # Details and names for layers
                    model_string, name_list = self.obtain_model_namelist(model)

                    # 导出神经网络的w与b，并进行统计分析
                    results = analyze_all_coefficients(self, model_string, name_list, model, save_hist_jpg)

                    # 储存统计分析结果
                    if save_results_txt:
                        self.save_statistical_analysis_results(results, f'{str(model._get_name())}_stat_analysis')

                    return None

                def save_parameters_to_text_automatic(self, model, name_model, qformat, precision, \
                                                      save_format={'float': 'float32', 'quantization': {'save': 'int'}}):

                    '''
                    这个函数是输出神经网络的权重与系数
                    model: 神经网络模型
                    name_model: 神经网络模型的名字（系数储存以次命名的主文件夹）
                    :return:
                    '''

                    import copy as copy

                    def save_all_coefficients_to_txt(model_string, name_list, name_model, model, save_format, qformat, precision):

                        # Put the coefficients inside a folder with model as its name
                        if os.path.isdir(name_model) is False:
                            os.mkdir(name_model)

                        for i, name in enumerate(model_string):

                            print(model_string[i])
                            saved_folder = os.path.join(name_model, name_list[i])
                            print(saved_folder)
                            if os.path.isdir(saved_folder) is False:
                                os.mkdir(saved_folder)

                            # Split the model's name and parameters for each line
                            param = [j for j in re.split(pattern='\:|\(|\,|\=|\)', string=model_string[i]) if j is not '']

                            if 'Conv2d' in name:

                                # Extract the kernel size
                                kernel_size = (int(param[5]), int(param[6]))

                                # Define filename to be saved
                                w_fn = f'ker{param[3]}_h{param[5]}_w{param[6]}_d{param[2]}_stride{param[8]}'
                                b_fn = f'ker{param[3]}'
                                w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')
                                b_fn2 = os.path.join(saved_folder, f'{name_list[i]}_bias_{b_fn}.txt')

                                # Extract the coefficients
                                w = eval(f'model.{name_list[i]}.weight.detach().numpy()')
                                b = eval(f'model.{name_list[i]}.bias.detach().numpy()')

                                # There are a lot of repeated codes, write as function when free!!!!!!!
                                if 'float' in save_format.keys():

                                    # 系数储存为 float 格式
                                    # 储存为整数位

                                    # 修改文件储存名字
                                    w_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                         qformat.replace('.', '_') + '_float_' + os.path.basename(w_fn2))
                                    b_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                         qformat.replace('.', '_') + '_float_' + os.path.basename(b_fn2))

                                    # 系数储存为 float
                                    np.savetxt(w_fn3, w.reshape(-1, kernel_size[1]), fmt='%.32f')
                                    np.savetxt(b_fn3, b.reshape(-1, 1), fmt='%.32f')
                                    print(f'储存成功 {w_fn3}')
                                    print(f'储存成功 {b_fn3}')

                                if 'quantization' in save_format.keys():

                                    if save_format['quantization']['save'] is 'int':

                                        # 系数储存为 int 格式
                                        # 储存为整数位

                                        # 修改文件储存名字
                                        w_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                             qformat.replace('.', '_') + '_int_' + os.path.basename(w_fn2))
                                        b_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                             qformat.replace('.', '_') + '_int_' + os.path.basename(b_fn2))

                                        # 系数储存, 并把Qn.m定点化从浮点格式转换成整数格式
                                        np.savetxt(w_fn3, w.reshape(-1, kernel_size[1]) * 2 ** precision, fmt='%0.f')
                                        np.savetxt(b_fn3, b.reshape(-1, 1) * 2 ** precision, fmt='%0.f')

                                        print(f'储存成功 {w_fn3}')
                                        print(f'储存成功 {b_fn3}')

                            elif 'PReLU' in name:

                                # Define filename to be saved
                                w_fn = f'ker{param[3]}'
                                w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')

                                # Extract the coefficients
                                w = eval(f'model.{name_list[i]}.weight.detach().numpy()')

                                if 'float' in save_format.keys():

                                    # 修改文件储存名字
                                    w_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                         qformat.replace('.', '_') + '_float_' + os.path.basename(w_fn2))

                                    # 系数储存为 float
                                    np.savetxt(w_fn3, w.reshape(-1, 1), fmt='%.32f')
                                    print(f'储存成功 {w_fn3}')

                                if 'quantization' in save_format.keys():

                                    if save_format['quantization']['save'] is 'int':

                                        # 修改文件储存名字
                                        w_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                             qformat.replace('.', '_') + '_int_' + os.path.basename(w_fn2))

                                        # 系数储存, 并把Qn.m定点化从浮点格式转换成整数格式
                                        np.savetxt(w_fn3, w.reshape(-1, 1) * 2 ** precision, fmt='%0.f')
                                        print(f'储存成功 {w_fn3}')

                            elif 'Linear' in name:

                                size = (int(param[3]), int(param[5]))

                                # Define filename to be saved
                                w_fn = f'in{param[3]}_out{param[5]}'
                                b_fn = f'{param[5]}'
                                w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')
                                b_fn2 = os.path.join(saved_folder, f'{name_list[i]}_bias_{b_fn}.txt')

                                # Extract the coefficients
                                w = eval(f'model.{name_list[i]}.weight.detach().numpy()')
                                b = eval(f'model.{name_list[i]}.bias.detach().numpy()')

                                if 'float' in save_format.keys():

                                    w_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                         qformat.replace('.', '_') + '_float_' + os.path.basename(w_fn2))
                                    b_fn3 = os.path.join(os.path.dirname(b_fn2),
                                                         qformat.replace('.', '_') + '_float_' + os.path.basename(b_fn2))

                                    # 系数储存为 float
                                    np.savetxt(w_fn3, w.T, fmt='%.32f')
                                    np.savetxt(b_fn3, b.reshape(-1, 1), fmt='%.32f')
                                    print(f'储存成功 {w_fn3}')
                                    print(f'储存成功 {b_fn3}')

                                if 'quantization' in save_format.keys():

                                    if save_format['quantization']['save'] is 'int':

                                        w_fn3 = os.path.join(os.path.dirname(w_fn2),
                                                             qformat.replace('.', '_') + '_int_' + os.path.basename(w_fn2))
                                        b_fn3 = os.path.join(os.path.dirname(b_fn2),
                                                             qformat.replace('.', '_') + '_int_' + os.path.basename(b_fn2))

                                        # 系数储存, 并把Qn.m定点化从浮点格式转换成整数格式
                                        np.savetxt(w_fn3, w.T * 2 ** precision, fmt='%.f')
                                        np.savetxt(b_fn3, b.reshape(-1, 1) * 2 ** precision, fmt='%.f')
                                        print(f'储存成功 {w_fn3}')
                                        print(f'储存成功 {b_fn3}')

                            else:
                                pass

                    # Details and names for layers
                    model_string, name_list = self.obtain_model_namelist(model)

                    # Save all the coefficients of the corresponding model
                    save_all_coefficients_to_txt(model_string, name_list, name_model, model, save_format, qformat, precision)

                def bbreg(self, boundingbox, reg):

                    if reg.shape[1] == 1:
                        reg = torch.reshape(reg, (reg.shape[2], reg.shape[3]))

                    w = boundingbox[:, 2] - boundingbox[:, 0] + 1
                    h = boundingbox[:, 3] - boundingbox[:, 1] + 1
                    b1 = boundingbox[:, 0] + reg[:, 0] * w
                    b2 = boundingbox[:, 1] + reg[:, 1] * h
                    b3 = boundingbox[:, 2] + reg[:, 2] * w
                    b4 = boundingbox[:, 3] + reg[:, 3] * h
                    boundingbox[:, :4] = torch.stack([b1, b2, b3, b4]).permute(1, 0)

                    return boundingbox

                def rerec(self, bboxA):

                    h = bboxA[:, 3] - bboxA[:, 1]
                    w = bboxA[:, 2] - bboxA[:, 0]

                    l = torch.max(w, h)
                    bboxA[:, 0] = bboxA[:, 0] + w * 0.5 - l * 0.5
                    bboxA[:, 1] = bboxA[:, 1] + h * 0.5 - l * 0.5
                    bboxA[:, 2:4] = bboxA[:, :2] + l.repeat(2, 1).permute(1, 0)

                    return bboxA

                def rerec_cks(self, bboxA):

                    '''
                    把比例不相同的窗口变成比例相同
                    :param bboxA:
                    :return:
                    '''

                    h = bboxA[:, 3] - bboxA[:, 1]
                    w = bboxA[:, 2] - bboxA[:, 0]

                    l = torch.max(w, h)

                    for i in range(bboxA.shape[0]):
                        for j in range(bboxA.shape[1]):

                            if j == 0:
                                bboxA[i, j] = bboxA[i, j] + w[i] * 0.5 - l[i] * 0.5
                            elif j == 1:
                                bboxA[i, j] = bboxA[i, 1] + h[i] * 0.5 - l[i] * 0.5

                    for i in range(bboxA.shape[0]):
                        for j in range(bboxA.shape[1]):
                            if j in [2, 3]:
                                bboxA[i, j] = bboxA[i, j-2] + l[i]

                    return bboxA

                def pad(self, boxes, w, h):

                    """
                    把候选框控制在 原图像的尺寸大小范围内
                    :param boxes:
                    :param w:
                    :param h:
                    :return:
                    """

                    boxes = boxes.trunc().int().cpu().numpy()
                    x = boxes[:, 0]
                    y = boxes[:, 1]
                    ex = boxes[:, 2]
                    ey = boxes[:, 3]

                    x[x < 1] = 1
                    y[y < 1] = 1
                    ex[ex > w] = w
                    ey[ey > h] = h

                    return y, ey, x, ex

                def pnet_bbox_output(self, boxes, image_inds, scale_picks, threshold):

                    from torchvision.ops.boxes import batched_nms

                    # 把 tensor 进行拼接
                    boxes = torch.cat(boxes, dim=0)
                    image_inds = torch.cat(image_inds, dim=0)
                    scale_picks = torch.cat(scale_picks, dim=0)

                    # NMS within each scale + image
                    boxes, image_inds = boxes[scale_picks], image_inds[scale_picks]

                    # NMS within each image
                    pick = batched_nms(boxes[:, :4], boxes[:, 4], image_inds, threshold)
                    boxes, image_inds = boxes[pick], image_inds[pick]

                    regw = boxes[:, 2] - boxes[:, 0]
                    regh = boxes[:, 3] - boxes[:, 1]
                    qq1 = boxes[:, 0] + boxes[:, 5] * regw
                    qq2 = boxes[:, 1] + boxes[:, 6] * regh
                    qq3 = boxes[:, 2] + boxes[:, 7] * regw
                    qq4 = boxes[:, 3] + boxes[:, 8] * regh
                    boxes = torch.stack([qq1, qq2, qq3, qq4, boxes[:, 4]]).permute(1, 0)
                    boxes = self.rerec(boxes)
                    y, ey, x, ex = self.pad(boxes, self.w, self.h)

                    return y, ey, x, ex, boxes, image_inds, scale_picks

                def pnet_bbox_output_cks(self, boxes, threshold):

                    from torchvision.ops.boxes import batched_nms
                    from torchvision.ops.boxes import nms

                    # 把 tensor 进行拼接
                    boxes = torch.cat(boxes, dim=0)

                    # NMS within each image
                    pick = nms(boxes=boxes[:, :4], scores=-boxes[:, 4], iou_threshold=threshold)
                    boxes = boxes[pick]

                    regw = boxes[:, 2] - boxes[:, 0]
                    regh = boxes[:, 3] - boxes[:, 1]
                    qq1 = boxes[:, 0] + boxes[:, 5] * regw
                    qq2 = boxes[:, 1] + boxes[:, 6] * regh
                    qq3 = boxes[:, 2] + boxes[:, 7] * regw
                    qq4 = boxes[:, 3] + boxes[:, 8] * regh
                    boxes = torch.stack([qq1, qq2, qq3, qq4, boxes[:, 4]]).permute(1, 0)
                    # boxes = self.rerec(boxes)
                    boxes = self.rerec_cks(boxes)
                    y, ey, x, ex = self.pad(boxes, self.w, self.h)

                    return y, ey, x, ex, boxes

                def quantize_model(self, model, signed=1, n_int=2, n_frac=13):

                    '''

                    统计分析神经网络系数并输出结果

                    inputs:
                    model: 神经网络模型如 mtcnn.pnet 或者 mtcnn.rnet 作为输入

                    Exmple of usage:

                    model_path = './model/mtcnn/facenet_pytorch/pt_files/'
                    pnet, rnet, onet = self.load_model(model_path)
                    self.analyze_parameters(pnet, save_hist_jpg='coef', save_results_txt=True)
                    self.analyze_parameters(rnet, save_hist_jpg='coef', save_results_txt=True)
                    self.analyze_parameters(onet, save_hist_jpg='coef', save_results_txt=True)

                    OR

                    self.analyze_parameters(pnet, save_hist_jpg=None, save_results_txt=True)
                    self.analyze_parameters(rnet, save_hist_jpg=None, save_results_txt=True)
                    self.analyze_parameters(onet, save_hist_jpg=None, save_results_txt=True)
                    '''

                    from fxpmath import Fxp

                    # Details and names for layers
                    model_string, name_list = self.obtain_model_namelist(model)

                    def convert_all_coefficients(model_string, name_list, model):

                        '''
                        此函数分析神经网络的所有系数(w,b)
                        :param self:
                        :param model:
                        :param name_list:
                        :param name_model:
                        :param mtcnn:
                        :param save_jpg:
                        :return:
                        '''

                        def modify_parameter(model, *args):

                            for model_string in args:
                                with torch.no_grad():

                                    for index, value in np.ndenumerate(eval(model_string)):

                                        # 提取系数
                                        coeff = eval(model_string + '[index].detach().numpy()')

                                        # 定点化相关系数
                                        num = Fxp(coeff, signed=signed, n_int=n_int, n_frac=n_frac)

                                        # 替换原有系数为定点数
                                        exec(model_string + '[index] = num.__float__()')

                            return model

                        for i, name in enumerate(model_string):

                            print('修改系数' + model_string[i])

                            # Split the model's name and parameters for each line
                            param = [j for j in re.split(pattern='\:|\(|\,|\=|\)', string=model_string[i]) if j is not '']

                            if 'Conv2d' in name:

                                # Extract the coefficients
                                w_var = f'model.{name_list[i]}.weight'
                                b_var = f'model.{name_list[i]}.bias'

                                # 修改模型 model 里的系数
                                model = modify_parameter(model, w_var, b_var)

                            elif 'PReLU' in name:

                                # Extract the coefficients
                                w_var = f'model.{name_list[i]}.weight'

                                # 修改模型 model 里的系数
                                model = modify_parameter(model, w_var)

                            elif 'Linear' in name:

                                # Extract the coefficients
                                w_var = f'model.{name_list[i]}.weight'
                                b_var = f'model.{name_list[i]}.bias'

                                # 修改模型 model 里的系数
                                model = modify_parameter(model, w_var, b_var)

                            else:

                                print('Pass 没有系数可修改' + model_string[i])
                                pass

                        return model

                    # 修改模型里的所有系数
                    model = convert_all_coefficients(model_string, name_list, model)

                    return model

                def quantize_pnet_rnet(self, pnet, rnet, coef_format, save=None, save_model=False):

                    '''
                    Pnet 与 Rnet 模型的定点化
                    :param pnet: pnet 神经网络模型
                    :param rnet: rnet 神经网络模型
                    :param save: 储存系数为 txt 文件
                    :param save_model: 把神经网络模型储存下来
                    :return:

                    使用方式：
                    先从 main 函数调用
                    model_path = './model/mtcnn/facenet_pytorch/pt_files/'
                    output_type = 'single'
                    coef_Qnm_format = {'original': None}
                    pnet, rnet, _ = self.load_model(model_path, output_type=output_type,
                                    coef_Qnm_format=coef_Qnm_format, inout_Qnm_format=inout_Qnm_format)

                    然后再写
                    pnet, rnet = self.quantize_pnet_rnet(pnet, rnet, save=1, save_model=True)

                    '''

                    def parse_format(fmt):

                        '''
                        把 Qn.m 格式拆解为整数位和小数位; n_int, n_frac
                        :param fmt: Qn.m 格式
                        :return:
                        '''

                        n_int = int(fmt.split('.')[0][1:]) - 1
                        n_frac = int(fmt.split('.')[1])

                        return n_int, n_frac

                    # Pnet 系数定点化
                    np, mp = parse_format(coef_format['pnet'])
                    self.pnet_Qnm_sig, self.pnet_Qnm_n, self.pnet_Qnm_m, self.pnet_Qnm_qformat = 1, np, mp, coef_format['pnet']
                    print(f'Pnet 定点化: {self.pnet_Qnm_qformat}')
                    pnet = self.quantize_model(pnet, signed=self.pnet_Qnm_sig, n_int=self.pnet_Qnm_n, n_frac=self.pnet_Qnm_m)

                    # Rnet 系数定点化
                    nr, mr = parse_format(coef_format['rnet'])
                    self.rnet_Qnm_sig, self.rnet_Qnm_n, self.rnet_Qnm_m, self.rnet_Qnm_qformat = 1, nr, mr, coef_format['rnet']
                    print(f'Rnet 定点化: {self.rnet_Qnm_qformat}')
                    rnet = self.quantize_model(rnet, signed=self.rnet_Qnm_sig, n_int=self.rnet_Qnm_n, n_frac=self.rnet_Qnm_m)

                    # 储存数据
                    if save:

                        # 把 Pnet 与 Rnet 的定点化系数储存成 txt 文件
                        save_format = {'float': 'float32', 'quantization': {'save': 'int'}}
                        self.save_parameters_to_text_automatic(name_model='pnet', model=pnet, qformat=self.pnet_Qnm_qformat,
                                                               precision=self.pnet_Qnm_m, save_format=save_format)
                        self.save_parameters_to_text_automatic(name_model='rnet', model=rnet, qformat=self.rnet_Qnm_qformat,
                                                               precision=self.rnet_Qnm_m, save_format=save_format)

                    if save_model:

                        # 储存 pt 模型
                        pnet_savename = self.pnet_Qnm_qformat.replace('.', '_')
                        rnet_savename = self.rnet_Qnm_qformat.replace('.', '_')
                        torch.save(pnet.state_dict(), f'pnet_{pnet_savename}.pt')
                        torch.save(rnet.state_dict(), f'rnet_{rnet_savename}.pt')
                        print('储存模型成功 ' + f'pnet_{pnet_savename}.pt')
                        print('储存模型成功 ' + f'rnet_{rnet_savename}.pt')

                    return pnet, rnet

                def bbreg(self, boundingbox, reg):

                    '''
                    完成 Rnet 的特性，在 Pnet 候选框进行修正
                    :param boundingbox: Pnet 候选框 （经过 Rnet Probs 概率筛选）
                    :param reg: Rnet 生成的候选框（严格而言是修正框）
                    :return:
                    boundingbox: 修正后的窗口
                    '''

                    # 计算 Pnet 窗口的长度和宽度
                    w = boundingbox[:, 2] - boundingbox[:, 0] + 1
                    h = boundingbox[:, 3] - boundingbox[:, 1] + 1

                    # Pnet 候选框修正
                    b1 = boundingbox[:, 0] + reg[:, 0] * w
                    b2 = boundingbox[:, 1] + reg[:, 1] * h
                    b3 = boundingbox[:, 2] + reg[:, 2] * w
                    b4 = boundingbox[:, 3] + reg[:, 3] * h

                    # 数据替换 boundingbox 的 (x1, y1, x2, y2)
                    boundingbox[:, :4] = torch.stack([b1, b2, b3, b4]).permute(1, 0)

                    return boundingbox

                def bbreg_modify(self, boundingbox, reg):

                    '''
                    完成 Rnet 的特性，在 Pnet 候选框进行修正
                    :param boundingbox: Pnet 候选框 （经过 Rnet Probs 概率筛选）
                    :param reg: Rnet 生成的候选框（严格而言是修正框）
                    :return:
                    boundingbox: 修正后的窗口
                    '''

                    for i in range(len(boundingbox)):
                        w = boundingbox[i, 2] - boundingbox[i, 0] + 1
                        h = boundingbox[i, 3] - boundingbox[i, 1] + 1

                        b1 = boundingbox[i, 0] + reg[i, 0] * w
                        b2 = boundingbox[i, 1] + reg[i, 1] * h
                        b3 = boundingbox[i, 2] + reg[i, 2] * w
                        b4 = boundingbox[i, 3] + reg[i, 3] * h

                        boundingbox[i, 0] = b1
                        boundingbox[i, 1] = b2
                        boundingbox[i, 2] = b3
                        boundingbox[i, 3] = b4

                    return boundingbox

                def after_process_rnet_bbox(self, pnet_boxes, rnet_boxes, rnet_probs, rnet_inv_thresh, iou_threshold):

                    '''
                    Rnet 结果的后期处理
                    :param pnet_boxes: Pnet 生成的候选框
                    :param rnet_boxes: Rnet 生成的候选框
                    :param rnet_probs: Rnet 生成的概率
                    :param rnet_inv_thresh: 概率阈值
                    :param iou_threshold: Rnet IOU 阈值
                    :return:
                    pnet_boxes: 经过 rnet_boxes 修正 (refine) 后的窗口
                    '''

                    from torchvision.ops.boxes import nms

                    def generateBoundingBoxRnet_inv_thresh(rnet_boxes, pnet_boxes, rnet_probs,
                                                           rnet_inv_thresh):
                        '''
                        根据 Rnet 生成的概率，提取相应的 Pnet 窗口， Rnet 窗口
                        :param rnet_boxes: Rnet 窗口
                        :param pnet_boxes: Pnet 窗口
                        :param rnet_probs: Rnet 概率值 （人脸 和 不是人脸的概率值)
                        :param rnet_inv_thresh: Rnet 概率阈值 人脸概率阈值反比 (inv_thresh = log(1/th - 1))
                        :return:
                        '''

                        # 因为没有选择调用 softmax 算子, 需要构建一个 probs 相减的变量 (不是人脸的概率 - 人脸的概率)
                        # 注意 probs 并没有通过 softmax 归一化，所以严格而言不算是概率值
                        probs_diff = rnet_probs[0, :] - rnet_probs[1, :]

                        # 判断人脸概率大于阈值
                        ipass = probs_diff < rnet_inv_thresh

                        # 根据 probs 判断阈值提取 Pnet 生成的人脸窗口
                        # pnet_boxes = torch.cat((pnet_boxes[ipass, :4], prob_base_2[ipass].unsqueeze(1)), dim=1)
                        # prob_base_2 = 2 ** rnet_probs[1, :] / (2 ** rnet_probs[0, :] + 2 ** rnet_probs[1, :])
                        prob_base_none = probs_diff
                        pnet_boxes = torch.cat((pnet_boxes[ipass, :4], prob_base_none[ipass].unsqueeze(1)), dim=1)

                        # 根据 probs 判断阈值提取 Rnet 生成的人脸窗口
                        rnet_boxes = rnet_boxes[:, ipass].permute(1, 0)

                        return pnet_boxes, rnet_boxes

                    with torch.no_grad():

                        # 根据 Rnet 概率阈值判断，筛选 Pnet 和 Rnet 的候选框
                        pnet_boxes, rnet_boxes = generateBoundingBoxRnet_inv_thresh(
                                                                    rnet_boxes=rnet_boxes.permute(1, 0),
                                                                    pnet_boxes=pnet_boxes,
                                                                    rnet_probs=rnet_probs.permute(1, 0),
                                                                    rnet_inv_thresh=rnet_inv_thresh)

                        # NMS 筛选窗口
                        pick = nms(boxes=pnet_boxes[:, :4], scores=-pnet_boxes[:, 4], iou_threshold=iou_threshold)
                        pnet_boxes, rnet_boxes = pnet_boxes[pick], rnet_boxes[pick]

                        # 通过 Rnet 的候选窗口修正 Pnet 的候选窗口
                        pnet_boxes = self.bbreg_modify(pnet_boxes, rnet_boxes)

                        # 修正 窗口，确保窗口比例不会偏差太大 (在 Pnet 计算后的处理也用了同样的函数)
                        pnet_boxes = self.rerec(pnet_boxes)

                    return pnet_boxes

                def after_process_rnet_bbox_original(self, boxes, reg, probs, image_inds):

                    '''
                    这个是 facenet pytorch 代码的原有模型。
                    原封不动，不做修改
                    :param boxes: Pnet 的候选框
                    :param reg: Rnet 生成的候选框
                    :param probs: Rnet 生成的概率值
                    :param image_inds: Rnet 批量值
                    :return:
                    boxes: 修正后的 Pnet 候选框
                    '''

                    from torchvision.ops.boxes import batched_nms

                    out0 = reg.permute(1, 0)  # reg 转置
                    out1 = probs.permute(1, 0)  # probs 转置
                    score = out1[1, :]  # 提取人脸概率值
                    ipass = score > self.thresholds[1]  # 判断人脸概率大于阈值
                    boxes = torch.cat((boxes[ipass, :4], score[ipass].unsqueeze(1)), dim=1)  # 根据 probs 判断阈值提取 Pnet 生成的人脸窗口
                    image_inds = image_inds[ipass]  # 没用的数据
                    mv = out0[:, ipass].permute(1, 0)  # 根据 probs 判断阈值提取 Rnet 生成的人脸窗口

                    # NMS 筛选窗口
                    pick = batched_nms(boxes[:, :4], boxes[:, 4], image_inds, 0.7)
                    boxes, image_inds, mv = boxes[pick], image_inds[pick], mv[pick]

                    # 通过 Rnet 的候选窗口修正 Pnet 的候选窗口
                    boxes = self.bbreg(boxes, mv)

                    # 修正 窗口，确保窗口比例不会偏差太大 (在 Pnet 计算后的处理也用了同样的函数)
                    boxes = self.rerec(boxes)

                    return boxes

                def main_save_model_coeffs_as_txt_pt(self, coef_format, save_txt=False, save_model_pt=False):

                    '''
                    储存系数的 main 函数
                    coef_format: 系数的定点化格式 {'pnet': 'Q3.13', 'rnet': 'Q2.14'}
                    save_txt: 把模型系数储存为 txt 文件
                    save_model_pt: 把模型储存为 pt 文件
                    :return: txt 文件

                    调用方法:
                    main_save_model_coeffs_as_txt(coef_format={'pnet': 'Q3.13', 'rnet': 'Q2.14'},
                                                save_txt=True,
                                                save_model_pt=True)


                    main_save_model_coeffs_as_txt(coef_format={'pnet': 'Q3.13', 'rnet': 'Q2.14'},
                                                save_txt=False,
                                                save_model_pt=False)
                    '''


                    # 装载 Pnet 与 Rnet 模型
                    path = './model/mtcnn/facenet_pytorch/pt_files/'

                    # 选择输出结果的种类
                    out_type = 'single'

                    # 加载原始系数
                    coef_Qnm_format = {'original': None}

                    # 加载模型
                    pnet, rnet, _ = self.load_model(model_path=path, output_type=out_type,
                                                    coef_Qnm_format=coef_Qnm_format)

                    # Pnet 与 Rnet 的定点化
                    pnet, rnet = self.quantize_pnet_rnet(pnet, rnet, coef_format,
                                                         save=save_txt, save_model=save_model_pt)

                    return pnet, rnet

                def remove_boxes_through_distance(self, depth, pnet_boxes, x1, y1, x2, y2):

                    """
                    这个函数计算 bounding box 里距离，然后判断相关距离下的窗口是否符合人脸大小
                    :param depth: 深度数据
                    :param pnet_boxes: pnet_boxes
                    :param x1: pnet_boxes 里的 x1 位置 (向量)
                    :param y1: pnet_boxes 里的 x1 位置 (向量)
                    :param x2: pnet_boxes 里的 x2 位置 (向量)
                    :param y2: pnet_boxes 里的 x2 位置 (向量)
                    :param depth_ir_size_ratio: 深度图与 IR 图的尺度可能不一样, 如果是一样就是 1.
                    depth (40*32) 如果是 IR 图的一半 (80*64), 就是 0.5
                    :return:
                    """

                    def count_head_points(head_distance, fov, head_length, res):

                        """
                        计算在相应的距离下，理论上, 人脸点的数量
                        :param head_distance: 人头距离
                        :param fov: TOF 的 FOV 视场角
                        :param head_length: 可以是人脸宽度或者高度
                        :param res: 图像分辨率 (80, 64, 40, 32, ...)
                        :return:
                        """

                        # 计算人脸在 head_distance 这个距离下, 人脸占用的视场角
                        # 调用 arctan 函数, 为了加速嵌入式的运算, 使用查表的方式 并以 radian 而不是 ° 为单位
                        # 方法一, 直接 arctan 计算
                        angle_head1 = 2*np.arctan(head_length/(2*head_distance))
                        # 方法二, 通过查表计算
                        angle_head = 2*self.arc_tan_func_lut(x_vec=self.x_arctan, y_vec=self.y_arctan,
                                                                 xi=head_length/(2*head_distance))
                        print([angle_head1/np.pi*180, angle_head/np.pi*180, angle_head/np.pi*180-angle_head1/np.pi*180])

                        # 计算人脸占用点的数量
                        head_num_points = int(res/fov * angle_head)

                        return head_num_points

                    pnet_boxes2, head_distance = [], []

                    # 循环所有的窗口
                    for i in range(len(pnet_boxes)):

                        # 计算窗口的中心点, 和相应框的 width 与 height
                        x_c, y_c = int((x1[i] + x2[i])*0.5), int((y1[i] + y2[i])*0.5)
                        head_width, head_height = int(x2[i] - x1[i]), int(y2[i] - y1[i])

                        # depth 的大小与 IR 图不一致，所以x_c,y_c要做相应的索引变换 (IR 80*64; depth 40*32)
                        x_c, y_c = int(x_c*self.depth_ir_size_ratio), int(y_c*self.depth_ir_size_ratio)
                        head_distance = depth[:, :, y_c, x_c]/10  # 计算 depth 框中心的距离值 (以厘米为单位 /10)

                        ########################################################################################

                        # 计算在 head_distance 距离下, 人脸点的数量应该介于什么值的上下限 (ub: upper bound; lb: lower bound)
                        ub_face_height = count_head_points(head_distance=head_distance, fov=self.TOF_FOV_height, head_length=self.head_height[1], res=self.h)  # 人脸长度点的数量上限
                        lb_face_height = count_head_points(head_distance=head_distance, fov=self.TOF_FOV_height, head_length=self.head_height[0], res=self.h)  # 人脸长度点的数量下限
                        ub_face_width = count_head_points(head_distance=head_distance, fov=self.TOF_FOV_width, head_length=self.head_width[1], res=self.w)  # 人脸宽度点的数量上限
                        lb_face_width = count_head_points(head_distance=head_distance, fov=self.TOF_FOV_width, head_length=self.head_width[0], res=self.w)  # 人脸宽度点的数量下限

                        ########################################################################################

                        self.head_distance_all.append([self.ith_image, head_distance.item(), head_width, head_height])

                        # 如果人脸的长宽介于 lb 与 ub 之间, 认为是正确的, 导入 pnet_boxes2
                        if ((head_width <= ub_face_width) and (head_width >= lb_face_width)) and ((head_height <= ub_face_height) and (head_height >= lb_face_height)):

                            pnet_boxes2.append(pnet_boxes[i])
                            print(f'head distance = {head_distance} cm')
                            print(f'{lb_face_width}<= head_width = {head_width} <= {ub_face_width}')
                            print(f'{lb_face_height}<= head_width = {head_height} <= {ub_face_height}')

                        else:

                            pass

                    # 在 0 维度上 concatenate
                    if len(pnet_boxes2) >= 1:

                        pnet_boxes2 = torch.stack(pnet_boxes2, dim=0)

                        y1, y2, x1, x2 = self.pad(pnet_boxes2, self.w, self.h)

                    else:

                        pnet_boxes2 = np.array(pnet_boxes2)
                        x1, y1, x2, y2 = np.array([]), np.array([]), np.array([]), np.array([])

                    return pnet_boxes2, x1, y1, x2, y2

                def keep_max_pnet_prob_boxes(self, boxes, x1, y1, x2, y2):

                    """
                    选择最高概率的窗口
                    :param boxes: pnet boxes (pnet 窗口)
                    :param x1: boxes 里的 x1 值
                    :param y1: boxes 里的 Y1 值
                    :param x2: boxes 里的 x2 值
                    :param y2: boxes 里的 y2 值
                    :return:
                    """

                    # 提取人脸概率最大的框
                    num_max_boxes = self.pnet_boxes_num_max

                    if boxes.shape[0] >= 1:

                        # 窗口的最大数量不能大于现有窗口的数量
                        # 如果 num_max_boxes 大于现有窗口的数量, 把 num_max_boxes 制成现有窗口的数量
                        num_max_boxes = num_max_boxes if num_max_boxes < boxes.shape[0] else boxes.shape[0]

                        # pnet boxes 的排序是按照置信度最高的往下排序
                        # 所以取0到num_max_boxes个boxes就可以表述最高置信度的 num_max_boxes 个窗口
                        x1 = x1[0: num_max_boxes]
                        y1 = y1[0: num_max_boxes]
                        x2 = x2[0: num_max_boxes]
                        y2 = y2[0: num_max_boxes]

                        # 提取前 num_max_boxes 个窗口并进行排序
                        boxes = boxes[0: num_max_boxes, :].view(num_max_boxes, -1)

                    return boxes, x1, y1, x2, y2

                def keep_max_rnet_prob_boxes(self, boxes, num_max_boxes=1):

                    """
                    选择最高概率的窗口
                    :param boxes:
                    :param num_max_boxes:
                    :return:
                    """

                    # 提取人脸概率最大的框

                    if boxes.shape[0] >= 1:

                        # Number of boxes cannot exceed boxes
                        num_max_boxes = num_max_boxes if num_max_boxes < boxes.shape[0] else boxes.shape[0]

                        # Reshape the pnet_boxes
                        boxes = boxes[0: num_max_boxes, :].view(num_max_boxes, -1)

                    return boxes

                def main_sum_over_weight(self, path, pnet, rnet, format):

                        '''
                        把 conv1 深度的权重全加起来
                        目的：把 Pnet 与 Rnet 的输入 RGB 图像改成 灰度图像
                        :param param: 神经网络模型
                        :param format: {'pnet': 'Q4.12', 'rnet': 'Q2.14'}
                        :return:
                        '''

                        import torch
                        from fxpmath import Fxp

                        # 加载原始模型
                        pnet_temp, rnet_temp, _ = self.load_model(path, output_type='single', coef_Qnm_format={'original': 'None'})

                        def sum_over_weight(param, format):

                            # 提取weight和bias
                            weight = param.weight
                            bias = param.bias

                            # 提取输入卷积的尺寸大小
                            kernels, depth, height, width = weight.shape

                            # 构建输出的卷积
                            conv = torch.nn.Conv2d(in_channels=1, out_channels=kernels, kernel_size=(3, 3), stride=(1, 1))

                            # 遍历所有权重
                            with torch.no_grad():
                                for k in range(kernels):
                                    for i in range(height):
                                        for j in range(width):

                                            # 在 d 维度相加
                                            value = weight[k, :, i, j].sum().detach().numpy()

                                            if format is None:
                                                pass
                                            else:
                                                value = Fxp(value, dtype=format).__float__()

                                            conv.weight[k, 0, i, j] = torch.tensor(value)

                            with torch.no_grad():
                                for i in range(len(bias)):

                                    value = bias[i].data.numpy()
                                    value = Fxp(value, dtype=format).__float__()
                                    bias[i] = torch.tensor(value)

                            conv.bias = bias

                            return conv

                        # 在原始模型上的 conv1 叠加权重, 生成后替换 pnet 与 rnet 里的 conv1 权重
                        pnet.conv1 = sum_over_weight(pnet_temp.conv1, format['pnet'])
                        rnet.conv1 = sum_over_weight(rnet_temp.conv1, format['rnet'])

                        return pnet, rnet

                def model_forward(self, img, depth, pnet, rnet):

                    def plot_pnet_graph(self, img, boxes, name):

                        import copy as copy
                        img_plot = copy.deepcopy(img[0, 0, :, :]).detach().numpy()
                        img_plot = (255 * (img_plot - img_plot.min()) / (img_plot.max() - img_plot.min())).astype(
                            'uint8')
                        img_plot = np.dstack([img_plot] * 3)

                        for i in range(boxes.shape[0]):
                            # Start coordinate, here (5, 5)
                            # represents the top left corner of rectangle
                            start_point = (x1[i], y1[i])

                            # Ending coordinate, here (220, 220)
                            # represents the bottom right corner of rectangle
                            end_point = (x2[i], y2[i])

                            # Blue color in BGR
                            color = (0, 255, 0)

                            # Line thickness of 2 px
                            thickness = 1

                            # Using cv2.rectangle() method
                            # Draw a rectangle with blue line borders of thickness of 2 px
                            cv2.rectangle(img_plot, start_point, end_point, color, thickness)

                        # 绘图
                        # cv2.imshow('img', img_plot)
                        img_plot2 = copy.copy(img_plot)
                        cv2.putText(img=img_plot, text=f'{len(boxes)}', org=(1, 10), fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.4, color=[0, 255, 0])

                        # 生成 jpg 图像
                        cv2.imwrite(f'img_pnet_{self.ith_image}{name}.jpg', img_plot)

                        return None

                    def plot_rnet_graph(self, img, boxes, name):

                        import copy as copy
                        img_plot = copy.deepcopy(img[0, 0, :, :]).detach().numpy()
                        img_plot = (255 * (img_plot - img_plot.min()) / (img_plot.max() - img_plot.min())).astype(
                            'uint8')
                        img_plot = np.dstack([img_plot] * 3)

                        for i in range(boxes.shape[0]):
                            # Start coordinate, here (5, 5)
                            # represents the top left corner of rectangle
                            start_point = (int(boxes[i][0]), int(boxes[i][1]))

                            # Ending coordinate, here (220, 220)
                            # represents the bottom right corner of rectangle
                            end_point = (int(boxes[i][2]), int(boxes[i][3]))

                            # Blue color in BGR
                            color = (0, 255, 0)

                            # Line thickness of 2 px
                            thickness = 1

                            # Using cv2.rectangle() method
                            # Draw a rectangle with blue line borders of thickness of 2 px
                            cv2.rectangle(img_plot, start_point, end_point, color, thickness)

                        # 绘图
                        # cv2.imshow('img', img_plot)

                        # 生成 jpg 图像
                        cv2.imwrite(f'img_rnet_{self.ith_image}{name}.jpg', img_plot)

                        return None

                    from torch.nn.functional import interpolate
                    from torchvision.ops.boxes import batched_nms
                    from torchvision.ops.boxes import nms

                    pnet_boxes, image_inds, scale_picks = [], [], []
                    # offset = 0

                    # 每一个缩放后的图像导入 Pnet 做前向计算
                    for (scale, size) in zip(self.scales, self.sizes):

                        # 根据缩放比例，降采样
                        # im_data = interpolate(img, size=size, mode="area")

                        # scale between [-1, 1] for image data [0, 255]
                        # im_data = (im_data - 127.5) * 0.0078125

                        # 计算定点化斜率和偏置
                        # Qnm_n_frac 是第一层的定点化的小数位
                        # Qnm_n_frac = 12  # Q4.12; 12
                        # self.slope_ = int(2 ** (Qnm_n_frac + 1) / (self.img_max - self.img_min))
                        # self.intercept_ = int(-(2*self.img_min / (self.img_max - self.img_min) + 1)*2**Qnm_n_frac)

                        # 图像降采样处理
                        im_data = self.resize_pnet(img, size)

                        # 归一化
                        im_data = 2 * (im_data - self.img_min) * self.invert_img_max_min - 1

                        # Pnet 的前向计算
                        # reg, probs = pnet(im_data)  # 原始数据前向计算
                        x0, x1, x2, x3, x4, x5, x6, x7, a1, probs, reg, string = pnet(im_data)

                        # 统计分析神经网络的输入输出
                        # saved_jpg_name = 0  # 不存JPG: 0/None; 存JPG: 'pnet'
                        # saved_filename = 0  # 不存txt: 0/None; 存txt: f'pnet_input_output_{x0.shape[2]}_{x0.shape[3]}'
                        # results = self.statistical_analysis_and_plot_histogram(
                        #     x0, x1, x2, x3, x4, x5, x6, x7, a1, probs, reg,
                        #     string=string, saved_jpg_name=saved_jpg_name, saved_filename=saved_filename)

                        # 第一次概率筛选：通过概率挑选人脸的候选框
                        # boxes_scale_original, image_inds_scale = self.generateBoundingBox(reg, probs[:, 1, :, :], scale, self.thresholds[0])
                        boxes_scale = self.generateBoundingBox_cks(reg=reg, probs=a1, scale=scale, thresh=self.inv_thresholds[0])
                        print(f'image_size = {size}; number of boxes = {len(boxes_scale)}')
                        # boxes.append(boxes_scale)
                        # image_inds.append(image_inds_scale)

                        # 第二次筛选：通过 NMS 筛选候选框
                        # pick = batched_nms(boxes=boxes_scale[:, :4], scores=boxes_scale[:, 4], idxs=image_inds_scale, iou_threshold=0.5)
                        pick = nms(boxes=boxes_scale[:, :4], scores=-boxes_scale[:, 4], iou_threshold=self.pnet_iou_thresh[0])
                        # scale_picks.append(pick + offset)  # pick 需要添加上一组的 offset，输出正确的 indices
                        # offset += boxes_scale.shape[0]  # 这一组候选窗口的数量

                        # 挑选出候选窗口
                        pnet_boxes.append(boxes_scale[pick])
                        # image_inds.append(image_inds_scale[pick])

                    # 第三次筛选：通过 NMS 筛选候选框 [不同 Pnet 降采样后的 boxes 比较]
                    # _, _, _, _, _, image_inds, scale_picks = self.pnet_bbox_output(boxes, image_inds, scale_picks, threshold=0.7)
                    y1, y2, x1, x2, pnet_boxes = self.pnet_bbox_output_cks(boxes=pnet_boxes, threshold=self.pnet_iou_thresh[1])
                    # image_inds = torch.cat(image_inds, dim=0)
                    print(f'Generated boxes after PNet = {len(pnet_boxes)}')

                    # 把所有生成的窗口绘出来
                    plot_pnet_graph(self, img, pnet_boxes, name='_ori')

                    # 如果是模式零, 直接 return pnet_boxes[0, :] (最大置信度的窗口), continue 下一帧, 不需要跑 Rnet
                    if self.mode == 0:
                        plot_pnet_graph(self, img, pnet_boxes[0, :].unsqueeze(0), name=f'_mode_0_{1/(2**pnet_boxes[0, 4] + 1):0.2f}')
                        return pnet_boxes[0, :].unsqueeze(0)

                    # 如果最高置信度的窗口, 其置信度大于一个很大的阈值 (举例: 0.97)，人脸的概率非常非常大, 直接输出
                    # 不需要 Rnet 去评估相关窗口是否有人脸
                    if pnet_boxes.shape[0] > 0:  # 至少有1个窗口
                        if pnet_boxes[0, 4] <= self.prob_pnet_max_direct_output:  # 窗口置信度大于阈值
                            #绘图
                            plot_pnet_graph(self, img, pnet_boxes[0, :].unsqueeze(0), name=f'_max_prob_{1/(2**pnet_boxes[0, 4] + 1):0.2f}')
                            if self.mode in [1, 2]:
                                plot_rnet_graph(self, img, pnet_boxes[0, :].unsqueeze(0), name=f'_{1/(2**pnet_boxes[0, -1] + 1):0.2f}')
                            return pnet_boxes[0, :].unsqueeze(0)  # 直接返回
                    else:
                        # 既然没有窗口, 直接返回, 不需要进行下一步的计算
                        plot_pnet_graph(self, img, pnet_boxes, name=f'')
                        plot_rnet_graph(self, img, pnet_boxes, name=f'')
                        return pnet_boxes

                    # 通过距离过滤 pnet_boxes 窗口, 减少不符合人脸窗口的数量
                    if self.depth_filter_on_off is True:
                        pnet_boxes, x1, y1, x2, y2 = self.remove_boxes_through_distance(depth, pnet_boxes, x1, y1, x2, y2)

                    # 保留前 self.pnet_boxes_num_max 置信度最高概率的前几个窗口
                    pnet_boxes, x1, y1, x2, y2 = self.keep_max_pnet_prob_boxes(boxes=pnet_boxes, x1=x1, y1=y1, x2=x2, y2=y2)

                    # 绘图
                    if pnet_boxes.shape[0] > 0:
                        plot_pnet_graph(self, img, pnet_boxes, name=f'_{1/(2**pnet_boxes[0, -1] + 1):0.2f}')
                    else:
                        plot_pnet_graph(self, img, pnet_boxes, name=f'')
                        plot_rnet_graph(self, img, pnet_boxes, name=f'')

                    '*****************************************************************************************************************'

                    # Rnet

                    # 储存需要导入 Rnet 计算的N个图像 (N, 1, 24, 24) 格式
                    im_data = []

                    # 如果 Pnet 成功生成候选框
                    if pnet_boxes.shape[0] > 0:

                        # 循环所有 Pnet 候选窗口
                        for k in range(len(y1)):

                            # 如果坐标排序是正确的
                            if y2[k] > (y1[k] - 1) and x2[k] > (x1[k] - 1):

                                # 原始图截取 Pnet 生成的候选框
                                # img_k = img[image_inds[k], :, (y1[k] - 1):y2[k], (x1[k] - 1):x2[k]].unsqueeze(0)
                                img_k = img[0, 0, (y1[k] - 1):y2[k], (x1[k] - 1):x2[k]].unsqueeze(0).unsqueeze(0)

                                # 截取图降采样到 24*24
                                # im_data.append(interpolate(img_k, size=(24, 24), mode="area"))

                                # 截取图降采样/升采样到 24*24
                                img_k_24_24 = self.resize_rnet_to_24_24(img_k)

                                # 归一化
                                img_k_24_24 = 2 * (img_k_24_24 - self.img_min) * self.invert_img_max_min - 1

                                im_data.append(img_k_24_24)

                        im_data = torch.cat(im_data, dim=0)  # list 行方向拼接 构成 torch tensor 格式
                        # im_data = (im_data - 127.5) * 0.0078125  # 归一化

                        # Rnet 的前向计算
                        # reg, probs = rnet(im_data)
                        # im_data = im_data[0, :, :, :].unsqueeze(0)
                        x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a1, probs, reg, string = rnet(im_data)

                        # 统计分析神经网络的输入输出
                        # saved_jpg_name = 0  # 不存JPG: 0/None; 存JPG: 'rnet'
                        # saved_filename = 0  # 不存txt: 0/None; 存txt: f'rnet_input_output_{x0.shape[2]}_{x0.shape[3]}'
                        # results = self.statistical_analysis_and_plot_histogram(
                        #     x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a1, probs, reg,
                        #     string=string, saved_jpg_name=saved_jpg_name, saved_filename=saved_filename)

                        # 通过 Rnet 的输出结果， 修正 Pnet 候选框 (原码)
                        # pnet_boxes = self.after_process_rnet_bbox_original(boxes=pnet_boxes, reg=reg, probs=probs,
                        #                                          image_inds=image_inds)

                        # 通过 Rnet 的输出结果， 修正 Pnet 候选框
                        # pnet_boxes = self.after_process_rnet_bbox_original(boxes=pnet_boxes, reg=reg, probs=probs, image_inds=image_inds)
                        pnet_boxes = self.after_process_rnet_bbox(pnet_boxes=pnet_boxes,
                                                                  rnet_boxes=reg,
                                                                  rnet_probs=a1,
                                                                  rnet_inv_thresh=self.inv_thresholds[1],
                                                                  iou_threshold=self.rnet_iou_thresh)

                    # 保留最高概率的窗口 (Rnet 输出一个窗口)
                    pnet_boxes = self.keep_max_rnet_prob_boxes(boxes=pnet_boxes, num_max_boxes=1)

                    # 把所有生成的窗口绘出来
                    if pnet_boxes.shape[0] >= 1:
                        plot_rnet_graph(self, img, pnet_boxes, name=f'_{1/(2**pnet_boxes[0, -1] + 1):0.2f}')
                    else:
                        plot_rnet_graph(self, img, pnet_boxes, name='')

                    return pnet_boxes

                def main(self):

                    # Load image (torch.tensor format and float32) selection = 'RGB' or 'grayscale
                    # ir_group = self.load_image2(selection='grayscale')
                    ir_group, depth_group = self.load_image3_8132(choice=10)

                    '************************************************************************************************'

                    # 装载 Pnet 与 Rnet 模型
                    path = './model/mtcnn/facenet_pytorch/pt_files/'

                    # 选择输出结果的种类
                    # output_type ='every':每一层输入输出都输出 or 'every_quantization': 每一层量化 or 'single'
                    out_type = 'every_quantization'

                    # Load 权重的Qn.m系数  (加载运行 main_save_model_coeffs_as_txt_pt 后所生成的 pt)
                    # coef_Qnm_format = '{'original': None} or {'quantization': {'pnet': 'Q3.13', 'rnet': 'Q2.14'}}
                    coef_Qnm_format = {'quantization': {'pnet': 'Q3.13', 'rnet': 'Q2.14'}}

                    # 设定输入输出的 Qn.m
                    # inout_Qnm_format = {'pnet': 'Q3.13', 'rnet': 'Q2.14'} or {'pnet': 'Q4.12', 'rnet': 'Q3.13'} or {'pnet': None, 'rnet': None}
                    inout_Qnm_format = {'pnet': 'Q3.13', 'rnet': 'Q2.14'}

                    # 选择储存输入输出的数据 (pnet 与 rnet 前向计算的时候，会导出每个输出结果）
                    save_in_out = {'pnet': False, 'rnet': False}

                    # 加载模型
                    pnet, rnet, _ = self.load_model(model_path=path, output_type=out_type,
                                                    coef_Qnm_format=coef_Qnm_format, inout_Qnm_format=inout_Qnm_format,
                                                    save_in_out=save_in_out)

                    # 算法模型优化/修改
                    # 把 conv1 的深度维度从3改成1
                    pnet, rnet = self.main_sum_over_weight(path, pnet, rnet, format={'pnet': 'Q4.12', 'rnet': 'Q2.14'})

                    '************************************************************************************************'

                    # 对原始图像进行升采样
                    from torchvision.transforms import Resize
                    from torchvision.transforms import InterpolationMode
                    # resize = Resize(size=(self.h, self.w), interpolation=InterpolationMode.BILINEAR)
                    # ir_group, depth_group = resize(ir_group), resize(depth_group)

                    # 循环每一帧图像
                    for i in range(0, len(ir_group), 1):

                        # 第几帧
                        self.ith_image = i
                        print(f'img {i}')

                        # 当前帧的图像
                        img, depth = ir_group[i].unsqueeze(0), depth_group[i].unsqueeze(0)

                        # 提取最大值最小值
                        self.img_max, self.img_min = img.max(), img.min()
                        self.invert_img_max_min = 1 / (self.img_max - self.img_min)

                        # 把每一帧图像导入模型计算，最终返回窗口
                        boxes = self.model_forward(img=img, depth=depth, pnet=pnet.eval(), rnet=rnet.eval())

                        if len(boxes) > 0:
                            print(boxes)

                        # 保存图像
                        # cv2.imwrite(f'ir_{i}.jpg', np.array((img - self.img_min)*self.invert_img_max_min*255).astype('uint8')[0, 0, :, :])

                        # depth = depth_group[i, 0, :, :]
                        # cv2.imwrite(f'depth_{i}.jpg', np.array((depth - depth.min())/(depth.max() - depth.min())*255).astype('uint8'))

                    np.savetxt('test.txt', self.head_distance_all, fmt='%1.1f')

                    return None

    class human_pose():

        class mediapipe_pose():

            def __init__(self):
                pass

            def webcam(self):

                import cv2
                import mediapipe as mp
                mp_drawing = mp.solutions.drawing_utils
                mp_drawing_styles = mp.solutions.drawing_styles
                mp_pose = mp.solutions.pose

                # For webcam input:
                cap = cv2.VideoCapture(0)
                with mp_pose.Pose(
                        min_detection_confidence=0.5,
                        min_tracking_confidence=0.5) as pose:
                    while cap.isOpened():
                        success, image = cap.read()
                        if not success:
                            print("Ignoring empty camera frame.")
                            # If loading a video, use 'break' instead of 'continue'.
                            continue

                        # To improve performance, optionally mark the image as not writeable to
                        # pass by reference.
                        image.flags.writeable = False
                        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                        results = pose.process(image)

                        # Draw the pose annotation on the image.
                        image.flags.writeable = True
                        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                        mp_drawing.draw_landmarks(
                            image,
                            results.pose_landmarks,
                            mp_pose.POSE_CONNECTIONS,
                            landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
                        # Flip the image horizontally for a selfie-view display.
                        cv2.imshow('MediaPipe Pose', cv2.flip(image, 1))
                        if cv2.waitKey(5) & 0xFF == 27:
                            break
                cap.release()

            def image(self, img_folder, img_format='jpg', segmentation='no', saved_image='no'):

                import glob
                import cv2
                import mediapipe as mp
                mp_drawing = mp.solutions.drawing_utils
                mp_drawing_styles = mp.solutions.drawing_styles
                mp_pose = mp.solutions.pose

                # For static images:
                BG_COLOR = (192, 192, 192)  # gray

                with mp_pose.Pose(

                        static_image_mode=True,
                        model_complexity=2,
                        enable_segmentation=True,
                        min_detection_confidence=0.5) as pose:

                    # Loop through all images inside the folder
                    for file in glob.glob(img_folder + '/*.' + img_format):

                        # Read image
                        image = cv2.imread(file)

                        # obtain the size of the image
                        image_height, image_width, image_channel = image.shape

                        # Turn grayscale image into a 3 channels RGB image
                        if image_channel == 1:
                            image = np.stack((image,) * 3, axis=-1)

                        # Convert the BGR image to RGB before processing.
                        results = pose.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

                        if not results.pose_landmarks:
                            continue
                        print(
                            f'Nose coordinates: ('
                            f'{results.pose_landmarks.landmark[mp_pose.PoseLandmark.NOSE].x * image_width}, '
                            f'{results.pose_landmarks.landmark[mp_pose.PoseLandmark.NOSE].y * image_height})'
                        )

                        annotated_image = image.copy()

                        if segmentation == 'yes':

                            # Draw segmentation on the image.
                            # To improve segmentation around boundaries, consider applying a joint
                            # bilateral filter to "results.segmentation_mask" with "image".

                            condition = np.stack((results.segmentation_mask,) * 3, axis=-1) > 0.1
                            bg_image = np.zeros(image.shape, dtype=np.uint8)
                            bg_image[:] = BG_COLOR
                            annotated_image = np.where(condition, annotated_image, bg_image)

                        # Draw pose landmarks on the image.
                        mp_drawing.draw_landmarks(
                            annotated_image,
                            results.pose_landmarks,
                            mp_pose.POSE_CONNECTIONS,
                            landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

                        # Save the image if intended
                        if saved_image == 'yes':
                            cv2.imwrite(file[0:-4] + '_marks.jpg', annotated_image)

                        # Plot pose world landmarks.
                        mp_drawing.plot_landmarks(
                            results.pose_world_landmarks, mp_pose.POSE_CONNECTIONS)

                        # Plot the image
                        # cv2.imshow('annotated_image', annotated_image)

                return None

    class palm():

        class mediapipe_palm():

            def __init__(self):
                pass

            def webcam(self):

                import cv2
                import numpy as np
                import matplotlib.pyplot as plt
                import mediapipe as mp
                import time
                from PIL import Image

                # video capture
                cap = cv2.VideoCapture(0)

                mpHands = mp.solutions.hands
                hands = mpHands.Hands()
                mpDraw = mp.solutions.drawing_utils

                pTime = 0
                cTime = 0

                while True:

                    success, img = cap.read()

                    imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    imgGray = np.zeros(np.shape(img), dtype='uint8')
                    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                    for i in range(3):
                        imgGray[:, :, i] = gray_img

                    results = hands.process(imgRGB)
                    h, w, c = img.shape

                    if results.multi_hand_landmarks:
                        for handLms in results.multi_hand_landmarks:
                            for id, lm in enumerate(handLms.landmark):
                                if id == 0:
                                    # Coordinates of the corresponding ID
                                    cx, cy = int(lm.x * w), int(lm.y * h)
                                    cv2.circle(img, (cx, cy), 15, (0, 0, 0), 3, cv2.FILLED)

                            mpDraw.draw_landmarks(img, handLms, mpHands.HAND_CONNECTIONS)

                    cTime = time.time()
                    fps = 1 / (cTime - pTime)
                    pTime = cTime
                    cv2.putText(img, str(int(fps)), (10, 100), cv2.FONT_HERSHEY_COMPLEX, 3, (0, 0, 0), 3)

                    cv2.imshow('Image', img)

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break

                # When everything done, release the capture and destroy the windows
                cap.release()
                cv2.destroyAllWindows()

class data_manipulation():

    def __init__(self):
        return None

    def convert_cat_conts_data_to_tensor(df, cat_cols, cont_cols, y_col, problem='regression'):

        '''
        Extract the categorical, continuous and y attributes in the dataframe
        Output as torch tensor format
        :param df: dataframe
        :param cat_cols: categorical attributes (a list of strings for the name of the attributes)
        :param cont_cols: continuous attributes (a list of strings for the name of the attributes)
        :param y_col: Actual output attribute (list for the name of the attribute)
        :param df: dataframe which its columns have been converted into categorical type
        :return:
        cats: categorical data in tensor format
        conts: continuous data in tensor format
        y: actual output as tensor (shape is N rather than N*1)

        Example:
        cat_cols = ['Hour', 'AMorPM', 'Weekday']
        cont_cols = ['pickup_longitude', 'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude', 'passenger_count',
                 'dist_km']
        y_col = ['fare_amount']  # this column contains the labels
        cats, conts, y = convert_cat_conts_data_to_tensor(df, cat_cols, cont_cols, y_col)
        '''

        # Initialize the categorical variable for storage
        cats = np.zeros((df.shape[0], len(cat_cols)), dtype=np.int8)

        # Convert dataframe to categorical data
        for i, cat in enumerate(cat_cols):

            # Convert dataframe column into categorical type and extract all its categorical values in integer format
            df[cat] = df[cat].astype('category')
            cats[:, i] = df[cat].cat.codes.values

            # Convert continuous variables to a tensor
        conts = np.stack([df[col].values for col in cont_cols], 1)

        # Convert both categorical and continuous numpy array to tensor
        cats = torch.tensor(cats, dtype=torch.int64)
        conts = torch.tensor(conts, dtype=torch.float)

        if problem == 'regression':
            # y data (has to be column vector)
            y = torch.tensor(df[y_col].values, dtype=torch.float32).reshape(-1, 1)
        elif problem == 'classification':
            # y data (has to be column vector)
            y = torch.tensor(df[y_col].values).flatten()

        return df, cats, conts, y

class performance_metrics():

    def __init__(self):
        return

    def metrics(self, actual, predict):

        '''
        :param actual: actual data set (column vector)
        :param predict: predicted data set (column vector)
        :return:
        cm: confusion matrix
        TP, TN, FP, FN : true positives, true negatives, false positives, false negatives
        accuracy, precision, recall and F1 metrics
        '''

        # Import the confusion matrix library
        from sklearn.metrics import confusion_matrix

        # Calculate the confusion matrix
        cm = confusion_matrix(actual, predict)

        # If all the actual and predict values are the same
        # modify the cm to be of 2*2 matrix size
        if np.sum(predict == 0) == predict.__len__():
            cm = np.array([[cm[0][0], 0], [0, 0]])
        elif np.sum(predict == 1) == predict.__len__():
            cm = np.array([[0, 0], [0, cm[0][0]]])

        # Calculate true positive, true negative, false positive and false negative counts
        TP, TN, FP, FN = cm[1, 1], cm[0, 0], cm[0, 1], cm[1, 0]

        # Accuracy, Recall, Precision and F1 metrics
        accuracy = (TP + TN) / (TP + FP + FN + TN)

        if TP + FP != 0:
            precision = TP / (TP + FP)
        else:
            precision = np.nan

        if TP + FN != 0:
            recall = TP / (TP + FN)
        else:
            recall = np.nan

        F1 = (2 * precision * recall) / (precision + recall)

        return cm, TP, TN, FP, FN, accuracy, precision, recall, F1