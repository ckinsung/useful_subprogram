import numpy as np
import AI_cks as ai
import torch
import matplotlib.pyplot as plt
import torch.nn as nn
import random
import pandas as pd
from icecream import ic
import cv2
import os
import copy
# import pcl
import pickle
from natsort import natsorted
import glob
import pcl_cks as pcl2


class main_ruimuke_3D_processing():
    '''
    3D感知技术与实践（第二期）
    '''

    class lesson_1():

        def __init__(self):
            pass

    class lesson_2():

        def __init__(self):
            pass

    class lesson_3():

        '''
        3D datatype conversion and manipulation (3D数据表示和转换)
        '''

        def __init__(self):
            pass

        def main_voxelize_pc(self):

            '''
            Convert point cloud data to voxel matrix
            input:
            pc: point cloud (N * 3 format)

            :return:
            voxel_mat: voxel matrix
            '''

            def generate_sphere():
                # 随意生成球体点云数据
                np.random.seed(0)
                phi = np.linspace(0, np.pi, 100)
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.outer(np.sin(theta), np.cos(phi))
                y = np.outer(np.sin(theta), np.sin(phi))
                z = np.outer(np.cos(theta), np.ones_like(phi))
                x = x - np.random.rand(100, 100) / 5 / 2  # 添加噪点
                y = y - np.random.rand(100, 100) / 5 / 2  # 添加噪点
                z = z - np.random.rand(100, 100) / 5 / 2  # 添加噪点
                pc = np.array([x.ravel(), y.ravel(), z.ravel()], dtype=np.float32).T

                return pc

            def plot_figure(voxel_mat):
                # Display voxel matrix
                fig = plt.figure(1)
                bx = fig.gca(projection='3d')
                bx.voxels(voxel_mat, facecolors='r', edgecolors='k')
                plt.title('Voxel')
                plt.show(block=True)

                return None

            # 读取点云数据
            # pc = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_4/pc.csv', delimiter=',').astype(np.float32)
            # voxel_mat = pcl2.datastructure().pc_to_voxel_array(pc, pstart=(-0.3, -0.3, 0.9), pend=(0.6, 0.3, 1.5),
            #                                                    grid_sz=0.02)

            pc = generate_sphere()

            # 格点化
            voxel_mat = pcl2.datastructure().pc_to_voxel_array(pc, pstart=(-1.2, -1.2, -1.2), pend=(1.2, 1.2, 1.2),
                                                               grid_sz=0.1)

            # 绘图
            plot_figure(voxel_mat)

            return None

        def main_generate_ply(self):

            '''
            Assign grayscale image colour to the depth image and output a ply file
            input:
            depth image
            grayscale image

            :return:
            ply file
            '''

            def plot_figure(img_gray, img_dep):

                # 显示灰度图和强度图
                plt.clf()
                plt.figure(1)
                plt.subplot(1, 2, 1)
                plt.imshow(img_gray, cmap='gray')
                plt.title('gray')
                plt.subplot(1, 2, 2)
                plt.imshow(img_dep, cmap='jet')
                plt.title('depth')
                plt.show(block=True)

                return None

            def depth_to_pc(CAM_WID, CAM_HGT, CAM_CX, CAM_CY, img_dep):

                x, y = np.meshgrid(range(CAM_WID), range(CAM_HGT))
                x = x.astype(np.float32) - CAM_CX
                y = y.astype(np.float32) - CAM_CY
                img_z = img_dep.copy()
                if False:  # 如需矫正视线到Z的转换
                    f = (CAM_FX + CAM_FY) / 2.0
                    img_z *= f / np.sqrt(x ** 2 + y ** 2 + f ** 2)
                pc_x = img_z * x / CAM_FX  # X=Z*(u-cx)/fx
                pc_y = img_z * y / CAM_FY  # Y=Z*(v-cy)/fy
                pc = np.array([pc_x.ravel(), pc_y.ravel(), img_z.ravel()]).T

                return pc

            import math
            import numpy as np
            import cv2
            import matplotlib.pyplot as plt
            from mpl_toolkits.mplot3d import Axes3D

            CAM_WID, CAM_HGT = 640, 480  # 图像尺寸
            CAM_FX, CAM_FY = 795.209, 793.957  # fx/fy
            CAM_CX, CAM_CY = 332.031, 231.308  # cx/cy

            # 加载灰度图\深度图数据
            img_gray = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_3/head_gray.csv', delimiter=',').astype(np.float32)
            img_dep = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_3/head_dep.csv', delimiter=',').astype(np.float32)

            # 绘图
            plot_figure(img_gray, img_dep)

            # 深度图转点云
            pc = depth_to_pc(CAM_WID, CAM_HGT, CAM_CX, CAM_CY, img_dep)

            # 亮度图上限调整
            img_gray = (np.round(img_gray / np.max(img_gray) * 255)).astype(int)

            # 计算三角剖分位置
            idx = np.arange(CAM_HGT * CAM_WID).reshape(CAM_HGT, CAM_WID)
            tri_up = np.array([idx, np.roll(idx, -1, axis=0), np.roll(np.roll(idx, -1, axis=0), 1, axis=1)])
            tri_dn = np.array([idx, np.roll(idx, 1, axis=1), np.roll(np.roll(idx, 1, axis=1), -1, axis=0)])

            # 生成 ply 文件
            saved_folder = './results/ruimuke/lesson 3/main_generate_ply/'
            fp = open(saved_folder + 'head.ply', 'wt')
            fp.write('ply\nformat ascii 1.0\nelement vertex %d\n' % (CAM_HGT * CAM_WID))
            fp.write(
                'property float32 x\nproperty float32 y\nproperty float32 z\nproperty uint8 red\nproperty uint8 green\nproperty uint8 blue\n')
            fp.write('element face %d\n' % ((CAM_HGT - 1) * (CAM_WID - 1) * 2))
            fp.write('property list uint8 int32 vertex_index\nend_header\n')

            for p, c in zip(pc, img_gray.ravel()):
                fp.write('%f %f %f %d %d %d\n' % (p[0], p[1], p[2], c, c, c))

            for y in range(0, CAM_HGT - 1):
                for x in range(1, CAM_WID):
                    fp.write('3 %d %d %d\n' % (tri_up[0, y, x], tri_up[1, y, x], tri_up[2, y, x]))
                    fp.write('3 %d %d %d\n' % (tri_dn[0, y, x], tri_dn[1, y, x], tri_dn[2, y, x]))
            fp.close()

            return None

    class lesson_4():

        '''
        filtering techniques for depth image and point cloud data (深度图和点云数据底层处理算法)
        '''

        def __init__(self):
            pass

        def main_bilateral_filter_depth(self):

            '''
            bilateral filter for depth image
            :return:
            smoothened depth image
            '''

            def plot_graph(depth, imgf_mean, imgf_bilateral):

                plt.clf()
                fig = plt.figure(1)
                fig.set_size_inches(8, 6)

                ax = fig.add_subplot(1, 3, 1)
                plt.imshow(depth, cmap='gray')
                plt.title('original')

                ax = fig.add_subplot(1, 3, 2)
                plt.imshow(imgf_mean, cmap='gray')
                plt.title('5*5 mean')

                ax = fig.add_subplot(1, 3, 3)
                plt.imshow(imgf_bilateral, cmap='gray')
                plt.title('5*5 mean + bilateral filter')

                plt.show(block=True)

                return None

            depth = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_4/depth_head.csv', delimiter=',').astype(np.float32)

            # Initialize the output image
            imgf_bilateral, imgf_mean = np.ones(np.shape(depth)), np.ones(np.shape(depth))

            # define parameters
            ker_size = 5
            sigma_depth = 300
            IMG_HGT, IMG_WID = np.shape(depth)

            ker_size_half = ker_size // 2  # Calculate half width of the kernel size
            ker1 = np.ones((ker_size, ker_size))  # Initialize the mean filter kernel

            for y in range(ker_size_half, IMG_HGT - ker_size_half):
                for x in range(ker_size_half, IMG_WID - ker_size_half):
                    # sliding window for the convolution
                    win_dep = depth[y - ker_size_half: y + ker_size_half + 1, x - ker_size_half: x + ker_size_half + 1]

                    # calculate the bilateral filter kernel's elements
                    ker2 = np.exp(-(win_dep - depth[y, x]) ** 2 / (sigma_depth ** 2))

                    ker12 = ker1 * ker2  # bilateral filter kernel
                    ker10 = ker1  # mean filter

                    ker12 = ker12 / np.sum(ker12)  # normalization bilateral filter kernel
                    ker10 = ker10 / np.sum(ker10)  # normalization mean filter kernel

                    # applying the kernel on the pixel
                    imgf_bilateral[y, x] = np.sum(ker12 * win_dep)
                    imgf_mean[y, x] = np.sum(ker10 * win_dep)

            # Plot the results
            plot_graph(depth, imgf_mean, imgf_bilateral)

            return None

        def main_pc_smooth(self):

            '''
            Moving least squares (移动最小二乘法滤波)
            Smoothing through interpolation of point cloud data
            Fitting K neighbourhood points and interpolate/deduce its current point using the fitted curve
            :return:
            '''

            def generate_sphere():
                # 随意生成球体点云数据
                np.random.seed(0)
                phi = np.linspace(0, np.pi, 100)
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.outer(np.sin(theta), np.cos(phi))
                y = np.outer(np.sin(theta), np.sin(phi))
                z = np.outer(np.cos(theta), np.ones_like(phi))
                x = x - np.random.rand(100, 100) / 5 / 2  # 添加噪点
                y = y - np.random.rand(100, 100) / 5 / 2  # 添加噪点
                z = z - np.random.rand(100, 100) / 5 / 2  # 添加噪点
                pc = np.array([x.ravel(), y.ravel(), z.ravel()], dtype=np.float32).T

                return pc

            def mls(p, pc_nn):
                num = pc_nn.shape[0]

                # 计算近邻的加权平面拟合
                pc_w = pc_nn
                pc_w = pc_w - np.mean(pc_w, axis=0)

                # 用特征值分解计算平面方向
                M = np.dot(pc_w.T, pc_w)  # covariance matrix
                E, F = np.linalg.eig(M)  # E: 特征值, F: 特征向量
                idx = np.argsort(E)

                uz = F[:, idx[0]].ravel()  # 平面法向量 (对应最小特征值)
                ux = F[:, idx[1]].ravel()  # 平面方向x (对应次小特征值)
                uy = F[:, idx[2]].ravel()  # 平面方向y (对应最大特征值)

                # 计算目标点到平面的投影
                px, py, pz = np.sum(p * ux), np.sum(p * uy), np.sum(p * uz)

                # 计算所有邻近点到平面的投影, 以 px, py 为中心平移
                pc_nnx = np.dot(pc_nn, ux) - px  # (用内积) 计算领域点在平面上的投影
                pc_nny = np.dot(pc_nn, uy) - py  # 投影点以待滤波器的投影位置为远点
                pc_nnz = np.dot(pc_nn, uz)

                # 计算2次拟合函数，拟合系数为w
                pc_nnx.shape = pc_nny.shape = pc_nnz.shape = num
                V = np.array([np.ones(num), pc_nnx, pc_nny, pc_nnx * pc_nny, pc_nnx ** 2, pc_nny ** 2]).T
                w = np.dot(np.linalg.inv(np.dot(V.T, V)), np.dot(V.T, pc_nnz))

                # 计算拟合函数在原点数值，沿着法向量方向调整作为目标点平滑结果
                p_new = p + (w[0] - pz) * uz

                return p_new

            def sample_spherical(npoints, ndim=3):
                vec = np.random.randn(ndim, npoints)
                vec /= np.linalg.norm(vec, axis=0)
                return vec

            def plot_figure(pc, pc_output):
                # Plot the subplots with different view perspective
                plt.clf()
                fig = plt.figure(1)
                fig.set_size_inches(12, 8)

                ax = fig.add_subplot(1, 2, 1, projection='3d')
                ax.plot(pc[:, 0], pc[:, 1], pc[:, 2], 'k.', markersize=0.5)
                ax.set_xlabel('X')
                ax.set_ylabel('Y')
                ax.set_zlabel('Z')
                ax.set_xlim([-1.2, 1.2])
                ax.set_ylim([-1.2, 1.2])
                ax.set_zlim([-1.2, 1.2])
                ax.view_init(azim=45, elev=15)
                plt.title('original sphere (black)')

                ax = fig.add_subplot(1, 2, 2, projection='3d')
                ax.plot(pc_output[:, 0], pc_output[:, 1], pc_output[:, 2], 'r.', markersize=0.5)
                ax.set_xlabel('X')
                ax.set_ylabel('Y')
                ax.set_zlabel('Z')
                ax.set_xlim([-1.2, 1.2])
                ax.set_ylim([-1.2, 1.2])
                ax.set_zlim([-1.2, 1.2])
                ax.view_init(azim=45, elev=15)
                plt.title('smoothened sphere (red)')
                plt.show(block=True)

                return None

            # pc = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_4/pc.csv', delimiter=',').astype(np.float32)

            # 随意生成球体点云数据
            pc = generate_sphere()

            # PCL数据对象
            cloud = pcl.PointCloud()
            cloud.from_array(pc.astype(np.float32))

            # 构建 kdtree 用于快速最近邻搜索
            kdtree = cloud.make_kdtree_flann()

            # 生成用于查询的几个点云(可以是所有的点 pc 或者一部分点 pc[...])
            searchPoint = pcl.PointCloud()
            searchPoint.from_array(pc)  # searchPoint.from_array(pc[0:5, :])

            # K 近邻查询 (对应 Matlab 的 [indices,dists] = findNearestNeighbors(ptCloud, point, K))
            K = 40  # 搜寻邻近K个点
            [idx, dist] = kdtree.nearest_k_search_for_cloud(searchPoint, K)

            # 初始化平滑后的点云
            pc_output = np.zeros(np.shape(pc))

            for i in range(cloud.size):
                # 根据临近点曲面拟合后, 计算出来补偿的点
                p_new = mls(pc[i, :], pc[idx[i], :])

                # 对输出点云赋值
                pc_output[i, :] = p_new

            # Plot the figure
            plot_figure(pc, pc_output)

            return None

        def main_filter_depth_noise_removal(self):

            '''
            filtering through convolution method
            method 1) sobel / gradient method (calculate the gradient and compare it with a threshold)
            method 2) variance method  (calculate the variance within the convolved kernel and compare with it with a
            specified threshold)
            :return:
            filtered image
            '''

            def gradient_edge(img, th):
                win = 3
                img_sobelx = cv2.Sobel(img, cv2.CV_64F, 1, 0, ksize=win)
                img_sobely = cv2.Sobel(img, cv2.CV_64F, 0, 1, ksize=win)
                img_edge = np.sqrt(img_sobelx ** 2 + img_sobely ** 2)
                mask = img_edge > th

                return mask

            def variance_edge(img, th):
                win = 3
                img_var0 = cv2.blur(img, (win, win)) ** 2
                img_var1 = cv2.blur(img ** 2, (win, win))
                img_var = img_var1 - img_var0
                mask = img_var > th

                return mask

            def plot_graph(mask1, mask2):
                plt.clf()
                fig = plt.figure(1)
                fig.set_size_inches(8, 6)

                ax = fig.add_subplot(1, 2, 1)
                plt.imshow(mask1, cmap='gray')
                plt.title('edge gradient')

                ax = fig.add_subplot(1, 2, 2)
                plt.imshow(mask2, cmap='gray')
                plt.title('edge variance')

                plt.show(block=True)

                return None

            # 加载原始点云数据
            depth = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_2/img_dep_640_480.csv', delimiter=',').astype(
                np.float32)

            # 找出边缘
            mask_gradient = gradient_edge(depth, th=0.1)  # 梯度的方法
            mask_variance = variance_edge(depth, th=2e-4)  # 方差的方法

            # 绘图
            plot_graph(mask_gradient, mask_variance)

            return None

        def main_filter_pc_noise_removal_through_find_neighbors(self):

            '''
            Check if a point in a pointcloud is an outlier. Removes it if it is an outlier.
            Outlier is determined by checking the maximum distance of itself with its farthest point. If the distance
            is more than the specified threshold, it is determined to be an outlier.

            Distance is found based on two methods
            method 1: use OpenCV to build KD-Tree
            method 2: use PCL library to build KD-Tree

            通过临近点查询的方式，确定该点是否散点。
            如果确定是散点，剔除。
            :return:
            '''

            def plot_figure(pc, pc_output):

                # Plot the subplots with different view perspective
                plt.clf()
                fig = plt.figure(1)
                fig.set_size_inches(12, 8)

                ax = fig.add_subplot(1, 2, 1, projection='3d')
                ax.plot(pc[:, 0], pc[:, 1], pc[:, 2], 'k.', markersize=0.5)
                ax.set_xlabel('X')
                ax.set_ylabel('Y')
                ax.set_zlabel('Z')
                ax.set_xlim([-1.2, 1.2])
                ax.set_ylim([-1.2, 1.2])
                ax.set_zlim([-1.2, 1.2])
                ax.view_init(azim=45, elev=15)
                plt.title('original sphere (black)')

                ax = fig.add_subplot(1, 2, 2, projection='3d')
                ax.plot(pc_output[:, 0], pc_output[:, 1], pc_output[:, 2], 'r.', markersize=0.5)
                ax.set_xlabel('X')
                ax.set_ylabel('Y')
                ax.set_zlabel('Z')
                ax.set_xlim([-1.2, 1.2])
                ax.set_ylim([-1.2, 1.2])
                ax.set_zlim([-1.2, 1.2])
                ax.view_init(azim=45, elev=15)
                plt.title('smoothened sphere (red)')
                plt.show(block=True)

                return None

            def plot_figure_dynamic(pc, pc_new):

                # 装载私有库
                import sys
                sys.path.append('../../code')
                import pc_view as view_pc_3d

                CAM_WID, CAM_HGT = 640, 480
                CAM_FX, CAM_FY = 795.209, 793.957
                CAM_CX, CAM_CY = 332.031, 231.308

                IMG_WID, IMG_HGT = CAM_WID, CAM_HGT

                view_pc_3d.pc_view(pc, cam_fx=CAM_FX, cam_fy=CAM_FY, \
                                   cam_cx=CAM_CX, cam_cy=CAM_CY, \
                                   cz=1.1, \
                                   img_wid=IMG_WID, img_hgt=IMG_HGT, \
                                   dmin=0.5, dmax=1.5)

                view_pc_3d.pc_view(pc_new, cam_fx=CAM_FX, cam_fy=CAM_FY, \
                                   cam_cx=CAM_CX, cam_cy=CAM_CY, \
                                   cz=1.1, \
                                   img_wid=IMG_WID, img_hgt=IMG_HGT, \
                                   dmin=0.5, dmax=1.5)

                return None

            import numpy as np
            # 逐点根据K近邻最大距离过滤

            # 加载原始点云数据
            pc = np.genfromtxt('./Data/pcl_cks/ruimuke/lesson_4/pc.csv', delimiter=',').astype(np.float32)

            # pc = pc[0:-1:10, :]  # 降采样，加速

            method = 1  # method 1 or 2

            # 用opencv实现KNN检测
            if method == 1:

                import cv2
                # algorithm=1: KD-tree, trees=5:构建5棵树，加快搜索(1-16), checks=50: 查询递归次数
                flann = cv2.FlannBasedMatcher(dict(algorithm=1, trees=16), dict(checks=50))

                # KNN搜索
                print('KNN (openCV)')
                matches = flann.knnMatch(pc, pc, k=10)

                print('filtering...')
                pc_new = np.array([pc[i] for i, m in enumerate(matches) if max([n.distance for n in m]) < 0.005])

                # Plot figure dynamic
                plot_figure_dynamic(pc, pc_new)

            # 用PCL实现KNN检测
            elif method == 2:

                import pcl
                cloud = pcl.PointCloud(pc.astype(np.float32))

                # 构建kdtree用于快速最近邻搜索
                kdtree = cloud.make_kdtree_flann()

                # K近邻搜索
                print('KNN (PCL)')
                [idx, dist] = kdtree.nearest_k_search_for_cloud(cloud, 10)

                print('filtering...')
                pc_new = pc[np.max(dist, axis=1) < 0.005 ** 2]

            plot_figure(pc, pc_new)

            return None

    class lesson_5():

        def __init__(self):
            pass


class main_pytorch_for_deep_learning_with_python_bootcamp_udemy_jose_portilla():

    def main_custom_images_cnn(self):

        import torch
        import torch.nn as nn
        import torch.nn.functional as F
        from torch.utils.data import DataLoader
        from torchvision import datasets, transforms, models
        from torchvision.utils import make_grid

        import numpy as np
        import pandas as pd
        import matplotlib.pyplot as plt

        import os
        from PIL import Image

        def check_total_files_inside_root_folder(root):

            # Examine the data
            img_names = []

            for folder, subfolders, filenames in os.walk(root):
                for img in filenames:
                    img_names.append(folder + '\\' + img)

            print('Images: ', len(img_names))

            return img_names

        def disp_5_images(train_loader):

            # Grab the first batch of 10 images
            for images, labels in train_loader:
                break

            # Print the labels
            print('Label:', labels.numpy())
            print('Class:', *np.array([class_names[i] for i in labels]))

            # 5 images stacked horizontally in a single row
            im = make_grid(images, nrow=5)

            # Inverse normalize the images
            inv_normalize = transforms.Normalize(
                mean=[-0.485 / 0.229, -0.456 / 0.224, -0.406 / 0.225],
                std=[1 / 0.229, 1 / 0.224, 1 / 0.225]
            )
            im_inv = inv_normalize(im)

            # Print the images
            plt.figure(figsize=(12, 4))
            plt.imshow(np.transpose(im_inv.numpy(), (1, 2, 0)))
            plt.show()

            return None

        def transform_setting():

            # Transform create more data points for training
            train_transform = transforms.Compose([
                transforms.RandomRotation(10),  # rotate +/- 10 degrees
                transforms.RandomHorizontalFlip(),  # reverse 50% of images
                transforms.Resize(224),  # resize shortest side to 224 pixels
                transforms.CenterCrop(224),  # crop longest side to 224 pixels at center
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406],  # Standardization, img[ch] = (img[ch] - mean[ch])/std[ch]
                                     [0.229, 0.224, 0.225])  # transforms.Normalize(mean, std)
            ])

            # Since it is a test set, no need random rotation and horizontal flip
            test_transform = transforms.Compose([
                transforms.Resize(224),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406],
                                     [0.229, 0.224, 0.225])
            ])

            return train_transform, test_transform

        root = './Data/CATS_DOGS'

        # List out all the image files inside the root folder
        img_names = check_total_files_inside_root_folder(root)

        # Define the transforms settings for both training and testing set (rotation, resize, crop, normalization)
        train_transform, test_transform = transform_setting()

        # Load the train data and test data
        # datasets.ImageFolder: A generic data loader where the images are arranged in an orderly manner.
        train_data = datasets.ImageFolder(os.path.join(root, 'train'), transform=train_transform)
        test_data = datasets.ImageFolder(os.path.join(root, 'test'), transform=test_transform)

        # Load the train_loader and test_loader
        torch.manual_seed(42)
        train_loader = DataLoader(train_data, batch_size=10, shuffle=True)
        test_loader = DataLoader(test_data, batch_size=10, shuffle=True)

        # Printing the characteristics of the training data
        class_names = train_data.classes
        print(class_names)
        print(f'Training images available: {len(train_data)}')
        print(f'Testing images available:  {len(test_data)}')

        # Display the images
        disp_5_images(train_loader)

        # Define the AI model
        model = ai.classification.CNN_custom_dogs_cats()

        # Train the model
        train_losses, train_correct = model.train(train_loader)

        # Validate the test data
        test_losses, test_correct = model.validate(test_loader)

        return None

    def main_cifar10_cnn(self):

        '''
        The CIFAR-10 dataset is similar to MNIST, except that instead of one color channel (grayscale) there are three channels (RGB).
        Where an MNIST image has a size of (1,28,28), CIFAR images are (3,32,32). There are 10 categories an image may fall under:
         ['plane', '  car', ' bird', '  cat', ' deer', '  dog', ' frog', 'horse', ' ship', 'truck']
        :return:
        '''

        def view_a_batch_of_images(data, class_names):

            '''
            Show a few images of the data
            :param data: input data from the dataloader type
            :param class_names: class names of the labels
            :return:
            '''

            # Widen the printed array
            np.set_printoptions(formatter=dict(int=lambda x: f'{x:5}'))

            # Grab the first batch of 10 images
            for images, labels in data:
                break

            # Print the labels
            print('Label:', labels.numpy())
            print('Class: ', *np.array([class_names[i] for i in labels]))

            # Print the images
            im = make_grid(images, nrow=5)  # the default nrow is 8
            plt.figure(1)
            # Shift the image dimension from (3, 70, 172) to (70, 172, 3)
            plt.imshow(np.transpose(im.numpy(), (1, 2, 0)))
            plt.show(block=False)

        import torch
        from torchvision import datasets, transforms
        from torchvision.utils import make_grid
        from torch.utils.data import DataLoader

        # Load CIFAR10 data from datasets
        transform = transforms.ToTensor()
        train_data = datasets.CIFAR10(root='./Data', train=True, download=True, transform=transform)
        test_data = datasets.CIFAR10(root='./Data', train=False, download=True, transform=transform)

        torch.manual_seed(101)
        batch_size_train, batch_size_test = 10, 10000
        train_loader = DataLoader(train_data, batch_size=batch_size_train, shuffle=True)
        test_loader = DataLoader(test_data, batch_size=batch_size_test, shuffle=False)

        # The class labels are ordered as follow
        class_names = ['plane', '  car', ' bird', '  cat', ' deer', '  dog', ' frog', 'horse', ' ship', 'truck']

        # Choice to train or load the model ('train' or 'load')
        choice = 'train'

        # plot some examples
        view_a_batch_of_images(train_loader, class_names)

        if choice == 'train':

            # Define the model
            model = ai.classification().CNN_CIFAR10()

            # Train the model
            epochs = 10
            train_losses, train_correct = model.train(train_loader, epochs)

        elif choice == 'load':

            # Save and load the model
            # torch.save(model.state_dict(), './model/CIFAR10/CIFAR10-CNN-Model-master.pt')
            model = ai.classification().CNN_CIFAR10()
            model.load_state_dict(torch.load('./model/CIFAR10/CIFAR10-CNN-Model-master.pt'))

        # Validate the model
        test_loss, test_correct = model.validate(test_loader, class_names)

        # Evaluate test data
        print(f'Total correct predictions = {test_correct}')
        print(f'Test accuracy: {test_correct * 100 / 10000:.3f}%')

        return None

    def main_mnist_cnn(self):

        from torch.utils.data import DataLoader
        from torchvision import datasets, transforms

        '''
        Define transform
        we can apply multiple transformations (reshape, convert to tensor, normalize, etc.) to the incoming data.
        '''
        transform = transforms.ToTensor()

        '''
        PyTorch makes the MNIST dataset available through torchvision. The first time it's called, the dataset will be 
        downloaded onto your computer to the path specified. From that point, torchvision will always look for a local 
        copy before attempting another download.
        Load the train test set
        '''
        train_data = datasets.MNIST(root='./data', train=True, download=True, transform=transform)
        test_data = datasets.MNIST(root='./data', train=False, download=True, transform=transform)

        torch.manual_seed(42)
        train_loader = DataLoader(train_data, batch_size=10, shuffle=True)
        test_loader = DataLoader(test_data, batch_size=10, shuffle=False)

        # Build the model
        model = ai.classification.CNN_MNIST()

        # Train the model
        train_losses, train_correct = model.train(train_loader)

        # Validate the test data
        test_losses, test_correct = model.validate(test_loader)

        return None

    def main_mnist_cnn_cuda(self):

        def train(train_data, train_label, model, criterion, epochs=5):

            import time
            start_time = time.time()

            train_losses = []
            train_correct = []

            # Define the optimization algorithm
            optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

            batch_size = 10

            for i in range(epochs):

                trn_corr = 0

                b = 0

                pos = range(0, 60000 + batch_size, batch_size)

                # Run the training batches
                for j in range(0, len(pos) - 1):

                    b += 1

                    # X_train is of the shape (batch_size,C,H,W)
                    X_train, y_train = train_data[pos[j]:pos[j + 1], :, :, :], train_label[pos[j]:pos[j + 1]]

                    # Apply the model
                    y_pred = model.forward(X_train)  # we don't flatten X-train here

                    # Calculate the loss value
                    loss = criterion(input=y_pred, target=y_train)

                    # Tally the number of correct predictions
                    predicted = torch.max(y_pred.data, dim=1)[1]

                    # Calculate the correct predictions
                    batch_corr = (predicted == y_train).sum()

                    # Calculate the total correct predictions
                    trn_corr += batch_corr

                    # Update the parameters
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()

                    # Print interim results
                    if b % 600 == 0:
                        print(f'epoch: {i:2}  batch: {b:4} [{10 * b:6}/60000]  loss: {loss.item():10.8f}  \
            accuracy: {trn_corr.item() * 100 / (10 * b):7.3f}%')

                train_losses.append(loss)
                train_correct.append(trn_corr)

            print(f'\nDuration: {time.time() - start_time:.0f} seconds')  # print the time elapsed

            return train_losses, train_correct

        def validate(test_data, y_test, model, criterion):

            # Define variables for storing loss values vs epoch
            # Suggest to define the variables as global variables (self) in the future
            test_losses = []
            test_correct = []
            tst_corr = 0

            # Run the testing batches
            with torch.no_grad():
                # Apply the model
                y_val = model.forward(test_data)

                # Tally the number of correct predictions
                predicted = torch.max(y_val.data, 1)[1]
                tst_corr += (predicted == y_test).sum()

            loss = criterion(y_val, y_test)
            test_losses.append(loss)
            test_correct.append(tst_corr)

            return test_losses, test_correct

        from torch.utils.data import DataLoader
        from torchvision import datasets, transforms

        '''
        Define transform
        we can apply multiple transformations (reshape, convert to tensor, normalize, etc.) to the incoming data.
        '''
        transform = transforms.ToTensor()

        '''
        PyTorch makes the MNIST dataset available through torchvision. The first time it's called, the dataset will be 
        downloaded onto your computer to the path specified. From that point, torchvision will always look for a local 
        copy before attempting another download.
        Load the train test set
        '''
        train_data = datasets.MNIST(root='./data', train=True, download=True, transform=transform)
        test_data = datasets.MNIST(root='./data', train=False, download=True, transform=transform)

        print('initial'.center(50, '*'))
        ai.useful_func().check_cuda()

        # Obtain the train test data
        # (unsqueeze: insert 1 dimension) (size from torch.Size([60000, 28, 28]) to torch.Size([60000, 1, 28, 28]))
        train_data, train_label = train_data.data.unsqueeze(1).float(), train_data.targets
        test_data, test_label = test_data.data.unsqueeze(1).float(), test_data.targets

        # Pass to cuda (Didn't pass the data to dataloader as didn't manage to find a way to load to cuda)
        # May miss the chance to shuffle, flip transform, angle transform, etc.
        train_data, train_label = train_data.cuda(), train_label.cuda()
        test_data, test_label = test_data.cuda(), test_label.cuda()

        torch.manual_seed(42)
        print('Pass training data to Cuda'.center(50, '*'))
        ai.useful_func().check_cuda()

        # Build the model
        model = ai.classification.CNN_MNIST_cuda().cuda()

        print('Pass model to cuda'.center(50, '*'))
        ai.useful_func().check_cuda()
        print(''.center(50, '*'))

        # Train the model
        criterion = nn.CrossEntropyLoss()  # Define the loss function
        train_losses, train_correct = train(train_data, train_label, model, criterion, epochs=5)

        # Validate the test data
        test_losses, test_correct = validate(test_data, test_label, model, criterion)

        return None

    def main_mnist_ann(self):

        # Flattening out the image ends up removing some of the 2D information, such as the relationship of a pixel to
        # its neighboring pixels

        from torch.utils.data import DataLoader
        from torchvision import datasets, transforms

        '''
        Define transform
        we can apply multiple transformations (reshape, convert to tensor, normalize, etc.) to the incoming data.
        '''
        transform = transforms.ToTensor()

        '''
        PyTorch makes the MNIST dataset available through torchvision. The first time it's called, the dataset will be 
        downloaded onto your computer to the path specified. From that point, torchvision will always look for a local 
        copy before attempting another download.
        Load the train test set
        '''
        train_data = datasets.MNIST(root='./Data', train=True, download=True, transform=transform)
        test_data = datasets.MNIST(root='./Data', train=False, download=True, transform=transform)

        '''Checking the characteristics of the data
        type(train_data) Out: torchvision.datasets.mnist.MNIST
        type(train_data[0]) Out: tuple
        type(train_data[0][0]) Out: torch.Tensor  (is 28*28 MNIST image)
        type(train_data[0][1]) Out: int (is the label for the image)
        '''

        # image, label = train_data[0]
        # image.shape Out: torch.Size([1, 28, 28]) (it's a grayscale image, so has 1 in front)
        # plt.imshow(image.reshape((28, 28)), cmap='gray')

        '''
        Our training set contains 60,000 records. If we look ahead to our model we have 784 incoming features, 
        hidden layers of 120 and 84 neurons, and 10 output features. Including the bias terms for each layer, 
        the total number of parameters being trained is:
        (784×120)+120+(120×84)+84+(84×10)+10 = 94080+120+10080+84+840+10 = 105,214 
        For this reason it makes sense to load training data in batches using DataLoader.

        Creating a PyTorch Dataset and managing it with Dataloader keeps your data manageable and helps to simplify your 
        machine learning pipeline. Dataset stores all your data, and Dataloader can be used to iterate through the 
        data, manage batches, transform the data, and much more.

        batch_size: number of records we process at a time
        '''
        torch.manual_seed(101)
        # shuffle because data may be organized in an orderly fashion
        # MNIST training data has 60000 samples (with batch_size of 100), there are total of 600 runs in each epoch
        # MNIST testing data has 10000 samples
        batch_size_train, batch_size_test = 100, 500
        train_loader = DataLoader(train_data, batch_size_train, shuffle=True)
        test_loader = DataLoader(test_data, batch_size_test, shuffle=False)

        # Build the model
        model = ai.classification.MultilayerPerceptron(in_sz=784, out_sz=10, hidden_layers=[120, 84], dropout_p=0)

        # Train the model (MNIST dataset; accuracy = 99.277% with dropout probability of 0)
        train_losses, train_correct = model.train_model(train_loader, batch_size_train, epochs=10)

        # Validate the test data (test_correct = 9759)
        test_losses, test_correct = model.validate_data(test_loader, batch_size_test)

        return None

    def main_tabular_data(self):

        '''
        https://www.kaggle.com/c/new-york-city-taxi-fare-prediction
        This function may serve as a general solution for tabular dataset (spreadsheets, SQL tables, etc.)
        consists of columns of values. Neural networks can learn to make connections we probably wouldn't
        have developed on our own. However, to do this we have to handle categorical values separately from continuous ones.

        problem type:
        regression: y is continuous type (NYCTaxiFares.csv: determine taxi fare price)
        classification: y is categorical type (NYCTaxiFares.csv: determine if the taxi fare price is more or less than $10)
        '''

        def print_df(df):

            '''
            Print the various attributes to understand better the characteristics of the dataframe
            '''

            print(df.columns), print(df.describe()), print(df.dtypes), print(df.info())
            print(df['fare_class'].value_counts()), print(df['passenger_count'].value_counts())
            print(df['fare_amount'].describe())

            return None

        def change_time_to_datetime_format(df):

            '''
            Convert datetime format to Hour, AMorPM and Weekday
            '''

            # Convert to datetime format
            df['EDTdate'] = pd.to_datetime(df['pickup_datetime'].str[:19])

            # Data falls in April of 2010 which occurred during Daylight Savings Time in New York.
            # For that reason, make an adjustment/ to EDT using UTC-4 (subtracting four hours).
            df['EDTdate'] = df['EDTdate'] - pd.Timedelta(hours=4)

            # Extract the hour information
            df['Hour'] = df['EDTdate'].dt.hour

            # Divide the time into am or pm
            df['AMorPM'] = np.where(df['Hour'] < 12, 'am', 'pm')

            # Extract the weekday information
            df['Weekday'] = df['EDTdate'].dt.strftime("%a")

            return df

        def haversine_distance(df, lat1, long1, lat2, long2):
            """
            Calculates the haversine distance between 2 sets of GPS coordinates in df
            """
            r = 6371  # average radius of Earth in kilometers

            phi1 = np.radians(df[lat1])
            phi2 = np.radians(df[lat2])

            delta_phi = np.radians(df[lat2] - df[lat1])
            delta_lambda = np.radians(df[long2] - df[long1])

            a = np.sin(delta_phi / 2) ** 2 + np.cos(phi1) * np.cos(phi2) * np.sin(delta_lambda / 2) ** 2
            c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
            d = (r * c)  # in kilometers

            return d

        def train_test_split(cats, conts, y, batch_size, test_size):

            # DATA SHUFFLED ALREADY
            cat_train = cats[:(batch_size - test_size)]
            cat_test = cats[(batch_size - test_size):batch_size]
            con_train = conts[:(batch_size - test_size)]
            con_test = conts[(batch_size - test_size):batch_size]

            y_train = y[:(batch_size - test_size)]
            y_test = y[(batch_size - test_size):batch_size]

            return cat_train, cat_test, con_train, con_test, y_train, y_test

        def feature_engineering_dataframe(df, problem):

            # Feature engineering datetime data of the dataset
            df = change_time_to_datetime_format(df)

            # Calculate the distance based on the pickup and dropoff location (latitude and longitude)
            df['dist_km'] = haversine_distance(df, 'pickup_latitude', 'pickup_longitude', 'dropoff_latitude',
                                               'dropoff_longitude')

            # Define the categorical attributes, continuous attributes and the labels
            # Obtain the relevant columns from the dataframe and convert them into tensor
            cat_cols = ['Hour', 'AMorPM', 'Weekday']
            cont_cols = ['pickup_longitude', 'pickup_latitude', 'dropoff_longitude', 'dropoff_latitude',
                         'passenger_count',
                         'dist_km']
            # Extract 'fare_amount' column for regression problem; 'fare_class' for classification problem
            y_col = ['fare_amount'] if problem == 'regression' else [
                'fare_class'] if problem == 'classification' else None
            df, cats, conts, y = ai.data_manipulation.convert_cat_conts_data_to_tensor(df, cat_cols, cont_cols, y_col,
                                                                                       problem)

            # Define the embedding size for the embedding
            cat_szs = [len(df[col].cat.categories) for col in cat_cols]
            emb_szs = [(size, min(50, (size + 1) // 2)) for size in cat_szs]

            return df, cats, conts, y, cat_szs, emb_szs

        def validation_test_data(model, problem, cat_test, con_test, y_test):

            # Calculate the error of the test data
            with torch.no_grad():

                y_val = model(cat_test, con_test)

                if problem == 'regression':
                    loss = torch.sqrt(model.loss_function(y_val, y_test))
                    print(f'RMSE: {loss:.8f}')

                elif problem == 'classification':
                    loss = model.loss_function(y_val, y_test)
                    y_val = y_val.argmax(axis=1)
                    print(f'CE Loss: {loss:.8f}')
                    cm, TP, TN, FP, FN, accuracy, precision, recall, F1 = \
                        ai.performance_metrics().metrics(y_test.numpy(), y_val.numpy())
                    print(f'confusion matrix {cm}'), print(f'accuracy {accuracy:.3f}')

            plt.figure(1)
            plt.plot(range(epochs), model.losses)
            plt.xlabel('epochs')
            plt.ylabel('RMSE') if problem == 'regression' else plt.ylabel(
                'Cross Entropy Loss') if problem == 'classification' else None
            plt.grid()
            plt.show()

        # Define this function is 'regression' or 'classification' problem
        problem = 'regression'

        # Import the data as dataframe
        df = pd.read_csv('./Data/NYCTaxiFares.csv')

        # Understand the characteristics of the dataframe
        # print_df(df)

        # Feature engineering of the dataframe
        df, cats, conts, y, cat_szs, emb_szs = feature_engineering_dataframe(df, problem)

        # Train test data split
        batch_size = 60000  # Full size = cats.shape[0]; to speed up, can choose smaller batch size (60000)
        test_size = int(batch_size * 0.2)
        cat_train, cat_test, con_train, con_test, y_train, y_test = train_test_split(cats, conts, y, batch_size,
                                                                                     test_size)

        # Define the model
        torch.manual_seed(33)
        out_sz = 1 if problem == 'regression' else 2 if problem == 'classification' else None
        model = ai.tabular_data.kaggle_NYC_taxi_fare(problem, emb_szs, conts.shape[1], out_sz, hidden_layers=[200, 100],
                                                     p=0.4)

        # Train the model
        epochs = 300
        model.train_model(cat_train, con_train, y_train, problem, epochs)

        # Validate the model
        validation_test_data(model, problem, cat_test, con_test, y_test)

        # Save the model
        torch.save(model, 'model.pt')

        # Load the model
        # model.load_state_dict(torch.load('./model/TaxiModelFull.pt'))

        return None

    def main_dnn_iris_dataset(self):

        '''
        This main function fit a DNN model to the iris dataset
        :return:
        '''

        # Read the data in dataframe format
        df = pd.read_csv('./DATA/iris.csv')

        # Convert dataframe to numpy array for x and y
        X = df.drop('target', axis=1).values
        y = df['target'].values

        # Split the dataset into training and testing set
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=33)

        # Convert from numpy array to torch tensor format
        X_train = torch.tensor(X_train, dtype=torch.float32)
        X_test = torch.tensor(X_test, dtype=torch.float32)
        y_train = torch.tensor(y_train, dtype=torch.int64)
        y_test = torch.tensor(y_test, dtype=torch.int64)
        # y_train = F.one_hot(torch.LongTensor(y_train))  # not needed with Cross Entropy Loss
        # y_test = F.one_hot(torch.LongTensor(y_test))

        # Define a deep neural network for iris dataset and train the model
        torch.manual_seed(32)
        epochs = 100
        model = ai.classification().iris()
        model.train_model(X_train, y_train, epochs)

        # Evaluate the test data
        with torch.no_grad():
            y_val = model.forward(X_test)
            loss = model.loss_function(y_val, y_test)
            y_val = y_val.argmax(axis=1)

        # Calculate the performance metrics
        cm, TP, TN, FP, FN, accuracy, precision, recall, F1 = ai.performance_metrics().metrics(y_test.numpy(),
                                                                                               y_val.numpy())
        print(cm), print([accuracy, precision, recall, F1])

        # Plot the graph
        fig = plt.figure(1)
        plt.plot(range(epochs), model.losses)
        plt.ylabel('Loss')
        plt.xlabel('epoch')
        plt.show()

    def main_dnn_linear_equation_fitting(self):

        '''
        Deep neural network with 1 input and 1 output
        :return:
        '''

        # Generating arbitrary input
        x = torch.linspace(1, 50, 50).reshape(-1, 1)
        torch.manual_seed(71)  # to obtain reproducible results
        e = torch.randint(-8, 9, (50, 1), dtype=torch.float)
        y = 2 * x + 1 + e

        # Calling the model
        epochs = 50
        model = ai.regression().linear_model_in_1_out_1()
        model.train_model(x, y, epochs)

        with torch.no_grad():
            x1 = torch.tensor(np.array([x.min(), x.max()]).reshape(-1, 1))
            y1 = model.model(x1)
            x1 = x1.numpy()
            y1 = y1.numpy()

        # plotting the graph
        fig = plt.figure(1)
        ax = fig.add_subplot(1, 2, 1)
        plt.plot(range(epochs), model.losses)
        plt.ylabel('Loss')
        plt.xlabel('epoch')

        ax = fig.add_subplot(1, 2, 2)
        plt.scatter(x.numpy(), y.numpy())
        plt.plot(x1, y1, 'r')
        plt.title('Current Model')
        plt.ylabel('y')
        plt.xlabel('x')

        plt.show()

    def main_gpu_cuda_trial(self):

        import torch

        # Check cuda version, memory, device id, etc
        ai.useful_func().check_cuda()

        a = torch.FloatTensor([1.0, 2.0])
        print(a.device)

        a = torch.FloatTensor([1.0, 2.0]).cuda()
        print(a.device)

        import torch
        import torch.nn as nn
        import torch.nn.functional as F
        from torch.utils.data import Dataset, DataLoader
        from sklearn.model_selection import train_test_split

        import pandas as pd

        class Model(nn.Module):

            def __init__(self, in_features=4, h1=8, h2=9, out_features=3):
                super().__init__()
                self.fc1 = nn.Linear(in_features, h1)  # input layer
                self.fc2 = nn.Linear(h1, h2)  # hidden layer
                self.out = nn.Linear(h2, out_features)  # output layer

            def forward(self, x):
                x = F.relu(self.fc1(x))
                x = F.relu(self.fc2(x))
                x = self.out(x)
                return x

        torch.manual_seed(32)
        model = Model()

        # From the discussions here: discuss.pytorch.org/t/how-to-check-if-model-is-on-cuda
        next(model.parameters()).is_cuda

        gpumodel = model.cuda()
        next(gpumodel.parameters()).is_cuda

        df = pd.read_csv('./Data/iris.csv')
        X = df.drop('target', axis=1).values
        y = df['target'].values
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=33)

        X_train = torch.FloatTensor(X_train).cuda()
        X_test = torch.FloatTensor(X_test).cuda()
        y_train = torch.LongTensor(y_train).cuda()
        y_test = torch.LongTensor(y_test).cuda()

        trainloader = DataLoader(X_train, batch_size=60, shuffle=True)
        testloader = DataLoader(X_test, batch_size=60, shuffle=False)

        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

        import time
        epochs = 100
        losses = []
        start = time.time()
        for i in range(epochs):
            i += 1
            y_pred = gpumodel.forward(X_train)
            # y_pred = model.forward(X_train)
            loss = criterion(y_pred, y_train)
            losses.append(loss)

            # a neat trick to save screen space:
            if i % 10 == 1:
                print(f'epoch: {i:2}  loss: {loss.item():10.8f}')

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        print(f'TOTAL TRAINING TIME: {time.time() - start}')

        return None


class main_kaikeba_fundamental_of_AI_basis_training():

    def lesson_13_generate_sentence(self):

        from icecream import ic

        # 如何让计算机生成像仁一样的话

        grammar_rule = """
        复合句子 = 句子 连词 复合句子 | 句子 
        句子 = 主语s 谓语 宾语s
        谓语 = 喜欢 | 讨厌 | 吃掉 | 玩 | 跑
        主语s = 主语 和 主语 | 主语
        宾语s = 宾语 和 宾语 | 主语
        主语 = 冠词 定语 代号
        宾语 = 冠词 定语 代号
        代号 = 名词 | 代词
        名词 = 苹果 | 鸭梨 | 西瓜 | 小狗 | 小猫 | 滑板 | 老张 | 老王
        代词 = 你 | 我 | 他 | 他们 | 你们 | 我们 | 它
        定语 = 漂亮的 | 今天的 | 不知名的 | 神秘的 | 奇奇怪怪的
        冠词 = 一个 | 一只 | 这个 | 那个
        连词 = 但是 | 而且 | 不过
        """

        for line in grammar_rule.split('\n'):
            if not line.strip():
                continue
            else:
                ic(line)
                target, expand = line.split('=')
                ic(target)
                ic(expand)

        return None

    def lesson_13_water_pouring(self):

        '''
        两个杯子分别能cheng
        :return:
        '''

        from icecream import ic

        def successors(x, y, X, Y):

            # If x = y, the dictionary does not allow the same key
            # so, 倒空x will be replaced by 倒空y which is then replaced by x倒入y中 which finally replaced by y倒入x中
            return {
                (0, y): '倒空x',
                (x, 0): '倒空y',
                (x + y - Y, Y) if x + y >= Y else (0, x + y): 'x倒入y中',
                (X, x + y - X) if y + x >= X else (x + y, 0): 'y倒入x中',
                (X, y): '装满x',
                (x, Y): '装满y'
            }

        def search_solution(capacity1, capacity2, goal, start=(0, 0)):

            '''
            搜寻函数
            :param capacity1:
            :param capacity2:
            :param goal:
            :param start:
            :return:
            '''

            # Create a list type paths to store all the new path
            paths = [[('init', start)]]

            # Create a set type to store path that has passed before
            explored = set()

            while True:

                # 取任意一个路径 (这里取第一个路径)
                path = paths.pop(0)

                # 边缘数字
                frontier = path[-1]
                (x, y) = frontier[-1]

                for state, action in successors(x, y, capacity1, capacity2).items():

                    ic(frontier, state, action)

                    # 为了避免死循环, 添加如果相关路径已经被探索后, 跳出
                    if state in explored:
                        continue

                    # 加到新的路径里
                    new_path = path + [(action, state)]

                    # 如果我们的 goal 在 state 里边
                    if goal in state:
                        # 中断，输出结果
                        return new_path
                    else:
                        # 加进去这个新路径，还需要继续探索
                        paths.append(new_path)

                    explored.add(state)

            return None

        path = search_solution(90, 40, 30, (0, 0))

        for p in path:
            print('--=>')
            print(p)

    def lesson_14_linear_equation(self):

        '''
        人工智能基础能力培养010期-预修课\第14章 lesson2-机器学习初探
        This is a program that calculates w based on fixed x, y and b
        Direct calculation is y = wx + b -> w = (y - b)/x
        Backpropagation method is using gradient descent to calculate w
        :return:
        '''

        # Define the loss function
        def loss(xs, w, b, ys):
            return ((w * xs + b) - ys) ** 2

        # Define the gradient of the loss functions
        def gradient(w, x, b, y):
            return 2 * ((w * x + b) - y) * x

        np.random.seed(10)

        # Arbitrarily set a random w and b
        w, b = np.random.randint(-10, 10), np.random.randint(-10, 10)

        # Set the x and y point
        x, y = 0.2, 0.3

        for i in range(100):
            # Calculate the gradient with respect to w
            w_gradient = gradient(w, x, b, y)

            # Backpropagate the w
            w = w + (-1) * w_gradient

            ic(w)
            ic(loss(x, w, b, y))

        # The gradient descent method should come to a result that is the same as below
        ic((y - b) / w)

        return None

    def lesson_14_kmeans(self):

        '''
        人工智能基础能力培养010期-预修课\第14章 lesson2-机器学习初探\
        kmeans k均值算法
        :return:
        '''

        import random
        from collections import defaultdict
        from matplotlib.colors import BASE_COLORS

        np.random.seed(10)

        points0 = np.random.normal(loc=0, size=(100, 2))
        points1 = np.random.normal(loc=1, size=(100, 2))
        points2 = np.random.normal(loc=2, size=(100, 2))
        points3 = np.random.normal(loc=5, size=(100, 2))

        points = np.concatenate([points0, points1, points2, points3])

        k = 3

        def random_centers(k, points):

            for i in range(k):
                yield random.choice(points[:, 0]), random.choice(points[:, 1])

        def mean(points):
            all_x, all_y = [x for x, y in points], [y for x, y in points]

            return np.mean(all_x), np.mean(all_y)

        def distance(p1, p2):
            x1, y1 = p1
            x2, y2 = p2

            return np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

        def kmeans(k, points, centers=None):

            colors = list(BASE_COLORS.values())

            if not centers:
                centers = list(random_centers(k=k, points=points))

            ic(centers)

            for i, c in enumerate(centers):
                plt.scatter([c[0]], [c[1]], s=90, marker='*', c=colors[i])

            plt.scatter(*zip(*points), c='black')

            centers_neighbor = defaultdict(set)

            for p in points:
                closet_c = min(centers, key=lambda c: distance(p, c))
                centers_neighbor[closet_c].add(tuple(p))

            for i, c in enumerate(centers):
                _points = centers_neighbor[c]
                all_x, all_y = [x for x, y in _points], [y for x, y in _points]
                plt.scatter(all_x, all_y, c=colors[i])

            plt.show()

            new_centers = []

            for c in centers_neighbor:
                new_c = mean(centers_neighbor[c])
                new_centers.append(new_c)

            threshold = 1
            distances_old_and_new = [distance(c_old, c_new) for c_old, c_new in zip(centers, new_centers)]
            ic(distances_old_and_new)
            if all(c < threshold for c in distances_old_and_new):
                return centers_neighbor
            else:
                kmeans(k, points, new_centers)

        kmeans(4, points=points)

        return None

    def lesson_14_kmeans_china_map(self):

        from pylab import mpl
        import math
        import re
        import networkx as nx
        import random
        from collections import defaultdict

        def geo_distance(origin, destination):
            """
            Calculate the Haversine distance.

            Parameters
            ----------
            origin : tuple of float
                (lat, long)
            destination : tuple of float
                (lat, long)

            Returns
            -------
            distance_in_km : float

            Examples
            --------
            origin = (48.1372, 11.5756)  # Munich
            destination = (52.5186, 13.4083)  # Berlin
            round(distance(origin, destination), 1)
            504.2
            """
            lon1, lat1 = origin
            lon2, lat2 = destination
            radius = 6371  # km

            dlat = math.radians(lat2 - lat1)
            dlon = math.radians(lon2 - lon1)
            a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
                 math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
                 math.sin(dlon / 2) * math.sin(dlon / 2))
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
            d = radius * c

            return d

        def get_random_center(all_x, all_y):
            r_x = random.uniform(min(all_x), max(all_x))
            r_y = random.uniform(min(all_y), max(all_y))

            return r_x, r_y

        def generate_all_x_all_y_location(city_location):

            all_x = []
            all_y = []

            for _, location in city_location.items():
                x, y = location

                all_x.append(x)
                all_y.append(y)

            return all_x, all_y

        def iterate_once(centers, closet_points, threshold=5):
            have_changed = False

            for c in closet_points:
                former_center = centers[c]

                neighbors = closet_points[c]

                neighbors_center = np.mean(neighbors, axis=0)

                if geo_distance(neighbors_center, former_center) > threshold:
                    centers[c] = neighbors_center
                    have_changed = True
                else:
                    pass  ## keep former center

            return centers, have_changed

        def kmeans(Xs, k, threshold=5):
            all_x = Xs[:, 0]
            all_y = Xs[:, 1]

            K = k

            centers = {'{}'.format(i + 1): get_random_center(all_x, all_y) for i in range(K)}

            changed = True

            while changed:
                closet_points = defaultdict(list)

                for x, y, in zip(all_x, all_y):
                    closet_c, closet_dis = min([(k, geo_distance((x, y), centers[k])) for k in centers],
                                               key=lambda t: t[1])
                    closet_points[closet_c].append([x, y])

                centers, changed = iterate_once(centers, closet_points, threshold)
                print('iteration')

            return centers

        def draw_cities(cities, i, color=None):

            plt.figure(i)
            city_graph = nx.Graph()
            city_graph.add_nodes_from(list(cities.keys()))
            nx.draw(city_graph, cities, node_color=color, with_labels=True, node_size=30)
            plt.show(block=False)

        mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
        mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题

        coordination_source = """
        {name:'兰州', geoCoord:[103.73, 36.03]},
        {name:'嘉峪关', geoCoord:[98.17, 39.47]},
        {name:'西宁', geoCoord:[101.74, 36.56]},
        {name:'成都', geoCoord:[104.06, 30.67]},
        {name:'石家庄', geoCoord:[114.48, 38.03]},
        {name:'拉萨', geoCoord:[102.73, 25.04]},
        {name:'贵阳', geoCoord:[106.71, 26.57]},
        {name:'武汉', geoCoord:[114.31, 30.52]},
        {name:'郑州', geoCoord:[113.65, 34.76]},
        {name:'济南', geoCoord:[117, 36.65]},
        {name:'南京', geoCoord:[118.78, 32.04]},
        {name:'合肥', geoCoord:[117.27, 31.86]},
        {name:'杭州', geoCoord:[120.19, 30.26]},
        {name:'南昌', geoCoord:[115.89, 28.68]},
        {name:'福州', geoCoord:[119.3, 26.08]},
        {name:'广州', geoCoord:[113.23, 23.16]},
        {name:'长沙', geoCoord:[113, 28.21]},
        //{name:'海口', geoCoord:[110.35, 20.02]},
        {name:'沈阳', geoCoord:[123.38, 41.8]},
        {name:'长春', geoCoord:[125.35, 43.88]},
        {name:'哈尔滨', geoCoord:[126.63, 45.75]},
        {name:'太原', geoCoord:[112.53, 37.87]},
        {name:'西安', geoCoord:[108.95, 34.27]},
        //{name:'台湾', geoCoord:[121.30, 25.03]},
        {name:'北京', geoCoord:[116.46, 39.92]},
        {name:'上海', geoCoord:[121.48, 31.22]},
        {name:'重庆', geoCoord:[106.54, 29.59]},
        {name:'天津', geoCoord:[117.2, 39.13]},
        {name:'呼和浩特', geoCoord:[111.65, 40.82]},
        {name:'南宁', geoCoord:[108.33, 22.84]},
        //{name:'西藏', geoCoord:[91.11, 29.97]},
        {name:'银川', geoCoord:[106.27, 38.47]},
        {name:'乌鲁木齐', geoCoord:[87.68, 43.77]},
        {name:'香港', geoCoord:[114.17, 22.28]},
        {name:'澳门', geoCoord:[113.54, 22.19]}
        """

        city_location = {
            '香港': (114.17, 22.28)
        }

        test_string = "{name:'兰州', geoCoord:[103.73, 36.03]},"

        pattern = re.compile(r"name:'(\w+)',\s+geoCoord:\[(\d+.\d+),\s(\d+.\d+)\]")

        # Convert city_location data from string to dictionary
        for line in coordination_source.split('\n'):
            city_info = pattern.findall(line)
            if not city_info:
                continue

            # following: we find the city info

            city, long, lat = city_info[0]

            long, lat = float(long), float(lat)

            city_location[city] = (long, lat)

        # Plot the city location as graph
        city_graph = nx.Graph()
        city_graph.add_nodes_from(list(city_location.keys()))
        plt.figure(1)
        nx.draw(city_graph, city_location, with_labels=True, node_size=30)
        plt.show(block=False)

        # K-means: Initial k random centers
        random.seed(10)
        all_x, all_y = generate_all_x_all_y_location(city_location)
        K = 5
        centers = {'{}'.format(i + 1): get_random_center(all_x, all_y) for i in range(K)}

        # Perform K-means
        closet_points = defaultdict(list)
        for x, y, in zip(all_x, all_y):
            closet_c, closet_dis = min([(k, geo_distance((x, y), centers[k])) for k in centers], key=lambda t: t[1])
            closet_points[closet_c].append([x, y])

        centers = kmeans(np.array(list(city_location.values())), k=5, threshold=1)

        plt.figure(2)
        plt.scatter(all_x, all_y)
        plt.scatter(*zip(*centers.values()))
        plt.show(block=False)

        plt.figure(3)
        for c, points in closet_points.items():
            plt.scatter(*zip(*points))
        plt.show(block=False)

        city_location_with_station = {
            '能源站-{}'.format(i): position for i, position in centers.items()
        }

        city_location_with_station

        draw_cities(city_location_with_station, 4, color='green')
        draw_cities(city_location, 4, color='red')

        return None

    def lesson_14_kmeans_news_title(self):

        import numpy as np
        import pandas as pd
        import matplotlib.pyplot as plt
        import seaborn as sns
        from sklearn.feature_extraction import text
        from sklearn.feature_extraction.text import TfidfVectorizer
        from sklearn.cluster import KMeans
        from nltk.tokenize import RegexpTokenizer
        from nltk.stem.snowball import SnowballStemmer
        import re
        from sklearn.cluster import \
            KMeans  # 使用方法详见： http://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html

        # 读取数据集
        data = pd.read_csv("./Data/news headlines/abcnews-date-text.csv", error_bad_lines=False,
                           usecols=["headline_text"])

        # Reduce to 10000 number of headlines
        data = data.head(10000)  # 获取部分数据快速运行，你可以尝试修改使用的数据量查看后续的建模效果，不过注意使用的数据越多后续模型训练的时间越长

        # 打印数据信息
        data.info()

        # Sort the headline_text and display the first 8
        data[data['headline_text'].duplicated(keep=False)].sort_values('headline_text').head(8)

        # 删除重复行，pandas.DataFrame.drop_duplicates 使用方法详见：https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop_duplicates.html
        ########## 第一题 ～ 1行 ##########
        data = data.drop_duplicates()

        '''
        TFIDF（term frequency–inverse document frequency）
        在信息检索中，tf–idf 或 TFIDF（term frequency–inverse document frequency）是一种数值统计，
        旨在反映单词对语料库中文档的重要性。在信息检索，文本挖掘和用户建模的搜索中，它通常用作加权因子。 
        tf-idf 值与单词在文档中出现的次数成正比，同时被单词在语料库中的出现频率所抵消，
        这有助于调整某些单词通常会更频繁出现的事实。 如今，tf-idf是最流行的术语加权方案之一。
        在数字图书馆领域，有83％的基于文本的推荐系统使用tf-idf。

        搜索引擎经常使用tf–idf加权方案的变体作为在给定用户查询时对文档相关性进行评分和排名的主要工具。
        tf–idf可成功用于各种领域的停用词过滤，包括文本摘要和分类

        排名函数中最简单的是通过将每个查询词的tf–idf相加得出，许多更复杂的排名函数是此简单模型的变体。
        '''
        desc = data['headline_text'].values
        ############ 第二题 ～ 1行 ############
        ############ your code start ############
        # TfidfVectorizer 使用方法详见：http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html
        vectorizer = TfidfVectorizer()
        ############ your code end ############
        X = vectorizer.fit_transform(desc)

        word_features = vectorizer.get_feature_names()
        print(len(word_features))
        print(word_features[5000:5100])

        '''
        stemming 是将单词还原为词干（即词根形式）的过程。 词根形式不一定是单词本身，而是可以通过连接正确的后缀来生成单词。 
        例如，“fish”，“fishes”和“fishing”这几个词的词干都是“fish”，这是一个正确的单词。 
        另一方面，“study”，“studies”和“studying”一词源于“studi”，这不是一个正确的英语单词。

        Tokenization 将句子分解为单词和标点符号
        A tokenizer that splits a string using a regular expression, 
        which matches either the tokens or the separators between tokens.
        '''
        stemmer = SnowballStemmer(
            'english')  # SnowballStemmer 使用方法详见： https://www.kite.com/python/docs/nltk.SnowballStemmer
        tokenizer = RegexpTokenizer(
            r'[a-zA-Z\']+')  # RegexpTokenizer 使用方法详见： https://www.kite.com/python/docs/nltk.RegexpTokenizer

        def tokenize(text):
            """先进行 stemming 然后 tokenize
            params:
            text: 一个句子

            return:
            tokens 列表
            """

            ############ 第三题 ～ 1行 （使用列表推导） ############
            ############ your code start ############
            return tokenizer.tokenize(stemmer.stem(text))
            ############ your code end ############

        # 使用停用词、stemming 和自定义的 tokenizing 进行 TFIDF 向量化
        punc = ['.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', "%"]
        stop_words = text.ENGLISH_STOP_WORDS.union(punc)
        vectorizer2 = TfidfVectorizer(stop_words=stop_words, tokenizer=tokenize)
        X2 = vectorizer2.fit_transform(desc)
        word_features2 = vectorizer2.get_feature_names()
        print(len(word_features2))
        print(word_features2[:50])

        vectorizer3 = TfidfVectorizer(stop_words=stop_words, tokenizer=tokenize, max_features=1000)
        X3 = vectorizer3.fit_transform(desc)
        words = vectorizer3.get_feature_names()

        '''
        使用手肘法选择聚类簇的数量
        随着聚类数k的增大,样本划分会更加的精细,每个簇的聚合程度会逐渐提高,那么误差平方和SSE自然会逐渐变小,
        并且当k小于真实的簇类数时,由于k的增大会大幅增加每个簇的聚合程度,因此SSE的下降幅度会很大,而当k到达真实聚类数时,
        再增加k所得到的聚合程度回报会迅速变小,所以SSE的下降幅度会骤减,然后随着k值的继续增大而趋于平缓,
        也就是说SSE和k的关系类似于手肘的形状,而这个肘部对应的k值就是数据的真实聚类数.因此这种方法被称为手肘法.
        '''
        wcss = []
        for i in range(1, 11):
            ############ 第四题 ～ 1行 （初始化 KMeans） ############
            ############ your code start ############
            kmeans = KMeans(n_clusters=i, random_state=0)
            ############ your code end ############
            kmeans.fit(X3)
            wcss.append(kmeans.inertia_)
        plt.plot(range(1, 11), wcss)
        plt.title('The Elbow Method')
        plt.xlabel('Number of clusters')
        plt.ylabel('WCSS')
        plt.savefig('elbow.png')
        plt.show()

        print(words[250:300])

        kmeans = KMeans(n_clusters=3, n_init=20,
                        n_jobs=1)  # n_init(number of iterations for clsutering) n_jobs(number of cpu cores to use)
        kmeans.fit(X3)
        # We look at 3 the clusters generated by k-means.
        # argsort 使用方法详见： https://numpy.org/doc/stable/reference/generated/numpy.argsort.html
        common_words = kmeans.cluster_centers_.argsort()[:, -1:-26:-1]
        for num, centroid in enumerate(common_words):
            print(str(num) + ' : ' + ', '.join(words[word] for word in centroid))

        return None

    def lesson_16_elements(self):

        def one_hot(elements):
            # elements are list which are then converted to set (to make unique)
            # and then reconverted back to list
            es = list(set(elements))

            encoding = []

            for e in elements:
                # Generate zeros repeatedly based on length of elements
                zeros = [0] * len(es)

                # Assign the position of the element as 1
                zeros[es.index(e)] = 1

                # Append the results
                encoding.append(zeros)

            return encoding

        def one_hot_sklearn():
            from sklearn.preprocessing import OneHotEncoder

            enc = OneHotEncoder(handle_unknown='ignore')
            X = [['Male', 1], ['Female', 3], ['Female', 2]]
            enc.fit(X)

            return enc

        ic(one_hot(['巴黎', '纽约', '罗马', '东京', '巴黎']))

        ic(one_hot_sklearn)

        return None

    def lesson_16_mnist_logistic_regression_digit_0_and_6(self):

        '''
        MNIST logistic regression
        Only select data 0 and 6 for model building

        It's better to write another subfunction for logistic regression for 10 digits
        https://atmamani.github.io/projects/ml/mnist-digits-classification-using-logistic-regression-scikit-learn/

        :return:
        '''

        from torchvision.datasets import MNIST
        import numpy as np
        import matplotlib.pyplot as plt
        from sklearn.linear_model import LogisticRegression
        from icecream import ic

        train = MNIST('dataset', train=True, download=True)
        test = MNIST('dataset', train=False, download=True)

        # Select data 0 and 6
        zero_and_six_train = [t for i, t in enumerate(train) if train.targets[i] in [0, 6]]
        zero_and_six_test = [t for i, t in enumerate(test) if test.targets[i] in [0, 6]]

        # Reshape data to (Nth dataset) * (28*28 columns)
        zero_and_six_train_x = np.array([np.array(t).reshape(-1, ) for t, label in zero_and_six_train])
        zero_and_six_test_x = np.array([np.array(t).reshape(-1, ) for t, label in zero_and_six_test])
        zero_and_six_train_y = np.array([label for t, label in zero_and_six_train])
        zero_and_six_test_y = np.array([label for t, label in zero_and_six_test])

        # Introduce the logistic regression model
        lr_model = LogisticRegression(solver='lbfgs', max_iter=1000)
        lr_model.fit(zero_and_six_train_x, zero_and_six_train_y)

        '''
        lr_model.classes_  % list out all the classes out: array([0, 6])

        lr_model.coef_ % print weights -> lr_model.coef_.shape -> (1, 784)
        If there are multiple classes 
        lr_model.coef_[0] for 1st class
        lr_model.coef_[1] for 2nd class ...

        lr_model.intercept_ # for all classes - this is a One-vs-All classification

        y = mx + c -> coef_ * input + intercept_

        plt.imshow(lr_model.coef_[0].reshape(28, 28).round(2));  # proof of concept
        https://atmamani.github.io/projects/ml/mnist-digits-classification-using-logistic-regression-scikit-learn/
        link above performs logistic regression for 10 digits numbers

        Multiply the matrix with the coefficients and plus the intercepts 
        example 1)
        img_pred = (lr_model.coef_ * zero_and_six_train_x[0,:] + lr_model.intercept_).reshape(28,28)
        plt.imshow(img_pred) 
        plot shows it's digit 0
        if sum all the matrix elements is less than 0, then it's 0
        np.sum(img_pred) < 0 -> 0

        example 2)
        img_pred = (lr_model.coef_ * zero_and_six_train_x[1,:] + lr_model.intercept_).reshape(28,28)
        plt.imshow(img_pred) 
        plot shows it's digit 6
        if sum all the matrix elements is more than 0, then it's 6
        np.sum(img_pred) > 0 -> 6

        idx_6 = np.sum(lr_model.coef_ * zero_and_six_train_x + lr_model.intercept_, axis=1) > 0
        idx_0 = ~idx_6
        pos_0 = np.where(idx_6 == False)
        pos_6 = np.where(idx_6 == True)

        Verification of the criterion if > 0 means the output is 6 and <0 means the output is 0 is indeed the case

        '''

        for i in range(5):
            index = np.random.choice(range(len(zero_and_six_test_x)))
            ic(lr_model.predict([zero_and_six_test_x[index]]))
            plt.imshow(zero_and_six_test_x[index].reshape(28, 28))
            plt.show()

        ic(lr_model.score(zero_and_six_train_x, zero_and_six_train_y))
        ic(lr_model.score(zero_and_six_test_x, zero_and_six_test_y))
        ic(lr_model.coef_)
        ic(lr_model.intercept_)

        plt.imshow(lr_model.coef_.reshape(28, 28))
        plt.show()

        return None

    def lesson_17_knn_iris(self):

        from sklearn.datasets import load_iris
        from icecream import ic
        from collections import Counter

        def knn(x, y, query, k=3, clf=True):
            history = {tuple(x_): y_ for x_, y_ in zip(x, y)}
            neighbors = sorted(history.items(), key=lambda x_y: np.sum((np.array(x_y[0]) - np.array(query)) ** 2))[:k]
            neighbors_y = [y for x, y in neighbors]

            if clf:
                return Counter(neighbors_y).most_common(1)[0][0]
            else:
                return np.mean(neighbors_y)

        '''
        Load the iris dataset 
        Features are 'sepal length', 'sepal width', 'petal length', 'petal width' -> load.iris().feature_names
        '''
        x = load_iris()['data']
        y = load_iris()['target']

        query = [6, 3.3, 6, 2.2]

        ic(query in x)

        history = {tuple(x_): y_ for x_, y_ in zip(x, y)}

        k = 3
        # Couldn't understand code below, revisit later
        # 找出最接近 query 的 k 个点
        # 向量的剪发一定得需要 np.array
        # 没有开根号，因为只是对比大小
        neighbors = sorted(history.items(), key=lambda x_y: np.sum((np.array(x_y[0]) - np.array(query)) ** 2))[:k]
        neighbors_y = [y for x, y in neighbors]
        ic(Counter(neighbors_y).most_common(1)[0])


class main_kaikeba_cv():

    def __init__(self):
        pass

    def week7_tutorial(self):
        '''
        第一章 初阶计算机视觉：图像处理
        :return:
        '''

        # 1 拿到老师给定的图片：week7_211017/week7_homework.png。
        file = './Data/kaikeba_computer_vision/tutorial/week7_20211017/week7_homework.png'
        img = cv2.imread(file)

        # 2 对图片进行滤波操作。参考week7_211017/week7_class_code_after_class.py
        kernel = np.ones((3, 3), dtype=np.float32)
        kernel = kernel / np.sum(kernel)
        img_blur = cv2.filter2D(img, -1, kernel)
        cv2.imwrite('2_blur.jpg', img_blur)

        # 3 修改滤波核的数值和滤波核的大小，调整出最好的效果。
        kernel = np.ones((7, 7), dtype=np.float32)
        kernel = kernel / np.sum(kernel)
        img_blur2 = cv2.filter2D(img, -1, kernel)
        cv2.imwrite('3_best_blur.jpg', img_blur2)

        # 4 制作自己的logo水印的照片
        size_img = np.shape(img)
        logo = cv2.putText(np.zeros(size_img, dtype=np.uint8), text='cks kaikeba', org=(50, 900),
                           fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=3, color=(255, 255, 255), thickness=5)
        cv2.imwrite('4_logo.jpg', logo)

        # 5 将水印添加到图片上。参考week7_211017/week7_class_code_after_class.py
        mask = logo == 255
        img_blur_watermark = copy.copy(img_blur2)
        img_blur_watermark[mask] = logo[mask]
        cv2.imwrite('5_watermark.jpg', img_blur_watermark)

        # 6 图像围绕任意点进行刚体旋转的公式推导：https://zhuanlan.zhihu.com/p/399377684

        return None

    def week8_tutorial_10_digits_classification(self):
        '''
        第二章中阶计算机视觉 图像描述
        :return:
        '''

        return None

        # def week9_tutorial_11(self):

        '''
        第三章中阶到高阶的关键CNN方法
        '''

    def week9_tutorial_cuda(self):
        '''

        CV核心基础WEEK9 ：依赖硬件算力提升模型性能：cuda编程
        https://gitee.com/anjiang2020_admin/ccv10
        Pipeline:
        1  week8作业
        2  计算机视觉的常用模型
        3  CNN统一了提特征与决策
        4  GPU Schema
        5  认识pycuda,并用pycuda完成矩阵乘法

        作业：使用pycuda完成LeNet模型的以下模块：
        1. [必做]conv
        2. pooling
        3. relu
        4. linear
        5. backward

        要求：
           1 要求用pycuda库利用gpu的多线程技术，完成卷积层的计算。
           2 可以用自己定义的kernel函数,也可以用pycuda提供的核函数
           3 自己定义核函数的时候，可以参考week9_pycuda_example_6.py来实现
        其它参考材料：
            1. week8参考作业：week9_20210313/week8作业答案课堂讲解.ipynb
            2. 卷积层用nn.conv2d来实现，相关参考代码：week6/conv.py
            3. 卷积的声明，更改默认weight，默认bias,对图片进行卷积，示例子代码在：week9_20210313/conv.py 的14行,27行,29行,55行
            4. 图像滤波器：filter.py
            5. pycuda-master: pycuda源码
            6. week9_pe_5.py : 自定义核函数，打印出“hello world"
            7. week9_pe_4.py : 自定义核函数，掌握threadIdx,blockIdx等内置变量的意义。
            8. week9_pycuda_example_2.py ： 自定义加法核函数
            9. week9_pycuda_example_3.py :  自定义乘法核函数
            10. week9_pycuda_example_6.py :  自定义矩阵乘法核函数
            11. week9_pycude_example_1.py :  利用gpuarray来调用gpu进行计算。
            12. week9_cvf.py : pycuda 自带api使用,gpuarray,以及自带核函数的使用


        :return:
        '''


class main_kaikeba_python_basis_training():
    '''
    人工智能Python能力培养
    '''

    def chap_2_python_script_first_time(self):

        '''
        .\开课吧\人工智能Python能力培养\第2天 初试 Python 脚本\素材
        :return:
        '''

        def sec_03_data_structure_string():
            '''
            3.python的数据类型-字符串类型
            :return:
            '''

            # python的数据类型

            # 什么是数据类型？
            # 数据类型就是数据的表现形式，比如 你好 就是一个字符串，200就是一个数字，
            # 在程序当中除了这种常用的字符和数字外还有很多其它的数据表现形式

            # 常用的数据类型
            # 1。字符串类型
            love = 'iloveyou'
            hello = '你好 世界'
            s = "i like " \
                "you"

            # 大字符串，可以换行
            # s = '''
            # 比如说这一个
            # 很长很长的文章内容。。。
            # '''

            #  在python中提供来一个方法用户获取当前变量的数据类型，
            #  type() 函数 返回一个数据或变量的数据类型结果

            # res = type(s) # <class 'str'>  str == string
            # print(s,res)

            # 使用引号定义的字符串，可以嵌套别的引号
            # s = 'i 'love' you' 单双引号可以互相嵌套，但是不是嵌套自己
            # s = 'i "love" you'

            s = '''
            窗前'明'月光
            疑似"地上"霜
            举"""头望"""明月
            '''
            # print(s)

            # 关于转义字符 \n 表示换行   \t 表示一个制表符 。。。
            s = '窗前\n明月光'
            s = r'窗前\n明月光'  # 可以取消转义字符
            # print(s)

        def sec_04_data_structure_number():
            # 数字类型 Number
            '''
            4.Number数字类型
            int   整型
            float 浮点类型
            complex 复数
            bool  布尔类型（True，False）
            '''

            varn = 521
            varn = -1111

            varn = 3.1415926

            varn = 0x10  # 十六进制

            varn = b'001100111'  # bytes

            # 复数
            varn = 5 + 6j  # complex

            # 布尔类型  bool
            varn = True
            varn = False
            # print(varn,type(varn))

            # 数值类型可以参与运算
            a = 10
            b = 20
            print(a + b)

        def sec_05_list():
            '''
            list 列表类型
            :return:
            '''
            # 列表用来表示一系列数据，例如： 需要记录一组数字或其它数据
            # 列表中存储的数据可以是任意类型的
            # 在需要记录多个数据时，可以使用中括号进行定义 [],
            # 并且每个数据之间使用逗号分隔 ,
            # 例如以下数据，定义了几组数字
            varlist = [192, 168, 200, 68]
            # print(varlist,type(varlist))  # <class 'list'>

            # 列表中存储的每一组数据，称为元素
            varlist = ['a', 'b', 521, 'pai', 3.1415926]
            # print(varlist)

            # 列表中存储的数据，可以通过下标的方式进行获取
            '''
            关于列表中的下标
              0   1   2    3    4 
            ['a','b',521,'pai',3.1415926]
             -5   -4   -3  -2   -1
            '''
            # print(varlist[0])
            # print(varlist[2])
            # print(varlist[-3])

            # 列表中元素的值可以是任意类型
            # 那么列表中元素的值可不可以存储一个列表,称为 二级列表（二维列表） 或者 多级列表 （多维列表）
            varlist = [1, 2, 3, [11, 22, 33], 'a', 'b', 'c']
            print(varlist)

            '''
             0 1 2    3        4   5   6
            [1,2,3,[11,22,33],'a','b','c']


             0  1  2
            [11,22,33]
            '''
            print(varlist[3])
            print(varlist[3][1])

            return None

        def sec_06_tuple():
            # tuple 元组类型

            '''
            + 在定义多个数据内容时，可以选择使用List列表类型
            + 还可以使用元组类型来定义，元组和列表非常像，都时用于存储多个数据时使用
            + 元组使用小括号进行定义（），列表使用中括号进行定义
            '''
            # tuple
            vart = (1, 2, 3, 'a', 'b')
            # print(vart,type(vart))
            # print(vart[3])

            # 注意在定义元组时，如果元组中只有一个元素，那么需要加, 不然就不是元组类型了
            # vart = (123,)

            # 元组的其它定义方式
            vart = 1, 2, 3
            # print(vart,type(vart))

            # 列表与元组的区别
            '''
            + 列表使用中括号定义，
            + 元组使用小括号定义，
            + 列表中的值可以被改变，
            + 元组中的值是不可以改变的
            '''
            # 定义列表
            varl = [1, 2, 3]
            # 通过下标来修改元素的值
            varl[2] = 33
            # print(varl)

            # 定义元组
            vart = (1, 2, 3)
            vart[2] = 33
            print(vart)  # 错误，元组的值不能改变
            # 'tuple' object does not support item assignment

            return None

        def sec_07_dictionary():
            # Dict 字典类型

            '''
            + 字典也是用于存储一组或多组数据时使用，使用大括号 {}来定义
            + 字典是 键值对 的存储方式 name ：admin
            + 键和值之间使用冒号进行分隔，多组键值对之间使用逗号分隔
            + 键必须是字符串或数字类型，值可以是任意类型
            + 键名不能重复，值可以重复
            '''
            # 比如需要记录一本书的相关数据 书名，作者，价格，。。。
            vard = {'title': '<<鬼谷子>>', 'author': '鬼谷子', 'price': '29.99'}
            # print(vard,type(vard))
            # {'title': '<<鬼谷子>>', 'author': '鬼谷子', 'price': '29.99'} <class 'dict'>

            # 获取字典中的值
            # print(vard['title'])

            # 字典中的键不能重复使用，否则会覆盖
            vard = {'a': 10, 'b': 10, 'c': 20, 'a': 'aa', 1: 'abcdef', '2': '2222'}
            # print(vard)

            # tip: 在python之前的版本中，字典是无序的

            return None

        def sec_08_set():
            # set集合类型

            '''
            + set集合是一个 无序且元素不重复的 集合的数据类型
            + set集合使用 中括号或者set()方法来定义
            '''

            # 集合的定义方式
            # vars = {1,2,3,'a','b',1}
            # vars = set('123456')

            # 如果需要定义一个空集合时 只能使用 set()方法,因为大括号时定义的空字典
            # vars = {}
            # vars = set()
            # print(vars,type(vars)) # <class 'set'>

            a = {1, 2, 3, 'a'}
            # 给集合添加元素
            # a.add('b')
            # 无法获取集合中的单个元素，但是可以添加和删除
            # a.discard('a')
            # print(a)
            # 检查当前的元素是否在集合中
            # print(1 in a)

            # 集合主要用于运算，交集，差集，并集，对称集合
            a = {1, 2, 3, 'a', 'b'}
            b = {1, 'a', 22, 33}

            print(a & b)  # 交集  {1, 'a'}
            print(a - b)  # 差集 {'b', 2, 3}  a 集合有，b集合没有的
            print(a | b)  # 并集 {1, 2, 3, 33, 'a', 'b', 22} 两个集合，放到一起，并且去重
            print(a ^ b)  # 对称差集 {33, 2, 3, 'b', 22}

            return None

        def sec_09_data_type_conversion():
            # 9.基础数据类型转换
            # 数据类型转换

            # 数据类型总结
            '''
            字符串 string
            数字类型 Number
                整型 int
                浮点 float
                复数
                布尔 bool
            列表  list
            元组  tuple
            字典  dict
            集合  set

            可变数据类型：列表，字典，集合
            不可不数据类型： 字符串，数字，元组

            容器类型数据 ： 字符串，列表，元组，集合，字典
            非容器类型数据： 数字，布尔类型
            '''

            # 数据类型转换
            '''
            什么是数据类型转换？
                把一个数据类型转换为另一个数据类型，例如 字符串转为数字
            为什么需要数据类型转换？
                因为不同的数据类型之间不能运算
            数据类型转换的形式？
                自动类型转换
                强制类型转换
            '''

            # 自动类型转换
            # 当两个不同的值进行运算时，结果会向更高的精度进行计算
            # True ==> 整型 ==> 浮点 ==> 复数
            a = 123
            b = True  # 在和数字运算时 True转为数字1，False转为数字 0
            # print(a+b)
            # print(12.5+22)
            # print(True+3.14)

            '''
            if 表达式：
                真区间
            else：
                假区间
            '''
            # if a:
            #     print('真')
            # else:
            #     print('假')

            # 强制类型转换
            '''
            下面的函数，可以把其它类型的数据，转换为对应的数据类型
            str()
            int()
            float()
            bool()
            list()
            tuple()
            dict()
            set()
            '''
            a = 'love'
            # a = '123'
            # a = '123.5'
            # a = '123abc'
            # a = 123
            # a = 3.14
            # a = True
            # a = [1,2,3]
            # a = (22,33)
            # a = {11,22}
            # a = {'a':1,'b':2}
            print(a, type(a))

            #  str() 可以把所有的其它数据类型转换为字符串类型
            # res = str(a)
            # 字符串转数字类型时，如果字符串中时纯数字，可以转换
            # 其它容器类型不能转为数字int类型
            # res = int(a)
            # 浮点类型的转换和int类型一样，不过转换的结果是浮点类型
            # res = float(a)
            # bool可以把其它类型转换布尔类型的True或False
            # 需要总结，哪些情况转bool的结果是 False
            '''
                '0' True
                '' False
                 0
                 0.0
                 False
                 []
                 {}
                 ()
                 set()
            '''
            # res = bool(a)

            #
            # print(res,type(res))

        def sec_10_container_conversion():
            # 容器类型数据转换

            # list 列表
            '''
             数字类型是 非容器类型，不能转换为列表
             字符串 转换为列表时  会把字符串中的每一个字符当做列表的元素
             集合 可以转换为 list列表类型
             元组 可以转换为 list列表类型
             字典 可以转换为 list列表类型,只保留了字典中的键
            '''
            # n = {'name':'zhangsan','age':20}
            # res = list(n)
            # print(n,type(n),res,type(res))

            # tuple 元组
            '''
            数字类型 非容器类型，不能转换为元组
            其它容器类型的数据进行转换时，和列表一样
            '''
            # n = {'name':'zhangsan','age':20}
            # res = tuple(n)
            # print(n,type(n),res,type(res))

            # set 集合
            '''
            数字类型 非容器类型，不能转换为 集合
            字符串,列表，元组 可以转为 集合 结果是无序的
            字典转换为集合时，只保留了字典的键 key

            '''
            # n = {'a':1,'b':2}
            # res = set(n)
            # print(n,type(n),res,type(res))

            # dict 字典
            '''
            数字类型 非容器类型，不能转换为 字典
            字符串不能直接转换为 字典

            列表可以转换为字典，要求是一个二级列表，并且每个二级元素只能有两个值
            元组可以转换为字典，要求是一个二级元组，并且每个二级元素只能有两个值
            '''

            n = 123
            # n = '12'
            # n = [[1,2],['a','b'],['11',11]]
            # n = ((1,2),(3,4))

            # res = dict(n)
            # print(n,type(n),res,type(res))

    def chap_3_python_operation_flow_control(self):

        def chap_3_1_python_operators():
            def chap_3_1_1_arithmetic_operation_string_operation():
                # 运算符
                # 算  字  赋 比  逻 位  它

                # 算术运算符 + - * /  % ** //
                a = 10
                b = 20
                # print(a + b)
                # print( a - b )
                # print( a * b)
                # print( a / b)
                # print(2 ** 3)
                # print( 9 % 2 )
                # print( 9 // 2 )

                # 字符串运算
                '''
                1. 字符串与数字不能直接参与运算
                2. 字符串和字符串使用 + 结果是字符串的拼接
                3. 字符串如果和数字 使用 * 那么就是重复前面的字符串
                '''
                # print('1'+2) # X
                # print('1'+'2') # '12'
                # print('1'*5) # '11111'

                # 关于字符串的拼接
                l = 'love'
                # i = 'i '+l+' you'
                # i = f'i {l} you'
                # i = 'i {} you'.format(l)
                # i = 'i {l} you'.format(l=l)
                # print(i)

                return None

            def chap_3_1_2_assignment_comparison_logic_operation():
                return None

            return None

    def chap_5_advanced_functions(self):

        # 初步认识 递归函数  3 2 1 0
        def digui(num):
            print(num)  # 3 2 1 0
            # 检测当前的值是否到了零
            if num > 0:
                # 调用函数本身
                digui(num - 1)
            print(num)  # 0 1 2 3
            return None

        digui(3)

        def outer_function(xyz):
            def scaling():
                nonlocal xyz

                xyz = xyz * 3
                print(xyz)

                return xyz

            return scaling

        func = outer_function()

        return None

    def chap_9_tuple(self):

        def chap_9_1_tuple_slicing():

            # 注意，定义元组时，如果只有一个元素，也需要使用 逗号
            # vart = (1,)
            # print(vart,type(vart))

            # 元组的切片操作  和列表是一样的
            vart = (1, 2, 3, 4, 5, 5, 4, 3, 2, 1)
            res = vart[:]  # 获取全部
            res = vart[::]  # 获取全部
            res = vart[1:]  # 从索引1开始获取到最后
            res = vart[1:3]  # 从索引1开始到索引3之前
            res = vart[:3]  # 从索引 0 开始 到 索引 3之前
            res = vart[1:5:2]  # 从索引1开始到索引5之前，步进值为2
            res = vart[::2]  # 从索引 0 开始 到 最后 ，步进值为2
            res = vart[5:1:-1]  # 从索引5开始 到索引 1，步进值为-1  倒着输出

            # 获取元组的长度 len()
            # res = len(vart)

            # 统计一个元素在元组中出现的次数
            # res = vart.count(5)

            vart = ('张学友', '吴恩达', '柳岩', '吴恩达')
            # 获取一个元素在元组的索引值
            res = vart.index('吴恩达')
            res = vart.index('吴恩达', 2)  # 从指定下标位置开始查找
            res = vart.index('吴恩达', 2, 5)  # 从指定的 索引区间内 查找

            # 元组的 + * 运算 ，合并元组，组成新的元组
            res = (1, 2, 3) + ('a', 'b')
            res = (1, 2, 3) * 3

            # 检测一个元素是否在元组中
            # res = 22 in res
            res = 22 not in res

            print(res)

        def chap_9_2_generate_generator_using_tuple():

            #  元组推导式  生成器
            '''
            元组推导式
                列表推导式结果返回了一个列表，元组推导式返回的是生成器
                语法：
                    列表推导式 ==> [变量运算 for i in 容器]  ==> 结果 是一个 列表
                    元组推导式 ==> (变量运算 for i in 容器)  ==> 结果 是一个 生成器

            生成器是什么？
                生成器是一个特殊的迭代器，生成器可以自定义，也可以使用元组推导式去定义
                生成器是按照某种算法去推算下一个数据或结果，只需要往内存中存储一个生成器，节约内存消耗，提升性能
            语法：
                （1） 里面是推导式，外面是一个() 的结果就是一个生成器
                 (2) 自定义生成器，含有yield关键字的函数就是生成器
                     含有 yield 关键字的函数，返回的结果是一个迭代器，换句话说，生成器函数就是一个返回迭代器的函数

            如何使用操作生成器？
                生成器是迭代器的一种，因此可以使用迭代器的操作方法来操作生成器
            '''

            # 列表推导式
            varlist = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            # newlist = [i**2 for i in varlist]
            # print(newlist) # [1, 4, 9, 16, 25, 36, 49, 64, 81]

            # 元组推导式 生成器 generator
            newt = (i ** 2 for i in varlist)
            print(newt)  # <generator object <genexpr> at 0x1104cd4d0>

            # 使用next函数去调用
            # print(next(newt))
            # print(next(newt))

            # 使用list或tuple函数进行操作
            # print(list(newt))
            # print(tuple(newt))

            # 使用 for 进行遍历
            # for i  in newt:
            #     print(i)

        def chap_9_3_generate_generator_using_yield():

            # 。yield 关键字
            '''
            yield关键字使用在 生成器函数中
                + yield 和函数中的 return 有点像
                + 共同点：执行到这个关键字后会把结果返回
                + 不同点：
                    + return 会把结果返回，并结束当前函数的调用
                    + yield 会返回结果，并记住当前代码执行的位置，下一次调用时会从上一次离开的位置继续向下执行
            '''

            # 定义一个普通函数
            def hello():
                print('hello 1')
                return 1  # return在函数中会把结果返回，并且结束当前的函数，后面的代码不再执行
                print('world 2')
                return 2

            #
            # hello()
            # hello()

            # 使用 yield定义一个 生成器函数
            def hello():
                print('hello 1')
                yield 1
                print('world 2')
                yield 2
                print('haha 3')
                yield 3

            # 调用生成器函数，返回一个迭代器
            res = hello()

            # 使用生成器返回的迭代器
            # r = next(res)
            # print(r)
            # r = next(res)
            # print(r)

            # 使用生成器返回的迭代器
            # 适应list类似的函数 去调用生成器返回的迭代器时，会把迭代器的返回结果，作为容器的元素
            # r = list(res)
            # print(r)

            # 使用生成器返回的迭代器
            # for i in res:
            #     print(i)

            '''
            上面的生成器函数调用时的过程
            首先 调用来生成器函数，返回来一个迭代器
            1。第一次去调用迭代器：
                走到当前的生成器函数中，遇到了 yield 1，把1返回，并且记住来当前的执行状态(位置)，暂停了执行，等待下一次的调用

            2。第二层去调用迭代器
                从上一次遇到的yield位置开始执行，遇到了 yield 2 ，把2返回，并记住状态，暂停执行，等待下一次调用

            3。第三次去调用迭代器
                从上一次遇到的yield位置开始执行，遇到 yield 3 ， 把3返回，并记住了状态，暂停执行，等待下一次调用

            如果在最后又调用了迭代器，那么会从上一次的 yield位置开始，结果后面没有了，直接就超出范围，报错
            '''

            # 练习题：使用 生成器 改写 斐波那契数列函数
            # 1，1，2，3，5，8，13，。。。。

        def chap_9_4_homework_fibonacci():

            '''
            Fibonacci numbers
            :return:
            [1，1，2，3，5，8，13，... ]
            '''

            # Define for the 1st number
            a = 1

            yield a

            # Define for the 2nd number
            b = 1

            yield b

            # Define for the 3rd number
            c = a + b

            yield c

            # To avoid infinite loop
            i = 1

            # As the subsequent numbers are calculated based on the previous two numbers
            # It has to be an infinite loop
            # Define for the 4th number and so on
            while True:

                a = b
                b = c
                c = a + b

                i = i + 1

                if i == 100:
                    break

                yield c

    def chap_10_dictionary(self):

        def chap_10_1_how_to_define_dict():

            # 字典定义和基本操作

            # 一，字典定义

            # 1。 使用{}定义
            vardict = {'a': 1, 'b': 2, 'c': 2}

            # 2。 使用 dict(key=value,key=value) 函数进行定义
            vardict = dict(name='zhangsan', sex='男', age=22)

            # 3。 数据类型的转换  dict(二级容器类型) 列表或元组，并且是二级容易才可以转换
            vardict = dict([['a', 1], ['b', 2], ['c', 3]])  # {'a': 1, 'b': 2, 'c': 3}

            # 4。zip压缩函数，dict转类型
            var1 = [1, 2, 3, 4]
            var2 = ['a', 'b', 'c', 'd']

            # 转换的原理和上面的第三种 是一个原理
            vardict = dict(zip(var1, var2))  # {1: 'a', 2: 'b', 3: 'c', 4: 'd'}

            # 二，字典数据的操作 获取，添加，更新，删除

            var1 = {'a': 1, 'b': 2, 'c': 3}
            var2 = {1: 'a', 2: 'b', 3: 'c', 4: 'd'}

            # res = var1 +  var2 # XXXX  TypeError
            # res = var1 * 3 # xxxx TypeError

            # 获取元素
            res = var1['a']

            # 修改元素
            res = var1['a'] = 111

            # 删除元素
            del var1['a']

            # 添加元素
            var1['aa'] = 'AA'

            # 如果字典中的key重复了，会被覆盖
            # var1['aa'] = 'aa'

            # 三 成员检测  ,只能检测key，不能检测value
            res = 'AA' in var1
            res = 'AA' not in var1

            # 获取当前字典的长度 只能检测当前又多少个键值对
            res = len(var1)

            # 获取当前字典中的所有 key 键
            res = var1.keys()
            # 获取字典中所有的 value 值
            res = var1.values()
            # 获取当前字典中所有 键值对
            res = var1.items()

            # 四， 对字典进行遍历
            # （1）在遍历当前的字典时，只能获取当前的key
            for i in var1:
                print(i)  # 只能获取 key
                print(var1[i])  # 通过字典的key获取对应value

            # （2）遍历字典时，使用 items() 函数，可以在遍历中获取key和value
            for k, v in var1.items():
                print(k)  # 遍历时的 key
                print(v)  # 遍历时的 value

            print('====' * 20)
            # (3) 遍历字典的所有key
            for k in var1.keys():
                print(k)

            print('====' * 20)
            # (4) 遍历字典的所有 value
            for v in var1.values():
                print(v)

        def chap_10_2_update_del_pop_dict():

            #  字典相关函数

            # len(字典) #获取字典的键值对个数
            # dict.keys() # 获取当前字典的所有key 键，组成的列表
            # dict.values() # 获取当前字典的所有 value 值，组成的列表
            # dict.items() # 返回由字典项 ((键, 值) 对) 组成的一个新视图
            # iter(d) 返回以字典的键为元素的迭代器。

            vardict = {'a': 1, 'b': 2, 'c': 3}

            # dict.pop(key) # 通过 key 从当前字典中弹出键值对  删除
            # res = vardict.pop('a')

            # dict.popitem()   LIFO: Last in, First out.后进先出
            # res = vardict.popitem()  # 把最后加入到字典中的键值对删除并返回一个元组

            # 使用key获取字典中不存在元素，会报错
            # print(vardict['aa'])
            # 可以使用get获取一个元素，存在则返回，不存在默认返回None
            # res = vardict.get('aa')
            # res = vardict.get('aa','abc')

            # dict.update(),更新字典,如果key存在，则更新，对应的key不存在则添加
            # vardict.update(a=11,b=22)
            # vardict.update({'c':33,'d':44})

            # dict.setdefault(key[,default])
            # 如果字典存在键 key ，返回它的值。
            # 如果不存在，插入值为 default 的键 key ，并返回 default 。
            # default 默认为 None。

            res = vardict.setdefault('aa', '123')

            print(res)
            print(vardict)

    class chap_12_file_operation():

        def __init__(self):
            pass

        def chap_12_4_function_for_file_operation(self):

            vars = 5211  # int类型无法写入
            vars = ['hello', 'world', '1', '2']
            vars = {'name': 'zs', 'age': '22'}
            with open('./4.txt', 'w', encoding='utf-8') as fp:
                # fp.write(vars)  # 只能写入字符串类型数据
                fp.writelines(vars)  # 可以写入容器类型数据，注意容器中的元素也必须是字符串类

        def chap_12_4_read_file_functions(self):

            with open('./4.txt', 'r', encoding='utf-8') as fp:
                # fp.seek(3)  # 设置指针的位置
                # res = fp.read()  # 默认从当前指针开始读取到最后
                # res = fp.read(3)  # 设置读取的字节长度
                # res = fp.readline()  # 一次只读取一行内容
                # print(res)
                # res = fp.readline(3)  # 可以读取当前行中的指定字节数
                # res = fp.readlines()  # 一次读取多行数据，每一行作为一个元素，返回一个列表
                res = fp.readlines(6)  # 按照行进行读取，可以设置读取的字节数，设置的字节数不足一行按一行算
                print(res)

            # truncate() 截断文件内容
            with open('./test/4.txt', 'r+', encoding='utf-8') as fp:
                res = fp.truncate(5)
                # 默认从文件的首行的首个字符开始进行截断，截断的长度为size个字节数，
                # size如果为0，则从当前位置截断到最后

        def chap_12_exercise_username_password(self):

            '''
            注册功能
                1。需要用户名和密码以及确认密码
                2。注册时如果用户名已经存在，则不能再次注册

            登录功能
                1。需要使用已经注册的用户信息登录
                2。密码输入错误3次后，锁定账户信息（不能再使用这个账户进行登录操作）

            练习题：多思考，尽量独立自主的完成
            '''

            def load_database(path):

                # Import the username database
                fid = open(path, 'r+', encoding='utf-8')
                database = fid.readlines()

                # Extract the username and password in dictionary format
                username_pw_dict = {(i.strip('\n')).split(':')[0]: (i.strip('\n')).split(':')[1] for i in database}
                # The above single line of code can replace the following 3 lines of codes
                '''
                # Remove \n if the username database has \n
                username_list = [name[:-1] if name[-1:] == '\n' else name for name in database]

                # Convert username_list into dictionary type
                loc = [name.index(':') for name in username_list]
                username_pw_dict = {name[0:loc[i]]:name[loc[i]+1:] for i, name in enumerate(username_list)}
                '''

                return username_pw_dict

            def check_username_if_registered(username, password, database):

                # Check if input username has been used before
                status = username in database.keys()

                # Output the status depending if the name has been registered before
                if status == True:

                    print('Username has been used before, registration unsuccessful')

                else:

                    print('Username successfully registered.')

                    # Write the newly created username and password to the database
                    with open(database_path, 'a', encoding='utf-8') as fid:
                        fid.write('\n' + username + ':' + password)

                return status

            def check_username_and_password_are_correct(username, password, database):

                # Check if input username has been used before
                status_username = username in database.keys()

                # Output the status depending if the name has been registered before
                if status_username == True:

                    count = 1

                    while True:

                        status_password = (password == database[username])

                        if status_password == True:

                            account_locked = False
                            print('Log in Success!')
                            break

                        else:

                            count = count + 1
                            password = input('Password keyed in wrongly! Please key in again the password!')

                        if count == 3:
                            print('Password keyed in wrongly for 3 times! Account locked.')
                            account_locked = True
                            break

                else:

                    print('Username has not been registered before. Please register.')
                    account_locked = False

                return account_locked

            # Load database (dictionary type)
            database_path = './Data/kaikeba_python_basis_training/ch12/username_library.txt'
            database = load_database(database_path)

            # Two choices, user wants to register for new account or log in to the account
            status = input('Register (1) or Log In (2)')

            # For registration, requests user to key in desired username and password
            if status == '1':

                username = input('username')
                password = input('password')

                # Return if the registration is successful
                status_registered = check_username_if_registered(username, password, database)

            elif status == '2':

                username = input('username')
                password = input('password')

                # Check if password is keyed in correctly
                status_account_locked = check_username_and_password_are_correct(username, password, database)

    class chap_13_built_in_and_time_functions():

        def __init__(self):
            pass

        def chap_13_1_pickle_function(self):
            # 序列化-pickle

            import pickle

            '''
            什么是序列化？为什么需要序列化？
            1。序列化是指可以把python中的数据，以文本或二进制的方式进行转换，并且还能反序列化为原来的数据
            2。一般来说数据在程序与网络中进行传输和存储时，需要以更加方便的形式进行操作，因此需要对数据进行序列化

            pickle模块提供的函数

                dumps() 序列化，返回一个序列化后的二进制数据 可以把一个python的任意对象序列化成为一个二进制
                    pickle.dumps(var)
                loads() 反序列化，返回一个反序列化后的python对象  可以把一个序列化后的二进制数据反序列化为python的对象
                    pickle.dumps(var)

                dump() 序列化，把一个数据对象进行序列化并写入到文件中
                    参数1，需要序列化的数据对象
                    参数2，写入的文件对象
                    pickle.dump(var,fp)
                load() 发序列化，在一个文件中读取序列化的数据，并且完成一个反序列化
                    参数1，读取的文件对象
                    pickle.load(fp)

            '''

            import pickle

            # Serialization of the data
            x = 'i love you'
            x = [1, 2, 3, 4]
            x = {'name': 'kinsung', 'age': 37}
            res = pickle.dumps(x)
            print(res)

            # Deserialization of the data
            res2 = pickle.loads(res)
            print(res2)

            vars = {'name': '张三', 'age': 20, 'sex': 'm'}
            database_path = './Data/kaikeba_python_basis_training/ch13/file_pickle.txt'
            with open(database_path, 'wb') as fp:
                # 在此处调用pickle模块的方法
                pickle.dump(vars, fp)

            # 读
            with open(database_path, 'rb') as fp:
                # 在此处调用pickle模块的方法
                newdict = pickle.load(fp)
            print(newdict)

        def chap_13_2_json_function(self):

            # 序列化 json
            '''
            什么是json？
            JSON (JavaScript Object Notation)
            JSON 是一个受 JavaScript 的对象字面量语法启发的轻量级数据交换格式。
            JSON 在js语言中是一个对象的表示方法，和Python中的字典的定义规则和语法都很像
            JSON 在互联网中又是一种通用的数据交换，数据传输，数据定义的一种数据格式

            python中提供的json模块 (import json)，可以把一些符合转换的python数据对象，转为json格式的数据

                json.dumps()
                json.loads()

                json.dump()
                json.load()

            '''

            import json
            # 以下语法格式定义的是一个 字典 数据类型

            vardict = {'name': 'admin', 'age': 20, 'sex': '男'}
            vardict = [1, 2, 3]
            vardict = [{'name': 'admin', 'age': 20, 'sex': '男'}, {'name': 'aa', 'age': 21, 'sex': 'm'}]

            vardict = 'abcdef'  # 只是转为了字符串而已，
            vardict = 521  # 只是转为了数字而且

            # print(vardict,type(vardict))

            # 使用 json模块的 dumps方法进行 json格式的转换
            res = json.dumps(vardict)
            # print(res,type(res))

            # 使用 loads方法进行反转换
            res = json.loads(res)
            # print(res,type(res))

            # dump  load
            # 写
            vardict = [{'name': 'admin', 'age': 20, 'sex': '男'}, {'name': 'aa', 'age': 21, 'sex': 'm'}]
            # with open('./data.json','w') as fp:
            #     json.dump(vardict,fp)

            # 读
            with open('./data.json', 'r') as fp:
                new = json.load(fp)
            print(new)

        def chap_13_3_math_module(self):

            # 数学模块 Math

            import math
            # math的相关函数。一部分
            # math.ceil()  向上取整,内置函数 round() 四舍五入
            res = math.ceil(2.55)

            # math.floor() 向下取整，
            res = math.floor(2.55)

            # math.pow() 计算数值的n次方,结果是浮点
            res = math.pow(2, 3)

            # math.sqrt() 开平方运算，结果是浮点
            res = math.sqrt(12)

            # math.fabs() 计算绝对值,结果是浮点
            res = math.fabs(-3.14)

            # math.modf() 把一个数值拆分成小数和整数组成的元组
            res = math.modf(3)  # (0.0, 3.0)

            # math.copysign(x,y)  把第二个参数的正负符合拷贝给第一个参数,结果为浮点数
            res = math.copysign(-3, 99)

            # math.fsum() 将一个容器类型数据中的元素进行一个求和运算，结果为浮点数
            # res = math.fsum('123')  # X TypeError: must be real number, not str
            # res = math.fsum({1,2,3}) # 注意：容器中的元素必须是可以运算的number类型

            # math.factorial(x)  以一个整数返回 x 的阶乘
            res = math.factorial(10)  #

            # 常量
            # 数学常数 π = 3.141592...，精确到可用精度。
            res = math.pi
            # print(res)

            print('hello 你瞅啥?')

            return None

        def chap_13_4_random_module(self):

            # 。随机模块 random

            import random

            # random.random()  返回 0 - 1 之间的随机小数 (左闭右开)
            res = random.random()

            # random.randrange([开始值]，结束值，[步进值]) 随机获取指定范围内的整数
            res = random.randrange(5)  # 一个参数，从0到整数之间的值，左闭右开
            res = random.randrange(5, 10)  # 两个参数，从第一个值到第二个值之间的随机数，左闭右开
            # res = random.randrange(5,10,2) # 三个参数，按照指定步进值从第一个值到第二个值之间的随机数，左闭右开
            # 随机数的应用场景：数字验证码，高并发下的订单号。。。

            # random.randint() 随机产生指定范围内的随机整数
            res = random.randint(5, 10)

            # random.uniform() 获取指定返回内的随机小数
            res = random.uniform(5, 10)

            # random.choice() 随机获取容器类型中的值
            res = random.choice('123')
            res = random.choice([1, 2, 3, 4])

            # random.shuffle() 随机打乱当前列表中的值,没有返回值，直接打乱原数据
            arr = [1, 2, 3, 4, 5]
            res = random.shuffle(arr)
            # print(res,arr)

            return None

        def chap_13_5_os1_module(self):

            # 系统接口模块 os

            import os

            # os.getcwd() 获取当前的工作目录,注意获取的不是当前脚本的目录，
            res = os.getcwd()

            # 如果在当前目录执行这个脚本文件，那么getcwd获取的就是当前的文件目录
            # 如果把执行的目录切换到其它位置，在执行当前脚本，那么获取的就是你执行这个脚本时的目录

            # os.chdir() # 修改当前的工作目录
            # os.chdir('/Users/yc/')

            # 修改工作目录后，再去获取工作目录
            # res = os.getcwd()

            # os.listdir() 获取当前或指定目录中的所有项（文件，文件夹，隐藏文件），组成的列表
            # res = os.listdir() # 不指定目录时，默认为当前的工作目录  == linux 中的 ls -al   == windows dir
            # res = os.listdir(path='/users/yc/Desktop/code') # == linux 中的 ls -al   == windows dir

            # os.mkdir(文件夹路径，权限)  # 创建文件夹
            # os.mkdir('aa',0o777)  # 默认在工作目录创建一个人文件夹

            '''
                关于系统中的文件权限，仅限linux系统
                drwxr-xr-x   4 yc  staff   128 11 27 11:40 aa
                dr----x--x   2 yc  staff    64 11 27 11:42 abc
                第一位 d代表是一个目录，如果是-则表示为一个文件
                前三位的rwx 代表文件所有人( u )的权限
                中间三位的 r-x 代表文件所属组( g )的权限
                最后三位的 r-x 代表其他人( o )的权限

                其中 r w x 代表不同的操作权限  777 分别代表 所有人，所属组，和其它
                r 表示是否可读，   4
                w 表示是否可写     2
                x 表示是否可执行   1
            '''
            # os.mkdir('/users/yc/Desktop/code/abc/a/b/c') # 不能递归创建

            # os.makedirs() 可以递归创建文件夹
            # os.makedirs('/users/yc/Desktop/code/abc/a/b/c/')
            # print(res)

            return None

        def chap_13_6_os2_module(self):

            # 系统接口模块 os

            import os

            # os.rmdir() 删除 空 文件夹
            # os.rmdir('./a')  # a 是一个空文件夹
            # os.rmdir('./b')  # b 是 含有一个文件夹的 目录 OSError: Directory not empty: './b'
            # os.rmdir('./c')  # c 是 含有一个文件的  目录   OSError: [Errno 66] Directory not empty: './c'

            # os.removedirs() 递归删除空文件夹
            '''
            连续创建几个空文件
            abc/
                def/
                    aaa/
            ./abc/def/aaa/

            在mac系统中连续创建了abc目录后又在里面创建def，又在def里面创建aaa
            此时。使用os.removedirs('./abc/def/aaa/') 删除时，只删除了aaa。
            为什么？
            因为mac系统中的文件夹只要被使用过，都会默认创建一个隐藏文件 .DS_Store，因此这个文件夹不在是空文件夹了

            '''
            # os.removedirs('./abc/def/aaa/')

            # os.remove()  删除文件
            # os.remove('./abc/.DS_Store')

            # os.rename() 修改文件或文件夹的名字
            # os.rename('./a','./AAA')

            # os.system() 执行操作系统中的命令
            os.system('python3 3.内置模块-数学模块-Math.py')
            os.system('ls')

            return None

        def chap_13_7_os_path(self):
            # 。os.path  系统模块中的路径模块

            import os

            # 将相对路径转化为绝对路径  ***
            res = os.path.abspath('./')  # /Users/yc/Desktop/code
            # Out[270]: 'C:\\Users\\PC\\Google 云端硬盘\\programming subfunctions\\Python\\AI'

            # 获取路径中的主体部分 就是返回路径中的最后一部分
            res = os.path.basename('/Users/yc/Desktop/code')  # code
            res = os.path.basename('./Data/CATS_DOGS/test/CAT/9374.jpg')
            # '9374.jpg'

            # 获取路径中的路径部分  返回路径中最后一部分之前的内容
            res = os.path.dirname('./Data/CATS_DOGS/test/CAT/9374.jpg')
            # Out[267]: './Data/CATS_DOGS/test/CAT'

            # join()  链接多个路径，组成一个新的路径
            res = os.path.join('./a/b/c/', '2.jpg')  # ./a/b/c/2.jpg

            # split() 拆分路径，把路径拆分为路径和主体部分，
            res = os.path.split('./Data/CATS_DOGS/test/CAT/9374.jpg')
            # ('./Data/CATS_DOGS/test/CAT', '9374.jpg')

            # splitext() 拆分路径，可以拆分文件后缀名
            res = os.path.splitext('./Data/CATS_DOGS/test/CAT/9374.jpg')
            # Out[269]: ('./Data/CATS_DOGS/test/CAT/9374', '.jpg')

            # 获取文件的大小  字节数
            res = os.path.getsize('./3.内置模块-数学模块-Math.py')

            # 检测是否是一个文件夹,是否存在
            res = os.path.isdir('/Users/yc')

            # 检测文件是否存在  ***
            res = os.path.isfile('./3.内置模块-数学模块-Math.py')

            # exists() **** 检测路径是否存在，既可以检测文件，也可以检测路径
            res = os.path.exists('/Users/yc/Desktop/code/3.内置模块-数学模块-Math.py')

            #
            a = '/Users/yc/Desktop/code/3.内置模块-数学模块-Math.py'
            b = '/Users/yc/../yc/Desktop/code/3.内置模块-数学模块-Math.py'
            # 检测两个path路径是否同时指向了一个目标位置 （两个路径必须真实）
            res = os.path.samefile(a, b)
            print(res)

            return None

        def chap_13_8_shutil(self):

            '''

            The shutil module offers a number of high-level operations on files and collections of files.
            In particular, functions are provided which support file copying and removal.
            For operations on individual files, see also the os module.

            # 。高级模块  shutil
            '''

            import shutil

            # shutil == shell util

            # copy 复制文件  把一个文件拷贝到指定的目录中
            shutil.copy('./data.json', './a/da.json')

            # copy2 和copy方法一样，可以把拷贝文件到指定目录，保留了原文件的信息（操作时间和权限等）

            # copyfile 拷贝文件的内容（打开文件，读取内容，写入到新的文件中）

            # copytree 可以把整个目录结构和文件全部拷贝到指定目录中，但是要求指定的目标目录必须不存在
            # b 目标文件夹必须不存在
            shutil.copytree('./a', './b/')

            # rmtree() 删除整个文件夹
            shutil.rmtree('a')

            # move 移动文件或文件夹到指定目录，也可以用于修改文件夹或文件的名称
            shutil.move('./b', './abc')

            return None

        def chap_13_9_zipfile(self):

            '''

            The ZIP file format is a common archive and compression standard.
            This module provides tools to create, read, write, append, and list a ZIP file.
            Any advanced use of this module will require an understanding of the format,
            as defined in PKZIP Application Note.

            #  压缩模块 zipfile
            '''

            import zipfile, os

            # 压缩文件 操作
            with zipfile.ZipFile('spam1.zip', 'w') as myzip:
                myzip.write('data.json')
                myzip.write('data.txt')
                myzip.write('data2.txt')

            # 解压缩文件
            with zipfile.ZipFile('spam.zip', 'r') as myzip:
                myzip.extractall('./')

            # 如果压缩当前文件夹中的所有文件？
            with zipfile.ZipFile('spam.zip', 'w', zipfile.ZIP_DEFLATED) as myzip:
                # 获取当前目录中所有的项
                arr = os.listdir('./')
                for i in arr:
                    myzip.write(i)

            # 使用shutil模块进行归档压缩
            import shutil
            # 参数1 创建的压缩文件名称，参数2，指定的压缩格式，zip，tar 参数3 要压缩的文件或文件夹路径
            shutil.make_archive('a', 'zip', os.path.abspath('pcl_cks.py'))

            return None

        def chap_13_1_time_module(self):

            # 。 时间模块  time

            import time

            '''
            概念：
                1。 时间戳： 1574905882.6581771 表示从1970年1月1日0时0分0秒到现在的一个秒数，目前可以计算到2038年
                2。 时间字符串： Thu Nov 28 09:54:08 2019  
                3。 时间元组： time.struct_time(tm_year=2019, tm_mon=11, tm_mday=28, tm_hour=9, tm_min=55, tm_sec=32, tm_wday=3, tm_yday=332, tm_isdst=0)

            '''

            # *** 1. 获取当前系统的时间戳
            res = time.time()
            # 1646821808.8581314

            # 2. 获取当前系统时间，时间字符串
            res = time.ctime()
            # 'Wed Mar  9 18:33:13 2022'

            # 3. 获取当前系统时间， 时间元组
            res = time.localtime()
            # time.struct_time(tm_year=2022, tm_mon=3, tm_mday=9, tm_hour=18, tm_min=34, tm_sec=44, tm_wday=2, tm_yday=68, tm_isdst=0)

            # 4. 以上时间字符串和时间元组可以通过指定的时间戳来获取
            t = 1564000082.6581771
            res = time.ctime(t)
            res = time.localtime()
            print(res)

            # 5. 使用localtime方法获取的时间元组，如何格式化成为 xxxx年xx月xx日 时：分：秒  星期几
            print(
                f'{res.tm_year}年{res.tm_mon}月{res.tm_mday}日 {res.tm_hour}：{res.tm_min}：{res.tm_sec} 星期{res.tm_wday + 1}')

            # *** 6. strftime() 格式化时间 年-月-日  时：分：秒 星期几
            res = time.strftime('%Y-%m-%d %H:%M:%S %w')

            # *** 7。 sleep(秒) 在给定的秒数内暂停调用线程的执行。该参数可以是浮点数，以指示更精确的睡眠时间。
            print(time.strftime('%Y-%m-%d %H:%M:%S %w'))
            time.sleep(3)
            print(time.strftime('%Y-%m-%d %H:%M:%S %w'))

            # 、计算程序的运行时间
            time.perf_counter()

            # 100万次的字符串比较 需要执行的时间

            start = time.perf_counter()
            for i in range(1000000):
                if 'abc' > 'acd':
                    pass
            end = time.perf_counter()
            print(end - start)  # 0.14171751

            #
            start = time.perf_counter()
            for i in range(1000000):
                if 103 > 100:
                    pass
            end = time.perf_counter()
            print(end - start)  # 0.164985942

            return None

        def chap_13_2_calendar_module(self):

            import calendar

            # 返回指定年份和月份的数据，月份的第一天是周几，和月份中的天数。
            # calendar.monthrange（年，月）
            def showdate(year, month):
                res = calendar.monthrange(year, month)
                days = res[1]  # 当前月份的天数
                w = res[0] + 1  # 当前月份第一天周几信息
                print(f'====={year}年{month}月的日历信息=====')
                print(' 一   二  三  四   五  六  日 ')
                print('*' * 28)
                # 实现日历信息的输出
                d = 1
                while d <= days:
                    # 循环周
                    for i in range(1, 8):
                        # 判断是否输出
                        if d > days or (i < w and d == 1):
                            print(' ' * 4, end="")
                        else:
                            print(' {:0>2d} '.format(d), end="")
                            d += 1
                    print()
                print('*' * 28)

            showdate(2019, 12)

            return None

        def chap_13_3_calendar_module2(self):

            # 万年历

            import calendar, time, os
            # 每个月份的日历信息的基础上，可以完成上一月和下一月信息的显示，
            # 并且可以指定年或月

            def showdate(year, month):
                res = calendar.monthrange(year, month)
                days = res[1]  # 当前月份的天数
                w = res[0] + 1  # 当前月份第一天周几信息
                print(f'====={year}年{month}月的日历信息=====')
                print(' 一   二  三  四   五  六  日 ')
                print('*' * 28)
                # 实现日历信息的输出
                d = 1
                while d <= days:
                    # 循环周
                    for i in range(1, 8):
                        # 判断是否输出
                        if d > days or (i < w and d == 1):
                            print(' ' * 4, end="")
                        else:
                            print(' {:0>2d} '.format(d), end="")
                            d += 1
                    print()
                print('*' * 28)

            # 获取当前系统的年，月
            dd = time.localtime()
            year = dd.tm_year  # 获取年
            month = dd.tm_mon  # 获取月

            while True:
                # os.system('clear')
                # 默认输出当前年月的日历信息
                showdate(year, month)
                print('< 上一月   下一月 >')
                # 获取用户的输入
                c = input('请输入您的选择：')
                # 判断用户的输入内容
                if c == '<':
                    month -= 1
                    if month < 1:
                        month = 12
                        year -= 1
                elif c == '>':
                    month += 1
                    if month > 12:
                        month = 1
                        year += 1
                else:
                    print('输入内容错误，请重新输入')

    class chap_14_regular_expression():

        def __init__(self):
            pass

        def chap_14_1_knowing_regular_expression_operations(self):

            # 正则表达式

            '''
            正则表达式就是使用字符、转义字符、和特殊字符等组成的一个规则
            使用这个规则去对文本内容进行搜索、匹配和替换等功能

            正则表达式的组成
                普通字符： 大小写字符、数字、符合等
                转义字符： \w  \W \d \D \s \S
                特殊字符： . * ? + {} [] ()
                匹配模式： I U ...
            '''

            # 使用正则表达式进行匹配的基本语法
            import re

            # 定义要匹配的字符串
            varstr = 'iloveyou521to123si4567mida'

            # 定义规则 正则表达式
            # reg = '521'
            reg = '\d\d\d'  # \d 是转义字符  代表单个的数字
            # 调用正则表达式相关函数
            res = re.findall(pattern=reg, string=varstr)
            print(res)

            return None

        def chap_14_2_re_match_search(self):

            # re正则模块的相关函数

            '''
            re.match() 函数
                从头开始匹配，如果第一个就符合要求，那么匹配成功，
                如果第一个不符合规则，返回None
                匹配成功后返回Match对象，
                成功后可以使用group()和span()方法获取数据和下标区间

            re.search() 函数
                从字符串的开头开始进行搜索式的匹配
                匹配成功则返回Match对象，匹配失败则返回None
                成功后可以使用group()和span()方法获取数据和下标区间

            search和match方法的区别？
            # search: 从开始搜索到最后 (直到最后都没有pattern, 就返回 None)
            match: 从第一个字符串开始搜索（如果第一个不对，就返回 None)
            '''

            import re

            # 定义的字符串
            varstr = 'iloveyou521tosimiloveda'

            # 定义正则表达式
            reg = 'love'

            # 使用match函数 杰克曼，
            res = re.match(pattern=reg, string=varstr)
            print(res)  # 成功返回Match对象，失败返回None
            print(res.group())  # 获取匹配的数据
            print(res.span())  # 获取匹配的数据的下标区间

            # 使用search函数
            res = re.search(pattern=reg, string=varstr)
            print(res)
            print(res.group())
            print(res.span())

            return None

        def chap_14_3_findall_finditer_sub_split_compile(self):

            # re模块的常用函数

            '''
            re.match() 函数
                从头开始匹配，如果第一个就符合要求，那么匹配成功，
                如果第一个不符合规则，返回None
                匹配成功后返回Match对象，
                成功后可以使用group()和span()方法获取数据和下标区间

            re.search() 函数
                从字符串的开头开始进行搜索式的匹配
                匹配成功则返回Match对象，匹配失败则返回None
                成功后可以使用group()和span()方法获取数据和下标区间

            re.findall()

            re.finditer()

            re.sub()

            re.split()
            '''

            import re

            # 定义的字符串
            varstr = 'iloveyou521tosimiloveda'

            # 定义正则表达式
            reg = 'love'

            # re.findall函数,按照正则表达式的规则去字符串中进行搜索匹配所有符合规则的元素，结果返回一个列表，如果没有找到，则返回空列表
            res = re.findall(pattern=reg, string=varstr)
            print(res)

            # re.finditer()函数 和findall是一样的搜索匹配规则，但是结果返回由Match对象组成的迭代器
            res = re.finditer(pattern=reg, string=varstr)
            for i in res:
                print(i.group())

            # re.sub() 搜索替换
            '''
            按照正则表达式的规则去搜索匹配要替换的字符串，完成字符串的替换
            pattern 正则表达式的规则，匹配需要被替换的字符
            repl：  替换后的字符
            string： 原始字符串
            '''
            res = re.sub(pattern=reg, repl='live', string=varstr)
            print(res)

            # re.split() 按照指定的正则规则，进行数据切割
            varstr = 'hello1my2name3is4chuange'
            res = re.split(pattern='\d', string=varstr)
            print(res)

            varstr = 'hello.my.name.is.chuange'
            res = re.split(pattern='\.', string=varstr)
            print(res)

            # compile() 可以直接将正则表达式定义为 正则对象，使用正则对象直接操作

            arr = [
                'i love 123 you',
                'i love 234 you',
                'i love 456 you',
                'i love 789 you',
            ]

            reg = '\d{3}'
            for i in arr:
                res = re.search(pattern=reg, string=i)
                print(res.group())

            reg = re.compile('\d{3}')
            for i in arr:
                res = reg.search(i).group()
                print(res)

            return None

        def chap_14_4(self):

            # 正则表达式的规则 - 重点
            '''
            普通字符串： 大小写字母、数字、符号。。。
            转义字符： \w \W \d \D ...
            特殊字符: . * + ? ^ $ () [] {}
            模式符号： I U
            '''

            import re

            # 定义字符串
            varstr = '@ _2i4l22oveyou'

            # 转义字符
            reg = '\w'  # 代表 单个 字母、数字、下划线
            reg = '\W'  # 代表 单个 非 字母、数字、下划线
            reg = '\d'  # 代表 单个的 数字
            reg = '\D'  # 代表 单个的 非数字
            reg = '\s'  # 代表 单个的 空白符或者制表符
            reg = '\S'  # 代表 单个的 非 空白符或者制表符
            reg = '\w\w\w\d\d'  # 转义字符的组合使用

            # 特殊字符（元字符） . * + ? {} [] ()   ^ $

            varstr = '_AAAalove123 @ _2ial2345oveyou'

            reg = '.'  # . 点 代表 单个的 任意字符，除了换行符之外
            reg = 'A*'  # * 代表匹配次数 任意次数
            reg = '\w+'  # + 代表匹配次数 至少匹配一次
            reg = '\W*'
            reg = '\w+?'  # 拒绝贪婪，就是前面的匹配规则只要达成即可
            reg = '\w*?'  # 拒绝贪婪，就是前面的匹配规则只要达成即可
            reg = '\w{8}'  # {}代表匹配次数，{8}一个数字时，代表必须要匹配的次数
            reg = '\w{2,4}'  # {2,4} 两个数字时，表示匹配的区间次数
            reg = '[a-zA-Z]'  # 代表取值的范围 a-zA-Z代表大小写字母 0-9代表数字
            reg = '[a-zA-Z0-9_]'  # 等同于 \w
            reg = '\w{2}(\d{4})\w'  # （） 代表子组，就是在整个匹配的结果中，再单独提取一份小括号的内容

            res = re.search(pattern=reg, string=varstr)
            print(res)
            print(res.groups())

            # 匹配手机号
            varstr = '17610195211'
            reg = '^1\d{10}$'  # ^ 代表开头   $ 代表结尾
            res = re.search(reg, varstr)
            print(res)

            # 正则的模式 re.I 代表不区分大小写
            varstr = 'iLOVEyou'

            reg = '[a-z]{5,10}'
            res = re.search(reg, varstr, re.I)
            print(res)

            '''
            练习题
            1。 定义一个正则表达式来验证邮箱是否正确

            2。 完善 手机号码的正则表达式

            3。 定义一个匹配IP的正则表达式 0-255   255.255.255.254
            '''

    class chap_15_module_and_package():

        def chap_15_1_import_module(self):
            # 在当前脚本中如果需要使用一些已经定义好的功能时，可以选择对应的模块，导入后使用

            # 例如使用系统模块 time
            import time
            print(time.time())

            # 例如使用自定义异常处理 模块
            import My

            # 使用模块中定义的类
            obj = My.MyException()
            print(obj)

            # 使用模块中的函数
            My.func()

            # 使用模块中定义的变量
            print(My.love)

            print(My.__name__)

            # 想使用模块中的内容时，除了导入模块，还可以在在指定模块中导入指定的内容
            from My import love  # 导入My模块中的love变量
            from My import love as lv  # 导入My模块中的love变量，起个别名
            print(love)
            print(lv)

            return None

        def chap_15_2_import_package_method(self):
            '''
            This function describes the various methods to import package
            :return:
            '''

            import sys
            path_location = './Data/kaikeba_python_basis_training/ch15/'
            sys.path.append(path_location)
            # from package import *

            from directory_tree import display_tree
            display_tree(path_location + '/package')

            # 如果需要使用包可以直接导入包

            # 1. 直接把包当作模块导入，可以用的内容是 __init__.py文件中定义的
            # 不推荐这种用法
            import package
            package.funcpa()

            # 2。 可以导入模块中的所有内容
            # 注意这个内容是由 __init__.py文件中定义的 __all__ 这个变量指定的模块
            # 好处是可以直接导入指定的所以模块，并且使用时，直接使用指定的模块名即可
            # from package import *
            # a.funca()
            # b.funcb()

            # 3。 导入指定包中的指定模块
            from package import a
            a.funca()

            # 4。从指定包的指定模块中导入指定的内容
            from package.b import funcb
            funcb()

            # 5。从指定包的子包中导入模块
            from package.ps import c
            c.funcc()

            # 6。 从指定包的子包的指定模块中导入指定内容
            from package.ps.d import funcd
            funcd()

            # 导入方式的分类 绝对导入，相对导入

            return None

        def chap_15_3_organize_package_and_module(self):
            # 导入方式的分类

            '''
            # 绝对导入
            # 绝对导入的方式会使用[搜索路径]去查找和导入指定的包或模块
            import 模块
            import 包
            import 包.模块
            from 模块 import 内容
            from 包 import 模块
            from 包.模块 import 内容


            # 相对导入 注意：相对导入只能在非主程序的模块中使用，不需要直接运行的模块文件
            from .包名/模块名 import 模块/内容
            from ..包名/模块名 import 模块/内容

            . 代表当前
            ..代表上一级
            '''

            # 导入c模块。c模块中使用了相对导入
            from package.ps import c

            # 了解 搜索路径
            # 在导入模块或包时，程序查找的路径
            '''
            主要的搜索路径
            1. 当前导入模块的程序所在的文件
            2. python的扩展目录中 C:/Users/username/AppData/local/.../Python37/lib
            3. python解释器指定的其它 第三方模块位置 /lib/sitepackages
            '''
            # 在当前脚本中查看 包或模块 的 搜索路径
            import sys
            print(sys.path)
            '''
            [   
                '', 
                '/Library/Frameworks/Python.framework/Versions/3.7/lib/python37.zip', 
                '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7', 
                '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/lib-dynload',
                '/Library/Frameworks/Python.framework/Versions/3.7/lib/python3.7/site-packages'
             ]
            '''

            # 可以自己定义一个路径，加入到搜索路径中
            sys.path.append('/Users/yc/Desktop')

            return None

    class chap_17_errors_exception_log():

        def __init__(self):
            pass

        def chap_17_1_define_error(self):

            # 什么是异常

            '''
            什么是异常？

            异常简单理解，就是非正常，没有达到预期目标。
            异常是一个事件，并且这个异常事件在我们程序员的运行过程中出现，会影响我们程序正常执行。

            异常分两种：
                1. 语法错误导致的异常
                2. 逻辑错误导致的异常

            在程序无法正常运行处理时，就会出现一个异常，在python中异常是一个对象，表示一个错误。
            例如：获取一个不存在的索引

            Traceback (most recent call last):
              File "/Users/yc/Desktop/code/1.异常处理-什么是异常？.py", line 12, in <module>
                print(varlist[3])
            IndexError: list index out of range


            IndexError  异常类
            list index out of range  异常信息

            '''
            varlist = [1, 2, 3]
            print(varlist[3])

            '''
            如何处理异常？

            1. 如果错误发生的情况是可以预知的，那么就可以使用流程控制进行预防处理
                比如： 两个数字的运算，其中一个不是数字，运算就会出错。这时可以去判断来预防
            '''
            n2 = 3
            if isinstance(n2, int):
                res = 10 + n2
                print(res)

            '''
            2. 如果错误的发生条件不可预知，就可以使用 try。。。except。。 在错误发生时进行处理
            语法：
            try:
                可能发生异常错误的代码
            except:
                如果发生异常则进入 except 代码块进行处理
            '''

            # 假设读取的文件不存在，会发生错误，可以使用两种方式进行处理，
            # 1。可以在文件读取前先判断当前的文件是否存在
            # 2。也可以使用try 。。。 except。。在错误发生时进行处理
            # 注意：try。。except。。是在错误发生后进行的处理。和if有着根本性的区别。
            try:
                with open('./user.txt', 'r') as fp:
                    res = fp.read()
                print(res)
            except:
                print('文件不存在')

            print('程序的继续执行。。。')

            return None

        def chap_17_2_try_except_details(self):

            # try。。except 详细用法

            # 1。使用try。。except 处理指定的异常。如果引发了非指定的异常，则无法处理
            try:
                s1 = 'hello'
                int(s1)  # 会引发 ValueError
            except ValueError as e:
                # except IndexError as e:  #如果引发了非指定的异常，则无法处理
                print(e)

            # 2。 多分支处理异常类.不同的异常会走向不同的except处理
            s1 = 'hello'
            try:
                # int(s1) # ValueError
                s1[5]  # IndexError
            except IndexError as e:
                print('IndexError', e)
            except KeyError as e:
                print('KeyError', e)
            except ValueError as e:
                print('ValueError', e)

            # 3。通用异常类 Exception
            s1 = 'world'
            try:
                int(s1)
            except Exception as e:
                print('Exception ===', e)

            # 4. 多分支异常类+通用异常类.这样引发异常后会按照从上往下的顺序去执行对应的异常处理类。
            s1 = 'hello'
            try:
                # int(s1) # ValueError
                s1[5]  # IndexError
            except IndexError as e:
                print('IndexError', e)
            except KeyError as e:
                print('KeyError', e)
            except ValueError as e:
                print('ValueError', e)
            except Exception as e:
                print('Exception', e)

            # 5。 try...except...else...
            s1 = 'hello'
            try:
                # int(s1)
                s1[0]
            except IndexError as e:
                print('IndexError', e)
            except ValueError as e:
                print('ValueError', e)
            except Exception as e:
                print('Exception', e)
            else:
                print('try代码块中没有引发异常时，执行')

            print('如果上面的代码有异常并且进行了处理，那么后面的代码将继续执行')

            # 6。try...except..else..finally
            # finally 无论是否引发异常，都会执行。通常情况下用于执行一些清理工作。
            s1 = 'hello'
            try:
                int(s1)
                # s1[0]
                print('如果前面的代码引发了异常，这个代码块将不在继续执行。。')
            except IndexError as e:
                print('IndexError', e)
            except ValueError as e:
                print('ValueError', e)
            except Exception as e:
                print('Exception', e)
            else:
                print('try代码块中没有引发异常时，执行')
            finally:
                print('无论是否引发了异常，都会执行这个代码块')

            print('如果上面的代码有异常并且进行了处理，那么后面的代码将继续执行')

            # 7。使用 raise ，主动抛出异常
            try:
                # 可以使用 raise 主动抛出异常，并设置异常信息
                raise Exception('发生错误')
            except Exception as e:
                print('Exception', e)

            # 8。 assert 断言 （主要用于调试）
            assert 1 == 1  # 如果后面的表达式正确，则什么也不做
            assert 2 == 1  # 如果后面的表达式错误，则直接抛出 AssertionError

            return None

        def chap_17_3_exception_log(self):

            # 自定义异常处理类

            '''
            在出现异常后，对异常进行处理。并且把异常信息写入日志
            日志的基本格式：
                日期时间 异常的级别
                异常信息：引发的异常类，异常的信息，文件及行号。。
            '''
            import traceback
            import logging

            int('aa')

            try:
                int('aa')
            except:
                # 通过 traceback 模块获取异常信息
                errormsg = traceback.format_exc()
                print(errormsg)

            # 自定义异常日志处理类
            class Myexception():
                def __init__(self):
                    import traceback
                    import logging

                    # logging的基本配置
                    logging.basicConfig(
                        filename='./error.log',  # 日志存储的文件及目录
                        format='%(asctime)s  %(levelname)s \n %(message)s',  # 格式化存储的日志格式
                        datefmt='%Y-%m-%d %H:%M:%S'
                    )
                    # 写入日志
                    logging.error(traceback.format_exc())

            # 使用自定义异常处理类
            try:
                int('bb')
            except:
                print('在此处进行异常的处理')
                Myexception()  # 在异常处理的代码块中去调用自定义异常类

            print('abcdef')

            return None


class main_codewars_com():
    class kyu_4():

        def snail_sort(self):

            '''
            Snail Sort
            Given an n x n array, return the array elements arranged from outermost elements to the middle element,
            traveling clockwise.

            array = [[1,2,3],
                     [4,5,6],
                     [7,8,9]]
            snail(array) #=> [1,2,3,6,9,8,7,4,5]
            For better understanding, please follow the numbers of the next array consecutively:

            array = [[1,2,3],
                     [8,9,4],
                     [7,6,5]]
            snail(array) #=> [1,2,3,4,5,6,7,8,9]
            This image will illustrate things more clearly:


            NOTE: The idea is not sort the elements from the lowest value to the highest; the idea is to
            traverse the 2-d array in a clockwise snailshell pattern.

            NOTE 2: The 0x0 (empty matrix) is represented as en empty array inside an array [[]].

            '''

            array = [[1, 2, 3],
                     [4, 5, 6],
                     [7, 8, 9]]

            array = [[1, 2, 3],
                     [8, 9, 4],
                     [7, 6, 5]]

            array = [[1, 4, 1, 9, 1, 6],
                     [1, 2, 8, 9, 9, 0],
                     [8, 6, 3, 7, 7, 3],
                     [1, 7, 5, 8, 7, 1],
                     [0, 7, 1, 6, 9, 8],
                     [9, 5, 9, 8, 9, 4]]

            def snail(snail_map):

                import numpy as np

                if snail_map == list():
                    return [[]]

                array = np.array(snail_map)
                i, j = 0, 0
                rows, cols = np.shape(array)
                num = rows * cols
                array_status = np.zeros(np.shape(array))  # Store the status if the element has been checked before

                output = []  # Store the results

                array_status[i, j] == 1  # First element is the first output
                output.append(array[i, j])  # Insert the first element into the output

                path_direction = 'lr'  # lr, rl, ud, bu -> left right, right left, up down, bottom up

                for run in range(1, num):

                    # Generate all the possible next paths
                    next_ij = [[i, j + 1], [i + 1, j], [i, j - 1], [i - 1, j]]

                    # The next location should fall within the matrix size bound
                    next_ij = [[k, l] for k, l in next_ij if (k >= 0 and l >= 0) and (k <= rows - 1 and l <= cols - 1)]

                    for k, l in next_ij:
                        if array_status[k, l] == 0:  # If the next possible path has not been explored before
                            i, j = k, l  # Mark the location of the current element
                            array_status[k, l] = 1  # Mark the path as explored
                            output.append(array[k, l])  # Output the result
                            break

                return output

            print(snail(array))

            return None

    class kyu_5():

        def __init__(self):
            pass

        def Moving_Zeros_To_The_End(array):

            '''

            Write an algorithm that takes an array and moves all of the zeros to the end,
            preserving the order of the other elements.

            move_zeros([1, 0, 1, 2, 0, 1, 3]) # returns [1, 1, 2, 1, 3, 0, 0]

            '''

            return [i for i in array if i != 0] + [0] * array.count(0)

        def human_readable_time(self):

            '''

            Write a function, which takes a non-negative integer (seconds) as input and returns the time in a human-readable format (HH:MM:SS)

            HH = hours, padded to 2 digits, range: 00 - 99
            MM = minutes, padded to 2 digits, range: 00 - 59
            SS = seconds, padded to 2 digits, range: 00 - 59
            The maximum time never exceeds 359999 (99:59:59)

            '''

            seconds = 12348

            def make_readable(seconds):
                import math

                # Calculate minutes
                minutes = math.floor(seconds / 60)

                # Calculate hours
                hours = math.floor(minutes / 60)

                minutes_left = minutes - hours * 60

                seconds_left = seconds - minutes * 60

                # Return in 2 digits (leading zero padding)
                return f'{hours:02}:{minutes_left:02}:{seconds_left:02}'

            def make_readable_best(s):
                return '{:02}:{:02}:{:02}'.format(int(s / 3600), int(s / 60) % 60, s % 60)

            print(make_readable(seconds))

            return None

        def rgb_to_hex_conversion(self):

            '''

            The rgb function is incomplete. Complete it so that passing in RGB decimal values
            will result in a hexadecimal representation being returned. Valid decimal values for RGB are 0 - 255.
            Any values that fall out of that range must be rounded to the closest valid value.

            Note: Your answer should always be 6 characters long, the shorthand with 3 will not work here.

            The following are examples of expected output values:

            rgb(255, 255, 255) # returns FFFFFF
            rgb(255, 255, 300) # returns FFFFFF
            rgb(0,0,0) # returns 000000
            rgb(148, 0, 211) # returns 9400D3

            '''

            r, g, b = 255, 255, 255
            r, g, b = 255, 255, 300
            r, g, b = 0, 0, 0
            r, g, b = 148, 0, 211

            def rgb(r, g, b):

                # Ensure rgb values fall in between [0, 255]
                r, g, b = max(r, 0), max(g, 0), max(b, 0)
                r, g, b = min(r, 255), min(g, 255), min(b, 255)

                # Convert rgb values into hex, upper case, and two digits
                r_hex = hex(r)[2:].upper().zfill(2)
                g_hex = hex(g)[2:].upper().zfill(2)
                b_hex = hex(b)[2:].upper().zfill(2)

                # Concatenate into a single string
                output = f'{r_hex}{g_hex}{b_hex}'

                return output

            def rgb_2nd_way(r, g, b):

                # Ensure rgb values fall in between [0, 255]
                r, g, b = min(max(r, 0), 255), min(max(g, 0), 255), min(max(b, 0), 255)

                # Concatenate into a single string
                output = f'{r:02X}{g:02X}{b:02X}'

                return output

            def rgb_best(r, g, b):

                round = lambda x: min(255, max(x, 0))

                return ("{:02X}" * 3).format(round(r), round(g), round(b))

            def limit(num):
                if num < 0:
                    return 0
                if num > 255:
                    return 255
                return num

            def rgb_best2(r, g, b):
                return "{:02X}{:02X}{:02X}".format(limit(r), limit(g), limit(b))

            print(rgb(r, g, b))
            print(rgb_best(r, g, b))
            print(rgb_best2(r, g, b))

        def extract_domain_name_from_url(self):

            '''
            Write a function that when given a URL as a string, parses out just the domain name
            and returns it as a string. For example:

            domain_name("http://github.com/carbonfive/raygun") == "github"
            domain_name("http://www.zombie-bites.com") == "zombie-bites"
            domain_name("https://www.cnet.com") == "cnet"

            '''

            # url = "http://github.com/carbonfive/raygun"
            # url = "http://www.zombie-bites.com"
            # url = "https://www.cnet.com"
            url = "www.xakep.ru"

            # url = "https://www.codewars.com"
            # url = "http://google.co.jp"

            def domain_name(url):

                end_term = ['.ru', '.co.', '.com', '.net', '.org']
                front_term = ['www.', '://']

                text1 = []
                # Extract the front term
                for i in end_term:
                    if url.rfind(i) != -1:
                        text1 = url.split(i)[0]
                        break

                # Extract the middle term
                count = 0
                for i in front_term:
                    if text1.rfind(i) != -1:
                        text2 = text1.split(i)[1]
                        break
                    elif text1.rfind(i) == -1:
                        count = count + 1
                        if count == len(front_term):
                            text2 = text1

                return text2

            print(domain_name(url))

            return None

        def ROT_13_cryptography(self):

            '''

            ROT13 is a simple letter substitution cipher that replaces a letter with the letter 13 letters after
            it in the alphabet. ROT13 is an example of the Caesar cipher.

            Create a function that takes a string and returns the string ciphered with Rot13. If there are numbers
            or special characters included in the string, they should be returned as they are. Only letters from
            the latin/english alphabet should be shifted, like in the original Rot13 "implementation".

            test.assert_equals(rot13("test"),"grfg")
            test.assert_equals(rot13("Test"),"Grfg")

            '''

            message = 'test'

            # message = 'Test'

            def rot13(message):
                # Define the letters
                letters = "abcdefghijklmnopqrstuvwxyz"

                # Create dictionary to store the relevant alphabets' location
                dict_numbers = {i: n for i, n in enumerate(letters)}
                dict_letters = {n: i for i, n in enumerate(letters)}

                # Convert message to lower
                message2 = message.lower()

                # Convert the ROT13 in number format
                ROT13_numbers = [(dict_letters[a] + 13) % 26 for a in message2]

                # Obtain the letters for the corresponding number
                ROT13 = [dict_numbers[num] for num in ROT13_numbers]

                # Convert to upper case if the original message is upper case
                ROT13 = [ROT13[i].upper() if j.isupper() is True else ROT13[i] for i, j in enumerate(message)]

                # Join all the letters in the message to a single string format
                ROT13 = ''.join(ROT13)

                return ROT13

            print(rot13(message))

    class kyu_6():

        def __init__(self):
            pass

        def counting_duplicates(self):

            '''
            Count the number of Duplicates
            Write a function that will return the count of distinct case-insensitive alphabetic characters and
            numeric digits that occur more than once in the input string. The input string can be assumed to
            contain only alphabets (both uppercase and lowercase) and numeric digits.

            Example
            "abcde" -> 0 # no characters repeats more than once
            "aabbcde" -> 2 # 'a' and 'b'
            "aabBcde" -> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
            "indivisibility" -> 1 # 'i' occurs six times
            "Indivisibilities" -> 2 # 'i' occurs seven times and 's' occurs twice
            "aA11" -> 2 # 'a' and '1'
            "ABBA" -> 2 # 'A' and 'B' each occur twice
            :return:
            '''

            def duplicate_count(text):

                text = 'abcAuike'
                text = 'abcdef'

                # Lower the cases of all letters
                text2 = text.lower()

                # Convert string type to set (unique)
                text_set = set(text2)

                # Create a set to store all the duplicates
                duplicates = set([i for i in text_set if text2.count(i) >= 2])

                if duplicates == set():
                    # if it is an empty set
                    output = 0
                else:
                    output = len(duplicates)

                print(f'Total number of {output} duplicates')

                return output

        def who_likes_it(self):

            '''

            You probably know the "like" system from Facebook and other pages. People can "like" blog posts,
            pictures or other items. We want to create the text that should be displayed next to such an item.

            Implement the function which takes an array containing the names of people that like an item.
            It must return the display text as shown in the examples:

            []                                -->  "no one likes this"
            ["Peter"]                         -->  "Peter likes this"
            ["Jacob", "Alex"]                 -->  "Jacob and Alex like this"
            ["Max", "John", "Mark"]           -->  "Max, John and Mark like this"
            ["Alex", "Jacob", "Mark", "Max"]  -->  "Alex, Jacob and 2 others like this"
            Note: For 4 or more names, the number in "and 2 others" simply increases.

            '''

            names = []

            # names = ["Peter"]
            # names = ["Jacob", "Alex"]
            # names = ["Max", "John", "Mark"]
            # names = ["Alex", "Jacob", "Mark", "Max"]

            def likes(names):

                if names == list():
                    return "no one likes this"

                if len(names) == 1:
                    return names[0] + " likes this"
                elif len(names) == 2:
                    return names[0] + ' and ' + names[1] + " like this"
                elif len(names) == 3:
                    return names[0] + ', ' + names[1] + ' and ' + names[2] + " like this"
                else:
                    return names[0] + ', ' + names[1] + ' and ' + str(len(names) - 2) + " others like this"

            print(likes(names))

            return None

        def detect_pangram(self):

            '''

            A pangram is a sentence that contains every single letter of the alphabet at least once.
            For example, the sentence "The quick brown fox jumps over the lazy dog" is a pangram,
            because it uses the letters A-Z at least once (case is irrelevant).

            Given a string, detect whether or not it is a pangram. Return True if it is, False if not.
            Ignore numbers and punctuation.

            '''

            import string

            s = "The quick brown fox jumps over the lazy dog"
            s = "The quick brown, ;k, fox jumps over the lazy dog"

            def is_pangram(s):
                # Lower the string
                s = s.lower()

                # Put it into set if it is alphabet (contains no digits and punctuations)
                s_set = set([i for i in s if i.isalpha()])

                # It is a pangram if have all alphabets (26)
                output = True if len(s_set) == 26 else False

                return output

            print(is_pangram(s))

    class kyu_7():

        def __init__(self):
            pass

        def highest_and_lowest(self):
            '''
            In this little assignment you are given a string of space separated numbers,
            and have to return the highest and lowest number.

            Examples:
            high_and_low("1 2 3 4 5")  # return "5 1"
            high_and_low("1 2 -3 4 5") # return "5 -3"
            high_and_low("1 9 3 4 -5") # return "9 -5"

            Notes:
            All numbers are valid Int32, no need to validate them.
            There will always be at least one number in the input string.
            Output string must be two numbers separated by a single space, and highest number is first.
            '''

            def high_and_low_my_answer(numbers):
                numbers = "1 2 3 4 5"

                # Split string into list without spacing
                numbers_list = numbers.split(' ')

                # Convert string list into integer type
                numbers_list_int = [int(i) for i in numbers_list]

                # Convert the maximimum and minimum list
                output = str(max(numbers_list_int)) + ' ' + str(min(numbers_list_int))

                return output

            def high_and_low_best_answer(numbers):
                nn = [int(s) for s in numbers.split(" ")]

                return "%i %i" % (max(nn), min(nn))

            return None

        def get_the_middle_character(self):
            '''
            You are going to be given a word. Your job is to return the middle character of the word.
            If the word's length is odd, return the middle character. If the word's length is even,
            return the middle 2 characters.

            #Examples:

            Kata.getMiddle("test") should return "es"

            Kata.getMiddle("testing") should return "t"

            Kata.getMiddle("middle") should return "dd"

            Kata.getMiddle("A") should return "A"
            #Input

            A word (string) of length 0 < str < 1000 (In javascript you may get slightly more than 1000
            in some test cases due to an error in the test cases). You do not need to test for this.
            This is only here to tell you that you do not need to worry about your solution timing out.

            #Output

            The middle character(s) of the word represented as a string.
            '''

            def get_middle_my_answer(s):
                # s = "test" # es
                # s = 'testing' # t
                # s = 'middle'  # dd
                s = 'A'  # A

                # Calcualte the length of the input string
                len_string = len(s)

                # location
                i, j = int((len_string - 1) / 2), int(len_string / 2 + 1)
                output = s[i:j]

                print(output)

                return output

            def get_middle_best_answer(s):
                return s[int((len(s) - 1) / 2):int(len(s) / 2 + 1)]


class main_100_plus_python_challenging_programming_exercise():

    def __init__(self):
        pass

    def question_1_int_between_2000_3200_string(self):

        '''

        Level 1

        Question:
        Write a program which will find all such numbers which are divisible by 7 but are not a multiple of
        5, between 2000 and 3200 (both included).
        The numbers obtained should be printed in a comma-separated sequence on a single line.

        Hints:
        Consider use range(#begin, #end) method

        '''

        def func():
            # Output the list of integers divisible by 7 but not 5
            output = [str(i) for i in range(2000, 3201) if (i % 7 == 0 and i % 5 != 0)]

            # Join the string with ',' in between
            output = ','.join(output)

            return output

        print(func())

    def question_2_factorial(self):

        '''
        Level 1

        Question:
        Write a program which can compute the factorial of a given numbers.
        The results should be printed in a comma-separated sequence on a single line.
        Suppose the following input is supplied to the program:
        8
        Then, the output should be:
        40320

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        '''

        def factorial(num):
            a = 1
            for i in range(1, num + 1):
                a = a * i

            return a

        import time

        start = time.perf_counter()
        print(factorial(10000))
        end = time.perf_counter()
        print(end - start)

        return None

    def question_3_create_dictionary(self):

        '''
        Level 1

        Question:
        With a given integral number n, write a program to generate a dictionary that contains (i, i*i) such
        that is an integral number between 1 and n (both included). and then the program should print
        the dictionary.
        Suppose the following input is supplied to the program:
        8
        Then, the output should be:
        {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        Consider use dict()
        '''

        def func(num):
            # output = {i: i*i for i in range(1, num + 1)}  # method1
            output = dict([[i, i * i] for i in range(1, num + 1)])  # method2

            return output

        print(func(12))

        return None

    def question_4_produce_list_tuple(self):

        '''

        Level 1

        Question:
        Write a program which accepts a sequence of comma-separated numbers from console and
        generate a list and a tuple which contains every number.
        Suppose the following input is supplied to the program:
        34,67,55,33,12,98
        Then, the output should be:
        ['34', '67', '55', '33', '12', '98']
        ('34', '67', '55', '33', '12', '98')

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        tuple() method can convert list to tuple

        '''

        def func(input):
            text_list = input.split(',')

            return text_list, tuple(text_list)

        input = '34,67,55,33,12,98'
        output_list, output_tuple = func(input)
        print(output_list, output_tuple)

        return None

    def question_5_create_class(self):

        '''

        Level 1

        Question:
        Define a class which has at least two methods:
        getString: to get a string from console input
        printString: to print the string in upper case.
        Also please include simple test function to test the class methods.

        Hints:
        Use init method to construct some parameters

        '''

        class func():

            def __init__(self, input):
                self.input = input

            def getString(self):
                return self.input

            def printString(self):
                return self.input.upper()

        model = func('input')

        print(model.getString())
        print(model.printString())

    def question_6_calculates_formula(self):

        '''

        Question 6

        Level 2
        Question:
        Write a program that calculates and prints the value according to the given formula:
        Q = Square root of [(2 * C * D)/H]
        Following are the fixed values of C and H:
        C is 50. H is 30.
        D is the variable whose values should be input to your program in a comma-separated sequence.
        Example
        Let us assume the following comma separated input sequence is given to the program:
        100,150,180
        The output of the program should be:
        18,22,24

        Hints:
        If the output received is in decimal form, it should be rounded off to its nearest value (for
        example, if the output received is 26.0, it should be printed as 26)
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        def calc(D):
            C = 50
            H = 30

            output = ([str(int(((2 * C * d) / H) ** (0.5))) for d in D])

            return ','.join(output)

        D = [100, 150, 180]

        print(calc(D))

        return None

    def question_7_list_matrix_size_rows_cols(self):

        '''
        Level 2

        Question:
        Write a program which takes 2 digits, X,Y as input and generates a 2-dimensional array. The
        element value in the i-th row and j-th column of the array should be i*j.
        Note: i=0,1.., X-1; j=0,1,¡Y-1.

        Example
        Suppose the following inputs are given to the program:
        3,5
        Then, the output of the program should be:
        [[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8]]

        Hints:
        Note: In case of input data being supplied to the question, it should be assumed to be a console
        input in a comma-separated form.
        '''

        def func(pair):
            # Initialize
            rows = pair[0]
            cols = pair[1]

            # method 1
            # output = np.zeros((rows, cols), dtype='uint8')

            # for i in range(rows):
            #     for j in range(cols):
            #         output[i, j] = i*j

            # method 2
            output = [[row * col for col in range(cols)] for row in range(rows)]

            return output

        output = func((3, 5))
        print(output)

    def question_8_sort_string(self):

        '''

        Level 2

        Question:
        Write a program that accepts a comma separated sequence of words as input and prints the
        words in a comma-separated sequence after sorting them alphabetically.
        Suppose the following input is supplied to the program:
        without,hello,bag,world
        Then, the output should be:
        bag,hello,without,world

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        '''

        text = 'without,hello,bag,world'

        def sort_text(text):
            # Split the input string
            text = text.split(',')

            # Sort the string
            text.sort()

            # Join the list as a single string
            text = ','.join(text)

            return text

        print(text)
        print(sort_text(text))

    def question_9_turn_to_uppercase(self):

        '''

        Level 2

        Question
        Write a program that accepts sequence of lines as input and prints the lines after making all
        characters in the sentence capitalized.
        Suppose the following input is supplied to the program:
        Hello world
        Practice makes perfect
        Then, the output should be:
        HELLO WORLD
        PRACTICE MAKES PERFECT

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = 'Hello world\nPractice makes perfect'

        def turn_uppercase(text):
            text = text.upper()

            return text

        print(text)
        print(turn_uppercase(text))

        return None

    def question_10_make_text_unique_and_sort(self):

        '''

        Level 2

        Question:
        Write a program that accepts a sequence of whitespace separated words as input and prints the
        words after removing all duplicate words and sorting them alphanumerically.
        Suppose the following input is supplied to the program:
        hello world and practice makes perfect and hello world again
        Then, the output should be:
        again and hello makes perfect practice world

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        We use set container to remove duplicated data automatically and then use sorted() to sort the
        data.

        '''

        text = 'hello world and practice makes perfect and hello world again'

        def make_unique_sort(text):
            # Split the input text based on spacing and make it a set (unique)
            text = set(text.split(' '))

            # Change to list type for sorting
            text = list(text)

            # Sort the text
            text.sort()

            # Join the list as string
            text = ' '.join(text)

            return text

        print(text)
        print(make_unique_sort(text))

        return None

    def question_11_select_binary_number_divisible_5(self):

        '''

        Level 2

        Question:
        Write a program which accepts a sequence of comma separated 4 digit binary numbers as its
        input and then check whether they are divisible by 5 or not. The numbers that are divisible by 5
        are to be printed in a comma separated sequence.
        Example:
        0100,0011,1010,1001
        Then the output should be:
        1010

        Notes: Assume the data is input by console.

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = '0100,0011,1010,1001,1111,0101,0111'

        def func(text):
            # split text with separator ','
            text = text.split(',')

            # select the binary number which is divisible by 5
            text = [i for i in text if int(i, 2) % 5 == 0]

            # rejoin the string with ',' separator
            output = ','.join(text)

            return output

        print(text)
        print(func(text))

    def question_12_output_divisible_by_2(self):

        '''

        Level 2

        Question:
        Write a program, which will find all such numbers between 1000 and 3000 (both included) such
        that each digit of the number is an even number.
        The numbers obtained should be printed in a comma-separated sequence on a single line.

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        def func():
            # Select the number that is divisible by 2 in the range [1000, 3000]
            # Output as a list of string
            output = [str(i) for i in range(1000, 3001) if i % 2 == 0]

            # Join the list of string as a single string separated by ','
            output = ','.join(output)

            return output

        print(func())

    def question_13_calculate_number_of_letters_digits(self):

        '''

        Level 2

        Question:
        Write a program that accepts a sentence and calculate the number of letters and digits.
        Suppose the following input is supplied to the program:
        hello world! 123
        Then, the output should be:
        LETTERS 10
        DIGITS 3

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = 'hello world! 123'

        def func(text):

            count_letters, count_digits = 0, 0

            for i in text:

                if i.isalpha() is True:
                    count_letters += 1

                elif i.isdigit() is True:
                    count_digits += 1

                else:
                    pass  # good habit to write pass for otherwise

            output = f'LETTERS {count_letters}\nDIGITS {count_digits}'

            return output

        print(text)
        print(func(text))

        return None

    def question_14_calculate_number_of_upper_case_and_lower_case(self):

        '''

        Level 2

        Question:
        Write a program that accepts a sentence and calculate the number of upper case letters and
        lower case letters.
        Suppose the following input is supplied to the program:
        Hello world!
        Then, the output should be:
        UPPER CASE 1
        LOWER CASE 9

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = 'Hello world!'

        def func(text):

            count_upper = 0
            count_lower = 0

            for i in text:

                if i.isupper() is True:
                    count_upper += 1

                elif i.islower() is True:
                    count_lower += 1

                else:
                    pass  # good habit to write pass for otherwise

            output = f'UPPER CASE {count_upper}\nLOWER CASE {count_lower}'

            return output

        print(text)
        print(func(text))

        return None

    def question_15_sum_list_of_numbers_with_pattern(self):

        '''

        Level 2

        Question:
        Write a program that computes the value of a+aa+aaa+aaaa with a given digit as the value of a.
        Suppose the following input is supplied to the program:
        9
        Then, the output should be:
        11106

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        num = 9

        def func(num):
            # Make a list of digit of the pattern [a aa aaa aaaa]
            list_num = [int(str(num) * i) for i in range(1, 5)]

            # Sum the list
            output = sum(list_num)

            return output

        print(num)
        print(func(num))

    def question_16_select_odd_number(self):

        '''

        Level 2

        Question:
        Use a list comprehension to square each odd number in a list. The list is input by a sequence of
        comma-separated numbers.
        Suppose the following input is supplied to the program:
        1,2,3,4,5,6,7,8,9
        Then, the output should be:
        1,3,5,7,9

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = '1,2,3,4,5,6,7,8,9'

        def func(text):
            # string of separator ',' split
            text = text.split(',')

            # string is selected if it is an odd number
            output = [i for i in text if int(i) % 2 == 1]

            # join the string with ',' separator
            output = ','.join(output)

            return output

        print(text)
        print(func(text))

        return None

    def question_17_sum_account_deposit_withdrawal(self):

        '''

        Level 2

        Question:
        Write a program that computes the net amount of a bank account based a transaction log from
        console input. The transaction log format is shown as following:
        D 100
        W 200
        D means deposit while W means withdrawal.
        Suppose the following input is supplied to the program:
        D 300
        D 300
        W 200
        D 100
        Then, the output should be:
        500

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = 'D 300\nD 300\nW 200\nD 100'

        def func(text):
            # Split the text with '\n' separator
            text_list = text.split('\n')

            # Assign + or - sign depending if it is 'D' or 'W'
            num_list = [int(i[1:]) if i[0] is 'D' else -int(i[1:]) for i in text_list]

            # Sum the list of numbers
            output = sum(num_list)

            return output

        print(text)
        print(func(text))

        return None

    def question_18_check_password_validity(self):

        '''

        Level 3

        Question:
        A website requires the users to input username and password to register. Write a program to
        check the validity of password input by users.
        Following are the criteria for checking the password:
        1. At least 1 letter between [a-z]
        2. At least 1 number between [0-9]
        3. At least 1 letter between [A-Z]
        4. At least 1 character from [$#@]
        5. Minimum length of transaction password: 6
        6. Maximum length of transaction password: 12
        Your program should accept a sequence of comma separated passwords and will check
        them according to the above criteria. Passwords that match the criteria are to be printed,
        each separated by a comma.

        Example
        If the following passwords are given as input to the program:
        ABd1234@1,a F1#,2w3E*,2We3345
        Then, the output of the program should be:
        ABd1234@1

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        '''

        passwords = ['ABd1234@1', 'a F1#', '2w3E*', '2We3345']

        def func(passwords):

            '''
                1. At least 1 letter between [a-z]
                2. At least 1 number between [0-9]
                3. At least 1 letter between [A-Z]
                4. At least 1 character from [$#@]
                5. Minimum length of transaction password: 6
                6. Maximum length of transaction password: 12

            '''

            output = []

            for password in passwords:

                validity = 1
                count_lower, count_digit, count_upper, count_special, count_len = 0, 0, 0, 0, 0

                for i in password:

                    if (i.islower() is True):
                        count_lower += 1

                    elif (i.isdigit() is True):
                        count_digit += 1

                    elif (i.isupper() is True):
                        count_upper += 1

                    elif (i in ['$', '#', '@']):
                        count_special += 1

                    else:
                        pass

                if (count_lower >= 1) and (count_digit >= 1) and (count_upper >= 1) and (count_special >= 1):
                    validity *= 1
                else:
                    validity *= 0

                if (len(password) >= 6) and (len(password) <= 12):
                    validity *= 1
                else:
                    validity *= 0

                if validity is 1:
                    output.append(password)

            return output

        def func_regularization_method(passwords):

            '''
            regularization expression method is much more neat
            '''

            import re

            output = list()

            for password in passwords:

                validity = (re.search('[a-z]', password) is not None) and \
                           (re.search('[0-9]', password) is not None) and \
                           (re.search('[A-Z]', password) is not None) and \
                           (re.search('[\$\#\@]', password) is not None) and \
                           (6 <= len(password) <= 12)

                if validity is True:
                    output.append(password)

            return output

        print(passwords)
        print(func(passwords))
        print(func_regularization_method(passwords))

        return None

    def question_19_sort_array_type(self):

        '''

        Question 19

        Level 3
        Question:
        You are required to write a program to sort the (name, age, height) tuples by ascending order
        where name is string, age and height are numbers. The tuples are input by console. The sort
        criteria is:
        1: Sort based on name;
        2: Then sort based on age;
        3: Then sort by score.
        The priority is that name > age > score.
        If the following tuples are given as input to the program:
        Tom,19,80
        John,20,90
        Jony,17,91
        Jony,17,93
        Json,21,85
        Then, the output of the program should be:
        [('John', '20', '90'), ('Jony', '17', '91'), ('Jony', '17', '93'), ('Json', '21', '85'), ('Tom', '19', '80')]

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        We use itemgetter to enable multiple sort keys.

        '''

        input_strings = [('Tom', '19', '80'), ('John', '20', '90'), ('Jony', '17', '91'),
                         ('Jony', '17', '93'), ('Json', '21', '85')]

        def func(input_strings):
            import numpy as np

            # Convert list type to numpy array type
            array = np.array(input_strings)

            # Sort with 0, 1, 2nd column order
            idx = np.lexsort((array[:, 2], array[:, 1], array[:, 0]))

            # With the indices found, sort
            output = array[idx, :]

            # Output in list of tuples format
            output = [(i, j, k) for i, j, k in output]

            return output

        print(input_strings)
        print(func(input_strings))

        return None

    def question_20_generator_divisible_by_7(self):

        '''

        Level 3

        Question:
        Define a class with a generator which can iterate the numbers, which are divisible by 7, between a
        given range 0 and n.

        Hints:
        Consider use yield

        '''

        n = 100

        def func(n):

            array = [i for i in range(0, n) if i % 7 == 0]

            for i in array:
                yield i

            # Not a good answer as you use for loops twice

        def func2(n):

            for i in range(0, n):

                if i % 7 == 0:
                    yield i

        # res = func(n)
        res = func2(n)

        while True:
            print(next(res))

    def question_21_robot_up_down_left_right(self):

        '''

        Level 3

        Question
        A robot moves in a plane starting from the original point (0,0). The robot can move toward UP,
        DOWN, LEFT and RIGHT with a given steps. The trace of robot movement is shown as the
        following:
        UP 5
        DOWN 3
        LEFT 3
        RIGHT 2
        ¡The
        numbers after the direction are steps. Please write a program to compute the distance from
        current position after a sequence of movement and original point. If the distance is a float, then
        just print the nearest integer.

        Example:
        If the following tuples are given as input to the program:
        UP 5
        DOWN 3
        LEFT 3
        RIGHT 2
        Then, the output of the program should be:
        2

        Hints:
        In case of input data being supplied to the question, it should be assumed to be a console input.
        '''

        movement = ['UP 5', 'DOWN 3', 'LEFT 3', 'RIGHT 2']

        def func(movement):

            import numpy as np

            point = [0, 0]

            for instruction in movement:

                text_list = instruction.split(' ')

                direction, step = text_list[0], int(text_list[1])

                if direction == 'UP':
                    point[1] += step
                elif direction == 'DOWN':
                    point[1] -= step
                elif direction == 'LEFT':
                    point[0] -= step
                elif direction == 'RIGHT':
                    point[0] += step
                else:
                    pass

            distance = int(np.round(np.sqrt(point[0] ** 2 + point[1] ** 2)))

            return distance

        print(func(movement))

    def question_22_frequency_of_words_in_string(self):

        '''

        Level 3

        Question:
        Write a program to compute the frequency of the words from the input. The output should
        output after sorting the key alphanumerically.
        Suppose the following input is supplied to the program:
        New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3.
        Then, the output should be:
        2:2
        3.:1
        3?:1
        New:1
        Python:5
        Read:1
        and:1
        between:1
        choosing:1
        or:2
        to:1

        Hints
        In case of input data being supplied to the question, it should be assumed to be a console input.

        '''

        text = 'New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3.'

        def func(text):
            # Split the text with separator ' '
            text_list = list(set(text.split(' ')))

            # Sort the text
            text_list.sort()

            # Organize the output in the format word:count
            output = [f'{i}:{text.count(i)}' for i in text_list]

            # Join the output with \n as request
            output = '\n'.join(output)

            return output

        print(text)
        print(func(text))

    def question_23_square_of_number(self):

        '''

        level 1

        Question:
        Write a method which can calculate square value of number

        Hints:
        Using the ** operator

        '''

        def square_number(input):
            return input ** 2

        return None

    def question_24_doc_of_function(self):

        '''

        Level 1

        Question:
        Python has many built-in functions, and if you do not know how to use it, you can read document
        online or find some books. But Python has a built-in document function for every built-in
        functions.
        Please write a program to print some Python built-in functions documents, such as abs(), int(),
        raw_input()
        And add document for your own function

        Hints:
        The built-in document method is doc

        '''

        print(abs.__doc__)
        print(int.__doc__)
        print(input.__doc__)

        def square(num):
            '''Return the square value of the input number.
            The input number must be integer.
            '''

            return num ** 2

        print(square(2))
        print(square.__doc__)

        return None

    def question_25_define_a_class(self):

        '''

        Level 1

        Question:
        Define a class, which have a class parameter and have a same instance parameter.

        Hints:
        Define a instance parameter, need add it in init method
        You can init a object with construct parameter or set the value later

        '''

        class Person:
            # Define the class parameter "name"
            name = "Person"

            def __init__(self, name=None):
                # self.name is the instance parameter
                self.name = name

        jeffrey = Person("Jeffrey")
        print("%s name is %s" % (Person.name, jeffrey.name))

        nico = Person()
        nico.name = "Nico"
        print("%s name is %s" % (Person.name, nico.name))

        return None

    def question_33_define_dictionary_square(self):

        '''

        Define a function which can print a dictionary where the keys are numbers
        between 1 and 3 (both included) and the values are square of keys.

        Hints:
        Use dict[key]=value pattern to put entry into a dictionary.
        Use ** operator to get power of a number.

        '''

        print({i: i ** 2 for i in range(1, 4)})

        return None

    def question_34_define_dictionary_1_to_20(self):

        '''

        Define a function which can print a dictionary where the keys are numbers between 1 and 20
        (both included) and the values are square of keys.

        Hints:
        Use dict[key]=value pattern to put entry into a dictionary.
        Use ** operator to get power of a number.
        Use range() for loops.

        '''

        print({i: i ** 2 for i in range(1, 21)})

        return None

    def question_35_36_output_dictionary_keys_values(self):

        '''

        Question 35

        Define a function which can generate a dictionary where the keys are numbers between 1 and 20
        (both included) and the values are square of keys. The function should just print the values only.

        Hints:
        Use dict[key]=value pattern to put entry into a dictionary.
        Use ** operator to get power of a number.
        Use range() for loops.
        Use keys() to iterate keys in the dictionary. Also we can use item() to get key/value pairs.

        '''

        def func_values():
            dictionary = {i: i ** 2 for i in range(1, 21)}

            return dictionary.values()

        def func_keys():
            dictionary = {i: i ** 2 for i in range(1, 21)}

            return dictionary.keys()

        print(func_values())

        print(func_keys())

        return None

    def question_37_list_of_numbers(self):

        '''

        Define a function which can generate and print a list where the values are square of numbers
        between 1 and 20 (both included).

        Hints:
        Use ** operator to get power of a number.
        Use range() for loops.
        Use list.append() to add values into a list.

        '''

        print([i for i in range(1, 21)])

        return None


class main_ai_embedded_systems_algorithm_optimization_and_implementation():
    class chap_2():
        '''
        嵌入式软件变成模式和优化
        '''

        def __init__(self):
            pass

        def chap_2_2_4(self):
            '''
            底层运算的快速实现算法
            '''

            # 化除为整

            def division(a=300, b=11, scaling=8):
                '''
                division a/b in a fast way
                assuming scaling = 8
                a/11 ~ (a*2^8/11) * (1/2^8) = a * (2^8/11) / (2^8)
                numerator = 2^8 / 11 = 0b10111 -> 0b10000 + 0b111 -> 0001 0000 + 0000 1000 - 0000 0001
                2^8 / 11 * a -> ((a << 4) + (a << 3) - a)
                denominator = 2^8 which is equivalent of >> 8
                2^8 / 11 * a / 2^8 -> ((a << 4) + (a << 3) - a) >> 8
                '''

                a = int(a)

                real_output = a / 11
                output = ((a << 4) + (a << 3) - a) >> 8

                print(f'real_output:{real_output}; calculated_output:{output}')

                return None

    class chap_4():
        '''
        数值的表示和运算
        '''

        def __init__(self):
            pass

        def main_double_to_fixed_point_vice_versa(self):
            number = 3.3  # Number to be converted to fixed point
            scale = 8  # 8 bits conversion

            # Convert to fixed point
            fixed = ai.fixed_point_arithmetic().double_to_fixed(number, scale)

            # From fixed point converted back to floating point
            float_convert = ai.fixed_point_arithmetic().fixed_to_double(fixed, scale)

            # Error incurred due to fixed point conversion
            error_fixed = (float_convert - number) / number * 100

            print(
                f'input:{number} scale:{scale}; fixed bin:{bin(fixed)}; fixed float:{fixed}; float_convert:{float_convert}; error %:{np.round(error_fixed * 1e4) / 1e4}')

            return None


class TOF_opnous:

    def __init__(self):
        pass

    class save_and_load_depth_ir:

        def __init__(self, folder, total_frames, tof_module_type, load_file='pickle'):

            # Write down the image width, height and row_start for different TOF module
            if tof_module_type == '3030E':
                self.img_height, self.img_width = 100, 100
                self.row_start = 1
                self.ir_sort_max_count = -5
            elif tof_module_type == '8132':
                self.img_height, self.img_width = 32, 40
                self.row_start = 0
                self.ir_sort_max_count = None
            elif tof_module_type == '8133':
                self.img_height, self.img_width = 64, 80
                self.row_start = 0
                self.ir_sort_max_count = None
            elif tof_module_type == '8708B':
                self.img_height, self.img_width = 240, 320
                self.row_start = 0
                self.ir_sort_max_count = -100

            # main folder that contain the depth and ir folders
            self.main_data_folder = folder

            # Identify the list of filenames inside the depth folder
            self.depth_folder = os.path.join(self.main_data_folder, 'depth')
            self.depth_filenames_list = natsorted(glob.glob(os.path.join(self.depth_folder, '*.csv')))

            # Identify the list of filenames inside the ir folder
            self.ir_folder = os.path.join(self.main_data_folder, 'ir')
            self.ir_filenames_list = natsorted(glob.glob(os.path.join(self.ir_folder, '*.csv')))

            # Total frames
            self.total_frames = total_frames

            if load_file == 'pickle':

                # CSV files were loaded before and saved as pickle file
                # Load the saved pickle file
                self.load_pickle()

            elif load_file == 'csv':
                # Read the depth and ir csv files and write as pickle file
                self.save_csv_to_pickle()

            else:
                pass

        def save_csv_to_pickle(self):

            # Initialize the depth_list and ir_list to store the depth and ir matrices
            self.depth_list = np.zeros((self.total_frames, self.img_height, self.img_width))
            self.ir_list = np.zeros((self.total_frames, self.img_height, self.img_width))

            # Loop through all the frames
            for i in range(self.total_frames):

                # Print the depth and ir filename
                print(os.path.basename(self.depth_filenames_list[i]), os.path.basename(self.ir_filenames_list[i]))

                # Read csv files
                ir = np.genfromtxt(self.ir_filenames_list[i], delimiter=',').astype(np.float32)
                depth = np.genfromtxt(self.depth_filenames_list[i], delimiter=',').astype(np.float32)

                # Extract the ir and depth image to the list
                self.ir_list[i, :, :] = ir[self.row_start:, :]
                self.depth_list[i, :, :] = depth[self.row_start:, :]

                # Save as pickle file after the last frame is read
                if i == self.total_frames - 1:
                    saved_folder = os.path.join(self.main_data_folder, 'data.p')
                    print(f'Pickle file saved in {saved_folder}')
                    pickle.dump([self.ir_list, self.depth_list], open(saved_folder, 'wb'))

        def load_pickle(self):

            load_folder = os.path.join(self.main_data_folder, 'data.p')
            print(f'Pickle file loaded from {load_folder}')
            self.ir_list, self.depth_list = pickle.load(open(load_folder, "rb"))

    def save_i_image(self, img, i):

        print(f'Image number {i}')
        cv2.imwrite(f'img_{i:04d}.jpg', img)

if __name__ == '__main__':

    ##################################################################################################################

    # 睿慕课 - 3D感知与实践

    # main_class = main_ruimuke_3D_processing()

    # main_class.lesson_3().main_voxelize_pc()
    # main_class.lesson_3().main_generate_ply()
    # main_class.lesson_4().main_bilateral_filter_depth()
    # main_class.lesson_4().main_pc_smooth()
    # main_class.lesson_4().main_filter_pc_noise_removal_through_find_neighbors()
    # main_class.lesson_4().main_filter_depth_noise_removal()

    ##################################################################################################################

    # Online Udemy Course - PyTorch for Deep Learning with Python Bootcamp
    # main_class = main_pytorch_for_deep_learning_with_python_bootcamp_udemy_jose_portilla()

    # Run the main program
    # main_class.main_custom_images_cnn()
    # main_class.main_cifar10_cnn()
    # main_class.main_mnist_cnn()
    # main_class.main_mnist_cnn_cuda()
    # main_class.main_mnist_ann()
    # main_class.main_tabular_data()
    # main_class.main_dnn_iris_dataset()
    # main_class.main_dnn_linear_equation_fitting()
    # main_class.main_gpu_cuda_trial()

    ##################################################################################################################

    # 开课吧-人工智能基础能力培养010期-预修课
    # main_class = main_kaikeba_fundamental_of_AI_basis_training()

    # Run the main program
    # main_class.lesson_13_water_pouring()
    # main_class.lesson_14_linear_equation()
    # main_class.lesson_14_kmeans()
    # main_class.lesson_14_kmeans_china_map()
    # main_class.lesson_14_kmeans_news_title()
    # main_class.lesson_16_elements()
    # main_class.lesson_16_mnist_logistic_regression_digit_0_and_6()
    # main_class.lesson_17_knn_iris()

    ##################################################################################################################

    # 人工智能Python能力培养
    # main_class = main_kaikeba_python_basis_training()

    # main_class.chap_5_advanced_functions()
    # main_class.chap_12_file_operation().chap_12_4_read_file_functions()
    # main_class.chap_12_file_operation().chap_12_exercise_username_password()
    # main_class.chap_13_built_in_and_time_functions().chap_13_1_pickle_function()

    ##################################################################################################################

    # 人工智能Python能力培养
    # main_class = main_kaikeba_cv()

    # main_class.week7_tutorial()

    ##################################################################################################################

    main_class = ai.object_detection.face_detection.deep_learning.mtcnn_cks(thresholds=[0.8, 0.2, 0.7],
                                                                            min_face_size=13,
                                                                            factor=0.709,
                                                                            img_h=80, img_w=64,
                                                                            mode=2)
    main_class.main()

    ##################################################################################################################

    # Object Detection

    # Face detection algorithm main function
    # main_class = ai.object_detection.face_detection.traditional_method.haar_cascade_method()
    # main_class = ai.object_detection.face_detection.traditional_method.pico_method()
    # main_class = ai.object_detection.face_detection.deep_learning.mtcnn()
    # main_class = ai.object_detection.face_detection.deep_learning.facenet_pytorch_mtcnn()
    # main_class = ai.object_detection.human_pose.mediapipe_pose()
    # main_class = ai.object_detection.palm.mediapipe_palm()

    # main_class.webcam(selection='grayscale', resize=(320, 240), saved_video='yes')  # selection = 'rgb' or 'grayscale'
    # main_class.webcam()

    # img_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'face_examples', 'img0002.jpg')
    # main_class.image(img_path, selection='grayscale', resize=(100, 100), landmarks=False)

    # main_class.webcam(selection='grayscale', resize=(320, 240), saved_video='yes')  # selection = 'rgb' or 'grayscale'
    # main_class.webcam()

    # img_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'face_examples', 'img0002.jpg')
    # main_class.image(img_path, selection='grayscale', resize=(100, 100), landmarks=False)

    # csv_path = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8132', 'face_001.csv')
    # ir = np.genfromtxt(csv_path, delimiter=',').astype(np.float32)
    # main_class.TOF_opnous(ir)

    # QVGA data
    # folder = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '8708B', 'dev_752B21T02332')
    # param = TOF_opnous.save_and_load_depth_ir(folder, 1000, '8708B', 'pickle')

    # Dragonfly data
    # folder = os.path.join('.', 'Data', 'object_detection', 'face_detection', 'TOF', '3030E', '42B25745')
    # param = TOF_opnous.save_and_load_depth_ir(folder, 1000, '3030E', 'pickle')

    # for i in range(param.total_frames):
    #     img = main_class.TOF_opnous(param.ir_list[i, :, :])
    #     TOF_opnous().save_i_image(img, i)

    # main_class.image(img_folder='./test_data1', img_format='jpg', saved_image='no')

    ##################################### fixed point arithmetic #######################################################

    # main = main_ai_embedded_systems_algorithm_optimization_and_implementation()

    # main.chap_4().main_double_to_fixed_point_vice_versa()