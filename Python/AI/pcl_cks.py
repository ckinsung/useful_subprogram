import numpy as np
# import pcl
import matplotlib.pyplot as plt
import glob
import cv2
import copy as copy
import math

class read_file():

    def __init__(self):
        return

    def rawread_perframe(self, file, row_size, col_size):

        '''
        :param file: raw file to be read
        :param row_size: total number of rows
        :param col_size: total number of columns
        :return:
        : depth: depth image (row_size * col_size * frame_no)
        : ir: grayscale image
        '''

        filename = open(file, 'rb')
        filedata = filename.read()
        len1 = int(len(filedata) / 2)
        pixeldata = np.zeros((len1))
        frame_no = int(len1 / row_size / col_size / 3)

        # print(np.size(pixeldata))

        for i in range(len1):
            pixeldata[i] = filedata[2 * i] * 16 + filedata[2 * i + 1] / 16

        finaldata = np.zeros((row_size * 3, col_size, frame_no))
        # print(np.size(finaldata))

        for i in range(frame_no):
            for j in range(row_size * 3):
                for k in range(col_size):
                    finaldata[j, k, i] = pixeldata[i * row_size * 3 * col_size + j * col_size + k]

        g1 = np.zeros((row_size, col_size, frame_no))
        g2 = np.zeros((row_size, col_size, frame_no))
        g3 = np.zeros((row_size, col_size, frame_no))

        for i in range(row_size):
            g1[i, :, :] = finaldata[i * 3, :, :]
            g2[i, :, :] = finaldata[i * 3 + 1, :, :]
            g3[i, :, :] = finaldata[i * 3 + 2, :, :]

        depth = g1 * 4 + (np.floor(g2).astype(int) >> 8) / 4
        ir = g3

        return depth, ir

class datastructure():

    def __init__(self):
        return

    def dep2pc(self, CAM_FX, CAM_CX, CAM_FY, CAM_CY, CAM_WID, CAM_HGT, z, format_pc="m_by_3"):
        # INPUTS: intrinsic parameters
        # FX: Focal length along x direction, FX = F / sx (sx: pixel size along x direction)
        # FY: Focal length along y direction, FY = F / sy (sy: pixel size along y direction)
        # CAM_CX: Center of image along x direction
        # CAM_CY: Center of image along y direction
        # CAM_WID: width of the image
        # CAM_HGT: height of the image
        # z: depth image with its values as z-values in Cartesian coordinates (not distance)
        # format_pc: choice of the return format (3 choices: "m_by_3", "m_by_n_by_3", "3_by_m_by_n")

        # OUTPUT: point cloud data (matrix of n * 3 dimension where n is the total number of points)

        # Define the u and v in meshgrid format. u is column and v is row.
        # 先定义 XYZ 矩阵
        [u, v] = np.meshgrid(range(CAM_WID), range(CAM_HGT))

        tab_x = (u - CAM_CX) / CAM_FX
        tab_y = (v - CAM_CY) / CAM_FY

        # Calculate the point cloud location
        pc_x = z * tab_x
        pc_y = z * tab_y
        pc_z = z

        if format_pc == "m_by_3":

            # Flatten from the matrix format to the column format
            pc_x2, pc_y2, pc_z2 = pc_x.flat, pc_y.flat, pc_z.flat

            # Output to the point cloud format in n * 3 form
            pc = np.vstack((pc_x2, pc_y2, pc_z2)).T

            # Calculate the number of vertices from the point cloud
            total_points = np.shape(pc)[0]

        elif format_pc == "3_by_m_by_n":

            pc = np.zeros((3, np.shape(z)[0], np.shape(z, 3)[1]))
            pc[0, :, :] = pc_x
            pc[1, :, :] = pc_y
            pc[2, :, :] = pc_z

            # Calculate the number of vertices from the point cloud
            total_points = np.shape(pc)[1] * np.shape(pc)[2]

        elif format_pc == "m_by_n_by_3":

            pc = np.zeros((np.shape(z)[0], np.shape(z)[1], 3))
            pc[:, :, 0] = pc_x
            pc[:, :, 1] = pc_y
            pc[:, :, 2] = pc_z

            # Calculate the number of vertices from the point cloud
            total_points = np.shape(pc)[0] * np.shape(pc)[1]

        return pc, total_points

    def dep2pc2(self, CAM_FX, CAM_CX, CAM_FY, CAM_CY, CAM_WID, CAM_HGT, z):
        # This is a bad function for conversion from depth to point cloud
        # as it uses for loop to calculate each element one by one

        # 内参矩阵
        intrinsic_matrix = np.array([[CAM_FX, 0, CAM_CX], [0, CAM_FY, CAM_CY], [0, 0, 1]])

        # 先定义 XYZ 矩阵
        [u, v] = np.meshgrid(range(CAM_WID), range(CAM_HGT))
        X = np.zeros(np.shape(u))
        Y = np.zeros(np.shape(v))
        Z = np.zeros(np.shape(z))

        # 计算像素坐标
        zu = z * u
        zv = z * v
        z1 = z * 1

        # 计算 inverse matrix
        inv_matrix = np.linalg.inv(intrinsic_matrix)

        # 根据每一个像素计算 XYZ 值
        for i in range(CAM_HGT):
            for j in range(CAM_WID):
                # Convert each pixel to a column vector
                pixel_coordinate = np.array([[zu[i, j]], [zv[i, j]], [z1[i, j]]])
                # inverse matrix multiply column vector (pixel coordinate) = physical coordinate
                # I know in Matlab I can skip the for loops 2 times but not sure how to do it in Python, need to revisit
                [X[i, j], Y[i, j], Z[i, j]] = inv_matrix.dot(pixel_coordinate)

        # 生成点云
        pc = np.vstack((X.flat, Y.flat, Z.flat)).T

        return pc

    def pc2dep(self, pc, CAM_FX, CAM_CX, CAM_FY, CAM_CY, CAM_WID, CAM_HGT, EPS=1e-16):

        '''
        # 点云图转深度图的函数
        :param pc: 点云
        :param CAM_FX:
        :param CAM_CX:
        :param CAM_FY:
        :param CAM_CY:
        :param CAM_WID:
        :param CAM_HGT:
        :param EPS:
        :return:
        img_z 输出深度图
        '''

        # 滤除镜头后方的点
        valid = pc[:, 2] > EPS
        z = pc[valid, 2]

        # 点云反向映射到像素坐标位置
        u = np.round(pc[valid, 0] * CAM_FX / z + CAM_CX).astype(int)
        v = np.round(pc[valid, 1] * CAM_FY / z + CAM_CY).astype(int)

        # 滤除超出图像尺寸的无效像素
        valid = np.bitwise_and(np.bitwise_and((u >= 0), (u < CAM_WID)),
                               np.bitwise_and((v >= 0), (v < CAM_HGT)))
        u, v, z = u[valid], v[valid], z[valid]

        # 按距离填充生成深度图，近距离覆盖远距离
        img_z = np.full((CAM_HGT, CAM_WID), np.inf)
        for ui, vi, zi in zip(u, v, z):
            img_z[vi, ui] = min(img_z[vi, ui], zi)  # 近距离像素屏蔽远距离像素

        # 小洞和“透射”消除
        img_z_shift = np.array([img_z, \
                                np.roll(img_z, 1, axis=0), \
                                np.roll(img_z, -1, axis=0), \
                                np.roll(img_z, 1, axis=1), \
                                np.roll(img_z, -1, axis=1)])

        img_z = np.min(img_z_shift, axis=0)

        return img_z

    def pc_to_voxel_array(self, pc, pstart=(0.0, 0.0, 0.0), pend=(0.0, 0.0, 0.0), grid_sz=0.5e-2):

        '''
        :param pc: 输入点云 点云数组(numpy 数组) 每一行对应一个点的X/Y/Z坐标
        :param pstart: 体像素空间对应的立方体顶点X/Y/Z坐标最小值
        :param pend: 体像素空间对应的立方体顶点X/Y/Z坐标最大值
        :param grid_sz: 间分割的小立方体边长
        :return:
        voxel_mat: Nx*Ny*Nz 大小 的 boolean 矩阵
        '''

        # Volume pixel divided by the number of three cubes
        # 体像素划分的空间的三个坐标轴对应小立方体数量
        num_x = int(math.ceil((pend[0] - pstart[0]) / grid_sz))
        num_y = int(math.ceil((pend[1] - pstart[1]) / grid_sz))
        num_z = int(math.ceil((pend[2] - pstart[2]) / grid_sz))

        # Point cloud coordinates converted to the coordinates of volume pixel elements
        # 点云坐标转换成体像素数组元素下标
        pc0 = pc - np.array(pstart)  # non-negative 得到点云相对与 pstart 位置的坐标

        # 点云坐标以立方体边长为单位量化，得到数组下标
        pc_q = np.round(pc0 / grid_sz).astype(int)

        # Filter out pixels outside the volume pixel space
        # 滤除体像素空间外的像素
        valid = (pc_q[:, 0] >= 0) * (pc_q[:, 0] < num_x) * (pc_q[:, 1] >= 0) * (pc_q[:, 1] < num_y) \
                * (pc_q[:, 2] >= 0) * (pc_q[:, 2] < num_z)
        pc_q = pc_q[valid, :]

        # Build a three-dimensional array to store volume pixels
        # 构建三位数组存放体像素
        voxel_mat = np.zeros((num_x, num_y, num_z), dtype=bool)

        # 设置体像素值
        for x, y, z in pc_q:
            voxel_mat[x, y, z] = True

        # Display voxel matrix
        # fig = plt.figure()
        # bx = fig.gca(projection='3d')
        # bx.voxels(voxel_mat, facecolors='r', edgecolors='k')
        # plt.show()

        return voxel_mat

    def triangulation_depth_image(self, CAM_WID, CAM_HGT):
        # Speed is slow due to the usage of for loop. Consider improving it.
        # This function converts the depth image based on simple triangulation
        # INPUT: width and height of the camera sensor
        # OUTPUT: faces as a result of triangulation
        # 深度图上的简易三角剖分 以深度图平面上的均匀采样格点直接做三角化
        # 三角形顶点的连接方案选择根据深度图上像素位置按下图所示连接
        # CH3-课件.pdf 的第57页

        # Define an empty matrix to store all the faces
        matrix = np.zeros(((CAM_WID - 1) * (CAM_HGT - 1) * 2, 4))
        k = 0

        # Loop through each row and column
        for i in range(CAM_HGT - 1):
            for j in range(CAM_WID - 1):
                # Obtain the lower triangles for each ith row and jth column
                arr_triangle1 = np.array([[0 + i, 1 + i, 1 + i], [0 + j, 0 + j, 1 + j]])
                # Obtain the upper triangles for each ith row and jth column
                arr_triangle2 = np.array([[0 + i, 1 + i, 0 + i], [0 + j, 1 + j, 1 + j]])

                # Convert the locations from subscripts to indices format
                ind_triangle1 = np.ravel_multi_index(arr_triangle1, (480, 640))
                ind_triangle2 = np.ravel_multi_index(arr_triangle2, (480, 640))

                # Stack the indices
                # When stack the 2 triangles together it becomes a rectangle
                ind_rectangle = np.vstack((ind_triangle1, ind_triangle2))

                # Add the 2*1 column vectors with number 3 as the elements
                # 3 means each triangle consists of 3 vertices
                ind_vertices = np.array([[3], [3]])

                # Combine the number of vertices information and the indices of the rectangle
                ind_rect_vertices = np.hstack((ind_vertices, ind_rectangle))

                # Insert the lower and upper triangles to the defined matrix
                matrix[k:k + 2, ] = ind_rect_vertices
                k = k + 2

        num_of_faces = np.shape(matrix)[0]

        return matrix, num_of_faces

    def gray_img_depth_img_to_pc_and_triangulation(self, img_gray, img_dep, CAM_FX, CAM_CX, CAM_FY, CAM_CY, CAM_WID,
                                                   CAM_HGT):
        # INPUTS:
        # img_gray: grayscale image
        # img_dep: depth image
        # intrinsic parameters: CAM_FX, CAM_CX, CAM_FY, CAM_CY
        # image dimension: image width and height (CAM_WID, CAM_HGT)

        # OUTPUTS:
        # vertices: point cloud data + RGB color that is converted successfully from depth image + gray image
        # num_of_vertices: total number of points in the pc
        # triangles: triangulation of the pc
        # num_of_faces: total number of faces

        # Normalize the [min max] of gray image to [0 255]
        # Turn the gray scale image to integer type
        im_gray_0_255 = 255 * (img_gray - np.min(img_gray)) / np.max(img_gray)
        im_gray_0_255_round = np.round(im_gray_0_255).astype(int)

        # Flatten the gray image to vector
        pc_color = im_gray_0_255_round.flat

        # Gray image does not have color
        # Make each RGB values to be the same as the grayscale value
        pc_rgb = np.tile(pc_color, (3, 1)).T

        # Convert the depth image to point cloud format
        pc, num_of_vertices = dep2pc(CAM_FX, CAM_CX, CAM_FY, CAM_CY, CAM_WID, CAM_HGT, img_dep)

        # Combine the point cloud matrix and its RGB color together
        vertices = np.hstack((pc, pc_rgb))

        # Obtain the triangulations based on the simple triangulation rule
        triangles, num_of_faces = triangulation_depth_image(CAM_WID, CAM_HGT)

        return pc, vertices, num_of_vertices, triangles, num_of_faces

    def writePLY(self, filename, comment_object, vertices, num_of_vertices, triangles, num_of_faces):
        # Inputs:
        # filename: PLY filename to be saved
        # comment_object: comment on the object
        # vertices: point cloud
        # vertices[0:3, :] 为点云数据
        # vertices[3:6, :] 为点云RGB 数据

        # num_of_vertices: total number of points
        # triangles: triangulation of the vertices
        # triangles[:, 0] 为多少个点为一个三角分割面
        # triangles[:, 1:4] 为三角为面的位置
        # num_of_faces: total number of faces

        # Output:
        # filename.ply

        f = open(filename, 'wt')
        f.write('ply\n')
        f.write('format ascii 1.0\n')
        f.write('comment author: Chan Kin Sung\n')
        f.write('comment object: ' + comment_object + '\n')
        f.write('element vertex ' + str(num_of_vertices) + '\n')
        f.write('property float x\n')
        f.write('property float y\n')
        f.write('property float z\n')
        f.write('property uchar red\n')
        f.write('property uchar green\n')
        f.write('property uchar blue\n')
        f.write('element face ' + str(num_of_faces) + '\n')
        f.write('property list uchar int vertex_index\n')
        f.write('end_header\n')
        # f.write(str(vertices) + '\n')
        for i in range(num_of_vertices):
            f.write("%5.3f %5.3f %5.3f %i %i %i\n" % (vertices[i, 0], vertices[i, 1], vertices[i, 2],
                                                      vertices[i, 3], vertices[i, 4], vertices[i, 5]))
        for i in range(num_of_faces):
            f.write("%i %i %i %i\n" % (triangles[i, 0], triangles[i, 1], triangles[i, 2], triangles[i, 3]))
        f.close()


# class filter():
#
#     def __init__(self):
#         return
#
#     # This is a class for filtering/smoothing the depth image / point cloud
#
#     def filter_pc_noise_neighbors(self, pc, threshold, K):
#         # 这个自己写的代码和 pcl 自带的  pcl.make_statistical_outlier_filter
#         # 有些差异。
#         # 自己写的代码所耗的时间因为使用 for loop 的关系，也比较慢（慢10分之一）
#         # 建议使用  filter_pc_noise_neighbors2
#
#         # filter_pc_noise_neighbors 为半径过滤算法
#         # 搜索点云临近的 K 个点，计算距离的平均值和标准差
#         # 距离值大于 threshold 则判定为离群点，写成 NaN 值
#
#         # pc: 待过滤的点云数据 N*3 格式
#         # threshold: 阈值 / 门限，筛选离群点的条件
#         # K: 搜索临近的 K 个点云数量
#
#         import copy
#
#         # PCL数据对象
#         cloud = pcl.PointCloud()
#         cloud.from_array(pc.astype(np.float32))
#
#         # 复制输出点云数据为输入数据 pc
#         pc_inliers = copy.deepcopy(pc)
#         pc_outliers = copy.deepcopy(pc)
#
#         # 构建 kdtree 用于快速最近邻搜索
#         kdtree = cloud.make_kdtree_flann()
#
#         for i in range(cloud.size):
#             # 定义 pc_search 为每个点作为搜索
#             pc_search = pcl.PointCloud()
#             pc_search.from_array(np.array([cloud[i], ]).astype(np.float32))
#
#             # K近邻搜索并输出其距离值
#             [idx, dist] = kdtree.nearest_k_search_for_cloud(pc_search, K)
#
#             # 因为 kdtree.nearest_k_search_for_cloud 输出距离是2的平放，需要取平方根
#             dist2 = np.sqrt(dist)
#
#             # 把大于门限定义为 NaN 值
#             idx_removed = idx[dist2 >= threshold]
#             pc_inliers[idx_removed, :] = np.nan
#
#         # 去除所有的 NaN 值，输出有数值的点云数据
#         # 输出 inliers 和 outliers
#         nan_array = np.isnan(pc_inliers[:, 0])
#         not_nan_array = ~nan_array
#         pc_inliers = pc_inliers[not_nan_array, :]
#         pc_outliers = pc_outliers[nan_array, :]
#
#         return pc_inliers, pc_outliers
#
#     def filter_pc_noise_neighbors2(self, pc, sc, K):
#         # filter_pc_noise_neighbors2 为半径过滤算法
#         # 搜索点云临近的 K 个点，计算距离的平均值和标准差
#         # 距离值大于 threshold 则判定为离群点，写成 NaN 值
#
#         # pc: 待过滤的点云数据 N*3 格式
#         # sc: 阈值 / 门限，筛选离群点的条件 标准差的倍数, mean + sc*std
#         # K: 搜索临近的 K 个点云数量
#
#         import copy
#
#         # PCL数据对象
#         cloud = pcl.PointCloud()
#         cloud.from_array(pc.astype(np.float32))
#
#         # 构建 kdtree 用于快速最近邻搜索
#         # kdtree = cloud.make_kdtree_flann()
#
#         # pc: point could data
#         # K:  number of neighboring points to analyze for any given point
#         # threshold:   Any point with a mean distance larger than global will be considered outlier
#         # return: Statistical outlier filtered point cloud data,
#
#         # calculate statistical mean and its standard deviation
#         fil = cloud.make_statistical_outlier_filter()
#
#         # Set the number of points (k) to use for mean distance estimation
#         fil.set_mean_k(K)
#
#         # Set the standard deviation multiplier threshold
#         fil.set_std_dev_mul_thresh(sc)
#
#         # Set whether the indices should be returned, or all points except the indices
#         # True means output outliers
#         fil.set_negative(True)
#
#         # Apply the filter according to the previously set parameters and return a new pointcloud
#         cloud_outliers = fil.filter()
#
#         # False means output non-outliers
#         fil.set_negative(False)
#
#         # Output inliers
#         cloud_inliers = fil.filter()
#
#         # 输出 np.array 格式
#         pc_inliers = cloud_inliers.to_array()
#         pc_outliers = cloud_outliers.to_array()
#
#         return pc_inliers, pc_outliers
#
#     def filter_pc_noise_neighbors3(self, pc, threshold, K):
#         # 这个自己写的代码和 pcl 自带的  pcl.make_statistical_outlier_filter
#         # 有些差异。
#         # 自己写的代码所耗的时间因为使用 for loop 的关系，也比较慢（慢10分之一）
#         # 建议使用  filter_pc_noise_neighbors2
#
#         # filter_pc_noise_neighbors 为半径过滤算法
#         # 搜索点云临近的 K 个点，计算距离的平均值和标准差
#         # 距离值大于 threshold 则判定为离群点，写成 NaN 值
#
#         # pc: 待过滤的点云数据 N*3 格式
#         # threshold: 阈值 / 门限，筛选离群点的条件
#         # K: 搜索临近的 K 个点云数量
#
#         import copy
#
#         # PCL数据对象
#         cloud = pcl.PointCloud()
#         cloud.from_array(pc.astype(np.float32))
#
#         # 复制输出点云数据为输入数据 pc
#         pc_inliers = copy.deepcopy(pc)
#         pc_outliers = copy.deepcopy(pc)
#
#         # Predefine a variable to store k-dist information
#         dist_all = np.zeros(((np.shape(pc)[0]), 1))
#
#         kdtree = cloud.make_kdtree_flann()  # 构建 kdtree 用于快速最近邻搜索
#
#         # 找到方法不需要 for loop , 下次使用 ！！！ 改写以下代码
#         # [idx, dist] = kdtree.nearest_k_search_for_cloud(cloud, k) # input 是 cloud 不需要for loop cloud 里的每个点
#
#         for i in range(cloud.size):
#             # 定义 pc_search 为每个点作为搜索
#             pc_search = pcl.PointCloud()
#             pc_search.from_array(np.array([cloud[i], ]).astype(np.float32))
#
#             # K近邻搜索并输出其距离值
#             [idx, dist] = kdtree.nearest_k_search_for_cloud(pc_search, K)
#
#             # 因为 kdtree.nearest_k_search_for_cloud 输出距离是2的平放，需要取平方根
#             dist2 = np.sqrt(dist)
#
#             # 计算均值
#             mean_dist = np.mean(dist2)
#
#             # 把 k-mean 计算到的距离值储存在 dist_all 变量里
#             dist_all[i] = mean_dist
#
#         # 计算每一个点的平均距离值和标准差
#         mean_dist_all = np.mean(dist_all)
#         std_dist_all = np.std(dist_all)
#
#         # 如果 dist_i 的距离值大于门限，判定为离群点
#         idx_outliers = dist_all > threshold
#         idx_inliers = ~idx_outliers
#
#         # 输出结果
#         pc_inliers = pc_inliers[idx_inliers[:, 0], :]
#         pc_outliers = pc_outliers[idx_outliers[:, 0], :]
#
#         return pc_inliers, pc_outliers
#
#     def remove_outliers(self, depth, ir, ir_th, ir_grad_th, depth_grad_th, set_num):
#
#         '''
#         :param ir: This is the input grayscale image
#         :param ir_th: threshold set for the grayscale image
#         :param ir_grad_th: threshold set for the gradient of the grayscale image
#         :param depth_grad_th:  threshold set for the gradient of the depth image
#         :param set_num: Number to be set for matrix elements that meet the condition
#         :return:
#         depth: filtered depth image
#         '''
#
#         # Calculate the gradient of the depth image
#         depth_grad_x, depth_grad_y = np.gradient(depth)
#
#         # Calculate the gradient of the ir image
#         ir_grad_x, ir_grad_y = np.gradient(ir)
#
#         # Taking the absolute of the gradient
#         depth_grad = np.sqrt(depth_grad_x ** 2 + depth_grad_y ** 2)
#         ir_grad = np.sqrt(ir_grad_x ** 2 + ir_grad_x ** 2)
#
#         # Remove the low ir values
#         cond_ir_low = ir < ir_th
#         cond_ir = ir_grad > ir_grad_th
#         cond_depth = depth_grad > depth_grad_th
#
#         # Conditioning all the various criteria
#         cond = cond_depth | cond_ir | cond_ir_low
#
#         depth[cond] = set_num
#
#         return depth
#
#     def voxel_downsample(self, point_cloud, leaf_size):
#
#         '''
#         通过点云体素化来降采样
#         :param point_cloud: pointcloud of N*3 size
#         :param leaf_size: grid_size
#         :return:
#         filtered_points:  downsampled pointcloud of N*3 size
#         '''
#
#         filtered_points = []
#         # 计算边界点
#         x_min, y_min, z_min = np.amin(point_cloud, axis=0)  # 计算x y z 三个维度的最值
#         x_max, y_max, z_max = np.amax(point_cloud, axis=0)
#
#         # 计算 voxel grid维度
#         Dx = (x_max - x_min) // leaf_size + 1
#         Dy = (y_max - y_min) // leaf_size + 1
#         Dz = (z_max - z_min) // leaf_size + 1
#         print("Dx x Dy x Dz is {} x {} x {}".format(Dx, Dy, Dz))
#
#         # 计算每个点的voxel索引
#         h = list()  # h 为保存索引的列表
#         for i in range(len(point_cloud)):
#             hx = (point_cloud[i][0] - x_min) // leaf_size
#             hy = (point_cloud[i][1] - y_min) // leaf_size
#             hz = (point_cloud[i][2] - z_min) // leaf_size
#             # 当前点属于第几个像素块
#             h.append(hx + hy * Dx + hz * Dx * Dy)
#         h = np.array(h)
#
#         # 筛选点
#         h_indice = np.argsort(h)  # 返回h里面的元素按从小到大排序的索引
#         h_sorted = h[h_indice]
#         begin = 0
#         for i in range(len(h_sorted) - 1):
#             if h_sorted[i] == h_sorted[i + 1]:
#                 continue
#             else:
#                 # 体素块中心均值点
#                 point_idx = h_indice[begin: i + 1]
#                 filtered_points.append(np.mean(point_cloud[point_idx], axis=0))
#                 begin = i
#
#         # 返回下采样后点云
#         filtered_points = np.array(filtered_points, dtype=np.float64)
#
#         return filtered_points


class linear_movement():  #

    def __init__(self):
        return

    # This is a class for rotation and translational movement of point cloud

    def rot_mat_on_col_vec(self, ax=0, ay=0, az=0):

        #  旋转矩阵施加在列向量
        # 需要右乘 （教科书的矩阵乘积方式）
        # Example: R_col_vec = rotation.rot_mat_on_col_vec(ax=0, ay=np.pi/2, az=0)

        Rx = np.array([[1, 0, 0], [0, np.cos(ax), -np.sin(ax)], [0, np.sin(ax), np.cos(ax)]])

        Ry = np.array([[np.cos(ay), 0, np.sin(ay)], [0, 1, 0], [-np.sin(ay), 0, np.cos(ay)]])

        Rz = np.array([[np.cos(az), -np.sin(az), 0], [np.sin(az), np.cos(az), 0], [0, 0, 1]])

        R = np.dot(Rz, np.dot(Ry, Rx))

        return R

    def rot_mat_on_row_vec(self, ax=0, ay=0, az=0):

        #  旋转矩阵施加在横向量
        # 需要左乘
        # Example: R_row_vec = rotation.rot_mat_on_row_vec(ax=0, ay=np.pi/2, az=0)

        Rx = np.array([[1, 0, 0], [0, np.cos(ax), np.sin(ax)], [0, -np.sin(ax), np.cos(ax)]])

        Ry = np.array([[np.cos(ay), 0, -np.sin(ay)], [0, 1, 0], [np.sin(ay), 0, np.cos(ay)]])

        Rz = np.array([[np.cos(az), np.sin(az), 0], [-np.sin(az), np.cos(az), 0], [0, 0, 1]])

        R = np.dot(np.dot(Rx, Ry), Rz)

        return R

    def rotated_output(self, pc_nn, ax, ay, az, multiply_left_right='Right'):

        # 其实两个都是同样的结果
        # 只是一个是右乘积，一个是左乘积
        # 注意：算法会根据输入的角度 ax, ay, az
        # 先在x-轴旋转ax角度，在y-轴旋转ay角度后才在z-轴旋转az角度
        # 注意 Rx, Ry 和 Rz 不是 commutative 不满足交换律
        # 如果需要先旋转方向有异于 x->y->z, 建议旋转个别处理
        # 举例：
        # 先 rotated_output(np.random.rand(100,3), ax = 0  , ay = 0.1, az = 0)
        # 后 rotated_output(np.random.rand(100,3), ax = 0.1, ay = 0  , az = 0)
        # Example:
        # pc_rotated = rotation.rotated_output(np.random.rand(100,3), ax = 0.3, ay = 0.1, az = -0.3, multiply_left_right = 'Left')

        if multiply_left_right == 'Right':

            R_col_vec = linear_movement().rot_mat_on_col_vec(ax, ay, az)
            pc_output = np.dot(R_col_vec, pc_nn.T).T  # 右乘积

        elif multiply_left_right == 'Left':

            R_row_vec = linear_movement().rot_mat_on_row_vec(ax, ay, az)
            pc_output = np.dot(pc_nn, R_row_vec)  # 左乘积

        return pc_output

    def unit_vector_to_angle(self, u):

        '''
        :param u: unit vector
        :return:
        alpha: angle between the projected u on xy-plane on x-axis
        beta: angle between the u vector on z-axis
        '''

        # Rotational angle between the projected u on the xy plane and the x-axis
        alpha = np.arctan2(u[1], u[0])

        # Rotational angle between the u vector and the z-axis
        beta = np.arctan2(np.sqrt(u[0] ** 2 + u[1] ** 2), u[2])

        return alpha, beta

    def rot_pc_pca(self, pc, direction='x', u_choice_along_z='min', u_choice_along_x='max'):

        if u_choice_along_z == u_choice_along_x:
            raise Exception("It is not allowed to have alignment two eigenvectors of the same direction")

        # calculate the center position of the point cloud
        pc_mean = np.mean(pc, axis=0)

        # pca analysis to obtain the 3 eigenvectors of point cloud
        [u_min, u_middle, u_max] = pc_shape_analysis().pca(pc)

        # select which eigenvector to align along z-direction
        if u_choice_along_z == 'min':
            u = u_min
        elif u_choice_along_z == 'middle':
            u = u_middle
        elif u_choice_along_z == 'max':
            u = u_max

        # calculate the angle of the interested eigenvector
        [alpha_z, beta_z] = linear_movement().unit_vector_to_angle(u)

        # rotate the point cloud along z-axis
        pc_rot1 = linear_movement().rotated_output(pc - pc_mean, 0, 0, -alpha_z)

        # rotate the point cloud along the y-axis
        pc_rot2 = linear_movement().rotated_output(pc_rot1, 0, np.pi - beta_z, 0)

        # calculate eigenvector of rotated point cloud (pc_rot2) to align along x or y axis
        [u_min, u_middle, u_max] = pc_shape_analysis().pca(pc_rot2)

        if direction == 'y':
            offset = 0
        elif direction == 'x':
            offset = np.pi / 2

        # calculate the eigenvector of the point cloud after aligned along z-direction
        [alpha_xy, beta] = linear_movement().unit_vector_to_angle(u_middle)

        # rotate the point cloud such that its u_middle along 'x' or 'y' direction
        pc_rot3 = linear_movement().rotated_output(pc_rot2, 0, 0, offset - alpha_xy)

        return pc_rot3, pc_mean, alpha_z, beta_z, alpha_xy, offset

    def rot_based_on_pca_angle(self, pc, pc_mean, alpha_z, beta_z, alpha_xy, offset):

        '''
        This function used the alpha and beta angles (pca analysis) from the rot_pc_pca function
        and rotate a new point cloud pc

        So the various angles used (alpha_z, beta_z, and alpha_xy) has to obtain from the rot_pc_pca function
        :param pc: point cloud to be rotated
        :param pc_mean: rotated at this center of location
        :param alpha_z: rotate the pc along z-axis by alpha_z
        :param beta_z: rotate the pc along y-axis by beta_z
        :param alpha_xy: rotate again the pc along z-axis by alpha-xy
        :param offset: along the point cloud either along x or y axis (0 or pi/2 values)
        :return:
        :pc_rot3: point cloud that has already been rotated
        '''

        # rotate the point cloud along z-axis
        pc_rot1 = linear_movement().rotated_output(pc - pc_mean, 0, 0, -alpha_z)

        # rotate the point cloud along the y-axis
        pc_rot2 = linear_movement().rotated_output(pc_rot1, 0, np.pi - beta_z, 0)

        # rotate the point cloud such that its u_middle along 'x' or 'y' direction
        pc_rot3 = linear_movement().rotated_output(pc_rot2, 0, 0, offset - alpha_xy)

        return pc_rot3


class calibration():

    def __init__(self):
        return

    # This is a class for calibration

    def calibrate_checkboard(self, img_folder, checkerboard=(6, 9), extension='jpg'):

        '''
        :param img_folder:
        :param checkerboard:
        :return:
        mtx: intrinsic parameters of the form [[fx 0 cx],[0 fy cy],[0 0 1]]
        dist: distortion parameters of the form [k1, k2 p1 p2 k3]
        barrel distortion correction is of the form (1 + k1*r^2 + k2*r^4 + k3*r^6) for both x, y coordinates
        pincushion distortion correction is of the form (2*p1*x*y + p2*(r^2 + 2*x^2)) for x coordinate
        pincushion distortion correction is of the form (p1*(r^2 + 2y^2) + 2*p2*x*y) for y coordinate
        '''

        # Defining the dimensions of checkerboard
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        # Creating a vector to store the vectors of 3D points for each checkerboard image
        obj_points = []

        # Creating a vector to store the vectors of 2D points for each checkerboard image
        img_points = []

        # Defining the world coordinates for 3D points
        objp = np.zeros((1, checkerboard[0] * checkerboard[1], 3), np.float32)
        objp[0, :, :2] = np.mgrid[0:checkerboard[0], 0:checkerboard[1]].T.reshape(-1, 2)
        prev_img_shape = None

        # Extracting path of individual image stored in a given directory
        images = glob.glob('./' + img_folder + '/*.' + extension)

        for filename in images:

            # Read the image file
            img = cv2.imread(filename)

            # Convert from RGB image to gray image
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

            # 焦点粗检测
            # Find the chess board corners
            # If the desired number of corners are found in the image then ret = true
            ret, corners = cv2.findChessboardCorners(gray, checkerboard,
                                                     cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)

            # If desired number of corners are detected, we refine the pixel coordinates and display
            # them on the images of checker board
            if ret == True:
                obj_points.append(objp)

                # 焦点精检测
                # refining pixel coordinates for given 2d points.
                corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)

                img_points.append(corners2)

                # Draw and display the corners
                img = cv2.drawChessboardCorners(img, checkerboard, corners2, ret)

            # User can choose to display the images with its results
            # cv2.namedWindow('image', cv2.WINDOW_NORMAL)
            # cv2.resizeWindow('image', int(np.shape(gray)[0]/3), int(np.shape(gray)[1]/3))
            # cv2.imshow('image', img)
            # cv2.waitKey(0)

        # cv2.destroyAllWindows()

        # Performing camera calibration by passing the values of known 3D points (obj_points)
        # and corresponding pixel coordinates of the detected corners (img_points)
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, gray.shape[::-1], None, None)

        return ret, mtx, dist


class pc_shape_analysis():

    def __init__(self):
        return

    def pca(self, pc_nn):
        # 把点云堪称是一个3D椭圆体
        # PCA 分解来获取椭圆体的三个轴的方向

        # 用处
        # 1) 排列成直线的点云方向获取：最长轴方向
        # 2) 点云排列成平面时，计算平面法向量：最短轴方向

        # input: pc_nn （N*3)的点云数据
        # output: 三个轴的法向量

        # 点云去除均值
        pc_w = pc_nn - np.mean(pc_nn, axis=0)

        # 计算协方差矩阵
        # 这里没有加权重，如果以后有需要可以加权重 w (N * 1 列向量）
        # M = np.dot(pc_w.T, pc_w *w * ones(pc_w.shape[0], 3)) / np.sum(w)
        M = np.dot(pc_w.T, pc_w)

        # 寻找出特征值 和 特征向量
        E, F = np.linalg.eig(M)

        # 根据特征值的大小排序
        idx = np.argsort(E)

        # 输出的特征向量长度已经归一化为 1 了
        u_min = F[:, idx[0]].ravel()  # 法向量方向 （对应最小的特征值）
        u_middle = F[:, idx[1]].ravel()  # 平面方向 x （对应次小特征值）
        u_max = F[:, idx[2]].ravel()  # 平面方向 y （对应最大特征值）

        return u_min, u_middle, u_max

    def rotate_pc_along_z_direction(self, pc, u):

        '''
        :param pc: point cloud of N * 3 dimension
        :param u: 3-dimensional unit vector
        :return:
        '''

        # alpha is along xy plane, angle between the x-axis and the u vector
        alpha = np.arctan(u[1] / u[0])
        # beta is the tilted angle between the z-axis and the u vector
        beta = np.arctan(np.sqrt(u[0] ** 2 + u[1] ** 2) / u[2])

        # Put a negative sign as to revert the pc to the z-direction
        alpha = -alpha
        beta = -beta

        # Rotation about z-axis to parallel along x-direction using -alpha angle
        Rz = np.array([[np.cos(alpha), -np.sin(alpha), 0], [np.sin(alpha), np.cos(alpha), 0], [0, 0, 1]])

        # Rotation about y-axis to parallel along z-direction using -beta angle
        Ry = np.array([[np.cos(beta), 0, np.sin(beta)], [0, 1, 0], [-np.sin(beta), 0, np.cos(beta)]])

        pc2 = pc - np.mean(pc, axis=0)

        if np.shape(pc)[1] == 3:
            matrix = pc2.T
        else:
            matrix = pc2

        matrix2 = np.dot(Rz, matrix)
        matrix2 = np.dot(Ry, matrix2)

        pc_output = matrix2.T

        return pc_output

    def remove_non_ROI(self, pc, xlim, ylim, zlim, set_value=1):

        '''
        :param pc: input point cloud
        :param xlim: x-positions where the point cloud to be retained
        :param ylim: y-positions where the point cloud to be retained
        :param zlim: z-positions where the point cloud to be retained
        :return:
        pc：output point cloud with the corresponding ROI
        '''

        # remove x
        cond_x = (pc[:, 0] < xlim[1]) & (pc[:, 0] > xlim[0])

        # remove y
        cond_y = (pc[:, 1] < ylim[1]) & (pc[:, 1] > ylim[0])

        # remove z
        cond_z = (pc[:, 2] > zlim[0]) & (pc[:, 2] < zlim[1])

        cond = cond_x & cond_y & cond_z

        if set_value == 1:

            pc = pc[cond, :]

        elif set_value == 2:

            # There is no problem for computer running the following code but C code need to create a memory to store
            # the new variable due to removal of the points that meet the condition
            # So, it's better to make the pc[~cond, :] = some number
            # pc = pc[cond, :]
            pc[~cond, :] = 0

        return pc

    def pc_to_top_down_view(self, pc, ylim, zlim, step=100):

        '''
        This point cloud
        :param pc: input point cloud data
        :param ylim: y-direction extreme limit
        :param zlim: z-direction extreme limit
        :param step: step size for the partition of the matrix (default 10cm step size)
        :return:
        : matrix: output matrix with step size
        : counts: count of each matrix element
        '''

        # With step as the step size (10cm), calculate the number of points in between
        num_points_z = int((zlim[1] - zlim[0]) / step)
        num_points_y = int((ylim[1] - ylim[0]) / step)

        # Project 3D points to a 2D map
        # Typically np.zeros(15, 20)
        matrix = np.zeros((num_points_z, num_points_y))
        counts = np.zeros((num_points_z, num_points_y))

        for i in range(0, len(pc[:, 0])):

            x, y, z = pc[i, 0], pc[i, 1], pc[i, 2]

            if (x == 0) & (y == 0) & (z == 0):
                # This step is created because in remove_non_ROI function
                # pc[~cond, :] = 0 is used instead of pc = pc[cond, :]
                # to avoid create new memory for C program
                continue

            # Converting the y and z position into ith row and jth column
            # Also bringing the offset to the offset of zlim[0] and ylim[0]
            ith_row = int(np.floor((z - zlim[0]) / step))  # z-direction
            jth_col = int(np.floor((y - ylim[0]) / step))  # y-direction

            # make the zero position of ith_row to be the same parallel direction as the camera
            # y = mx + c -> typical: y = -x + 15 - 1
            # -1 is because the starting row is 0
            ith_row = -ith_row + num_points_z - 1

            if jth_col > num_points_y - 1:
                jth_col = num_points_y - 1

            if ith_row > num_points_z - 1:
                ith_row = num_points_z - 1

            # Only save the height (x) of the largest value
            if matrix[ith_row, jth_col] < x:
                # replace the corresponding matrix element with a larger x value (height)
                matrix[ith_row, jth_col] = x

            # With ith_row and jth_col (irrespective if x is higher or lower)
            # Add one count
            counts[ith_row, jth_col] = counts[ith_row, jth_col] + 1

        return matrix, counts

    def ROI_top_down_view(self, pc, xlim0, ylim0, zlim0, xlim, ylim, zlim, step=100):

        '''
        :param pc: point cloud
        :param xlim: limit the point cloud in xlim region
        :param ylim: limit the point cloud in ylim region
        :param zlim: limit the point cloud in zlim region
        :param step: step size, each element has size of 10 * 10 cm^2
        :return:
        matrix: top down view with the maximum height of the point cloud
        counts: the number of points along the x direction
        pc2: the point cloud that its ROI has been selected and used for geneating top down view matrix
        '''

        '''
        After the user define the region of interest, the floor function that round down the number in
        pc_to_top_down_view function will make the region outside of zlim[1] not usable, rendering matrix first row
        to have zero numbers
        Example: 1.52m in z direction, the remove_non_ROI will keep the number between <1.5m
        After floor function in  pc_to_top_down_view, 1.5 will be recorded
        '''

        # Remove the non region of interest
        pc2 = pc_shape_analysis().remove_non_ROI(pc, xlim0, ylim0, zlim0)

        # Change the pc to top-down view
        matrix, counts = pc_shape_analysis().pc_to_top_down_view(pc2, ylim, zlim, step)

        return matrix, counts, pc2


# class pc_fit():
#
#     # Below algorithms are from ch5
#
#     def __init__(self):
#         return
#
#     def line_det_pc(self, pc):
#
#         # 直线方程叙述方式
#         # p = d*t + m
#         # 在中心点 m 有一个 d 的法向量
#         # input: pc 点云
#         # output: d, m （法向量，中心位置）
#
#         # 点云的中心位置
#         m = np.mean(pc, axis=0)
#
#         # 点云去均值
#         pc0 = pc - m
#
#         # 协防差阵
#         C = np.dot(pc0.T, pc0)
#
#         # 寻找出特征值 和 特征向量
#         E, F = np.linalg.eig(C)
#
#         # 找出最大的特征值
#         idx = np.argmax(E)
#
#         # 取最大特征值的法向量
#         d = F[:, idx].ravel()
#
#         return d, m
#
#     def plane_det_pc(self, pc):
#
#         # 平面方程叙述方式
#         # pT . n = D
#         # 计算平面模型参数： <n, p> = D
#         # n: 平面法向量方向
#         # D: 原点到平面的距离 （沿着法向量方向，可能是负值）
#
#         # 平面上一点（点云中心)
#         m = np.mean(pc, axis=0)
#
#         # 点云去均值
#         pc0 = pc - m
#
#         # 协防差阵 Covariance matrix
#         C = np.dot(pc0.T, pc0)
#
#         # 寻找出特征值 和 特征向量
#         E, V = np.linalg.eig(C)
#
#         # 找出最小的特征值
#         idx = np.argmin(E)
#
#         # Smallest value of the normal vector
#         # 最小特征值的法向量
#         # 对应平面法线方向，归一化过了
#         n = V[:, idx].ravel()
#
#         # 平面上的点 m 在法向量上投影长度
#         D = np.sum(n * m)
#
#         return n, D
#
#     def calc_img_pc_norm(self, img_pc):
#
#         # 计算法向量，数据是以传感器图像素顺序排列的点云
#         # v = (a - c) cross (b - c)
#         # input: img_pc （三位数组排序方式）（深度图变成点云）（一定得是有序排列的输入数据）
#         # 这样能快速计算 法向量
#         # 缺点：对噪声敏感 （可以对 input 做平滑处理）
#         # 输入得是有序排列
#
#         # output: n
#         # n (m*n*3 的结构体）输出
#         # nx = n[:, :, 0] 是 x 方向的法向量；朝右 (right) 是+ve, 朝左 (left) 是-ve
#         # ny = n[:, :, 1] 是 y 方向的法向量；朝下 (down) 是+ve, 朝上 (up) 是-ve
#         # nz = n[:, :, 2] 是 z 方向的法向量；朝内 (into the page) 是+ve, 朝外(out of the page)是-ve
#
#         # 中间那个点减去第一个临近点 (a - c)
#         img_pc1 = img_pc - np.roll(img_pc, 1, axis=0)
#         # 中间那个点减去第二个临近点 (b - c)
#         img_pc2 = img_pc - np.roll(img_pc, 1, axis=1)
#
#         # 矢量叉乘 计算 v
#         img_norm = np.cross(img_pc1, img_pc2)
#
#         # 返回的法向量进行了归一化
#         return img_norm / np.linalg.norm(img_norm, axis=2)[:, :, np.newaxis]
#
#     def calc_pc_norm_knn(self, img_pc, k=64):
#
#         # 对没有排序的点云计算法向量（有序点云，使用 calc_imag_pc_norm 比较快）
#         # 速度比较慢，因为每一个点都需要计算特征值和特征向量
#         # 可以很好的排除离群点，计算的法向量比较正确和真实
#
#         def calc_pc_norm_eig():
#             # return the eigenvector from PCA analysis
#             m = np.mean(pc, axis=0)
#             pc0 = pc - m
#             C = np.dot(pc0.T, pc0)
#             E, V = np.linalg.eig(C)
#             idx = np.argmin(E)
#
#             # return the smallest eigenvalue's eigenvector
#             u_min = V[:, idx].ravel()
#
#             return u_min
#
#         pc = img_pc.reshape(-1, 3).astype(np.float32)
#
#         # 构建 kdtree 用于快速最近邻搜寻
#         cloud = pcl.PointCloud(pc)
#         kdtree = cloud.make_kdtree_flann()
#
#         # K近邻搜寻
#         print('KNN (PCL)')
#         [idx, dist] = kdtree.nearest_k_search_for_cloud(cloud, k)
#
#         # 对每个点根据近邻计算法向量方向
#         print('calc norm...')
#         img_norm = np.array([calc_pc_norm_eig(pc[i, :]) for i in idx])
#         img_norm.shape = img_pc.shape
#
#         # 法向量方向统一
#         # 会出现相反方向的两种法向量计算结果，在这里统一方向
#         img_norm[img_norm[:, :, 2] > 0, :] *= -1
#
#         return img_norm
#
#     def sphere_det_pc(self, pc):
#
#         ## 功能描述：
#         #   最小二乘法从球面点云获取球体的参数
#         # (x-cx)**2+(y-cy)**2+(z-cz)**2-R**2=0
#         # 输入：
#         #   pc:     待计算点坐标序列(每行对应一个点的X/Y/Z坐标)
#         # 输出：
#         #   球心坐标和半径
#
#         V = np.hstack((2 * pc, np.ones((pc.shape[0], 1))))
#         b = np.sum(pc ** 2, axis=1).reshape(-1, 1)
#         # w=inv(V'V)*V'b
#         w = np.dot(np.linalg.pinv(np.dot(V.T, V)), np.dot(V.T, b)).ravel()
#         r = (w[3] + np.sum(w[:3] ** 2)) ** 0.5
#
#         return w[:3], r
#
#     def pnt_to_vector_proj(self, v, d):
#
#         # 此函数计算点 v 在向量 d 的投影
#
#         v_hat = np.sum(v * d) * d / np.sum(d ** 2)
#
#         return v_hat
#
#     def pnt_to_line_proj(self, v, d, m=0):
#
#         # 此函数计算点 v 在直线上 (p = dt + m) 的投影
#         # 直线参数方程：p=dt+m
#         # m 是空间上的一个点
#         # d 则是一个法向量
#
#         v_hat = m + np.sum((v - m) * d) * d / np.sum(d ** 2)
#
#         return v_hat
#
#     def pnt_to_plane_proj(self, v, n, D):
#
#         ## 功能描述：
#         #   计算点v在平面上的投影坐标
#         # 平面方程是<p,n>=D
#
#         n0 = n / np.linalg.norm(n)  # 长度归一化
#
#         return v - n0 * (np.sum(n * v) - D)
#
#     def ring_det(self, pc):
#
#         ## 功能描述：
#         #   检测空间圆环
#         # 输入：
#         #   pc:     待计算点坐标序列(每行对应一个点的X/Y/Z坐标)
#         # 输出：
#         #   p：环心坐标
#         #   r：圆环半径和半径
#         #   n：圆环法向量方向
#
#         n, D = pc_fit.plane_det_pc(pc)  # n:平面法向量, D平面到原点距离
#         c, _ = pc_fit.sphere_det_pc(pc)  # c:球心
#
#         p = pc_fit.pnt_to_plane_proj(c, n, D)  # 球心在平面上的投影是环心
#         r = np.mean(np.linalg.norm(pc - p, axis=1))  # 半径
#
#         return p, r, n
#
#     def line_det_pc_ransac(self, pc, r=0.2, k=0.3, th=0.1, it=20, it1=3, verbose=True):
#
#         # ch5 pg41
#         # 线性 ransac 拟合
#         # 自动去除离群点然后重新拟合
#
#         # pc: 点云
#         # r: 点云自己尺寸 (相对总的点云尺寸的比例系数)
#         # k: 近邻数量门限 (相对总的点云尺寸的比例系数)
#         # th: 近邻距离门限
#
#         def pc_to_line_dist(d, m, pc):
#             # 计算 3D 空间点到直线的距离
#             # 输入:
#             # pc: 待计算点坐标序列 (向量)
#             # d, m: 直线参数模型: p = dt + m
#             # 输出:
#             # 每个点对应的距离
#             return np.linalg.norm(np.cross(pc - m, pc - m - d), axis=1) / np.linalg.norm(d)
#
#         N = pc.shape[0]
#
#         M, K = int(N * r), int(N * k)
#
#         idx = np.arange(N)
#
#         while it > 0:
#             if verbose: print('iteration', it)
#             it -= 1
#             np.random.shuffle(idx)
#             pc_sub = pc[idx[:M]]
#             d, m = pc_fit.line_det_pc(pc_sub)
#             dist = pc_to_line_dist(d, m, pc)
#
#             if np.sum(dist < th) > K:
#                 break
#
#         if it <= 0:
#             return (None, None)
#
#         if verbose:
#             print('matched')
#
#         while it1 > 0:
#             it1 -= 1
#             idx = dist < th
#             pc_sub = pc[idx]
#             d, m = pc_fit.line_det_pc(pc_sub)
#             dist = pc_to_line_dist(d, m, pc)
#
#         return d, m, idx, pc_sub
#
#     def pc_to_plane_dist(self, n, D, pc):
#
#         # 输入:
#         # pc: 待计距离的点云集合
#         # n, D: 平面模型参数 <n, p> = D
#         # 输出:
#         # 每个点对应的距离
#         # I did verify the formula is indeed correct
#
#         dist_to_plane = np.abs(np.sum(pc * n, axis=1) - D)
#
#         return dist_to_plane
#
#     def plane_det_pc_ransac(self, pc, r=0.2, k=0.4, th=0.06, it=100, it1=3, verbose=True):
#
#         # inputs:
#         # r: the percentage of points from total point to be selected for plane fitting in RANSAC
#         # k: percentage of points from total point that its distance from the plane is within threshold
#         # th: distance from the plane
#
#         N = pc.shape[0]
#         M, K = int(N * r), int(N * k)
#
#         idx = np.arange(N)
#
#         while it > 0:
#
#             if verbose: print('iteration', it)
#             it -= 1
#             np.random.shuffle(idx)
#             pc_sub = pc[idx[:M]]
#             n, D = pc_fit().plane_det_pc(pc_sub)
#             dist = pc_fit().pc_to_plane_dist(n, D, pc)
#
#             if np.sum(dist < th) > K:
#                 break
#
#         if it <= 0:
#             result = 'non-matched'
#             return (None, None, None, None, None, result)
#
#         if verbose:
#             print('matched')
#             result = 'matched'
#
#         while it1 > 0:
#             it1 -= 1
#             idx = dist < th
#             pc_sub = pc[idx]
#             n, D = pc_fit().plane_det_pc(pc_sub)
#             dist = pc_fit().pc_to_plane_dist(n, D, pc)
#
#         pc_non_sub = pc[~idx, :]
#
#         return n, D, idx, pc_sub, pc_non_sub, result
#
#     def model_det_pc_ransac(self, pc, dist_threshold=0.02, max_iter=100, model=pcl.SACMODEL_PLANE,
#                             algorithm=pcl.SAC_RANSAC):
#
#         # This function identify the plane from the pc and removes it
#         # inputs:
#         # pc: N*3 numpy array
#         # outputs:
#         # pc_plane: plane point cloud
#         # pc_plane_removed: pc with plane removed
#
#         # Convert pc from array format to cloud format
#         cloud = pcl.PointCloud()
#         cloud.from_array(pc)
#
#         # Calculate the surface normals for each point by fitting a plane to the nearest
#         # 50 neighbours to the candidate point
#         seg = cloud.make_segmenter_normals(ksearch=50)
#
#         # Do a little bit more optimisation once the plane has been fitted.
#         seg.set_optimize_coefficients(True)
#
#         # Fit a plane to the points
#         # pcl.SACMODEL_PLANE
#         # pcl.SACMODEL_LINE
#         # pcl.SACMODEL_CIRCLE2D
#         # pcl.SACMODEL_CIRCLE3D
#         # pcl.SACMODEL_SPHERE
#         # pcl.SACMODEL_CYLINDER
#         # pcl.SACMODEL_CONE
#         # pcl.SACMODEL_TORUS
#         # pcl.SACMODEL_PARALLEL_LINE
#         # pcl.SACMODEL_PERPENDICULAR_PLANE
#         # pcl.SACMODEL_PARALLEL_LINES
#         # pcl.SACMODEL_NORMAL_PLANE
#         # pcl.SACMODEL_NORMAL_SPHERE
#         # pcl.SACMODEL_PARALLEL_PLANE
#         # pcl.SACMODEL_NORMAL_PARALLEL_PLANE
#         # pcl.SACMODEL_STICK
#         seg.set_model_type(model)
#
#         # Use the various types of models
#         # pcl.SAC_RANSAC - RANdom SAmple Consensus
#         # pcl.SAC_LMEDS - Least Median of Squares
#         # pcl.SAC_MSAC - M-Estimator SAmple Consensus
#         # pcl.SAC_RRANSAC - Randomized RANSAC
#         # pcl.SAC_RMSAC - Randomized MSAC
#         # pcl.SAC_MLESAC - Maximum LikeLihood Estimation SAmple Consensus
#         # pcl.SAC_PROSAC - PROgressive SAmple Consensus
#         seg.set_method_type(algorithm)
#
#         seg.set_normal_distance_weight(0.05)
#
#         # Number of iterations for the RANSAC algorithm
#         seg.set_max_iterations(max_iter)
#
#         # The max distance from the fitted model a point can be for it to be an inlier
#         seg.set_distance_threshold(dist_threshold)
#
#         # Returns all the points that fit the model, and the parameters of the model
#         inliers, coefficients = seg.segment()
#
#         # Output the plane and without plane point cloud
#         pc_plane = cloud.extract(inliers, negative=False).to_array()
#         pc_plane_removed = cloud.extract(inliers, negative=True).to_array()
#
#         return pc_plane, pc_plane_removed, inliers, coefficients
#
#     def calc_img_pc_norm(self, img_pc):
#
#         # 此函数是计算法向量，数据是以传感器图像素顺序排列的点云
#         # 适合于无噪点点云数据
#         # 有噪点的点云数据，噪声导致法向量被淹没（可以考虑计算结果的法向量通过8*8的平滑滤波，降低噪声）
#         # ch5 pg26
#         # input: img_pc 是 m*n*3 format 的三位数组点云数据排列方式
#
#         # 点云数据 img_pc 按像素位置排列（计算方便）
#
#         # img_pc1_roll = np.roll(img_pc, (0, 1, 0), axis=(0, 1, 0))
#         # img_pc1_roll = np.roll(img_pc, (1, 0, 0), axis=(0, 0, 0)) is same as below
#         img_pc1_roll = np.roll(img_pc, 1, axis=0)
#         img_pc1 = img_pc - img_pc1_roll  # 中间点减去临近点
#
#         # img_pc2_roll = np.roll(img_pc, (0, 0, 1), axis=(0, 0, 1))
#         # img_pc2_roll = np.roll(img_pc, (0, 1, 0), axis=(0, 1, 0)) is same as below
#         img_pc2_roll = np.roll(img_pc, 1, axis=1)
#         img_pc2 = img_pc - img_pc2_roll  # 中间点减去临近点
#
#         # 注意，最外边法界数据去除不用
#         img_norm = np.cross(img_pc1, img_pc2)
#
#         # 返回的法向量进行了归一化
#         n = img_norm / np.linalg.norm(img_norm, axis=2)[:, :, np.newaxis]
#
#         return n
#
#     def icp(self, pc0, pc1, it=6, verbose=True):
#
#         # 点云匹配
#         # min ||(pc1 - m)*R0-(pc0-m0)||
#
#         import cv2
#
#         flann = cv2.FlannBasedMatcher(dict(algorithm=1, trees=5), dict(checks=50))
#
#         # 去均值
#         m0 = np.mean(pc0, axis=0)
#         m1 = np.mean(pc1, axis=0)
#         p = (pc0 - m0).astype(np.float32)
#         q = (pc1 - m1).astype(np.float32)
#
#         # 存放总的旋转矩阵
#         R0 = np.eye(3)
#
#         for i in range(it):
#             if True:
#                 # 对p的每一点，找到在q中对应点 (q中最近距离的点)
#                 # opencv 的搜索引擎
#                 q_match = np.array([q[m[0].trainIdx] for m in flann.knnMatch(p, q, k=1)])
#                 q_match -= np.mean(q_match, axis=0)
#
#                 # 计算旋转矩阵
#                 H = np.dot(q_match.T, p)
#                 u, _, vh = np.linalg.svd(H)
#                 R = np.dot(u, vh)  # 因为点是行向量作为坐标，所以和公式不一样
#
#                 # 调整点云位置
#                 q = np.dot(q, R)
#                 R0 = np.dot(R0, R)
#
#                 # if True:
#                 #     # 对q的每一点，找到在p中对应点 (p中最近距离的点)
#                 #     # opencv 的搜索引擎
#                 #     q_match = np.array([q[m[0].trainIdx] for m in flann.knnMatch(p, q, k=1)])
#                 #     q_match -= np.mean(q_match, axis=0)
#                 #
#                 #     # 计算旋转矩阵
#                 #     H = np.dot(q_match.T, p)
#                 #     u, _, vh = np.linalg.svd(H)
#                 #     R = np.linalg.inv(np.dot(u, vh))  # 注意取逆，这是因为需要作用在 q 点云上，而不是 p 点云
#                 #
#                 #     # 调整点云位置
#                 #     q = np.dot(q, R)
#                 #     R0 = np.dot(R0, R)
#
#                 if verbose:
#                     print('[%d] err:' % n, np.mean(np.abs(p - q)))
#
#         return R0, m0, m1
#
#     def pc_ext(self, pc, p0, d, k):
#
#         # 点云按法向量扩展
#
#         pcn = find_surf_dir_with_knn(pc, k, func_phi)
#
#         # 法向量方向修正，这里简化了问题，仅仅要求法向量背离给定的中心点 p0
#
#         pcn = np.array([n * np.sign(np.dot(n, p - p0)) for p, n in zip(pc, pcn)])
#
#         # 外点云
#         pc_out = pc + pcn * d
#
#         # 内点云
#         pc_in = pc - pcn * d
#
#         return pc_in, pc_out
#
#     def construct_SDF(self, pc, p0, d, k):
#
#         ## 构建 SDF
#         # pc 点云
#         # p0 点云内点中心 （用于确定法向量方向）
#         # d 点云沿法向量正负方向拓展的距离
#         # k 计算法向量的 knn 数量
#
#         # 点云按法向量方向拓展
#         pc_in, pc_out = pc_fit.pc_ext(pc, p0=P0, d=D, k=K)
#         pc_all = np.vstack((pc, pc_out, pc_in))
#
#         # 解线性方程计算 SDF
#         M = np.array([func_phi(np_lin.norm(pc_all[r] - pc_all, axis=1)) \
#                       for r in range(3 * N)])
#
#         # 解新型方程计算 SDF
#         M = np.array([func_phi(np_lin.norm(pc_all[r] - pc_all, axis=1)) \
#                       for r in range(3 * N)])
#
#         v = np.array([np.zeros(N),
#                       D * np.ones(N),
#                       -D * np.ones(N)]).ravel()
#
#         W = np.dot(np_lin.pinv(M), v).ravel()
#
#         return pc_all, w


class segmentation():

    def __init__(self):
        return

    def clustering(self, pc, tol=0.01, min_points=100, max_points=500):
        """
        This function performs clustering based on the minimum/maximum number of points of the clusters extracted
        Input parameters:

            cloud: Input cloud

            tol: tolerance (points within a radius of a sphere to be searched)
            (if it is too big, will enclose many clusters)
            tolerance 是设置 kdtree 的近邻搜索的搜索半径，
            从实验结果来看，tolerance 越大，检测到的范围也就越大；
            同时如果搜索半径取一个非常小的值，那么一个实际的对象就会被分割为多个聚类；
            如果将值设置得太高，那么多个对象就会被分割为一个聚类;

            min_points: minimal number of points to form a cluster
            max_points: maximal number of points that a cluster allows

        Output:
            cluster_indices: a list of list. Each element list contains the indices of the points that belongs to
                             the same cluster
        """

        pc = pc.astype(np.float32)

        cloud = pcl.PointCloud()
        cloud.from_array(pc)

        # Make the cloud a kdtree object
        tree = cloud.make_kdtree()

        # Create a cluster extraction object
        ec = cloud.make_EuclideanClusterExtraction()

        # Set tolerances for distance threshold
        # tol is the radius to be searched within a point
        ec.set_ClusterTolerance(tol)

        # Set minimum and maximum cluster size (in points)
        # NOTE: These are poor choices of clustering parameters
        # Your task is to experiment and find values that work for segmenting objects.
        ec.set_MinClusterSize(min_points)
        ec.set_MaxClusterSize(max_points)

        # Search the k-d tree for clusters
        ec.set_SearchMethod(tree)

        # Extract indices for each of the discovered clusters
        cluster_indices = ec.Extract()

        return cluster_indices


class plot_graph():

    def __init__(self):
        return

    def scatter_plot(self, pc, marker, axis_limit, azim=30, elev=90):
        '''
        :param pc: point cloud to be plot
        :param marker: marker colour and shape
        :return: graph plot
        '''

        plt.clf()
        ax = plt.figure(1).gca(projection='3d')
        ax.plot(pc[:, 0], pc[:, 1], pc[:, 2], marker, markersize=0.5)
        # plt.title('point cloud')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.set_xlim(axis_limit)
        ax.set_ylim(axis_limit)
        ax.set_zlim(axis_limit)
        ax.view_init(azim, elev)
        plt.show(block=True)

        return ax

    def scatter_plot_with_eigvectors(self, pc, u_min, u_middle, u_max):
        '''

        :param pc: point cloud to be plot
        :param u_min: eigenvector
        :param u_middle: eigenvector
        :param u_max: eigenvector
        :return: the graph
        '''

        pc_mean = np.mean(pc, axis=0)

        ax = plt.figure(1).gca(projection='3d')
        ax.plot(pc[:, 0], pc[:, 1], pc[:, 2], 'b.', markersize=0.5)
        ax.plot([0, u_min[0]] + pc_mean[0], [0, u_min[1]] + pc_mean[1], [0, u_min[2]] + pc_mean[2], color='red',
                alpha=0.8, lw=1)
        ax.plot([0, u_middle[0]] + pc_mean[0], [0, u_middle[1]] + pc_mean[1], [0, u_middle[2]] + pc_mean[2],
                color='red', alpha=0.8, lw=1)
        ax.plot([0, u_max[0]] + pc_mean[0], [0, u_max[1]] + pc_mean[1], [0, u_max[2]] + pc_mean[2], color='red',
                alpha=0.8, lw=1)
        plt.title('point cloud')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        # ax.set_xlim([-1, 1])
        # ax.set_ylim([-1, 1])
        # ax.set_zlim([-1, 1])
        # ax.view_init(azim=0, elev=90)
        plt.show(block=False)

        return None


class child_adult_analyze():

    def __init__(self):
        return

    def output_settings(self, data_type):

        # 以下参数是默认值

        # intrinsic parameters
        fx, fy = 67, 67
        cx, cy = 50, 50
        CAM_WID = 100
        CAM_HGT = 100
        resize = 1

        # extrinsic parameters
        installed_height = 1700
        angle = -48  # 俯视角向上/下->正数/负数 旋转为正确的 xyz 笛卡尔坐标  # VCSEL: -45; LED: -48
        # 与客户沟通的角度 = 90 + angle （法向量和墙面的角度）; 举例：90 - 48 = 42度法向量和墙面的夹角

        # 散点剔除的数量
        counts_threshold = 10  # VCSEL: 15; LED: 10 (LED 信号弱，得降低散点阈值）

        # Region of interest (点云剔除的 ROI 外的区域)
        xlim0 = [500, 1450]  # 从地面到顶部的高度
        ylim0 = [-1000, 1000]  # 左右两侧的距离
        zlim0 = [200, 1700]  # z 方向的距离

        # Region of interest (map_top_down 的 ROI 区域)
        xlim = [500, 1450]  # 从地面到顶部的高度
        ylim = [-1000, 1000]  # 左右两侧的距离
        zlim = [0, 1700]  # z 方向的距离
        step = 100  # 俯视图间隔

        # 单帧判断小孩的参数
        height_children_0 = 1200  # 高度阈值，小于这个阈值则判定为小孩  # VCSEL: 准确度高，所以使用1220； LED: 偏出5厘米，所以建议使用 1200
        height_children = height_children_0  # 动态设定高度阈值的变量
        step_height_children = 50  # 动态设定高度阈值的宽度
        children_threshold = (4, 2)  # (小孩的点的数量阈值, 小孩点里边成人的点的阈值)
        adult_threshold = 5  # 成人的点的数量阈值
        label_count_threshold = 3  # 每个标签最少要有多少个点才会导入评估

        # 时间滤波的输入参数
        step_up, step_down = +2, -1  # step_up 得是正整数, step_down 得是负整数
        threshold_tf = 8  # 客户反馈使用两秒更新，那 2 seconds * 2 step_up * 2帧率
        # actual formula is to set threshold_seconds = 2 -> threhsold_tf = threshold_seconds * step_up * frame_rate
        count_children, count_adult = 0, 0

        # 背景更新
        time = 100  # 背景更新时间
        frame_rate = 2  # 帧率
        bkg_avg_frame = time * frame_rate  # 背景更新帧数,
        threshold_bkg_dist = 200  # 背景和当下帧的距离差
        # 物体放进/拿出场景的速率 （举例：step_up_bkg, step_down_bkg = +1, +2，放进去的物体背景更新慢，取出来的物体背景跟新块)
        step_up_bkg, step_down_bkg = +1.0, +2.0  # step_up_bkg, step_down_bkg 得是正数
        # 初始化背景矩阵
        rows, cols = int((zlim[1] - zlim[0]) / step), int((ylim[1] - ylim[0]) / step)
        map_top_down_bkg, count_bkg_matrix = np.zeros((rows, cols), dtype=float), np.zeros((rows, cols), dtype=float)

        # 生成小孩误判区域表
        angle_blind = 55
        mask = child_adult_analyze().generate_blind_spot_region_children(angle_blind, rows, cols, ylim, zlim, step)

        # 填补洞的算法
        fill_holes = 'on'

        # 区域划分储存
        region_store_children = []
        region_store_adult = []
        region_size = 5

        if data_type == 'VCSEL':

            # .\data\csv\VCSEL 文件夹内的数据
            height_children = 1220
            counts_threshold = 15
            angle = -45

        elif data_type == 'LED':

            # .\data\csv\LED 文件夹内的数据
            height_children = 1200
            counts_threshold = 10
            angle = -48

        elif data_type == 'LED_MIDEA_20201127':

            # Use the following settings for LED2. After which, the boxes look upright and have exact distance.
            fx, fy = 70, 70
            cx, cy = 50, 50
            height_children = 1200
            counts_threshold = 10
            angle = -37

        elif data_type == 'LED_white_cover1':  # 美的提供的白色模块1; OPN600X_S3020_PD_v1.8
            fx, fy = 70, 70
            cx, cy = 50, 50
            height_children = 1200
            counts_threshold = 10
            angle = -40

        elif data_type == 'LED_white_cover2':  # 美的提供的白色模块2; OPN600X_S3020_PD_v1.8
            fx, fy = 70, 70
            cx, cy = 50, 50
            height_children = 1200
            counts_threshold = 10
            angle = -43

        elif data_type == 'LED_white_cover1_v2.3':  # 美的提供的白色模块1; OPN600X_S3020_PD_v2.3
            fx, fy = 70, 70
            cx, cy = 50, 50
            height_children_0, height_children = 1300, 1300
            counts_threshold = 10
            angle = -40
            installed_height = 1740
            adult_threshold = 3
            step_height_children = 25

        elif data_type == 'LED_white_cover5_v2.3':  # 美的提供的白色模块5; 硬件版本 v2.3
            fx, fy = 70, 70
            cx, cy = 47, 50
            height_children_0, height_children = 1300, 1300
            counts_threshold = 10
            angle = -40
            installed_height = 1740
            adult_threshold = 3
            step_height_children = 25

        return fx, fy, cx, cy, CAM_WID, CAM_HGT, resize, \
               installed_height, angle, \
               counts_threshold, \
               xlim0, ylim0, zlim0, \
               xlim, ylim, zlim, step, \
               height_children, height_children_0, step_height_children, children_threshold, adult_threshold, label_count_threshold, \
               step_up, step_down, threshold_tf, count_children, count_adult, \
               bkg_avg_frame, threshold_bkg_dist, step_up_bkg, step_down_bkg, map_top_down_bkg, count_bkg_matrix, \
               angle_blind, mask, \
               fill_holes, \
               region_store_adult, region_store_children, region_size

    def det_adult_child_single_frame(self, matrix, counts, height_children, children_threshold, adult_threshold, label_count_threshold):

        '''
        单帧判断成人/小孩的算法
        :param matrix: 俯视图
        :param counts: 俯视图的累计数
        :param height_children: 判断小孩的高度阈值
        :param children_threshold: 判断小孩点数量的阈值
        :param adult_threshold: 判断成人点数量的阈值
        :return:
        TF: 成人: 2; 小孩：1；不是成人/小孩: 0
        map_children: 小孩的二值图
        map_adult：不是小孩/成人的二值图
        '''

        from skimage import measure

        # 二值图（成人小孩都会在里边）
        map_all = matrix != 0

        # 区域标记 （类似 Matlab 的 bwlabel)
        label_image = measure.label(map_all, connectivity=1)

        # 找出标签的唯一值（从0开始到最大值）
        unique_labels = np.arange(0, np.max(label_image) + 1)

        # 初始化成人小孩的二值图
        map_children = np.zeros((np.shape(matrix)[0], np.shape(matrix)[1]), dtype=bool)
        map_adult = np.zeros((np.shape(matrix)[0], np.shape(matrix)[1]), dtype=bool)

        # 设定临时 TF 值，储存所有可能的结果
        TF_temp_children, TF_temp_adult = 0, 0

        # Loop 所有的标签，从1开始（因为0标签是背景）
        for ith_label in range(1, np.max(unique_labels) + 1):

            # 把当下标签的位置设定为 True 值
            condition = label_image == ith_label

            # 输出相关标签的所有距离值
            height_label = matrix[condition]

            # 计算大于小孩高度的点的数量。成人点的数量。
            count_adult = np.sum(height_label >= height_children)

            # 计算小于小孩高度的点的数量。小孩点的数量。
            count_children = np.sum(height_label < height_children)

            # 检测相关标签点的数量，如果点数量少，跳过，很可能是散点
            if (count_adult + count_children) <= label_count_threshold:
                continue

            # 相关标签：小孩的点的数量得大于 threshold[0] 阈值，成人的点得小于 threshold[1]
            if (count_children >= children_threshold[0]) & (count_adult <= children_threshold[1]):

                # 如果相关标签，小孩点的数量大于阈值
                # 和成人的点的数量少于阈值
                # 则很大可能是小孩
                TF_temp_children = 1

                # 生成小孩的二值图
                map_children = map_children | condition

            elif count_adult >= adult_threshold:

                TF_temp_adult = 2

                # 生成成人的二值图
                map_adult = map_adult | condition

        # 最终 TF 输出，以 TF_temp_children 为主
        if TF_temp_children == 1:
            TF = 1
        elif TF_temp_adult == 2:
            TF = 2
        else:
            TF = 0

        return TF, map_children, map_adult

    def multi_frames_filter(self, TF, count_children, count_adult, step_up, step_down, threshold_tf):

        '''
        :param TF: 单帧判断的结果
        :param threshold_tf: 阈值
        :param count_children: 小孩的计数器
        :param step_up: 叠加
        :param step_down: 相减
        :return:
        TF2: 成人小孩时间滤波后的结果
        '''

        # 设定最高上限为 threshold_tf 的 2 倍
        a_limit = 2 * threshold_tf

        # 根据当下帧的 TF 对计数器计算
        if TF == 1:
            count_children = count_children + step_up
            count_adult = count_adult + step_down
        elif TF == 2:
            count_children = count_children + step_down
            count_adult = count_adult + step_up
        else:
            count_children = count_children + step_down
            count_adult = count_adult + step_down

        # count 的最大值不能超出 a_limit
        count_children = np.min((count_children, a_limit))
        count_adult = np.min((count_adult, a_limit))

        # count 的最小值永远大于 0
        count_children = np.max((count_children, 0))
        count_adult = np.max((count_adult, 0))

        if count_children >= threshold_tf:
            TF2 = 1
        elif count_adult >= threshold_tf:
            TF2 = 2
        else:
            TF2 = 0

        return TF2, count_children, count_adult

    def update_bkg(self, img, img_bkg, count_bkg_matrix, bkg_avg_frame, threshold, step_up_bkg, step_down_bkg):

        '''
        :param img: 输入
        :param img_bkg: 输入背景
        :param count_bkg_matrix: 背景计数器（矩阵）
        :param bkg_avg_frame: 背景更新的帧数
        :param threshold: 背景与前景相减后大于阈值才判定为背景，不是因为数据波动
        :param step_up_bkg: 背景因为有物体置入后，背景的叠加次数
        :param step_down_bkg: 背景因为有物体离开后，背景的叠加次数
        :return:
        img_bkg: 更新后的背景图
        count_bkg_matrix: 更新后的背景计数器
        '''

        # 当帧与背景图做差分
        difference = img - img_bkg

        # 找出大于正负距离阈值的位置
        idx_positive = difference > +threshold
        idx_negative = difference < -threshold

        idx = idx_positive | idx_negative

        # 大于距离阈值的像素做叠加
        count_bkg_matrix[idx_positive] = count_bkg_matrix[idx_positive] + step_up_bkg
        count_bkg_matrix[idx_negative] = count_bkg_matrix[idx_negative] + step_down_bkg

        # 小于阈值的像素则制成 0 = 把没有变化的区域制成 0 = reset
        count_bkg_matrix[~idx] = 0

        # 累计一定的数后，则更新背景
        idx_bkg = count_bkg_matrix >= bkg_avg_frame

        # 把背景图的一直不动的位子制成当下帧的值
        img_bkg[idx_bkg] = img[idx_bkg]

        # 计数器 reset
        count_bkg_matrix[idx_bkg] = 0

        return img_bkg, count_bkg_matrix

    def false_positive_check(self, TF, map_children, map_adult, map_top_down, counts, mask, height_children,
                             children_threshold, adult_threshold, label_count_threshold):

        # 如果是判断为小孩的结果，才需要算法评估是不是误判
        # 如果判断是 TF = 0， 跳过
        if TF == 1:
            # mask 与 俯视图, counts 乘积
            map_top_down2, counts2 = copy.copy(map_top_down), copy.copy(counts)
            map_top_down2, counts2 = map_top_down2 * mask, counts2 * mask

            TF, map_children, map_adult = child_adult_analyze().det_adult_child_single_frame(
                map_top_down2, counts2, height_children, children_threshold, adult_threshold, label_count_threshold)

        return TF, map_children, map_adult

    def generate_blind_spot_region_children(self, angle, rows, cols, x_lim, y_lim, step):

        '''
        生成 二值图像 用来解决成人在边缘误判小孩的情况
        :param angle: 调整中心的角度
        :param rows: 行数
        :param cols: 列数
        :param x_lim: x 的最小最大值
        :param y_lim: y 的最小最大值
        :param step: 距离间隔
        :return:
        '''

        # 初始化二值图像
        mask = np.ones((rows, cols))

        # 可能 cols 是双数，所以最终得结果 matrix 矩阵不对称，所以加了个 step/2, matrix 就对称了
        x_range = np.arange(x_lim[0] + step / 2, x_lim[1] + step / 2, step)
        y_range = np.arange(y_lim[0], y_lim[1], step)

        # 生成矩阵，每个元素都是对应的 x 与 y 值
        xx, yy = np.meshgrid(x_range, y_range)

        # 把中心值 (0, 0) （相机的位置）放在矩阵的中下方
        yy = np.flipud(yy)

        # 计算斜率
        m = np.tan(angle / 180 * np.pi)

        # Loop 每一个元素
        for i in range(0, rows):
            for j in range(0, cols):

                # 相对元素的 x 值
                x = xx[i, j]

                # 根据 x 是在左侧或者右侧，使用不同的线性函数
                if x > 0:
                    y = +m * x
                else:
                    y = -m * x

                # 如果有关元素 yy[i, j]值是低于 理论的 y 值，则定义为1
                if yy[i, j] < y:
                    mask[i, j] = 0

        # 把左右两侧边缘的像素制 0
        # 如果成人或者小孩站在边缘左右两侧，算法不会做判断
        mask[:, 0] = 0
        mask[:, -1] = 0

        # plt.clf()
        # ax = plt.figure(1)
        # plt.imshow(mask)
        # plt.axis('off')
        # plt.show(block=False)

        return mask

    def plot_graph(self, height_children, pc, pc2, pc3, angle_blind, xlim, ylim, zlim, step, depth, ir, map_top_down2,
                   counts2, classify, i, TF, TF2, TF_ideal, TF_predict, region):

        # Plot the subplots with different view perspective
        plt.clf()
        fig = plt.figure(1)
        fig.set_size_inches(12, 8)

        ax = fig.add_subplot(2, 3, 1, projection='3d')
        ax.plot(np.array([height_children, height_children]), np.array([-1000, 1000]), np.array([0, 0]),
                'r--')  # height of children
        # ax.plot(pc[:, 0], pc[:, 1], pc[:, 2], 'r.', markersize=0.5)
        # ax.plot(pc2[:, 0], pc2[:, 1], pc2[:, 2], 'k.', markersize=0.5)
        ax.plot(pc3[:, 0], pc3[:, 1], pc3[:, 2], 'k.', markersize=0.5)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.set_xlim([0, 1600])
        ax.set_ylim([-1000, 1000])
        ax.set_zlim([0, 1700])
        ax.view_init(azim=0, elev=270)
        # ax.view_init(azim=179, elev=90)
        plt.title('pc xy view')

        ax = fig.add_subplot(2, 3, 2, projection='3d')

        # plot the field of view lines
        length = 100
        m = np.tan(angle_blind / 180 * np.pi)
        y1, y2 = np.linspace(0, 1000, length), np.linspace(0, -1000, length)
        z1, z2 = m * y1, -m * y2
        x1, x2 = np.zeros((length, 1)), np.zeros((length, 1))
        ax.plot(x1, y1, z1, 'r--')
        ax.plot(x2, y2, z2, 'r--')

        # plot the edge
        x_edge1, x_edge2 = 0, 0
        y_edge1, y_edge2 = ylim[1] - step, ylim[0] + step
        z_edge1, z_edge2 = m * y_edge1, -m * y_edge2
        ax.plot([x_edge1, x_edge1], [y_edge1, y_edge1], [z_edge1, zlim[1]], 'r--')
        ax.plot([x_edge2, x_edge2], [y_edge2, y_edge2], [z_edge2, zlim[1]], 'r--')

        # ax.plot(pc[:, 0], pc[:, 1], pc[:, 2], 'r.', markersize=0.5)
        # ax.plot(pc2[:, 0], pc2[:, 1], pc2[:, 2], 'k.', markersize=0.5)
        ax.plot(pc3[:, 0], pc3[:, 1], pc3[:, 2], 'k.', markersize=0.5)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.set_xlim([0, 2200])
        ax.set_ylim([-1000, 1000])
        ax.set_zlim([0, 1700])
        ax.view_init(azim=0, elev=0)
        plt.title('pc yz view')

        ax = fig.add_subplot(2, 3, 3)
        plt.imshow(np.rot90(depth))
        plt.axis('off')
        plt.clim(0, 2200)
        plt.title('depth')

        ax = fig.add_subplot(2, 3, 4)
        plt.imshow(map_top_down2)
        plt.axis('off')
        plt.colorbar()
        plt.clim(0, 1200)
        # plt.title('max height; TF = ' + str(TF) + 'TF2 = ' + str(TF2))
        plt.title('max height')
        color = 'b'

        ax = fig.add_subplot(2, 3, 5)
        plt.imshow(counts2)
        plt.axis('off')
        plt.colorbar()
        plt.clim(0, 40)
        plt.title('counts')

        ax = fig.add_subplot(2, 3, 6)
        plt.imshow(np.rot90(ir))
        plt.clim(0, 2000)
        plt.title('ir')

        plt.suptitle('point cloud ' + str(i + 1))

        offset = -0.02
        plt.figtext(0.48 + offset, 0.92, "TF = ", fontsize='large', color=color, ha='left')
        plt.figtext(0.52 + offset, 0.92, str(TF), fontsize='large', color=color, ha='left')
        plt.figtext(0.55 + offset, 0.92, 'TF2 = ', fontsize='large', color='b', ha='left')
        plt.figtext(0.60 + offset, 0.92, str(TF2), fontsize='large', color='b', ha='left')
        plt.figtext(0.62 + offset, 0.92, 'region = ', fontsize='large', color='b', ha='left')
        plt.figtext(0.69 + offset, 0.92, str(region), fontsize='large', color='b', ha='left')

        if classify == 'on':

            if (TF_ideal[i] == 0) & (TF_predict[i] == 0):
                color = 'b'
                classification = 'True Negative'
            elif (TF_ideal[i] == 0) & (TF_predict[i] == 1):
                color = 'r'
                classification = 'False Positive'
            elif (TF_ideal[i] == 1) & (TF_predict[i] == 0):
                color = 'r'
                classification = 'False Negative'
            elif (TF_ideal[i] == 1) & (TF_predict[i] == 1):
                color = 'b'
                classification = 'True Positive'

            plt.figtext(0.34 + offset, 0.92, "TF_actual = ", fontsize='large', color=color, ha='left')
            plt.figtext(0.44 + offset, 0.92, str(int(TF_ideal[i])), fontsize='large', color=color, ha='left')
            plt.figtext(0.62 + offset, 0.92, classification, fontsize='large', color=color, ha='left')

        else:
            classification = ''

        plt.show(False)
        plt.pause(0.2)

        return classification

    def plot_bkg_graph(self, map_top_down, count_bkg_matrix, bkg_avg_frame, step_up_bkg, i):

        # 绘背景更新的计数器和背景图
        fig = plt.figure(2)
        fig.clf()
        fig.set_size_inches(12, 8)
        ax = fig.add_subplot(1, 2, 1)
        plt.imshow(map_top_down)
        plt.colorbar()
        plt.clim(0, 1600)
        plt.title('map top down bkg')
        plt.axis('off')

        ax = fig.add_subplot(1, 2, 2)
        plt.imshow(count_bkg_matrix)
        plt.title('count bkg')
        plt.axis('off')
        plt.colorbar()
        plt.clim(0, int(bkg_avg_frame / step_up_bkg))
        plt.suptitle('point cloud ' + str(i + 1))

        plt.show(False)
        plt.pause(0.2)

        return

    def find_region(self, TF, TF2, region_store_children, region_store_adult, map_children, map_adult, region_size=5):

        '''
        :param TF: 小孩的判断结果 TF = 1 是小孩
        :param matrix: 二值图（如小孩的二值图）
        :param region_size: 最终要建立的空间区域数量
        :return:
        region: 小孩在哪个区域（如果 region_size = 5, region = 0, 1, 2, 3, 4 分别表示5个不同的区域
        '''

        region = -1  # 嵌入式制成不了空数组，所以制成 -1 而不是 []

        if (TF == 1) and (TF2 == 1):

            # 如果是小孩，计算小孩的位子
            region = child_adult_analyze().calc_region(map_children, region_size)
            region_store_children = region

        elif (TF != 1) and (TF2 == 1):

            region = region_store_children

        elif (TF == 2) and (TF2 == 2):

            # 如果是成人，计算成人的位子
            region = child_adult_analyze().calc_region(map_adult, region_size)
            region_store_adult = region

        elif (TF != 2) and (TF2 == 2):

            region = region_store_adult

        return region, region_store_children, region_store_adult

    def calc_region(self, matrix, region_size):

        # Sum over columns
        sum_cols = np.sum(matrix, axis=0)

        # Find the location of the maximum value
        region = np.argmax(sum_cols)

        # Calculate the step size to be grouped to a single region
        step = np.shape(matrix)[1] / region_size

        # calculate which region is the max located
        # example: for region_size = 5; matrix of 15*20 -> step = 4; region's range 0, 1, 2, 3, 4
        region = np.floor(region / step)

        return region
    
    def dynamic_height_children(self, TF2, height_children_0, step_height_children):
        
        if TF2 == 0:
            height_children = height_children_0
        elif TF2 == 1:
            height_children = height_children_0 + step_height_children
        elif TF2 == 2:
            height_children = height_children_0 - step_height_children

        return height_children

class machine_learning():

    def __init__(self):
        return

    def performance_metric(self, actual, predict):

        '''
        :param actual: actual data set (column vector)
        :param predict: predicted data set (column vector)
        :return:
        cm: confusion matrix
        TP, TN, FP, FN : true positives, true negatives, false positives, false negatives
        accuracy, precision, recall and F1 metrics
        '''

        # Import the confusion matrix library
        from sklearn.metrics import confusion_matrix

        # Calculate the confusion matrix
        cm = confusion_matrix(actual, predict)

        # If all the actual and predict values are the same
        # modify the cm to be of 2*2 matrix size
        if np.sum(predict == 0) == predict.__len__():
            cm = np.array([[cm[0][0], 0], [0, 0]])
        elif np.sum(predict == 1) == predict.__len__():
            cm = np.array([[0, 0], [0, cm[0][0]]])

        # Calculate true positive, true negative, false positive and false negative counts
        TP, TN, FP, FN = cm[1, 1], cm[0, 0], cm[0, 1], cm[1, 0]

        # Accuracy, Recall, Precision and F1 metrics
        accuracy = (TP + TN) / (TP + FP + FN + TN)

        if TP + FP != 0:
            precision = TP / (TP + FP)
        else:
            precision = np.nan

        if TP + FN != 0:
            recall = TP / (TP + FN)
        else:
            recall = np.nan

        F1 = (2 * precision * recall) / (precision + recall)

        return cm, TP, TN, FP, FN, accuracy, precision, recall, F1

    def write_metrics_txt(self, saved_filename, cm, TP, TN, FP, FN, accuracy, precision, recall, F1):

        '''
        This function writes all the performance metrics into a txt file
        :param saved_filename: filename to be saved
        :param cm: confusion matrix
        :param TP: true positive counts
        :param TN: true negative counts
        :param FP: false positive counts
        :param FN: false negative counts
        :param accuracy:
        :param precision:
        :param recall:
        :param F1:
        :return:
        '''

        file1 = open(saved_filename, "w")

        file1.writelines('confusion matrix \n')

        file1.write(str(cm[0, 0]) + ' ')

        file1.write(str(cm[0, 1]) + ' ' + '\n')

        file1.write(str(cm[1, 0]) + ' ')

        file1.write(str(cm[1, 1]) + ' ' + '\n')

        file1.writelines('\n')

        file1.writelines('TP; TN; FP; FN \n')

        file1.writelines(str([TP, TN, FP, FN]) + '\n')

        file1.writelines('\n')

        file1.writelines('accuracy; precision; recall; F1 \n')

        file1.writelines(str([accuracy, precision, recall, F1]) + '\n')

        file1.close()

        return


class matrix_manipulation():

    def __init__(self):
        return

    def fill_holes(self, matrix1, matrix2, fill_number=0, num_neighbors=3):

        '''
        This function is to fill any zero matrix element with the mean of its neighbors
        :param matrix1: input matrix to be checked
        :param matrix2: input matrix to be checked
        :param fill_number: number to be replaced
        :param num_neighbors: total number of neighbors used to calculate the mean value
        :return:
        matrix: matrix that its holes have been filled
        '''

        # 需要定义新的变量，不然 for 循环的填充会影响后来的填充结果（感谢 Tony 提点）
        matrix1_output = copy.copy(matrix1)
        matrix2_output = copy.copy(matrix2)

        # Loop through all the matrix elements
        # begin with the 1st row/col rather than 0th row/col to avoid effect of the edge
        for i in range(1, np.shape(matrix1)[0] - 1):
            for j in range(1, np.shape(matrix1)[1] - 1):

                # If the corresponding matrix element is same as the fill_number
                if matrix1[i, j] == fill_number:

                    # Extract all its neighbors
                    vector1 = np.array([matrix1[i - 1, j], matrix1[i + 1, j], matrix1[i, j - 1], matrix1[i, j + 1]])

                    # Only those neighbors that do not equal to the fill_number are used
                    idx = vector1 != fill_number

                    # If sufficient number of neighbors are found
                    if np.sum(idx) >= num_neighbors:
                        # Calculate the mean value
                        mean_value1 = np.mean(vector1[idx])

                        # Since the condition for vector1 is fulfilled, only then proceed with vector2
                        vector2 = np.array([matrix2[i - 1, j], matrix2[i + 1, j], matrix2[i, j - 1], matrix2[i, j + 1]])
                        mean_value2 = np.mean(vector2[idx])

                        matrix1_output[i, j] = mean_value1
                        matrix2_output[i, j] = mean_value2

        return matrix1_output, matrix2_output
