from torch import nn
import numpy as np
import torch
from fxpmath import Fxp

class FPA(object):

    def __init__(self, format, method=1):

        '''
        初始化定点化
        :param format: 定点化格式 Qn.m 表示 # 例子: 'Q3.13' 或者 'Q2.14'
        '''

        # 以 Q3.13 为例
        # n_frac = 13
        # n_int = 2
        # n_sign = 1
        # upper_float = 3.9998779296875
        # lower_float = -4
        # upper = 32767
        # lower = -32768

        self.format = format

        n_frac = int(format.split('.')[1])
        n_int = int(format.split('.')[0][1:]) - 1
        n_sign = int(1)
        n_total = n_sign + n_frac + n_int

        upper_float = 2 ** n_int - 2 ** -n_frac
        lower_float = -2 ** n_int
        upper = int(upper_float * 2 ** n_frac)
        lower = int(lower_float * 2 ** n_frac)

        self.n_frac = n_frac
        self.n_int = n_int
        self.n_sign = n_sign
        self.n_total = n_total
        self.upper_float = upper_float
        self.upper_int = upper
        self.lower_float = lower_float
        self.lower_int = lower
        self.method = method

    def __call__(self, x):

        '''
        定点化函数
        :param x: 输入值
        :return:
        '''

        if self.method is 1:  # 正常取整方式

            # 定点化计算 (取整)
            x = int(x * 2 ** self.n_frac)

        elif self.method is 2:  # 为了配合嵌入式，负数需要向下取整

            # 正数和负数都是向下取整
            x = np.floor(x * 2 ** self.n_frac)

        # 检测溢出 (正整数最大值)
        if x > self.upper_int:
            # print(f'!!!{x} 出现溢出, 大于>{self.upper_int}; 把{x}设定为{self.upper_int}!!!')
            x = self.upper_int

        # 检测溢出 (负整数最大值)
        if x < self.lower_int:
            # print(f'!!!{x} 出现溢出, 小于<{self.lower_int}; 把{x}设定为{self.lower_int}!!!')
            x = self.lower_int

        # Convert back to float after quantization
        x = x * 2 ** -self.n_frac

        self.x = x

        return x

def quantize(x, format):

    '''
    Quantization of the tensor, x
    :param x: input tensor
    :param format: format of the quantization
    :return:
    '''

    from fxpmath import Fxp   # 安装包的定点化类
    Fxp2 = FPA(format, method=2)  # 调用自己编写的定点化类

    with torch.no_grad():
        for index, value in np.ndenumerate(x):

            # 定点化并赋值
            # x[index] = Fxp(value, dtype=format).__float__()
            x[index] = Fxp2(value)

    return x

def dequantize(x, n_frac):

    '''
    定点化函数
    :param x: 输入值 (整数）
    :return:
    '''

    # 定点化计算 (取整)
    x = x * 2 ** -n_frac

    return x

def int16_to_bin16(x):
    '''
        16位
        二int转换成进制
    '''

    bin16 = lambda x: ''.join([str((x >> i) & 1) for i in range(15, -1, -1)])

    return bin16(x)

def bin16_to_int16(x):
    '''
        16位
        二进制转换成int
    '''

    sum = 0
    for i in range(0, len(x)):

        j = len(x) - i - 1
        xi = int(x[i])

        if j == 15 and xi == 1:

            sum += -xi*2**j

        else:

            sum += xi*2**j

    return sum

def int32_to_bin32(x):

    '''
        32位
        二int转换成进制
    '''

    bin32 = lambda x: ''.join([str((x >> i) & 1) for i in range(31, -1, -1)])

    return bin32(x)

def bin32_to_int32(x):
    '''
        32位
        二进制转换成int
    '''

    sum = 0
    for i in range(0, len(x)):

        j = len(x) - i - 1
        xi = int(x[i])

        if j == 31 and xi == 1:

            sum += -xi*2**j

        else:

            sum += xi*2**j

    return sum

def expand_bin16_bin32(x, Q0nm, Q1nm):

    '''
    x 如果只有16位，转换成32位，前后补0
    :param x:
    :param format: x 输入 Qnm的格式
    :return:
    '''

    # 输入格式
    n0 = Q0nm.split('.')
    n0_int = int(n0[0][1:]) - 1  # 输入格式的整数位 数量
    n0_frac = int(n0[1])  # 输入格式的小数位 数量

    # 输出格式
    n1 = Q1nm.split('.')
    n1_int = int(n1[0][1:]) - 1  # 输出格式的整数位 数量
    n1_frac = int(n1[1])  # 输出格式的整数位 数量

    out = '0'*(n0_int + n1_int) + x + '0'*(n1_frac - n0_frac)

    return out

def truncate(x_bin, Q0nm, Q1nm):

    '''
    x 二进制格式做截取，从 Q0nm 格式转换成 Q1nm 格式
    :param x_bin: 输入值 二进制格式
    :param Q0nm: 输入值的 Qnm 格式
    :param Q1nm: 输出值的 Qnm 格式
    :return:
    '''

    # 输入格式
    n0 = Q0nm.split('.')
    n0_int = int(n0[0][1:]) - 1  # 输入格式的整数位 数量
    n0_frac = int(n0[1])  # 输入格式的小数位 数量

    # 输出格式
    n1 = Q1nm.split('.')
    n1_int = int(n1[0][1:]) - 1  # 输出格式的整数位 数量
    n1_frac = int(n1[1])  # 输出格式的整数位 数量

    # 输入值 x 分成 符号位，整数位与小数位
    x_sign = x_bin[0]  # 提取符号位
    x_int = x_bin[1:1+n0_int]  # 提取整数位
    x_frac = x_bin[-n0_frac:]  # 提取小数位

    # x 进行拆分
    x_sign_trunc = x_int[-n1_int-1]
    x_int_trunc = x_int[-n1_int:]
    x_frac_trunc = x_frac[0:n1_frac]

    # 截取
    x_trunc = x_sign_trunc + x_int_trunc + x_frac_trunc

    return x_trunc

def wx_multiply(w, x, wQnm, xQnm):

    print(f'w, x = {w}, {x}')
    float0 = w * x
    print(f'wx float = {float0}')

    # 把 w 与 x 定点化
    fw = FPA(format=wQnm)
    fx = FPA(format=xQnm)
    w1 = fw.quantize(x=w)
    x1 = fx.quantize(x=x)
    print(f'w, x 定点化 = {w1} ({wQnm} 格式), {x1} ({xQnm} 格式)')

    # 嵌入式的整数运算
    y32 = w1 * x1
    print(f'y = w*x = {y32 }')

    # Q3.12 乘积 Q3.12 -》32位输出结果
    # 从整数转换成二进制
    bin1 = int32_to_bin32(x=y32)
    print(f'w*x 32位 二进制表达式 {bin1}')

    # wx 相乘后的格式
    newQ = f'Q{1 + 1 + fw.n_int + fx.n_int}.{fw.n_frac + fx.n_frac}'
    print(f'w*x = {wQnm} * {xQnm} -> {newQ}')

    # 导入下一层神经网络计算需要做截取 生成 xQnm 的格式
    bin2 = truncate(bin1, newQ, xQnm)
    print(f'w*x {bin1} 从 {newQ} 截取成 {bin2} {xQnm}')

    # x 的整数表达式
    int2 = bin16_to_int16(x=bin2)
    print(f'w*x 16位 截取 整数 表达式 {int2}')

    # 转换成浮点数比较原始值 float0 = w*x
    float2 = dequantize(x=int2, n_frac=fx.n_frac)

    print(f'原始值 w*x {float0}，定点运算的 w*x = {float2}')

    return y32, newQ

def main():

    # 输入值 原始值
    coef_format = 'Q1.15'
    x_format = 'Q4.12'
    w1, x1 = 0.8912, -1.234
    w2, x2 = 0.2912, -0.834
    b = 0.38
    y0 = w1 * x1 + w2 * x2 + b

    # w*x 相乘运算
    w1x1, newQnm = wx_multiply(w=w1, x=x1, wQnm=coef_format, xQnm=x_format)
    print('*' * 75)
    w2x2, newQnm = wx_multiply(w=w2, x=x2, wQnm=coef_format, xQnm=x_format)

    # 导入 b 加法 因为b是16位，需要与 wx 的大小相符，所以需要扩充（补0）
    f = FPA(format=coef_format)
    b = expand_bin16_bin32(x=int16_to_bin16(f.quantize(b)), Q0nm=coef_format, Q1nm=newQnm)
    b = bin32_to_int32(b)

    print('*' * 75)
    # 整数乘法加法
    y = w1x1 + w2x2 + b
    print(f'y = w1x1 + w2x2 + b = {w1x1} + {w2x2} + {b} = {y}')

    ##########################################################################

    y_float = dequantize(y, n_frac=int(newQnm.split('.')[1]))
    print(f'y = {y} 从整数转换成浮点，等于 {y_float}')
    print(f'与原始值相比 y0 = {y0}')

def linearize_matrix(x):

    '''
    把矩阵转换成一维度，作为全连接层的输入
    :param x: 矩阵
    :return:
    x_out: 1位矩阵
    '''

    # 开辟内存空间，储存输出结果
    x_out = np.zeros((x.shape[0], x.shape[1]*x.shape[2]*x.shape[3]))

    for pic in range(x.shape[0]):  # 遍历所有图
        for k in range(x.shape[3]):
            for j in range(x.shape[2]):
                for i in range(x.shape[1]):

                    idx = 192*k + 64*j + i
                    x_out[pic, idx] = x[pic, i, j, k]

    x_out = torch.Tensor(x_out)

    return x_out

class PNet(nn.Module):

    """
    MTCNN Pnet model
    """

    def __init__(self):

        super().__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)
        self.conv2 = nn.Conv2d(in_channels=10, out_channels=16, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=16)
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3)
        self.prelu3 = nn.PReLU(num_parameters=32)
        self.conv4_1 = nn.Conv2d(in_channels=32, out_channels=2, kernel_size=1)
        self.softmax4_1 = nn.Softmax(dim=1)
        self.conv4_2 = nn.Conv2d(in_channels=32, out_channels=4, kernel_size=1)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.conv3(x)
        x = self.prelu3(x)
        a = self.conv4_1(x)
        a = self.softmax4_1(a)
        b = self.conv4_2(x)

        return b, a

class PNet2(nn.Module):

    """
    MTCNN Pnet model
    This model is used for input output statistical analysis.
    Each output is returned
    """

    def __init__(self):

        super().__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)
        self.conv2 = nn.Conv2d(in_channels=10, out_channels=16, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=16)
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3)
        self.prelu3 = nn.PReLU(num_parameters=32)
        self.conv4_1 = nn.Conv2d(in_channels=32, out_channels=2, kernel_size=1)
        self.softmax4_1 = nn.Softmax(dim=1)
        self.conv4_2 = nn.Conv2d(in_channels=32, out_channels=4, kernel_size=1)

    def forward(self, x0):

        x1 = self.conv1(x0)
        x2 = self.prelu1(x1)
        x3 = self.pool1(x2)
        x4 = self.conv2(x3)
        x5 = self.prelu2(x4)
        x6 = self.conv3(x5)
        x7 = self.prelu3(x6)
        a1 = self.conv4_1(x7)
        a2 = self.softmax4_1(a1)
        b = self.conv4_2(x7)

        string1 = ['x0 conv1输入', 'x1 conv1输出; prelu1输入', 'x2 prelu1输出; pool1输入', 'x3 pool1输出; conv2输入',
                  'x4 conv2输出; prelu2输入', 'x5 prelu2输出; conv3输入', 'x6 conv3输出; prelu3输入',
                  'x7 prelu3输出; conv4_1输入; conv4_2输入', 'a1 conv4_1输出; softmax4_1输入',
                  'a2 softmax_4_1输出', 'b conv4_2输出']


        string2 = ['x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'a1', 'a2', 'b']

        return x0, x1, x2, x3, x4, x5, x6, x7, a1, a2, b, string2

class PNet3(nn.Module):

    """
    MTCNN Pnet model
    This model is used for quantization analysis.
    每一层的输入输出都做定点化处理
    """

    def __init__(self, format, save_in_out):

        super().__init__()

        self.save_in_out = save_in_out
        self.format = format
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)
        self.conv2 = nn.Conv2d(in_channels=10, out_channels=16, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=16)
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3)
        self.prelu3 = nn.PReLU(num_parameters=32)
        self.conv4_1 = nn.Conv2d(in_channels=32, out_channels=2, kernel_size=1)
        self.softmax4_1 = nn.Softmax(dim=1)
        self.conv4_2 = nn.Conv2d(in_channels=32, out_channels=4, kernel_size=1)

    def save(self, x0, x1, x2, x3, x4, x5, x6, x7, a1, a2, b):

        import os

        # 把数据存在文件夹内
        folder = os.path.join('pnet', f'input_size_h{x0.shape[2]}_w{x0.shape[3]}_d{x0.shape[1]}')
        if os.path.exists(folder) is False:
            os.makedirs(folder)

        with torch.no_grad():

            # 保存浮点数格式
            fmt = self.format.replace('.', '_')
            type = 'float'
            np.savetxt(os.path.join(folder, f'{0}_{fmt}_{type}_input_conv1_h{x0.shape[2]}_w{x0.shape[3]}_d{x0.shape[1]}.txt'), x0.reshape(-1, x0.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{1}_{fmt}_{type}_input_prelu1_h{x1.shape[2]}_w{x1.shape[3]}_d{x1.shape[1]}.txt'), x1.reshape(-1, x1.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{2}_{fmt}_{type}_input_pool1_h{x2.shape[2]}_w{x2.shape[3]}_d{x2.shape[1]}.txt'), x2.reshape(-1, x2.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{3}_{fmt}_{type}_input_conv2_h{x3.shape[2]}_w{x3.shape[3]}_d{x3.shape[1]}.txt'), x3.reshape(-1, x3.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{4}_{fmt}_{type}_input_prelu2_h{x4.shape[2]}_w{x4.shape[3]}_d{x4.shape[1]}.txt'), x4.reshape(-1, x4.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{5}_{fmt}_{type}_input_conv3_h{x5.shape[2]}_w{x5.shape[3]}_d{x5.shape[1]}.txt'), x5.reshape(-1, x5.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{6}_{fmt}_{type}_input_prelu3_h{x6.shape[2]}_w{x6.shape[3]}_d{x6.shape[1]}.txt'), x6.reshape(-1, x6.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{7}_{fmt}_{type}_input_conv4_1_h{x7.shape[2]}_w{x7.shape[3]}_d{x7.shape[1]}.txt'), x7.reshape(-1, x7.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{8}_{fmt}_{type}_input_softmax4_1_h{a1.shape[2]}_w{a1.shape[3]}_d{a1.shape[1]}.txt'), a1.reshape(-1, a1.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{9}_{fmt}_{type}_output_softmax4_1_h{a2.shape[2]}_w{a2.shape[3]}_d{a2.shape[1]}.txt'), a2.reshape(-1, a2.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{10}_{fmt}_{type}_input_conv4_2_h{x7.shape[2]}_w{x7.shape[3]}_d{x7.shape[1]}.txt'), x7.reshape(-1, x7.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{11}_{fmt}_{type}_output_conv4_2_h{b.shape[2]}_w{b.shape[3]}_d{b.shape[1]}.txt'), b.reshape(-1, b.shape[3]), fmt='%.32f')

            # 保存定点化格式
            n = int(self.format.split('.')[1])  # 小数位
            type = 'int'
            np.savetxt(os.path.join(folder, f'{0}_{fmt}_{type}_input_conv1_h{x0.shape[2]}_w{x0.shape[3]}_d{x0.shape[1]}.txt'), 2**n*x0.reshape(-1, x0.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{1}_{fmt}_{type}_input_prelu1_h{x1.shape[2]}_w{x1.shape[3]}_d{x1.shape[1]}.txt'), 2**n*x1.reshape(-1, x1.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{2}_{fmt}_{type}_input_pool1_h{x2.shape[2]}_w{x2.shape[3]}_d{x2.shape[1]}.txt'), 2**n*x2.reshape(-1, x2.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{3}_{fmt}_{type}_input_conv2_h{x3.shape[2]}_w{x3.shape[3]}_d{x3.shape[1]}.txt'), 2**n*x3.reshape(-1, x3.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{4}_{fmt}_{type}_input_prelu2_h{x4.shape[2]}_w{x4.shape[3]}_d{x4.shape[1]}.txt'), 2**n*x4.reshape(-1, x4.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{5}_{fmt}_{type}_input_conv3_h{x5.shape[2]}_w{x5.shape[3]}_d{x5.shape[1]}.txt'), 2**n*x5.reshape(-1, x5.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{6}_{fmt}_{type}_input_prelu3_h{x6.shape[2]}_w{x6.shape[3]}_d{x6.shape[1]}.txt'), 2**n*x6.reshape(-1, x6.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{7}_{fmt}_{type}_input_conv4_1_h{x7.shape[2]}_w{x7.shape[3]}_d{x7.shape[1]}.txt'), 2**n*x7.reshape(-1, x7.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{8}_{fmt}_{type}_input_softmax4_1_h{a1.shape[2]}_w{a1.shape[3]}_d{a1.shape[1]}.txt'), 2**n*a1.reshape(-1, a1.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{9}_{fmt}_{type}_output_softmax4_1_h{a2.shape[2]}_w{a2.shape[3]}_d{a2.shape[1]}.txt'), 2**n*a2.reshape(-1, a2.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{10}_{fmt}_{type}_input_conv4_2_h{x7.shape[2]}_w{x7.shape[3]}_d{x7.shape[1]}.txt'), 2**n*x7.reshape(-1, x7.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{11}_{fmt}_{type}_output_conv4_2_h{b.shape[2]}_w{b.shape[3]}_d{b.shape[1]}.txt'), 2**n*b.reshape(-1, b.shape[3]), fmt='%.f')

        print('*'*100)
        print(f'Input and output saved in {fmt}_{type} format in folder {folder}')

    def forward(self, x0):

        x0 = quantize(x0, self.format)  # 没有移位
        x1 = self.conv1(x0)
        x1 = quantize(x1, self.format)
        x2 = self.prelu1(x1)
        x2 = quantize(x2, self.format)
        x3 = self.pool1(x2)
        x3 = quantize(x3, self.format)
        x4 = self.conv2(x3)
        x4 = quantize(x4, self.format)
        x5 = self.prelu2(x4)
        x5 = quantize(x5, 'Q3.5')
        x6 = self.conv3(x5)
        x6 = quantize(x6, self.format)  # 这里的定点化可能和瞿洋不一样, 但是差异不大
        x7 = self.prelu3(x6)
        x7 = quantize(x7, 'Q3.5')  # 转成8位
        # x7 = quantize(x7, self.format)
        a1 = self.conv4_1(x7)
        a1 = quantize(a1, self.format)
        a2 = self.softmax4_1(a1)
        a2 = quantize(a2, self.format)
        b = self.conv4_2(x7)
        b = quantize(b, self.format)

        string1 = ['x0 conv1输入', 'x1 conv1输出; prelu1输入', 'x2 prelu1输出; pool1输入', 'x3 pool1输出; conv2输入',
                  'x4 conv2输出; prelu2输入', 'x5 prelu2输出; conv3输入', 'x6 conv3输出; prelu3输入',
                  'x7 prelu3输出; conv4_1输入; conv4_2输入', 'a1 conv4_1输出; softmax4_1输入',
                  'a2 softmax_4_1输出', 'b conv4_2输出']


        string2 = ['x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'a1', 'a2', 'b']

        print(f'Quantization {self.format} for input size of {list(x0.shape)} in the Pnet Model')

        # 把所有的输入输出的定点化结果储存位 txt 文件
        if self.save_in_out:
            self.save(x0, x1, x2, x3, x4, x5, x6, x7, a1, a2, b)

        return x0, x1, x2, x3, x4, x5, x6, x7, a1, a2, b, string2

class RNet(nn.Module):

    """
    MTCNN Rnet
    """

    def __init__(self):

        super().__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=28, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=28)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)
        self.conv2 = nn.Conv2d(in_channels=28, out_channels=48, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=48)
        self.pool2 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)
        self.conv3 = nn.Conv2d(in_channels=48, out_channels=64, kernel_size=2)
        self.prelu3 = nn.PReLU(num_parameters=64)
        self.dense4 = nn.Linear(in_features=576, out_features=128)
        self.prelu4 = nn.PReLU(num_parameters=128)
        self.dense5_1 = nn.Linear(in_features=128, out_features=2)
        self.softmax5_1 = nn.Softmax(dim=1)
        self.dense5_2 = nn.Linear(in_features=128, out_features=4)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)
        x = self.conv3(x)
        x = self.prelu3(x)
        x = x.permute(0, 3, 2, 1).contiguous()
        x = self.dense4(x.view(x.shape[0], -1))
        x = self.prelu4(x)
        a = self.dense5_1(x)
        a = self.softmax5_1(a)
        b = self.dense5_2(x)

        return b, a

class RNet2(nn.Module):

    """
    MTCNN Rnet
    This model is used for input output statistical analysis.
    Each output is returned
    """

    def __init__(self):

        super().__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=28, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=28)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)
        self.conv2 = nn.Conv2d(in_channels=28, out_channels=48, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=48)
        self.pool2 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)
        self.conv3 = nn.Conv2d(in_channels=48, out_channels=64, kernel_size=2)
        self.prelu3 = nn.PReLU(num_parameters=64)
        self.dense4 = nn.Linear(in_features=576, out_features=128)
        self.prelu4 = nn.PReLU(num_parameters=128)
        self.dense5_1 = nn.Linear(in_features=128, out_features=2)
        self.softmax5_1 = nn.Softmax(dim=1)
        self.dense5_2 = nn.Linear(in_features=128, out_features=4)

    def forward(self, x0):

        x1 = self.conv1(x0)
        x2 = self.prelu1(x1)
        x3 = self.pool1(x2)
        x4 = self.conv2(x3)
        x5 = self.prelu2(x4)
        x6 = self.pool2(x5)
        x7 = self.conv3(x6)
        x8 = self.prelu3(x7)
        x8 = x8.permute(0, 3, 2, 1).contiguous()
        x9 = self.dense4(x8.view(x8.shape[0], -1))
        x10 = self.prelu4(x9)
        a1 = self.dense5_1(x10)
        a2 = self.softmax5_1(a1)
        b = self.dense5_2(x10)

        string = ['x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'a1', 'a2', 'b']

        return x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a1, a2, b, string

class RNet3(nn.Module):

    """
    MTCNN Rnet
    This model is used for quantization analysis.
    每一层的输入输出都做定点化处理
    """

    def __init__(self, format, save_in_out):

        super().__init__()

        self.format = format
        self.save_in_out = save_in_out
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=28, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=28)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)
        self.conv2 = nn.Conv2d(in_channels=28, out_channels=48, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=48)
        self.pool2 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)
        self.conv3 = nn.Conv2d(in_channels=48, out_channels=64, kernel_size=2)
        self.prelu3 = nn.PReLU(num_parameters=64)
        self.dense4 = nn.Linear(in_features=576, out_features=128)
        self.prelu4 = nn.PReLU(num_parameters=128)
        self.dense5_1 = nn.Linear(in_features=128, out_features=2)
        self.softmax5_1 = nn.Softmax(dim=1)
        self.dense5_2 = nn.Linear(in_features=128, out_features=4)

    def save(self, x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a1, a2, b):

        import os

        # 把数据存在文件夹内
        folder = os.path.join('rnet', f'input_size_h{x0.shape[2]}_w{x0.shape[3]}_d{x0.shape[1]}')
        if os.path.exists(folder) is False:
            os.makedirs(folder)

        with torch.no_grad():

            # 保存浮点数格式
            fmt = self.format.replace('.', '_')
            type = 'float'
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_conv1_h{x0.shape[2]}_w{x0.shape[3]}_d{x0.shape[1]}.txt'), x0.reshape(-1, x0.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu1_h{x1.shape[2]}_w{x1.shape[3]}_d{x1.shape[1]}.txt'), x1.reshape(-1, x1.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_pool1_h{x2.shape[2]}_w{x2.shape[3]}_d{x2.shape[1]}.txt'), x2.reshape(-1, x2.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_conv2_h{x3.shape[2]}_w{x3.shape[3]}_d{x3.shape[1]}.txt'), x3.reshape(-1, x3.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu2_h{x4.shape[2]}_w{x4.shape[3]}_d{x4.shape[1]}.txt'), x4.reshape(-1, x4.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_pool2_h{x5.shape[2]}_w{x5.shape[3]}_d{x5.shape[1]}.txt'), x5.reshape(-1, x5.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_conv3_h{x6.shape[2]}_w{x6.shape[3]}_d{x6.shape[1]}.txt'), x6.reshape(-1, x6.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu3_h{x7.shape[2]}_w{x7.shape[3]}_d{x7.shape[1]}.txt'), x7.reshape(-1, x7.shape[3]), fmt='%.32f')
            # np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_permute_h{x8.shape[2]}_w{x8.shape[3]}_d{x8.shape[1]}.txt'), x8.reshape(-1, x8.shape[3]), fmt='%.32f')
            # np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_flatten_h{x8.shape[2]}_w{x8.shape[3]}_d{x8.shape[1]}.txt'), x8.reshape(-1, x8.shape[3]), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_dense4_len{x8.numel()}.txt'), x8.view(-1, 1), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu4_len{x9.numel()}.txt'), x9.reshape(-1, 1), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_dense5_1_len{x10.numel()}.txt'), x10.reshape(-1, 1), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_softmax5_1_len{a1.numel()}.txt'), a1.reshape(-1, 1), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_dense5_2_len{x10.numel()}.txt'), x10.reshape(-1, 1), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_output_softmax5_1_len{a2.numel()}.txt'), a2.reshape(-1, 1), fmt='%.32f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_output_dense5_2_len{b.numel()}.txt'), b.reshape(-1, 1), fmt='%.32f')

            # 保存定点化格式
            n = int(self.format.split('.')[1])  # 小数位
            type = 'int'
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_conv1_h{x0.shape[2]}_w{x0.shape[3]}_d{x0.shape[1]}.txt'), 2**n*x0.reshape(-1, x0.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu1_h{x1.shape[2]}_w{x1.shape[3]}_d{x1.shape[1]}.txt'), 2**n*x1.reshape(-1, x1.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_pool1_h{x2.shape[2]}_w{x2.shape[3]}_d{x2.shape[1]}.txt'), 2**n*x2.reshape(-1, x2.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_conv2_h{x3.shape[2]}_w{x3.shape[3]}_d{x3.shape[1]}.txt'), 2**n*x3.reshape(-1, x3.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu2_h{x4.shape[2]}_w{x4.shape[3]}_d{x4.shape[1]}.txt'), 2**n*x4.reshape(-1, x4.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_pool2_h{x5.shape[2]}_w{x5.shape[3]}_d{x5.shape[1]}.txt'), 2**n*x5.reshape(-1, x5.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_conv3_h{x6.shape[2]}_w{x6.shape[3]}_d{x6.shape[1]}.txt'), 2**n*x6.reshape(-1, x6.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu3_h{x7.shape[2]}_w{x7.shape[3]}_d{x7.shape[1]}.txt'), 2**n*x7.reshape(-1, x7.shape[3]), fmt='%.f')
            # np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_permute_h{x8.shape[2]}_w{x8.shape[3]}_d{x8.shape[1]}.txt'), 2**n*x8.reshape(-1, x8.shape[3]), fmt='%.f')
            # np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_flatten_h{x8.shape[2]}_w{x8.shape[3]}_d{x8.shape[1]}.txt'), 2**n*x8.reshape(-1, x8.shape[3]), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_dense4_len{x8.numel()}.txt'), 2**n*x8.view(-1, 1), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_prelu4_len{x9.numel()}.txt'), 2**n*x9.reshape(-1, 1), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_dense5_1_len{x10.numel()}.txt'), 2**n*x10.reshape(-1, 1), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_softmax5_1_len{a1.numel()}.txt'), 2**n*a1.reshape(-1, 1), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_input_dense5_2_len{x10.numel()}.txt'), 2**n*x10.reshape(-1, 1), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_output_softmax5_1_len{a2.numel()}.txt'), 2**n*a2.reshape(-1, 1), fmt='%.f')
            np.savetxt(os.path.join(folder, f'{fmt}_{type}_output_dense5_2_len{b.numel()}.txt'), 2**n*b.reshape(-1, 1), fmt='%.f')

        print('*'*100)
        print(f'Input and output saved in {fmt}_{type} format in folder {folder}')

    def forward(self, x0):

        x0 = quantize(x0, self.format)
        x1 = self.conv1(x0)
        x1 = quantize(x1, self.format)
        x2 = self.prelu1(x1)
        x2 = quantize(x2, self.format)
        x3 = self.pool1(x2)
        x3 = quantize(x3, self.format)
        x4 = self.conv2(x3)
        x4 = quantize(x4, self.format)
        x5 = self.prelu2(x4)
        x5 = quantize(x5, self.format)
        x6 = self.pool2(x5)
        x6 = quantize(x6, self.format)
        x7 = self.conv3(x6)
        x7 = quantize(x7, self.format)
        x8 = self.prelu3(x7)
        x8 = quantize(x8, self.format)
        x8 = x8.permute(0, 3, 2, 1).contiguous()
        x8 = x8.view(x8.shape[0], -1)
        x9 = self.dense4(x8)
        x9 = quantize(x9, self.format)
        x10 = self.prelu4(x9)
        x10 = quantize(x10, self.format)
        a1 = self.dense5_1(x10)
        a1 = quantize(a1, self.format)
        a2 = self.softmax5_1(a1)
        a2 = quantize(a2, self.format)
        b = self.dense5_2(x10)
        b = quantize(b, self.format)

        string = ['x0', 'x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'a1', 'a2', 'b']

        print(f'Quantization {self.format} for input size of {list(x0.shape)} in the Rnet Model')

        # 把所有的输入输出的定点化结果储存位 txt 文件
        if self.save_in_out:
            self.save(x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a1, a2, b)

        return x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a1, a2, b, string

class ONet(nn.Module):

    """
    MTCNN Onet
    """

    def __init__(self):

        super().__init__()

        self.conv1 = nn.Conv2d(3, 32, kernel_size=3)
        self.prelu1 = nn.PReLU(32)
        self.pool1 = nn.MaxPool2d(3, 2, ceil_mode=True)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3)
        self.prelu2 = nn.PReLU(64)
        self.pool2 = nn.MaxPool2d(3, 2, ceil_mode=True)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3)
        self.prelu3 = nn.PReLU(64)
        self.pool3 = nn.MaxPool2d(2, 2, ceil_mode=True)
        self.conv4 = nn.Conv2d(64, 128, kernel_size=2)
        self.prelu4 = nn.PReLU(128)
        self.dense5 = nn.Linear(1152, 256)
        self.prelu5 = nn.PReLU(256)
        self.dense6_1 = nn.Linear(256, 2)
        self.softmax6_1 = nn.Softmax(dim=1)
        self.dense6_2 = nn.Linear(256, 4)
        self.dense6_3 = nn.Linear(256, 10)

    def forward(self, x):

        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)
        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)
        x = self.conv4(x)
        x = self.prelu4(x)
        x = x.permute(0, 3, 2, 1).contiguous()
        x = self.dense5(x.view(x.shape[0], -1))
        x = self.prelu5(x)
        a = self.dense6_1(x)
        a = self.softmax6_1(a)
        b = self.dense6_2(x)
        c = self.dense6_3(x)

        return b, c, a

