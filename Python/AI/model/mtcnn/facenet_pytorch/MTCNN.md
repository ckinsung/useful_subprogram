# MTCNN

# Facenet Pytorch

## mtcnn.py

### \__init__

```python
def __init__(
    self, image_size=160, margin=0, min_face_size=20,
    thresholds=[0.6, 0.7, 0.7], factor=0.709, post_process=True,
    select_largest=True, selection_method=None, keep_all=False, device=None
):
    super().__init__()

    self.image_size = image_size
    self.margin = margin
    self.min_face_size = min_face_size
    self.thresholds = thresholds
    self.factor = factor
    self.post_process = post_process
    self.select_largest = select_largest
    self.keep_all = keep_all
    self.selection_method = selection_method

    self.pnet = PNet()
    self.rnet = RNet()
    self.onet = ONet()

    self.device = torch.device('cpu')
    if device is not None:
        self.device = device
        self.to(device)

        if not self.selection_method:
            self.selection_method = 'largest' if self.select_largest else 'probability'
```

#### Keywords

```python
image_size {int} -- Output image size in pixels. The image will be square. (default: {160})

margin {int} -- Margin to add to bounding box, in terms of pixels in the final image. 

Note that the application of the margin differs slightly from the davidsandberg/facenet
repo, which applies the margin to the original image before resizing, making the margin
dependent on the original image size (this is a bug in davidsandberg/facenet).
(default: {0})

min_face_size {int} -- Minimum face size to search for. (default: {20})

thresholds {list} -- MTCNN face detection thresholds (default: {[0.6, 0.7, 0.7]})

factor {float} -- Factor used to create a scaling pyramid of face sizes. (default: {0.709})

post_process {bool} -- Whether or not to post process images tensors before returning.
(default: {True})

select_largest {bool} -- 
If True, if multiple faces are detected, the largest is returned.
If False, the face with the highest detection probability is returned.
(default: {True})

selection_method {string} -- Which heuristic to use for selection. Default None. 
If specified, will override select_largest:
    "probability": highest probability selected
    "largest": largest box selected
    "largest_over_theshold": largest box over a certain probability selected
    "center_weighted_size": box size minus weighted squared offset from image center
    (default: {None})

    keep_all {bool} -- 
    If True, all detected faces are returned, in the order dictated by the
    select_largest parameter. If a save_path is specified, the first face is saved to that
    path and the remaining faces are saved to <save_path>1, <save_path>2 etc.
    (default: {False})

    device {torch.device} -- The device on which to run neural net passes. Image tensors and
    models are copied to this device before running forward passes. (default: {None})
```

## Forward

```python

def forward(self, img, save_path=None, return_prob=False):

    """Run MTCNN face detection on a PIL image or numpy array. This method performs both
            detection and extraction of faces, returning tensors representing detected faces rather
            than the bounding boxes. To access bounding boxes, see the MTCNN.detect() method below.

            Arguments:
                img {PIL.Image, np.ndarray, or list} -- A PIL image, np.ndarray, torch.Tensor, or list.

            Keyword Arguments:
                save_path {str} -- An optional save path for the cropped image. Note that when
                    self.post_process=True, although the returned tensor is post processed, the saved
                    face image is not, so it is a true representation of the face in the input image.
                    If `img` is a list of images, `save_path` should be a list of equal length.
                    (default: {None})
                return_prob {bool} -- Whether or not to return the detection probability.
                    (default: {False})

            Returns:
                Union[torch.Tensor, tuple(torch.tensor, float)] -- If detected, cropped image of a face
                    with dimensions 3 x image_size x image_size. Optionally, the probability that a
                    face was detected. If self.keep_all is True, n detected faces are returned in an
                    n x 3 x image_size x image_size tensor with an optional list of detection
                    probabilities. If `img` is a list of images, the item(s) returned have an extra 
                    dimension (batch) as the first dimension.

            Example:
            >>> from facenet_pytorch import MTCNN
            >>> mtcnn = MTCNN()
            >>> face_tensor, prob = mtcnn(img, save_path='face.png', return_prob=True)
            """

    # Detect faces
    batch_boxes, batch_probs, batch_points = self.detect(img, landmarks=True)

    # Select faces
    if not self.keep_all:
        batch_boxes, batch_probs, batch_points = self.select_boxes(
            batch_boxes, batch_probs, batch_points, img, method=self.selection_method
        )

        # Extract faces
        faces = self.extract(img, batch_boxes, save_path)

        if return_prob:
            return faces, batch_probs
        else:
            return faces
```



## Useful Functions

### Save Coefficients for each layer

```python
import os
import numpy as np
from facenet_pytorch import MTCNN
mtcnn = MTCNN(keep_all=True, device='cpu')

def obtain(model):

    # Convert model to string
    string_model = str(next(model.modules()))

    # Split the string with '\n'
    string_split = string_model.split('\n')

    # Remove spacing
    output = [i.replace(' ', '') for i in string_split]

    # Remove the first and last row
    output = output[1:-1]

    return output

def save_all_coefficients_to_txt(model, name_list, name_model):

    # Put the coefficients inside a folder with model as its name
    if os.path.isdir(name_model) is False:
        os.mkdir(name_model)

        for i, name in enumerate(model):

            print(model[i])
            saved_folder = os.path.join(name_model, name_list[i])
            print(saved_folder)
            if os.path.isdir(saved_folder) is False:
                os.mkdir(saved_folder)

                # Split the model's name and parameters for each line
                param = [j for j in re.split(pattern='\:|\(|\,|\=|\)', string=model[i]) if j is not '']

                if 'Conv2d' in name:

                    # Extract the kernel size
                    kernel_size = (int(param[5]), int(param[6]))

                    # Define filename to be saved
                    w_fn = f'ker{param[3]}_h{param[5]}_w{param[6]}_d{param[2]}_stride{param[8]}'
                    b_fn = f'ker{param[3]}'
                    w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')
                    b_fn2 = os.path.join(saved_folder, f'{name_list[i]}_bias_{b_fn}.txt')

                    # Extract the coefficients
                    w = eval(f'mtcnn.{name_model}.{name_list[i]}.weight.detach().numpy()')
                    b = eval(f'mtcnn.{name_model}.{name_list[i]}.bias.detach().numpy()')

                    # Save all the coefficients for both weight and bias
                    np.savetxt(w_fn2, w.reshape(-1, kernel_size[1]), fmt='%.32f')
                    np.savetxt(b_fn2, b.reshape(-1, 1), fmt='%.32f')

                elif 'PReLU' in name:

                    # Define filename to be saved
                    w_fn = f'ker{param[3]}'
                    w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')

                    # Extract the coefficients
                    w = eval(f'mtcnn.{name_model}.{name_list[i]}.weight.detach().numpy()')

                    # Save all the coefficients for the weight
                    np.savetxt(w_fn2, w.reshape(-1, 1), fmt='%.32f')

                elif 'Linear' in name:

                    size = (int(param[3]), int(param[5]))

                    # Define filename to be saved
                    w_fn = f'in{param[3]}_out{param[5]}'
                    b_fn = f'{param[5]}'
                    w_fn2 = os.path.join(saved_folder, f'{name_list[i]}_weight_{w_fn}.txt')
                    b_fn2 = os.path.join(saved_folder, f'{name_list[i]}_bias_{b_fn}.txt')

                    # Extract the coefficients
                    w = eval(f'mtcnn.{name_model}.{name_list[i]}.weight.detach().numpy()')
                    b = eval(f'mtcnn.{name_model}.{name_list[i]}.bias.detach().numpy()')

                    # Save all the coefficients for both weight and bias
                    np.savetxt(w_fn2, w.T, fmt='%.32f')
                    np.savetxt(b_fn2, b.reshape(-1, 1), fmt='%.32f')

                else:
                    pass

if __name__ == '__main__':

    # Details of layers
    pnet_model = obtain(mtcnn.pnet)
    rnet_model = obtain(mtcnn.rnet)
    onet_model = obtain(mtcnn.onet)

    # Name for each layer
    pnet_name_list = [name.split(':')[0][1:-1] for name in pnet_model]
    rnet_name_list = [name.split(':')[0][1:-1] for name in rnet_model]
    onet_name_list = [name.split(':')[0][1:-1] for name in onet_model]

    # Save all the coefficients of the corresponding model
    save_all_coefficients_to_txt(pnet_model, pnet_name_list, 'pnet')
    save_all_coefficients_to_txt(rnet_model, rnet_name_list, 'rnet')
    save_all_coefficients_to_txt(onet_model, onet_name_list, 'onet')
```



### Save Forward 

#### Save all the bounding boxes 

Need to input the counter for the ith (i) image

##### Pnet

```python
# Save bounding boxes for pnet
img_cks = imgs[0, :, :, :].permute(1, 2, 0).detach().numpy().astype('uint8')
for ib in range(len(x)):
    # 在人脸画一个窗口
    cv2.rectangle(img_cks, (x[ib], y[ib]), (ex[ib], ey[ib]), color=(0, 255, 0), thickness=1)

cv2.imshow('img', img_cks)
cv2.imwrite(f'img_pnet_{i:04d}.jpg', img_cks)
```

##### Rnet

```python
# Save bounding boxes for pnet
img_cks = imgs[0, :, :, :].permute(1, 2, 0).detach().numpy().astype('uint8')
for ib in range(len(boxes)):
    # 在人脸画一个窗口
    x1, y1, x2, y2 = boxes[ib][0], boxes[ib][1], boxes[ib][2], boxes[ib][3]
    img_cks = cv2.rectangle(img_cks, pt1=(x1, y1), pt2=(x2, y2), color=(0, 255, 255), thickness=1)
    
cv2.imshow('img', img_cks)
cv2.imwrite(f'img_rnet_{i:04d}.jpg', img_cks)
```

##### Method2

###### Pnet

```python
import copy as copy
img_plot = copy.deepcopy(img[0,0,:,:]).detach().numpy()
img_plot = (255*(img_plot - img_plot.min())/(img_plot.max() - img_plot.min())).astype('uint8')
img_plot = np.dstack([img_plot]*3)

for i in range(len(boxes)):

	# Start coordinate, here (5, 5)
	# represents the top left corner of rectangle
	start_point = (x1[i], y1[i])
	  
	# Ending coordinate, here (220, 220)
	# represents the bottom right corner of rectangle
	end_point = (x2[i], y2[i])
	  
	# Blue color in BGR
	color = (0, 255, 0)
	  
	# Line thickness of 2 px
	thickness = 1
	  
	# Using cv2.rectangle() method
	# Draw a rectangle with blue line borders of thickness of 2 px
	cv2.rectangle(img_plot, start_point, end_point, color, thickness)

cv2.imshow('img', img_plot)
```

###### Rnet

```python
import copy as copy
img_plot = copy.deepcopy(img[0,0,:,:]).detach().numpy()
img_plot = (255*(img_plot - img_plot.min())/(img_plot.max() - img_plot.min())).astype('uint8')
img_plot = np.dstack([img_plot]*3)

for i in range(len(boxes)):

	# Start coordinate, here (5, 5)
	# represents the top left corner of rectangle
	start_point = (int(boxes[i][0]), int(boxes[i][1]))
	  
	# Ending coordinate, here (220, 220)
	# represents the bottom right corner of rectangle
	end_point = (int(boxes[i][2]), int(boxes[i][3]))
	  
	# Blue color in BGR
	color = (0, 255, 0)
	  
	# Line thickness of 2 px
	thickness = 1

	# Using cv2.rectangle() method
	# Draw a rectangle with blue line borders of thickness of 2 px
	cv2.rectangle(img_plot, start_point, end_point, color, thickness)

cv2.imshow('img', img_plot)

cv2.imwrite('imgc.jpg', img_plot)
```



#### Save all the inputs and outputs

Save the input and output values for each step of the neural network

##### Pnet

```python
def forward(self, x):
    
    np.savetxt(f'input_conv1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.conv1(x)
    np.savetxt(f'input_prelu1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.prelu1(x)
    np.savetxt(f'input_pool1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.pool1(x)
    np.savetxt(f'input_conv2_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.conv2(x)
    np.savetxt(f'input_prelu2_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.prelu2(x)
    np.savetxt(f'input_conv3_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.conv3(x)
    np.savetxt(f'input_prelu3_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.prelu3(x)
    np.savetxt(f'input_conv4_1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    a = self.conv4_1(x)
    np.savetxt(f'input_softmax4_1_h{a.shape[2]}_w{a.shape[3]}_d{a.shape[1]}.txt', a.reshape(-1, a.shape[3]), fmt='%.32f')
    a = self.softmax4_1(a)
    np.savetxt(f'output_softmax4_1_h{a.shape[2]}_w{a.shape[3]}_d{a.shape[1]}.txt', a.reshape(-1, a.shape[3]), fmt='%.32f')
    np.savetxt(f'input_conv4_2_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    b = self.conv4_2(x)
    np.savetxt(f'output_conv4_2_h{b.shape[2]}_w{b.shape[3]}_d{b.shape[1]}.txt', b.reshape(-1, b.shape[3]), fmt='%.32f')
    
    return b, a
```



##### Rnet

```python
def forward(self, x):
    x = x[0, :, :, :].unsqueeze(0)  # Select the first proposed image
    np.savetxt(f'input_conv1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.conv1(x)
    np.savetxt(f'input_prelu1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.prelu1(x)
    np.savetxt(f'input_pool1_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.pool1(x)
    np.savetxt(f'input_conv2_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.conv2(x)
    np.savetxt(f'input_prelu2_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.prelu2(x)
    np.savetxt(f'input_pool2_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.pool2(x)
    np.savetxt(f'input_conv3_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.conv3(x)
    np.savetxt(f'input_prelu3_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = self.prelu3(x)
    np.savetxt(f'input_permute_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    x = x.permute(0, 3, 2, 1).contiguous()
    np.savetxt(f'input_flatten_h{x.shape[2]}_w{x.shape[3]}_d{x.shape[1]}.txt', x.reshape(-1, x.shape[3]), fmt='%.32f')
    np.savetxt(f'input_dense4_len{x.numel()}.txt', x.view(-1, 1), fmt='%.32f')
    x = self.dense4(x.view(x.shape[0], -1))
    np.savetxt(f'input_prelu4_len{x.numel()}.txt', x.reshape(-1, 1), fmt='%.32f')
    x = self.prelu4(x)
    np.savetxt(f'input_dense5_1_len{x.numel()}.txt', x.reshape(-1, 1), fmt='%.32f')
    a = self.dense5_1(x)
    np.savetxt(f'input_softmax5_1_len{a.numel()}.txt', a.reshape(-1, 1), fmt='%.32f')
    a = self.softmax5_1(a)
    np.savetxt(f'input_dense5_2_len{x.numel()}.txt', x.reshape(-1, 1), fmt='%.32f')
    np.savetxt(f'output_softmax5_1_len{a.numel()}.txt', a.reshape(-1, 1), fmt='%.32f')
    b = self.dense5_2(x)
    np.savetxt(f'output_dense5_2_len{b.numel()}.txt', b.reshape(-1, 1), fmt='%.32f')
    return b, a
```



Save bounding box

```python
import copy as copy
img_plot = copy.deepcopy(img[0,0,:,:]).detach().numpy()
img_plot = (255*(img_plot - img_plot.min())/(img_plot.max() - img_plot.min())).astype('uint8')
img_plot = np.dstack([img_plot]*3)

for i in range(4):

	# Start coordinate, here (5, 5)
	# represents the top left corner of rectangle
	start_point = (x1[i], y1[i])
	  
	# Ending coordinate, here (220, 220)
	# represents the bottom right corner of rectangle
	end_point = (x2[i], y2[i])
	  
	# Blue color in BGR
	color = (0, 255, 0)
	  
	# Line thickness of 2 px
	thickness = 1
	  

	# Using cv2.rectangle() method
	# Draw a rectangle with blue line borders of thickness of 2 px
	cv2.rectangle(img_plot, start_point, end_point, color, thickness)

cv2.imshow('img', img_plot)


#######################################################################

import copy as copy
img_plot = copy.deepcopy(img[0,0,:,:]).detach().numpy()
img_plot = (255*(img_plot - img_plot.min())/(img_plot.max() - img_plot.min())).astype('uint8')
img_plot = np.dstack([img_plot]*3)

for i in range(len(boxes)):

	# Start coordinate, here (5, 5)
	# represents the top left corner of rectangle
	start_point = (int(boxes[i][0]), int(boxes[i][1]))
	  
	# Ending coordinate, here (220, 220)
	# represents the bottom right corner of rectangle
	end_point = (int(boxes[i][2]), int(boxes[i][3]))
	  
	# Blue color in BGR
	color = (0, 255, 0)
	  
	# Line thickness of 2 px
	thickness = 1

	# Using cv2.rectangle() method
	# Draw a rectangle with blue line borders of thickness of 2 px
	cv2.rectangle(img_plot, start_point, end_point, color, thickness)

cv2.imshow('img', img_plot)

cv2.imwrite('imgf.jpg', img_plot)
```



