import torch
from torch.utils.data import DataLoader
from package.face_dataset import FaceDataset
import os


class Trainer:

    def __init__(self, net, param_path, data_path):

        # 检测是否有GPU
        self.device = torch.device(device='cuda:0') if torch.cuda.is_available() else torch.device(device="cpu")

        # 把模型搬到device
        self.net = net.to(self.device)

        # 参数地址
        self.param_path = param_path

        # 打包数据
        self.datasets = FaceDataset(data_path)

        # 定义损失函数：类别判断（分类任务）
        self.cls_loss_func = torch.nn.BCELoss()

        # 定义损失函数：框的偏置回归
        self.offset_loss_func = torch.nn.MSELoss()

        # 定义损失函数：关键点的偏置回归
        self.point_loss_func = torch.nn.MSELoss()

        # 定义优化器
        self.optimizer = torch.optim.Adam(params=self.net.parameters(), lr=1e-3)

    def train(self, stop_value, landmark=False):

        """
            - 断点续传 --> 短点续训
            - transfer learning 迁移学习
            - pretrained model 预训练

        :param stop_value:
        :param landmark:
        :return:
        """

        # 加载上次训练的参数 (断点续训)(最好是把每一次的都记录下来)(有很多的概念)
        if os.path.exists(self.param_path):
            self.net.load_state_dict(torch.load(self.param_path))
        else:
            print("NO Param")

        # 封装数据加载器
        dataloader = DataLoader(self.datasets, batch_size=512, shuffle=True)

        epochs = 0

        while True:

            # 训练一轮
            for i, (img_data, _cls, _offset, _point) in enumerate(dataloader):

                # 数据搬家
                img_data = img_data.to(self.device)
                _cls = _cls.to(self.device)
                _offset = _offset.to(self.device)
                _point = _point.to(self.device)

                # PNet 与 RNet 输出两个；ONet 输出三个
                if landmark:
                    out_cls, out_offset, out_point = self.net(img_data)
                else:
                    out_cls, out_offset = self.net(img_data)

                # [B, 1, 1, 1] --> [B, 1]
                out_cls = out_cls.view(-1, 1)

                # [B, 4, 1, 1] --> [B, 4]
                out_offset = out_offset.view(-1, 4)

                ###########################################################################################

                # 选取置信度为0，1的正负样本求置信度损失
                # 0: 负样本 1：正样本 2：偏样本 (从 dataloder 里, 选择有脸和没脸的标签)
                cls_mask = torch.lt(_cls, 2)  # which is the same as -> cls_mask = _cls < 2 (class 只有 0 和 1呀)

                # 筛选复合条件的样本的：标签 (从 dataloder 里, 提取有脸和没脸的标签)
                cls = torch.masked_select(_cls, cls_mask)  # which is the same as cls = _cls[cls_mask]

                # 筛选符合条件的样本的：预测值 (从 net 的输出里, 提取dataloader相应有脸和没脸的标签)
                out_cls = torch.masked_select(out_cls, cls_mask)  # which is the same as out_cls = out_cls[cls_mask]

                # 求解分类的loss
                cls_loss = self.cls_loss_func(out_cls, cls)

                ###########################################################################################

                # 选取正样本和部分样本求偏移率的损失

                # 选取正样本和偏样本 (负样本没有窗口呀) (从 dataloader 里, 选择有脸的标签)
                offset_mask = torch.gt(_cls, 0)

                # bbox 的标签
                offset = torch.masked_select(_offset, offset_mask)

                # bbox 的预测值
                out_offset = torch.masked_select(out_offset, offset_mask)

                # bbox 损失
                offset_loss = self.offset_loss_func(out_offset, offset)

                ###########################################################################################

                # 定义最终的损失函数
                if landmark:

                    # 正和偏样本的 landmark 损失
                    point = torch.masked_select(_point, offset_mask)
                    out_point = torch.masked_select(out_point, offset_mask)
                    point_loss = self.point_loss_func(out_point, point)

                    # O-Net 的 最终损失！！！！！！！
                    # cls_loss (分类而言把偏样本踢了，只求正样本负样本的损失)
                    # offset_loss(把负样本踢出去了，只求正样本偏样本的损失)
                    # point_loss(把负样本踢出去了，只求正样本偏样本的损失)
                    # 类别的损失 + 偏移的损失 + 关键点的损失
                    # 如果只有负样本，会怎么样? : 只能求 cls_loss (分类)
                    # 如果只有正样本，会怎么样? : 能求 loss
                    # 如果只有偏样本，会怎么样? : 没有 cls_loss, 只能求 offset_loss 和 point_loss
                    # 如果没有 shuffle, 梯度会跑偏
                    # 都是同个权重的定义损失 (如果人脸很重要, 可以对 cls_loss 加权)
                    loss = cls_loss + offset_loss + point_loss

                else:

                    # P-Net和R-Net 的 最终损失！！！！！！！
                    loss = cls_loss + offset_loss

                if landmark:
                    # dataloader.set_description(
                    #     "epochs:{}, loss:{:.4f}, cls_loss:{:.4f}, offset_loss:{:.4f}, point_loss:{:.4f}".format(
                    #         epochs, loss.float(), cls_loss.float(), offset_loss.float(), point_loss.float()))
                    print("loss:{0:.4f}, cls_loss:{1:.4f}, offset_loss:{2:.4f}, point_loss:{3:.4f}".format(
                        loss.float(), cls_loss.float(), offset_loss.float(), point_loss.float()))
                else:
                    # dataloader.set_description(
                    #     "epochs:{}, loss:{:.4f}, cls_loss:{:.4f}, offset_loss:{:.4f}".format(
                    #         epochs, loss.float(), cls_loss.float(), offset_loss.float()))
                    print("loss:{0:.4f}, cls_loss:{1:.4f}, offset_loss:{2:.4f}".format(
                        loss.float(), cls_loss.float(), offset_loss.float()))

                # 清空梯度
                self.optimizer.zero_grad()

                # 梯度回传
                loss.backward()

                # 优化
                self.optimizer.step()

            # 保存模型（参数）
            torch.save(self.net.state_dict(), self.param_path)

            # 轮次加1
            epochs += 1

            print(f'Epoch = {epochs}')

            # 设定误差限制
            if loss < stop_value:
                break
