import torch
from torch import nn

class PNet(nn.Module):

    """
    MTCNN Pnet model
    """

    def __init__(self):

        super().__init__()

        # 第一层-卷积
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3, stride=1, padding=0)
        self.prelu1 = nn.PReLU(num_parameters=10)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)

        # 第二层-卷积
        self.conv2 = nn.Conv2d(in_channels=10, out_channels=16, kernel_size=3, stride=1, padding=0)
        self.prelu2 = nn.PReLU(num_parameters=16)

        # 第三层-卷积
        self.conv3 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=1, padding=0)
        self.prelu3 = nn.PReLU(num_parameters=32)

        # 第四层-人脸概率
        self.conv4_1 = nn.Conv2d(in_channels=32, out_channels=1, kernel_size=1, stride=1, padding=0)

        # 第四层-人脸窗口的偏移/误差
        self.conv4_2 = nn.Conv2d(in_channels=32, out_channels=4, kernel_size=1, stride=1, padding=0)

    def forward(self, x):

        # 第一层-卷积
        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        # 第二层-卷积
        x = self.conv2(x)
        x = self.prelu2(x)

        # 第三层-卷积
        x = self.conv3(x)
        x = self.prelu3(x)

        # 第四层-人脸概率
        cls = self.conv4_1(x)
        cls = torch.sigmoid(cls)

        # 第四层-人脸窗口的偏移/误差
        bbox_offset = self.conv4_2(x)

        return cls, bbox_offset

class RNet(nn.Module):

    """
    MTCNN Rnet model
    """

    def __init__(self):

        super().__init__()

        # 第一层-卷积
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=28, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=28)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)

        # 第二层-卷积
        self.conv2 = nn.Conv2d(in_channels=28, out_channels=48, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=48)
        self.pool2 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)

        # 第三层-卷积
        self.conv3 = nn.Conv2d(in_channels=48, out_channels=64, kernel_size=2)
        self.prelu3 = nn.PReLU(num_parameters=64)

        # 第四层-全连接
        self.dense4 = nn.Linear(in_features=576, out_features=128)
        self.prelu4 = nn.PReLU(num_parameters=128)

        # 第五层-人脸概率
        self.dense5_1 = nn.Linear(in_features=128, out_features=1)

        # 第五层-人脸窗口的偏移/误差
        self.dense5_2 = nn.Linear(in_features=128, out_features=4)

    def forward(self, x):

        # 第一层-卷积
        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        # 第二层-卷积
        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        # 第三层-卷积
        x = self.conv3(x)
        x = self.prelu3(x)

        # 向量化
        x = x.view(x.shape[0], -1)

        # 第四层-全连接
        x = self.dense4(x)
        x = self.prelu4(x)

        # 第五层-人脸概率
        cls = self.dense5_1(x)
        cls = torch.sigmoid(cls)

        # 第五层-人脸窗口的偏移/误差
        bbox_offset = self.dense5_2(x)

        return cls, bbox_offset

class ONet(nn.Module):

    """
    MTCNN Onet model
    """

    def __init__(self):

        super().__init__()

        # 第一层-卷积
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=3)
        self.prelu1 = nn.PReLU(num_parameters=32)
        self.pool1 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)

        # 第二层-卷积
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3)
        self.prelu2 = nn.PReLU(num_parameters=64)
        self.pool2 = nn.MaxPool2d(kernel_size=3, stride=2, ceil_mode=True)

        # 第三层-卷积
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3)
        self.prelu3 = nn.PReLU(num_parameters=64)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)

        # 第四层-卷积
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=2)
        self.prelu4 = nn.PReLU(num_parameters=128)

        # 第五层-全连接
        self.dense5 = nn.Linear(in_features=1152, out_features=256)
        self.prelu5 = nn.PReLU(num_parameters=256)

        # 第六层-人脸概率
        self.dense6_1 = nn.Linear(in_features=256, out_features=1)

        # 第六层-人脸窗口的偏移/误差
        self.dense6_2 = nn.Linear(in_features=256, out_features=4)

        # 第六层-人脸关键点的偏差
        self.dense6_3 = nn.Linear(in_features=256, out_features=10)

    def forward(self, x):

        # 第一层-卷积
        x = self.conv1(x)
        x = self.prelu1(x)
        x = self.pool1(x)

        # 第二层-卷积
        x = self.conv2(x)
        x = self.prelu2(x)
        x = self.pool2(x)

        # 第三层-卷积
        x = self.conv3(x)
        x = self.prelu3(x)
        x = self.pool3(x)

        # 第四层-卷积
        x = self.conv4(x)
        x = self.prelu4(x)

        # 向量化
        x = x.view(x.shape[0], -1)

        # 第五层-全连接
        x = self.dense5(x)
        x = self.prelu5(x)

        # 第六层-人脸概率
        cls = self.dense6_1(x)
        cls = torch.sigmoid(cls)

        # 第六层-人脸窗口的偏移/误差
        bbox_offset = self.dense6_2(x)

        # 第六层-人脸关键点的偏差
        landmarks_offset = self.dense6_3(x)

        return cls, bbox_offset, landmarks_offset
