import os
import glob
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from bs4 import BeautifulSoup
from PIL import Image, ImageDraw

path = os.path.join(os.getcwd(), 'CelebA datasets', 'Img2_with_mask', 'mask_dataset', 'train')

store_data = []

for img_file, xml_file in zip(glob.glob(path + '\*.jpg'), glob.glob(path + '\*.xml')):

    # Reading the data inside the xml
    # file to a variable under the name
    # data
    with open(xml_file, 'r') as f:
        data = f.read()

    # Passing the stored data inside
    # the beautifulsoup parser, storing
    # the returned object
    Bs_data = BeautifulSoup(data, "xml")

    # Finding all instances of tag
    # `xmin`
    xmin = Bs_data.find_all('xmin')
    ymin = Bs_data.find_all('ymin')
    xmax = Bs_data.find_all('xmax')
    ymax = Bs_data.find_all('ymax')

    # print(xmin, ymin, xmax, ymax)
    for wordxmin, wordymin, wordxmax, wordymax in zip(xmin, ymin, xmax, ymax):
        x1 = int(wordxmin.text)
        y1 = int(wordymin.text)
        x2 = int(wordxmax.text)
        y2 = int(wordymax.text)
        width, height = x2 - x1, y2 - y1
        store_data.append(f'{os.path.basename(img_file)} {wordxmin.text} {wordymin.text} {width} {height}')

    img = Image.open(img_file)

    # create rectangle image
    img = ImageDraw.Draw(img)
    img.rectangle([(x1, y1), (x2, y2)], outline="red")
    img.show()

# open file in write mode
with open(r'list_bbox_img_mask.txt', 'w') as fp:
    for item in store_data:
        # write each item on a new line
        fp.write("%s\n" % item)
    print('Done')