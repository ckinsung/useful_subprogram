# from package.model import nets2 as nets
from package.model import nets_with_mask as nets
import os
# from package import train
from package import train_mask as train

if __name__ == '__main__':

    # 权重存放地址
    param_path = "./param/p_net_mask2.pth"

    # 数据存放地址
    data_path = "./generate_samples/test_data/MTCNN_with_mask/12"

    # 如果没有这个参数存放目录，则创建一个目录
    if not os.path.exists("param"):
        os.makedirs("param")

    # 加载模型
    pnet = nets.PNet()

    # 开始训练
    t = train.Trainer(net=pnet, param_path=param_path, data_path=data_path)
    t.train(stop_value=1e-3)
