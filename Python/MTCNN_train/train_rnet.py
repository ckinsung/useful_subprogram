# from package.model import nets as nets
from package.model import nets2 as nets
import os
from package import train

if __name__ == '__main__':

    # 权重存放地址
    param_path = r"param/r_net_test2.pth"

    # 数据存放地址
    data_path = r"./generate_samples/test_data/MTCNN/24"

    # 如果没有这个参数存放目录，则创建一个目录
    if not os.path.exists("param"):
        os.makedirs("param")

    # 加载模型
    net = nets.RNet()

    # 开始训练
    t = train.Trainer(net, param_path, data_path)

    t.train(0.001)
