from package.model import nets
import os
from package import train

if __name__ == '__main__':
    param_path = r"param/o_net.pth"
    data_path = r"./generate_samples/test_data/MTCNN\48"
    if not os.path.exists("param"):
        os.makedirs("param")
    net = nets.ONet()
    t = train.Trainer(net, param_path, data_path)
    t.train(0.001, landmark=True)
