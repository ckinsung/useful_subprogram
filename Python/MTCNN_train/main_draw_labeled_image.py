"""
    把标注数据集里的图像导入
    把标注的框和关键点绘在图上
"""

import os
from PIL import Image
import random
random.seed(1)
import matplotlib.pyplot as plt
import numpy as np

box_path = r'.\CelebA datasets\Anno\list_bbox_celeba.txt'  # 人脸框数据路径
mark_path = r'.\CelebA datasets\Anno\list_landmarks_celeba.txt'  # 人脸关键点数据路径
img_path = r'.\CelebA datasets\Img\img_celeba'  # 图像数据路径

# 随机抽取一张图
shuffle_index = list(range(0, 202599))
random.shuffle(shuffle_index)
shuffle_index = np.array(shuffle_index)

with open(box_path, 'r') as f:

    annos = f.readlines()
    annos = np.array(annos[2:])[shuffle_index]

with open(mark_path, 'r') as f:

    mark = f.readlines()
    mark = np.array(mark[2:])[shuffle_index]

for anno, mark in zip(annos, mark):

    anno = anno.split()

    file_name, box = anno[0].strip(), [int(x.strip()) for x in anno[1:]]

    image = Image.open(os.path.join(img_path, file_name)).convert('RGB')

    mark = mark.split()[1:]

    x = []
    y = []
    for i in range(0, 10, 2):
        # print(i)
        x.append(int(mark[i]))
        y.append(int(mark[i + 1]))

    plt.clf()
    ax = plt.gca()  # 获取到当前坐标轴信息
    ax.xaxis.set_ticks_position('top')  # 将X坐标轴移到上面
    ax.invert_yaxis()
    # print(x, y)
    plt.imshow(image)
    plt.scatter(x, y)
    ax.add_patch(plt.Rectangle((box[0], box[1]), box[2], box[3] * 0.9, fill=False, linewidth=2, color='red'))
    plt.show()
    plt.pause(1)
