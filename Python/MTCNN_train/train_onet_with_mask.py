from package.model import nets_with_mask as nets
import os
# from package import train
from package import train_mask as train

if __name__ == '__main__':
    param_path = r"param/o_net_mask2.pth"
    data_path = r"./generate_samples/test_data/MTCNN_with_mask2\48"
    if not os.path.exists("param"):
        os.makedirs("param")
    net = nets.ONet()
    t = train.Trainer(net, param_path, data_path)
    # t.train(0.001, landmark=False)
    t.train(1e-5, landmark=False)
