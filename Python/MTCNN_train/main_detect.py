"""
    检测图像和视频的主函数
    验证模型的效果
"""

from PIL import Image
import cv2
import os
from package.fast_detect import Detector

def run_img(img_path):

    """
    把图像导入做前向计算
    :param img_path: 图像路径
    :return:
    """

    out_img = "./generate_samples/out_img"
    os.makedirs("./generate_samples/out_img", exist_ok=True)

    # 读取图像
    img = Image.open(img_path)

    # 人脸检测
    detect_boxes = detector.detect(img)

    # 绘制结果
    if len(detect_boxes) == 0:  # 如果没有框

        onet_boxes = detect_boxes

    else:

        pnet_boxes, rnet_boxes, onet_boxes = detect_boxes
        print("pnet:", pnet_boxes.shape)
        print("rnet:", rnet_boxes.shape)
        print("onet:", onet_boxes.shape)

    # 读取原始图
    img = cv2.imread(img_path)

    # 如果有框，把 ONet 的框绘出来
    if onet_boxes.shape[0] != 0:
        for box in onet_boxes:
            x1, y1, x2, y2 = int(box[0]), int(box[1]), int(box[2]), int(box[3])
            cv2.rectangle(img, (x1, y1), (x2, y2), color=(0, 0, 255), thickness=2)
            for i in range(5, 15, 2):
                cv2.circle(img, (int(box[i]), int(box[i + 1])), radius=1, color=(255, 255, 0), thickness=-1)

    # cv2.imwrite(os.path.join(out_img, img_name), img)
    cv2.imshow("img", img)
    cv2.waitKey(0)

def run_video(video_path):

    """
    把视频导入做前向计算
    :param video_path: 视频路径
    :return:
    """

    # 读取视频
    cap = cv2.VideoCapture(video_path)

    # get size and fps of video
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # 帧播放速率
    fps = cap.get(cv2.CAP_PROP_FPS)

    # VideoWriter_fourcc为视频编解码器
    # cv2.VideoWriter_fourcc('F', 'L', 'V', '1'),该参数是Flash视频，文件名后缀为.flv
    # cv2.VideoWriter_fourcc('P', 'I', 'M', 'I'),该参数是MPEG-1编码类型，文件名后缀为.avi
    fourcc = cv2.VideoWriter_fourcc('M', 'P', '4', '2')

    # create VideoWriter for saving 文件名中需要有数字编号
    save_name = os.path.join(os.path.dirname(video_path), os.path.basename(video_path)[0:-4] + '_label' + '.avi')
    outVideo = cv2.VideoWriter(save_name, fourcc, fps, (width, height))

    c = 0
    while cap.isOpened():

        ret, frame = cap.read()

        if ret == True:

            timeF = 1  # 每一帧检测一次

            if c % timeF == 0:

                img = frame[..., ::-1]
                img = Image.fromarray(img)
                detect_boxes = detector.detect(img)

                if len(detect_boxes) == 0:

                    onet_boxes = detect_boxes

                else:

                    pnet_boxes, rnet_boxes, onet_boxes = detect_boxes

                if onet_boxes.shape[0] != 0:

                    for box in onet_boxes:

                        x1, y1, x2, y2 = int(box[0]), int(box[1]), int(box[2]), int(box[3])
                        cv2.rectangle(frame, (x1, y1), (x2, y2), color=(0, 0, 255), thickness=2)

                        for i in range(5, 15, 2):

                            cv2.circle(frame, (int(box[i]), int(box[i + 1])), radius=1, color=(255, 255, 0),
                                       thickness=-1)
            c += 1
            # 将处理后的图片存入输出视频
            outVideo.write(frame)

            cv2.imshow('video', frame)
            c = cv2.waitKey(1)
            # 27表示Esc键
            if c == 27:
                break

        else:

            print("视频播放结束")
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':

    # 加载训练模型参数
    param_path = "param/param_well_trained"
    p_net, r_net, o_net = [os.path.join(param_path, "p_net.pth"),
                           os.path.join(param_path, "r_net.pth"),
                           os.path.join(param_path, "o_net.pth")]

    # 设定模型阈值
    detector = Detector(p_net, r_net, o_net, softnms=False, thresholds=[0.6, 0.7, 0.95])

    # 图片前向计算
    # img_path = os.path.join("./generate_samples/detect_img", "05.jpg")
    # run_img(img_path)

    # 视频前向计算
    video_path = os.path.join("./generate_samples/detect_video", "test01.mp4")
    run_video(video_path)
