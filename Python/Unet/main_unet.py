import numpy as np
from package.model.unet_model import Unet
import torch
from torch import nn
from torch import optim
from torchvision.transforms import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from PIL import Image
from matplotlib import pyplot as plt
import os
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
device = 'cuda:0' if torch.cuda.is_available() else 'cpu'


def make_dataset(root):

    '''
    生成图像与其mask路径的列表
    :param root:
    :return:
    '''

    import glob
    imgs_path = []

    # 循环所有带有 .png 和 _mask.png 字样的图的路径
    for img_path, mask_path in zip(glob.glob(root + '/*[0-9].png'), glob.glob(root + '/*[0-9]_mask.png')):

        # 添加至列表
        imgs_path.append((img_path, mask_path))

    return imgs_path


class LiverDataset(Dataset):

    def __init__(self, root, transform=None, target_transform=None):

        # 读取所有的 liver 图像 + liver mask 图像
        imgs_path = make_dataset(root)

        self.imgs_path = imgs_path
        self.transform = transform
        self.target_transform = target_transform

    def __getitem__(self, index):

        # 相关索引下的图像路径 x_path 和其 mask 的路径 y_path
        x_path, y_path = self.imgs_path[index]

        # 打开祥光图像
        img_x = Image.open(x_path)
        img_y = Image.open(y_path)

        if self.transform is not None:
            img_x = self.transform(img_x)

        if self.target_transform is not None:
            img_y = self.target_transform(img_y)

        return img_x, img_y

    def __len__(self):

        # 返回所有图像的数量
        return len(self.imgs_path)


def train_model(model, loss_fn, optimizer, dataloader, num_epochs=20):

    for epoch in range(num_epochs):

        print(f'Epoch {epoch}/{num_epochs-1}')
        print('-'*10)
        dt_size = dataloader.dataset.__len__()
        epoch_loss = 0
        step = 0

        for img, mask_target in dataloader:

            step += 1

            img = img.to(device=device)
            mask_target = mask_target.to(device=device)

            # 正向传播
            mask_predict = model(img)

            # 计算损失
            loss = loss_fn(mask_predict, mask_target)

            # 清空梯度
            optimizer.zero_grad()

            # 反向传播
            loss.backward()

            # 更新参数
            optimizer.step()

            epoch_loss += loss.item()

            if step % 200 == 0:
                print('%d/%d, train_loss:%0.3f' % (step, (dt_size-1)//dataloader.batch_size+1, loss.item()))

        print('epoch %d loss:%0.3f' % (epoch, epoch_loss))

        # Save model
        torch.save(model.state_dict(), "state_dict_model.pt")


def validation(model, dataloader, saved_folder):

    model.eval()

    with torch.no_grad():

        for i, (img, mask_target) in enumerate(dataloader):

            plt.figure(1)

            # 绘图 (真实图像)
            plt.subplot(1, 3, 1)
            plt.imshow(img[0, :, :, :].permute(1, 2, 0).numpy())
            plt.axis('off')
            plt.title('Image')

            # 绘图 (目标图像真实值 mask)
            plt.subplot(1, 3, 2)
            plt.imshow(mask_target[0, 0, :, :].numpy().astype('uint8'))
            plt.axis('off')
            plt.title('Mask Target')

            # 绘图 (图像预测值 mask)
            plt.subplot(1, 3, 3)
            mask_pred = model(img.to(device=device))[0, 0, :, :].to(device='cpu').numpy()
            plt.imshow((mask_pred*255).astype('uint8'))
            plt.axis('off')
            plt.title('Mask Predict')
            plt.pause(0.01)

            filename = saved_folder + 'new_%d.png' % i
            plt.savefig(filename)
            # Image.fromarray((mask_pred * 255).astype('uint8')).convert('L').save(filename)

# 数据转换
x_transforms = transforms.Compose([transforms.ToTensor(),  # 转换成 H C W  + Tensor 格式, 归一化 [0, 1]
                                   transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])])  # 归一化 [-0.5, 0.5]
y_transforms = transforms.ToTensor()

# 构建数据
batch_size = 1
train_dataset = LiverDataset(root='data/liver/train', transform=x_transforms, target_transform=y_transforms)
train_dataloader = DataLoader(dataset=train_dataset, batch_size=batch_size, shuffle=True)
test_dataset = LiverDataset(root='data/liver/val', transform=x_transforms, target_transform=y_transforms)
test_dataloader = DataLoader(dataset=test_dataset, batch_size=batch_size)

# 定义模型
net = Unet(in_ch=3, out_ch=1).to(device=device)  # 输入图像有3个通道，标签图像有1个通道

# 定义损失函数
loss_fn = torch.nn.BCELoss()

# 定义优化器
optimizer = optim.Adam(params=net.parameters(), lr=1e-3)

# 训练模型
train_model(model=net, loss_fn=loss_fn, optimizer=optimizer, dataloader=train_dataloader, num_epochs=20)

# 预测
# path = "package/pt_files/state_dict_model.pt"
path = "state_dict_model.pt"
net.load_state_dict(torch.load(path))
validation(model=net, dataloader=test_dataloader, saved_folder='results/liver/predict/')
