import torch
from torch import nn


# U_Net模型中的双卷积网络结构
class DoubleConv(nn.Module):
    '''
        double convolution 双卷积
        把两个卷积运算封装成一个函数
    '''

    def __init__(self, in_ch, out_ch):
        super(DoubleConv, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=in_ch, out_channels=out_ch, kernel_size=3, padding=1),  # padding -> 输入输出图像大小相同
            nn.BatchNorm2d(num_features=out_ch),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=out_ch, out_channels=out_ch, kernel_size=3, padding=1),
            nn.BatchNorm2d(num_features=out_ch),
            nn.ReLU(inplace=True)
        )

    def forward(self, input):
        return self.conv(input)


class Unet(nn.Module):

    def __init__(self, in_ch, out_ch):

        '''
        :param in_ch: 输入图像的层数 RGB (in_ch=3); 灰度图 (in_ch=1)
        :param out_ch: 输出图像 (mask) 的层数 (一般都是1层结果)
        '''

        super(Unet, self).__init__()

        ############################# Encoder 编码 #############################

        self.conv1 = DoubleConv(in_ch=in_ch, out_ch=64)  # 特征图大小不变
        self.pool1 = nn.MaxPool2d(kernel_size=2)  # 特征图大小长宽减半

        self.conv2 = DoubleConv(in_ch=64, out_ch=128)
        self.pool2 = nn.MaxPool2d(kernel_size=2)

        self.conv3 = DoubleConv(in_ch=128, out_ch=256)
        self.pool3 = nn.MaxPool2d(kernel_size=2)

        self.conv4 = DoubleConv(in_ch=256, out_ch=512)
        self.pool4 = nn.MaxPool2d(kernel_size=2)

        self.conv5 = DoubleConv(in_ch=512, out_ch=1024)

        ############################# Decoder 解码  #############################

        # 长宽翻倍，通道数减半
        self.up6 = nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=2, stride=2)
        self.conv6 = DoubleConv(in_ch=1024, out_ch=512)
        self.up7 = nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=2, stride=2)
        self.conv7 = DoubleConv(in_ch=512, out_ch=256)
        self.up8 = nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=2, stride=2)
        self.conv8 = DoubleConv(in_ch=256, out_ch=128)
        self.up9 = nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=2, stride=2)
        self.conv9 = DoubleConv(in_ch=128, out_ch=64)
        self.conv10 = nn.Conv2d(in_channels=64, out_channels=out_ch, kernel_size=1)

    def forward(self, x):
        c1 = self.conv1(x)
        p1 = self.pool1(c1)
        c2 = self.conv2(p1)
        p2 = self.pool2(c2)
        c3 = self.conv3(p2)
        p3 = self.pool3(c3)
        c4 = self.conv4(p3)
        p4 = self.pool4(c4)
        c5 = self.conv5(p4)

        up_6 = self.up6(c5)
        # 通道维拼接
        merge6 = torch.cat([up_6, c4], dim=1)
        c6 = self.conv6(merge6)
        up_7 = self.up7(c6)
        merge7 = torch.cat([up_7, c3], dim=1)
        c7 = self.conv7(merge7)
        up_8 = self.up8(c7)
        merge8 = torch.cat([up_8, c2], dim=1)
        c8 = self.conv8(merge8)
        up_9 = self.up9(c8)
        merge9 = torch.cat([up_9, c1], dim=1)
        c9 = self.conv9(merge9)
        c10 = self.conv10(c9)
        out = nn.Sigmoid()(c10)

        return out
