import os
import moviepy.video.io.ImageSequenceClip
import natsort

image_folder = 'results/dev_gesture_recognition/csv/20210813 见易峰晨客户后调整手势姿态手掌不对着传感器/02_手掌停止_拳头停止_左右上下/demo jpg'
fps = 60

image_files = [image_folder+'/'+img for img in os.listdir(image_folder) if img.endswith(".jpg")]
image_files = natsort.natsorted(image_files, reverse=False)
image_files = image_files[0:4600]
clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
clip.write_videofile('my_video.mp4')