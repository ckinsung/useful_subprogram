#!/usr/bin/env python
# encoding: utf-8
'''
@author: xiaobo wang
@contact: beidinangai@163.com
@file: mobilefacenet.py
@time: 2019/04/29 15:45
@desc: mobilefacenet backbone
'''

import torch
import torch.nn.functional as F
import torch.nn as nn
import math

def l2_norm(x, axis=1):
    norm = torch.norm(x, 2, axis, True)
    output = x / norm
    return output

class BottleNeck(nn.Module):
    def __init__(self, inp, oup, stride, expansion):
        super(BottleNeck, self).__init__()
        self.connect = stride == 1 and inp == oup

        self.conv = nn.Sequential(
            # 1*1 conv
            nn.Conv2d(inp, inp * expansion, 1, 1, 0, bias=False),
            nn.BatchNorm2d(inp * expansion),
            nn.PReLU(inp * expansion),

            # 3*3 depth wise conv
            nn.Conv2d(inp * expansion, inp * expansion, 3, stride, 1, groups=inp * expansion, bias=False),
            nn.BatchNorm2d(inp * expansion),
            nn.PReLU(inp * expansion),

            # 1*1 conv
            nn.Conv2d(inp * expansion, oup, 1, 1, 0, bias=False),
            nn.BatchNorm2d(oup),
        )

    def forward(self, x):
        if self.connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class ConvBlock(nn.Module):
    def __init__(self, inp, oup, k, s, p, dw=False, linear=False):
        super(ConvBlock, self).__init__()
        self.linear = linear
        if dw:
            self.conv = nn.Conv2d(inp, oup, k, s, p, groups=inp, bias=False)
        else:
            self.conv = nn.Conv2d(inp, oup, k, s, p, bias=False)

        self.bn = nn.BatchNorm2d(oup)
        if not linear:
            self.prelu = nn.PReLU(oup)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        if self.linear:
            return x
        else:
            return self.prelu(x)


class MobileFaceNet_Our(nn.Module):
    def __init__(self, feature_dim=512, aug=16):
        super(MobileFaceNet_Our, self).__init__()

        aug = aug
        kernel_size = 4
        self.conv1 = ConvBlock(3, aug * kernel_size, 3, 2, 1)
        self.dw_conv1 = ConvBlock(aug * kernel_size, aug * kernel_size, 3, 1, 1, dw=True)

        bottleneck_setting = [
            # t, c , n ,s
            [2, aug * kernel_size, 5*2, 2],
            [4, 2 * aug * kernel_size, 1*2, 2],
            [2, 2 * aug * kernel_size, 6*2, 1],
            [4, 2 * aug * kernel_size, 1*2, 2],
            [2, 2 * aug * kernel_size, 2*2, 1]
        ]
        self.cur_channel = aug * kernel_size
        block = BottleNeck
        self.blocks = self._make_layer(block, bottleneck_setting)

        self.conv2 = ConvBlock(2 * aug * kernel_size, 8 * aug * kernel_size, 1, 1, 0)
        self.linear7 = ConvBlock(8 * aug * kernel_size, 8 * aug * kernel_size, 7, 1, 0, dw=True, linear=True)  # for 112 size
        #self.linear7 = ConvBlock(8 * aug * kernel_size, 8 * aug * kernel_size, 9, 1, 0, dw=True, linear=True)  # for 144 size
        self.linear1 = ConvBlock(8 * aug * kernel_size, feature_dim, 1, 1, 0, linear=True)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, setting):
        layers = []
        for t, c, n, s in setting:
            for i in range(n):
                if i == 0:
                    layers.append(block(self.cur_channel, c, s, t))
                else:
                    layers.append(block(self.cur_channel, c, 1, t))
                self.cur_channel = c

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.dw_conv1(x)
        x = self.blocks(x)
        x = self.conv2(x)
        x = self.linear7(x)
        x = self.linear1(x)
        x = x.view(x.size(0), -1)

        return F.normalize(x)


if __name__ == "__main__":
    input = torch.Tensor(2, 3, 112, 112)
    net = MobileFaceNet_Our(feature_dim=512, aug=32) # aug = 1, 2, 4, 8, default=16, 32
    print(net)

    x = net(input)
    print(x.shape)
