import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models._utils as _utils

# from retinaface.models.net import FPN as FPN
# from retinaface.models.net import MobileNetV1 as MobileNetV1
# from retinaface.models.net import SSH as SSH

from net import FPN as FPN
from net import MobileNetV1 as MobileNetV1
from net import SSH as SSH


class ClassHead(nn.Module):
    def __init__(self, inchannels=512, num_anchors=3):
        super(ClassHead, self).__init__()
        self.num_anchors = num_anchors
        self.conv1x1 = nn.Conv2d(in_channels=inchannels, out_channels=self.num_anchors * 2, kernel_size=(1, 1), stride=1, padding=0)

    def forward(self, x):
        out = self.conv1x1(x)
        out = out.permute(0, 2, 3, 1).contiguous()

        return out.view(out.shape[0], -1, 2)


class BboxHead(nn.Module):
    def __init__(self, inchannels=512, num_anchors=3):
        super(BboxHead, self).__init__()
        self.conv1x1 = nn.Conv2d(in_channels=inchannels, out_channels=num_anchors * 4, kernel_size=(1, 1), stride=1, padding=0)

    def forward(self, x):
        out = self.conv1x1(x)
        out = out.permute(0, 2, 3, 1).contiguous()

        return out.view(out.shape[0], -1, 4)


class LandmarkHead(nn.Module):
    def __init__(self, inchannels=512, num_anchors=3):
        super(LandmarkHead, self).__init__()
        self.conv1x1 = nn.Conv2d(inchannels, num_anchors * 10, kernel_size=(1, 1), stride=1, padding=0)

    def forward(self, x):
        out = self.conv1x1(x)
        out = out.permute(0, 2, 3, 1).contiguous()

        return out.view(out.shape[0], -1, 10)


class RetinaFace(nn.Module):
    def __init__(self, cfg=None, phase='train'):
        """
        :param cfg:  Network related settings.
        :param phase: train or test.
        """
        super(RetinaFace, self).__init__()
        self.phase = phase
        # backbone = MobileNetV1()
        if cfg['name'] == 'mobilenet0.25':
            backbone = MobileNetV1()
            if cfg['pretrain']:
                checkpoint = torch.load("./weights/mobilenetV1X0.25_pretrain.tar", map_location=torch.device('cpu'))
                from collections import OrderedDict
                new_state_dict = OrderedDict()
                for k, v in checkpoint['state_dict'].items():
                    name = k[7:]  # remove module.
                    new_state_dict[name] = v
                # load params
                backbone.load_state_dict(new_state_dict)

        elif cfg['name'] == 'Resnet50':

            # Import the ResNet model
            import torchvision.models as models
            backbone = models.resnet50(pretrained=cfg['pretrain'])  # cfg['pretrain'] -> False

        # Use the model as backbone (actually without the last fully connected layer)
        # 去除了最后一层的全连接层作为 backbone
        # cfg['return_layers'] -> {'layer2': 1, 'layer3': 2, 'layer4': 3} Name layer2 as 1, layer3 as 2, ...
        self.body = _utils.IntermediateLayerGetter(model=backbone, return_layers=cfg['return_layers'])

        in_channels_stage2 = cfg['in_channel']
        in_channels_list = [
            in_channels_stage2 * 2,
            in_channels_stage2 * 4,
            in_channels_stage2 * 8,
        ]

        out_channels = cfg['out_channel']
        self.fpn = FPN(in_channels_list=in_channels_list, out_channels=out_channels)
        self.ssh1 = SSH(in_channel=out_channels, out_channel=out_channels)
        self.ssh2 = SSH(in_channel=out_channels, out_channel=out_channels)
        self.ssh3 = SSH(in_channel=out_channels, out_channel=out_channels)

        self.ClassHead = self._make_class_head(fpn_num=3, inchannels=cfg['out_channel'])
        self.BboxHead = self._make_bbox_head(fpn_num=3, inchannels=cfg['out_channel'])
        self.LandmarkHead = self._make_landmark_head(fpn_num=3, inchannels=cfg['out_channel'])

    def _make_class_head(self, fpn_num=3, inchannels=64, anchor_num=2):
        classhead = nn.ModuleList()
        for i in range(fpn_num):
            classhead.append(ClassHead(inchannels=inchannels, num_anchors=anchor_num))
        return classhead

    def _make_bbox_head(self, fpn_num=3, inchannels=64, anchor_num=2):
        bboxhead = nn.ModuleList()
        for i in range(fpn_num):
            bboxhead.append(BboxHead(inchannels=inchannels, num_anchors=anchor_num))
        return bboxhead

    def _make_landmark_head(self, fpn_num=3, inchannels=64, anchor_num=2):
        landmarkhead = nn.ModuleList()
        for i in range(fpn_num):
            landmarkhead.append(LandmarkHead(inchannels, anchor_num))
        return landmarkhead

    def forward(self, inputs):

        # Backbone (MobileNetV1 or ResNet50)
        # inputs.shape = [1, 3, 333, 500]
        # out[1] ResNet50 layer2 (文章 conv3_x) 的输出 out[1].shape = [1, 512, 42, 63]
        # out[2] ResNet50 layer3 (文章 conv4_x) 的输出 out[2].shape = [1, 1024, 21, 32]
        # out[3] ResNet50 layer4 (文章 conv5_x) 的输出 out[3].shape = [1, 2048, 11, 16]
        out = self.body(inputs)

        # FPN (Feature Pyramid Network)
        # fpn[0].shape -> [1, 256, 42, 63]
        # fpn[1].shape -> [1, 256, 21, 32]
        # fpn[2].shape -> [1, 256, 11, 16])
        fpn = self.fpn(out)

        # SSH (Single Stage Headless Face Detector)
        feature1 = self.ssh1(fpn[0])
        feature2 = self.ssh2(fpn[1])
        feature3 = self.ssh3(fpn[2])
        features = [feature1, feature2, feature3]

        # 这里并没有把 feature concat 在一起再用 detecion head, 而是先分别用
        # detecion head 后再 concat 起来
        # bbox_regressions.shape=(1050, 4), 因为每个点有两个 anchor,  (400+100+25) * 2 = 1050
        bbox_regressions = torch.cat([self.BboxHead[i](feature) for i, feature in enumerate(features)], dim=1)
        classifications = torch.cat([self.ClassHead[i](feature) for i, feature in enumerate(features)], dim=1)
        ldm_regressions = torch.cat([self.LandmarkHead[i](feature) for i, feature in enumerate(features)], dim=1)

        if self.phase == 'train':
            output = (bbox_regressions, classifications, ldm_regressions)
        else:
            output = (bbox_regressions, F.softmax(classifications, dim=-1), ldm_regressions)
        return output


if __name__ == "__main__":

    cfg_mnet = {
        'name': 'mobilenet0.25',
        'min_sizes': [[16, 32], [64, 128], [256, 512]],
        'steps': [8, 16, 32],
        'variance': [0.1, 0.2],
        'clip': False,
        'loc_weight': 2.0,
        'gpu_train': True,
        'batch_size': 32,
        'ngpu': 1,
        'epoch': 250,
        'decay1': 190,
        'decay2': 220,
        'image_size': 640,
        'pretrain': False,
        'return_layers': {'stage1': 1, 'stage2': 2, 'stage3': 3},
        'in_channel': 32,
        'out_channel': 64
    }
    cfg_re50 = {
        'name': 'Resnet50',
        'min_sizes': [[16, 32], [64, 128], [256, 512]],
        'steps': [8, 16, 32],
        'variance': [0.1, 0.2],
        'clip': False,
        'loc_weight': 2.0,
        'gpu_train': True,
        'batch_size': 24,
        'ngpu': 4,
        'epoch': 100,
        'decay1': 70,
        'decay2': 90,
        'image_size': 840,
        'pretrain': False,
        'return_layers': {'layer2': 1, 'layer3': 2, 'layer4': 3},
        'in_channel': 256,
        'out_channel': 256
    }
    fd = RetinaFace(cfg=cfg_re50)

    inputs = torch.randn(size=(1, 3, 640, 640))
    output = fd(inputs)
