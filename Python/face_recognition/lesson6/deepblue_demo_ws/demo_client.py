#coding=utf-8
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"
import cv2
import numpy as np
import os
from retinaface.detector import RetinafaceDetector
from backbone.arcfacenet import SEResNet_IR
from backbone.mobilefacenet_our import MobileFaceNet_Our
import torch
import torch.nn.functional as F
from align_faces import align_face


def show_candidates(filename, poi):
    image = cv2.imread(filename)
    for g in poi:
        x, y, r, b = list(map(int, g[0]))
        lms = g[2]
        for j in range(lms.shape[0]):
            cv2.circle(image, (int(lms[j, 0]), int(lms[j, 1])), 2, (0, 255, 0), 2)

        cv2.rectangle(image, (x, y), (r, b), (0,255,0), 2, 16)
        pos = (x + 3, y - 5)
        content = '%s sim %.3f' % (g[1], g[3])
        cv2.putText(image, content, pos, 0, 0.5, (0, 255, 0), 2, 16)
    pure_name = os.path.split(filename)[1]
    cv2.imwrite("./show/{}".format(pure_name), image)


if __name__ == "__main__":

    # Face Recognition System
    device = torch.device('cpu')

    # face detector net
    fd = RetinafaceDetector(device)

    # face recognition net
    fr = MobileFaceNet_Our(feature_dim=512, aug=32)
    fr.load_state_dict(torch.load('./backbone/epoch_15.pt', map_location=device)['state_dict'], strict=True)
    #face_model.load_state_dict(torch.load('./backbone/epoch_15.pt', map_location='cpu')['state_dict'], strict=True)
    fr.eval()
    recog_threshold = 0.32

    output_size = 112

    # 'build' or 'test'
    phase = 'test'

    # build gallery
    if phase == 'build':

        gallery_dir = 'target'

        # we assume that each subdirectory contains images of the same identity
        dirs = os.listdir(gallery_dir)
        num_images = 0
        dir2files = {}

        for d in dirs:
            sd = os.path.join(gallery_dir, d)
            if os.path.isdir(sd):
                files = os.listdir(sd)
                images = []
                for e in files:
                    ext = os.path.splitext(e)[1].lower()
                    if ext in ('.jpg', '.jpeg', '.png', '.bmp'):
                        images.append(os.path.join(sd, e))
                if len(images) > 0:
                    assert d not in dir2files
                    dir2files[d] = images
                    num_images += len(images)
        # Now build gallery feature database
        feat_dim = -1
        dir2feat = {}
        num_galleries = 0
        for d, file_path_list in dir2files.items():
            if len(file_path_list) > 0:
                input_tensor_list = []
                for i, fp in enumerate(file_path_list): # fp represents file path
                    cv_img = cv2.imread(fp)
                    bboxes, lms = fd.detect_faces(cv_img)
                    if bboxes.shape[0] > 0:
                        # detect face in image whose path is given by fp
                        # We assume that ground truth face is one that has maximum area. This assumption may be wrong.
                        # So one can give ground truth bbox if you know the ground truth face's position so we can skip
                        # detection phase and directly use what we've already known to get the landmark.
                        area = (bboxes[:, 2] - bboxes[:, 0] + 1) * (bboxes[:, 3] - bboxes[:, 1] + 1)
                        max_index = np.argmax(area)
                        max_face_lm = lms[max_index]
                        fr_input_cv_image = align_face(cv_img, max_face_lm)
                        input_tensor_list.append(torch.from_numpy(fr_input_cv_image.transpose((2, 0, 1)).astype(np.float32) / 255.0))
                if len(input_tensor_list) > 0:
                    with torch.no_grad():
                        feat = fr(torch.stack(input_tensor_list).to(device)).cpu()
                        feat_dim = feat.shape[1]
                    dir2feat[d] = feat
                    num_galleries += feat.shape[0]
        # Now we save gallery feature database to disk...
        gallery_feature = torch.zeros(num_galleries, feat_dim)
        s = 0
        e = 0
        idx2id = {}
        for d, feat in dir2feat.items():
            s = e
            e += feat.shape[0]
            gallery_feature[s:e] = feat
            for idx in range(s, e):
                idx2id[idx] = d
        torch.save({'gallery_feat': gallery_feature, 'meta_info': idx2id}, 'gallery_meta.pt')

    elif phase == 'test':
        print('load gallery features from disk...')
        disk_info = torch.load('gallery_meta.pt')
        gallery_feat = disk_info['gallery_feat']
        idx2id = disk_info['meta_info']
        test_dir = 'test'
        test_list = []
        for root, dirs, files in os.walk(test_dir):
            for fn in files:
                ext = os.path.splitext(fn)[1].lower()
                if ext in ('.jpg', '.bmp', '.png'):
                    test_image_path = os.path.join(root, fn)
                    test_list.append(test_image_path)
        for fp in test_list:

            # Read the image file with face
            cv_img = cv2.imread(fp)

            # Detect the face and output the bounding box and landmarks
            bbox, lms = fd.detect_faces(cv_img)

            if bbox.shape[0] > 0:

                # Store all the aligned faces to a variable
                input_blob = torch.zeros(bbox.shape[0], 3, output_size, output_size)

                # Loop through all the detected faces
                for i in range(bbox.shape[0]):

                    # Align the face of each detected faces
                    cropped_image = align_face(cv_img=cv_img, landmark=lms[i])

                    # Convert aligned face image from [0, 255] to [0, 1]
                    input_blob[i] = torch.from_numpy(cropped_image.transpose((2, 0, 1)).astype(np.float32) / 255.0)

                # for each face detected in fp, we'll compare them with gallery feature
                with torch.no_grad():

                    # probe features: shape is #probe x #dim [bbox.shape[0], feature_dim]
                    probe_feat = fr(input_blob.to(device)).cpu()

                # shape is #probe x #gallery (probability of each probe face with the gallery face)
                # Inner Product = Cosine Similarity to get the scores
                sim_scores = F.linear(input=probe_feat, weight=gallery_feat)

                # max_idx_per_probe = torch.argmax(sim_scores, 1)
                # Each probe faces with its best matched face id in the gallery
                max_scores_per_probe, max_idx_per_probe = torch.max(input=sim_scores, dim=1)

                # Considered a hit if more than the threshold
                hit = torch.where(max_scores_per_probe >= recog_threshold)[0]

                if hit.numel() == 0:

                    print('No gallery found in %s' % fp)

                else:

                    poi = []

                    for probe_idx in hit:

                        # Extract the bounding box for the matched probe face
                        box = [e for e in bbox[probe_idx.item(), :4]]

                        # Identify the gallery id
                        most_similar_gallery_id = max_idx_per_probe[probe_idx.item()].item()

                        # Get the score of the probe face with the matched id
                        sim = max_scores_per_probe[probe_idx.item()].item()

                        # Append the information of the matched probe face with the gallery ID
                        # [bounding box, ID's name, landmarks of probe face, cosine similarity value]
                        poi.append([box, idx2id[most_similar_gallery_id], lms[probe_idx.item()], sim])

                    # Plot the image and the bounding box for the matched ID
                    show_candidates(filename=fp, poi=poi)

    else:
        raise Exception('Unknown phase.')
