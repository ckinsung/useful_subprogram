import skimage.transform as trans
import numpy as np
import cv2

def align_face(cv_img, landmark, output_size=112):

    src = np.array([[38.2946, 51.6963],
                    [73.5318, 51.5014],
                    [56.0252, 71.7366],
                    [41.5493, 92.3655],
                    [70.7299, 92.2041]], dtype=np.float32)

    tr = trans.SimilarityTransform()

    tr.estimate(src=landmark, dst=src)

    M = tr.params[0:2, :]

    res_img = cv2.warpAffine(src=cv_img, M=M, dsize=(output_size, output_size), borderValue=0.0)

    return res_img