import torch
import torch.nn as nn


def ResNet18():

    model = ResNet(block=BasicBlock, layers=[2, 2, 2, 2])

    return model


def ResNet34():

    model = ResNet(block=BasicBlock, layers=[3, 4, 6, 3])

    return model


def ResNet50():

    model = ResNet(block=BottleneckBlock, layers=[3, 4, 6, 3])

    return model


def ResNet101():

    model = ResNet(block=BottleneckBlock, layers=[3, 4, 23, 3])

    return model


def ResNet152():

    model = ResNet(block=BottleneckBlock, layers=[3, 8, 36, 3])

    return model


class BasicBlock(nn.Module):

    # According to ResNet's paper, expansion is 1 for Basic Block
    expansion = 1

    def __init__(self, inplanes, planes, stride=1):

        """
            :param inplanes: input channel size
            :param planes: output channel size
            :param stride: stride for the first conv1 layer of the Basic Block
        """

        super(BasicBlock, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=inplanes, out_channels=planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(num_features=planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(in_channels=planes, out_channels=planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(num_features=planes)
        self.stride = stride

        # Ensure the the channels dimensions are the same for summation
        self.downsample = nn.Sequential()

        # If the input channels and output channels do not have the same channel size, make them the same dimension
        if stride != 1 or inplanes != self.expansion*planes:
            self.downsample = nn.Sequential(
                nn.Conv2d(in_channels=inplanes, out_channels=self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(num_features=self.expansion*planes)
            )

    def forward(self, x):

        # 1st layer of convolution
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        # 2nd layer of convolution
        out = self.conv2(out)
        out = self.bn2(out)

        # Downsample the input: x to have the same channel dimension as out
        out += self.downsample(x)
        out = self.relu(out)

        return out


class BottleneckBlock(nn.Module):

    # According to ResNet's paper, expansion is 4 for Bottleneck Block
    expansion = 4

    def __init__(self, inplanes, planes, stride=1):

        """
            :param inplanes: input channel size
            :param planes: output channel size
            :param stride: stride for the first conv1 layer of the Bottleneck Block
        """

        super(BottleneckBlock, self).__init__()

        # Define the layers for the block
        self.conv1 = nn.Conv2d(in_channels=inplanes, out_channels=planes, kernel_size=1, stride=stride, bias=False)
        self.bn1 = nn.BatchNorm2d(num_features=planes)
        self.conv2 = nn.Conv2d(in_channels=planes, out_channels=planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(num_features=planes)
        self.conv3 = nn.Conv2d(in_channels=planes, out_channels=self.expansion*planes, kernel_size=1, stride=1, bias=False)
        self.bn3 = nn.BatchNorm2d(num_features=self.expansion*planes)
        self.relu = nn.ReLU(inplace=True)

        # Ensure the the channels dimensions are the same for summation
        self.downsample = nn.Sequential()  # skip and do nothing

        # If the input channels and output channels do not have the same channel size, make them the same dimension
        if stride != 1 or inplanes != self.expansion*planes:
            self.downsample = nn.Sequential(
                nn.Conv2d(in_channels=inplanes, out_channels=self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(num_features=self.expansion*planes)
            )

    def forward(self, x):

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)
        out = self.bn3(out)

        # Downsample the input: x to have the same channel dimension as out
        out += self.downsample(x)
        out = self.relu(out)

        return out


class ResNet(nn.Module):

    def __init__(self, block, layers, feature_dim=512, drop_ratio=0.4, zero_init_residual=False):

        super(ResNet, self).__init__()

        self.inplanes = 64

        # input block, name conv1 in the ResNet paper
        self.input_layer = nn.Sequential(nn.Conv2d(in_channels=3, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
                                         nn.BatchNorm2d(num_features=64),
                                         nn.ReLU(inplace=True),
                                         nn.MaxPool2d(kernel_size=3, stride=2, padding=1))

        # 4 big blocks
        self.conv2_x = self._make_layer(block=block, planes=64, num_blocks=layers[0], stride=1)
        self.conv3_x = self._make_layer(block=block, planes=128, num_blocks=layers[1], stride=2)
        self.conv4_x = self._make_layer(block=block, planes=256, num_blocks=layers[2], stride=2)
        self.conv5_x = self._make_layer(block=block, planes=512, num_blocks=layers[3], stride=2)

        # output block
        self.output_layer = nn.Sequential(nn.BatchNorm2d(num_features=512 * block.expansion),
                                          nn.Dropout(p=drop_ratio),
                                          nn.Flatten(),
                                          nn.Linear(in_features=512 * block.expansion * 7 * 7, out_features=feature_dim),
                                          nn.BatchNorm1d(num_features=feature_dim))

        # Initialization of the weights of the model
        for layer in self.modules():
            if isinstance(layer, nn.Conv2d):
                nn.init.kaiming_normal_(tensor=layer.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(layer, nn.BatchNorm2d):
                nn.init.constant_(tensor=layer.weight, val=1)
                nn.init.constant_(tensor=layer.bias, val=0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for layer in self.modules():
                if isinstance(layer, BasicBlock):
                    nn.init.constant_(tensor=layer.bn2.weight, val=0)

    def _make_layer(self, block, planes, num_blocks, stride=1):

        """
        :param block: block class (can be Basic Block or Bottleneck Block)
        :param planes: output channel size
        :param num_blocks: total number of blocks in the body
        :param stride: stride for the first conv layer in the Basic Block / Bottleneck Block
        :return:
        a big block consists of multiple small blocks
        """

        # Only use the input stride (can be 1 or 2) for the first block
        # Subsequent block only use stride = 1
        strides = [stride] + [1]*(num_blocks-1)

        blocks = []

        # Append block to blocks
        for stride in strides:
            blocks.append(block(inplanes=self.inplanes, planes=planes, stride=stride))
            self.inplanes = planes * block.expansion

        return nn.Sequential(*blocks)

    def forward(self, x):

        # input block, name conv1 in the ResNet paper
        x = self.input_layer(x)

        # 4 blocks
        x = self.conv2_x(x)
        x = self.conv3_x(x)
        x = self.conv4_x(x)
        x = self.conv5_x(x)

        # output block
        x = self.output_layer(x)

        return x


if __name__ == "__main__":

    input = torch.randn(2, 3, 112, 112)

    # net = ResNet18()
    # net = ResNet34()
    # net = ResNet50()
    # net = ResNet101()
    net = ResNet152()

    print(net)
    print(f'model total parameters = {sum(p.numel() for p in net.parameters())}')

    x = net(input)
    print(x.shape)
