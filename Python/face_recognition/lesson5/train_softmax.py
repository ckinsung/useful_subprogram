#!/usr/bin/env python
# encoding: utf-8
import os
import torch.utils.data
import logging
from torch.nn import DataParallel
from SEResNet_IR import ResNet34, ResNet18
from margin.CosineMarginProduct import CosineMarginProduct
from margin.InnerProduct import InnerProduct
from dataset.casia_webface import CASIAWebFace
from torch.optim import lr_scheduler
import torch.optim as optim
import time
import numpy as np
import torchvision.transforms as transforms
import argparse
import logging as logger
logger.basicConfig(level=logger.INFO, format='%(levelname)s %(asctime)s %(filename)s: %(lineno)d] %(message)s',
                   datefmt='%Y-%m-%d %H:%M:%S')

def train(args):
    # gpu init
    multi_gpus = False
    if len(args.gpus.split(',')) > 1:
        multi_gpus = True
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpus
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    # log init
    if os.path.exists(args.save_dir):
        pass
    else:
        # raise NameError('model dir exists!')
        os.makedirs(args.save_dir)

    # dataset loader
    transform = transforms.Compose([
        transforms.ToTensor(),  # range [0, 255] -> [0.0,1.0]
        transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))  
    ])
    # train dataset
    trainset = CASIAWebFace(args.train_root, args.train_file_list, transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size,
                                              shuffle=True, num_workers=8, drop_last=True)

    # define backbone and margin layer
    net = ResNet34()
    if args.margin_type == 'InnerProduct':
        margin = InnerProduct(in_feature=512, out_feature=trainset.class_nums)
    elif args.margin_type == 'CosFace':
        margin = CosineMarginProduct(in_feature=512, out_feature=trainset.class_nums, s=args.scale_size)
    else:
        logging.fatal(args.margin_type, 'is not available!')
        exit(0)

    if args.resume:
        logging.info('resume the model parameters from: ', args.net_path, args.margin_path)
        net.load_state_dict(torch.load(args.net_path)['net_state_dict'])
        margin.load_state_dict(torch.load(args.margin_path)['fc_state_dict'])

    # define optimizers for different layer
    criterion_classi = torch.nn.CrossEntropyLoss().to(device)
    # optimizer_classi = optim.SGD([
    #     {'params': net.parameters(), 'weight_decay': 5e-4},
    #     {'params': margin.parameters(), 'weight_decay': 5e-4}
    # ], lr=0.1, momentum=0.9, nesterov=True)
    optimizer_classi = optim.Adam([{'params': net.parameters()},
                                   {'params': margin.parameters()}])
    scheduler_classi = lr_scheduler.MultiStepLR(optimizer_classi, milestones=[20, 35, 45], gamma=0.1)

    if multi_gpus:
        net = DataParallel(net).to(device)
        margin = DataParallel(margin).to(device)
    else:
        net = net.to(device)
        margin = margin.to(device)

    for epoch in range(1, args.total_epoch + 1):
        # train model
        logger.info('Train Epoch: {}/{} ...'.format(epoch, args.total_epoch))
        net.train()
        since = time.time()
        batch_idx = 0
        for data in trainloader:
            img, label = data[0].to(device), data[1].to(device)
            feature = net(img)
            output = margin(feature, label)
            loss_classi = criterion_classi(output, label)
            total_loss = loss_classi

            optimizer_classi.zero_grad()
            total_loss.backward()
            optimizer_classi.step()

            batch_idx += 1

            if batch_idx % 200 == 0:
                #current training accuracy
                _, predict = torch.max(output.data, 1)
                total = label.size(0)
                correct = (predict.cpu().numpy() == label.cpu().numpy()).sum()
                time_cur = (time.time() - since) / 100
                since = time.time()
                logger.info("Epochs: {:0>2d}, loss_classi: {:.4f}, train_accuracy: {:.4f}, time: {:.2f} s/iter, learning rate: {}"
                            .format(epoch,loss_classi.item(),correct/total,time_cur,scheduler_classi.get_lr()[0]))

        # save model
        if multi_gpus:
            net_state_dict = net.module.state_dict()
            margin_state_dict = margin.module.state_dict()
        else:
            net_state_dict = net.state_dict()
            margin_state_dict = margin.state_dict()

        if not os.path.exists(args.save_dir):
            os.mkdir(args.save_dir)
        save_name = 'epoch_%d.pt' % epoch
        torch.save({'net_state_dict': net_state_dict, 'fc_state_dict': margin_state_dict},
                   os.path.join(args.save_dir, save_name))

        scheduler_classi.step()
    logger.info('finishing training')


if __name__ == '__main__':

    """
        Codes github address: https://github.com/wujiyang/Face_Pytorch
    """

    parser = argparse.ArgumentParser(description='PyTorch for deep face recognition')

    # Do notice that the imported images have to be aligned (affine transformation)
    # Face cropped from the original images, transformed, interpolated for training
    # data from (D:\AI_dataset\casia-112_112_aligned_choose_this_one.tar.gz or my pan.baidu.com)
    parser.add_argument('--train_root', type=str, default='CASIA-WebFace_cut', help='train image root')
    parser.add_argument('--train_file_list', type=str, default='CASIA-WebFace_list_cut.txt', help='train list')
    
    parser.add_argument('--margin_type', type=str, default='InnerProduct', help='InnerProduct, CosFace')
    parser.add_argument('--scale_size', type=float, default=32.0, help='scale size')
    parser.add_argument('--batch_size', type=int, default=10, help='batch size')
    parser.add_argument('--total_epoch', type=int, default=50, help='total epochs')

    parser.add_argument('--save_freq', type=int, default=2000, help='save frequency')
    parser.add_argument('--test_freq', type=int, default=2000, help='test frequency')
    parser.add_argument('--resume', type=int, default=False, help='resume model')
    parser.add_argument('--net_path', type=str, default='', help='resume model')
    parser.add_argument('--margin_path', type=str, default='', help='resume model')

    parser.add_argument('--save_dir', type=str, default='./checkpoints', help='model save dir')
    parser.add_argument('--gpus', type=str, default='0', help='model prefix')

    args = parser.parse_args()

    train(args)


