import os

def generate_txt(target_dir):
    dirs = os.listdir(target_dir)
    imagenames = {}
    print('Process %s...' % target_dir)
    for d in dirs:
        pd = os.path.join(target_dir, d)
        if os.path.isdir(pd):
            files = os.listdir(pd)
            for e in files:
                ext = os.path.splitext(e)[1].lower()
                if ext in ('.jpg', '.bmp', '.jpeg', '.png'):
                    if (e.endswith(ext)):
                        if d not in imagenames:
                            imagenames[d] = []
                        imagenames[d].append(os.path.join(pd, e))

    next_label = 0
    name2label = {}
    with open('{}_list.txt'.format(target_dir), 'w') as f1:
        for name in imagenames:
            flag = 0
            ii = 0
            if len(imagenames[name]) > 0:
                #print (len(imagenames[name]))
                for e in imagenames[name]:
                    line = '{} {}'.format(e, next_label)
                    f1.write(line + '\n')
                    ii += 1
      

            if len(imagenames[name]) > 0:
                name2label[name] = next_label
                next_label += 1
    print('Finished!')

if __name__ == '__main__':
    target_dir = './CASIA-WebFace'
    generate_txt(target_dir)
