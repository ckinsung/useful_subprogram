import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter


class CosineMarginProduct(nn.Module):

    def __init__(self, in_feature=128, out_feature=10575, s=32.0, m=0.35):

        super(CosineMarginProduct, self).__init__()

        self.in_feature = in_feature
        self.out_feature = out_feature
        self.s = s
        self.m = m
        self.weight = Parameter(torch.Tensor(out_feature, in_feature))
        nn.init.xavier_uniform_(self.weight)


    def forward(self, input, label):

        # assume batch size = 1 sample = 1
        # F.normalize(input) ([1, 512]) -> 1 sample, each sample is a 512 feature vector (This is x)
        # F.normalize(self.weight) ([781, 512]) -> 781 faces (This is W)
        # norm(W_T) dot product with norm(x) = cosine_similarity
        # F.linear -> y = x@W_T + b
        cosine = F.linear(input=F.normalize(input), weight=F.normalize(self.weight), bias=None)

        # Construct the one hot matrix for the true label of each sample (10*781)
        one_hot = torch.zeros_like(cosine)
        one_hot.scatter_(dim=1, index=label.view(-1, 1), value=1.0)

        output = self.s * (cosine - one_hot * self.m)

        return output


if __name__ == '__main__':

    obj = CosineMarginProduct()

    output = obj.forward(input=torch.randn(size=(10, 128)), label=torch.randint(low=0, high=10, size=(10, 128)))

    print(output.shape)