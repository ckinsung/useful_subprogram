#!/usr/bin/env python
# encoding: utf-8

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Parameter

class InnerProduct(nn.Module):

    def __init__(self, in_feature=128, out_feature=10575):

        super(InnerProduct, self).__init__()

        self.in_feature = in_feature
        self.out_feature = out_feature

        self.weight = Parameter(torch.Tensor(out_feature, in_feature))
        nn.init.xavier_uniform_(self.weight)


    def forward(self, input, label):

        # label not used

        # assume batch size = 1 sample = 1
        # input ([1, 512]) -> 1 sample, each sample is a 512 feature vector (This is x)
        # self.weight ([781, 512]) -> 781 faces (This is W)
        # W_T dot product with x
        # F.linear -> y = x@W_T + b
        output = F.linear(input=input, weight=self.weight, bias=None)

        return output


if __name__ == '__main__':

    obj = InnerProduct()

    output = obj.forward(input=torch.randn(size=(10, 128)), label=torch.randint(low=0, high=10, size=(10, 128)))

    print(output.shape)