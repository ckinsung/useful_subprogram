import numpy as np

with open(file='analysis_result_train_inner_product.txt', mode='r+', encoding='utf8') as fd:

    i = 0
    accs = []
    acc_temp = []

    for line in fd:

        print(line)

        if line.count('train_accuracy') == 1:

            temp = float(line[line.find('train_accuracy: ') + 16:line.find(', time: ')])
            acc_temp.append(temp)
            i += 1

        else:

            accs.append(np.array(acc_temp).mean())
            acc_temp = []
            print(i)
            i = 0

import matplotlib.pyplot as plt
plt.figure(1)
plt.clf()
plt.plot(np.array(accs))
plt.xlabel('epoch')
plt.ylabel('train accuracy')
plt.grid()
# plt.title('Cosine Margin Product')
plt.title('Inner Product')
plt.savefig('fig2.jpg')
