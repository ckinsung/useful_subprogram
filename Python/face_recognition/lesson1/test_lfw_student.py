"""
    第1章_人脸识别概述
"""

import numpy as np
import scipy.io
import argparse

def getAccuracy(scores, flags, threshold):

    #请根据输入来计算准确率acc的值

    '''
    scores: 配对得分
    flags: 配对是正是负
    threshold: 输入阈值
    '''

    # 提取正样本对的 scores
    positive_pairs = scores[flags == 1]
    # 提取负样本对的 scores
    negative_pairs = scores[flags == -1]

    positive_pairs_num = np.sum(positive_pairs >= threshold)  # 预判正样本对正确的数量
    negative_pairs_num = np.sum(negative_pairs < threshold)  # 预判负样本对正确的数量
    sum_num = sum([positive_pairs_num, negative_pairs_num])  # 正与负样本对里，预测正确的总数

    # 计算准确率
    acc = sum_num/len(scores)

    return acc

def getThreshold(scores, flags, thrNum):

    # 请根据输入即验证集上来选取最佳阈值，目标是验证集上准确率最大时所对应的阈值平均(可存多个阈值)

    '''
    scores: 验证集配对的得分
    flags: 验证集对是正是负
    thrNum: 采样阈值间隔
    '''

    thresholds = np.arange(-thrNum, thrNum + 1) * 1.0 / thrNum

    # 提取正样本对的 scores
    positive_pairs = scores[flags == 1]
    # 提取负样本对的 scores
    negative_pairs = scores[flags == -1]

    result = []
    for th in thresholds:

        # 选择相关阈值 th 后
        positive_pairs_num = np.sum(positive_pairs >= th)  # 预判正样本对正确的数量
        negative_pairs_num = np.sum(negative_pairs < th)  # 预判负样本对正确的数量
        sum_num = sum([positive_pairs_num, negative_pairs_num])  # 正与负样本对里，预测正确的总数
        result.append([th, sum_num])

    # 把所有保存的数据编程 Numpy Array
    result = np.array(result)

    # 预测最多正确的结果, 则是最好的 threshold 阈值
    arg_max_pred = result[:, 1].argmax()
    bestThreshold = result[arg_max_pred, 0]

    return bestThreshold

def evaluation_10_fold(feature_path='./lfw_result.mat'):

    ACCs = np.zeros(10)

    # Read the matlab .mat file
    # result.keys() -> 'fl', 'fr', 'fold', 'flag'
    # result['fl'].shape -> (6000, 1024)
    # result['fr'].shape -> (6000, 1024)
    # result['fold'].shape -> (1, 6000)
    # result['flag'].shape -> (1, 6000)
    result = scipy.io.loadmat(feature_path)

    for i in range(10):

        fold = result['fold']
        flags = result['flag']   # 6000对样本配对情况，1表示同一个人
        featureLs = result['fl']  # 6000对左边6000样本特征
        featureRs = result['fr']  # 6000对右边6000样本特征

        valFold = fold != i
        testFold = fold == i
        flags = np.squeeze(flags)

        # 减去均值可要可不要
        mu = np.mean(np.concatenate((featureLs[valFold[0], :], featureRs[valFold[0], :]), 0), 0)
        mu = np.expand_dims(mu, 0)
        featureLs = featureLs - mu
        featureRs = featureRs - mu

        # 归一化
        featureLs = featureLs / np.expand_dims(np.sqrt(np.sum(np.power(featureLs, 2), 1)), 1)
        featureRs = featureRs / np.expand_dims(np.sqrt(np.sum(np.power(featureRs, 2), 1)), 1)

        # 匹配值
        scores = np.sum(np.multiply(featureLs, featureRs), 1)

        # 通过验证集寻找最优的阈值
        threshold = getThreshold(scores=scores[valFold[0]], flags=flags[valFold[0]], thrNum=10000)

        # 通过threshold计算测试集的准确率
        ACCs[i] = getAccuracy(scores=scores[testFold[0]], flags=flags[testFold[0]], threshold=threshold)

    return ACCs


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Testing')
    parser.add_argument('--feature_save_path', type=str, default='./lfw_result.mat',
                        help='The path of the extract features save, must be .mat file')
    args = parser.parse_args()
    ACCs = evaluation_10_fold(args.feature_save_path)
    for i in range(len(ACCs)):
        print('{}    {:.2f}'.format(i+1, ACCs[i] * 100))
    print('--------')
    print('AVE    {:.4f}'.format(np.mean(ACCs) * 100))