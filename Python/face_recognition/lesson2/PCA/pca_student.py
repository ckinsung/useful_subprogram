import cv2 as cv
import numpy as np


def most_frequent(List):
    counter = 0
    num = List[0]

    for i in List:
        curr_frequency = List.count(i)
        if (curr_frequency > counter):
            counter = curr_frequency
            num = i

    return num

# train data
def pca_compress(data_mat, k=120):
    '''
    :param data_mat: 输入数据
    :param k: 输入矩阵转换后的维度, k. 默认值 120
    :return:
    low_dim_data: X = data_mat - mean_vals 映射到 k 维度
    mean_vals: data_mat 的均值
    re_eig_vects: k 维度的特征向量
    '''
    # 1. 数据中心化
    # 2. 计算协方差矩阵
    # 3. 计算特征值和特征向量
    # 4.
    # 5.

    # feature to be along column direction, sample to be along row direction
    # (360*10304) -> (10304*360)
    # For machine learning in tabular form, usually the features are arranged along the row direction
    X = np.array(data_mat).transpose()

    # 1. 数据中心化 (10304*360) - (10304*1) -> (10304*360)
    mean_vals = X.mean(axis=1).reshape(-1, 1)
    X = X - mean_vals

    # 2. 计算协方差矩阵 (10304*360) @ (360*10304)
    cov = X @ X.transpose()

    # 3. 计算特征值和特征向量
    # eig_values, eig_vectors = np.linalg.eig(cov)

    # 计算 np.linalg.eig 太慢了, 计算完毕后保存为 pickle 文件
    import pickle
    # Save as pickle file
    # pickle.dump([eig_values, eig_vectors], open("saved_eig_mean_axis_1.p", "wb"))
    # Load pickle file
    eig_values, eig_vectors = pickle.load(open("saved_eig_mean_axis_1.p", "rb"))

    # Sort eigenvalues and eigenvectors
    idx = eig_values.argsort()[::-1]
    # eig_values = eig_values[idx]
    eig_vectors = eig_vectors[:, idx]

    # 提取前 k 个特征向量 (10304*10304) -> (10304, 120) 维度
    re_eig_vects = eig_vectors[:, 0:k]

    # low_dim_data 是 X 映射到 k 个特征向量的幅值
    # 把输入矩阵 X, 从 X.shape[0] 的维度转换成 k 维度
    # re_eig_vects.T @ X  -> (120*10304) @ (10304*360) -> (120*360)
    # low_dim_data = (120*360) 的意思是 360 个人脸样本(列向量), 每个样本的维度是 120
    low_dim_data = re_eig_vects.T @ X

    return low_dim_data, mean_vals, re_eig_vects


# test data (测试集图像 img 映射到 k 个维度的幅值)
def test_img(img, mean_vals, re_eig_vects):

    # 测试图像减去均值
    mean_removed = img - mean_vals

    # re_eig_vects.T (120*10304)
    # mean_removed (10304*1)
    proj_vec = re_eig_vects.T @ mean_removed

    return proj_vec


# compute the cosine distance between two input vectors
def compute_cosine_distance(vector1, vector2):
    return np.dot(vector1, vector2) / (np.linalg.norm(np.array(vector1)[0]) * (np.linalg.norm(np.array(vector2)[0])))


if __name__ == '__main__':

    # 1. use num 1- 9 image of each person to train
    data = []
    for i in range(1, 41):
        for j in range(1, 10):
            img = cv.imread('orl_faces/s' + str(i) + '/' + str(j) + '.pgm', 0)
            width, height = img.shape
            img = img.reshape((img.shape[0] * img.shape[1]))
            data.append(img)

    low_dim_data, mean_vals, re_eig_vects = pca_compress(data_mat=data, k=120)


    # 2. use num 10 image of each person to test
    correct = 0
    for k in range(1, 41):  # 循环40个组合, 并提取每个组合第10张人脸
        img = cv.imread('orl_faces/s' + str(k) + '/10.pgm', 0)
        img = img.reshape((img.shape[0] * img.shape[1]), 1)
        vec_test_img = test_img(img, mean_vals, re_eig_vects).reshape(-1)
        distance_mat = []

        # 计算 img 的特征与与所有训练集的图的距离
        for i in range(1, 41):
            for j in range(1, 10):

                # 计算测试图像的向量与训练图像的向量的距离
                vec_train_img = low_dim_data[:, (i - 1) * 9 + j - 1]
                distance_mat.append(compute_cosine_distance(vector1=vec_train_img, vector2=vec_test_img))

        # 最大距离的相似度最高
        num_ = np.argmax(distance_mat)
        # 找出7个最近的特征向量, 然后这7个，最多的则是最匹配的人脸
        ind = np.argpartition(distance_mat, -7)[-7:]
        class_ = most_frequent([int(i/9) + 1 for i in ind])

        # 提取相似度最高的人脸
        # class_ = int(np.argmax(distance_mat) / 9) + 1

        if class_ == k:
            correct += 1
        print('s' + str(k) + '/10.pgm is the most similar to s' +
              str(class_))
    print("accuracy: %lf" % (correct / 40))
