# import the necessary packages
import numpy as np
import cv2

def original_lbp(image):

    """
        origianl local binary pattern
    """

    def generate_binary_num(img):

        """
            一个3*3的矩阵, 计算 local binary pattern 值
        """

        # 定义矩阵变量
        img_bin = np.zeros(shape=img.shape, dtype=np.int8)

        # > 还是选额 >= 呢？发现选择 > 的结果和老师一样
        img_bin[img > img[1, 1]] = 1

        # 把周围一圈（顺时钟) 的 0 和 1 构成字符串
        bin_num = f'{img_bin[0, 0]}{img_bin[0, 1]}{img_bin[0, 2]}{img_bin[1, 2]}{img_bin[2, 2]}{img_bin[2, 1]}{img_bin[2, 0]}{img_bin[1, 0]}'

        # 二进制（字符串) 转换成整数
        num = int(bin_num, 2)

        return num

    rows = image.shape[0]
    cols = image.shape[1]
    lbp_image = np.zeros((rows, cols), np.uint8)

    # 计算每个像素点的lbp值，具体范围如上lbp_image
    for i in range(1, rows-1):
        for j in range(1, cols-1):

            # 在 [i, j] 位置, 提取3*3的矩阵
            img_crop = image[i-1: i+2, j-1: j+2]

            # 计算并得出当下 [i, j] 位置的 local binary pattern 值
            num = generate_binary_num(img=img_crop)

            # 替换变量的相应 [i, j] 位置的值为 local binary pattern 的值
            lbp_image[i, j] = num

    # 去掉边缘一圈没有计算处理的值
    lbp_image = lbp_image[1:-1, 1:-1]

    return lbp_image

if __name__ == '__main__':
    image = cv2.imread("./lms.jpg", 0)
    cv2.imshow("image", image)
    org_lbp_image = original_lbp(image)
    cv2.imshow("org_lbp_image", org_lbp_image)
    cv2.waitKey()